# Create module list and derive cpp/h files from it
SET(MODULES)
LIST(APPEND MODULES
	vtkFullMonteTetraMeshWrapper
	vtkFullMonteArrayAdaptor
	vtkFullMontePacketPositionTraceSetToPolyData
	vtkFullMonteMeshFromUnstructuredGrid
	)

SET(MODULE_H_FILES)
SET(MODULE_CPP_FILES)
FOREACH(MOD ${MODULES})
	LIST(APPEND MODULE_CPP_FILES "${MOD}.cpp")
	LIST(APPEND MODULE_H_FILES ${MOD}.h)
ENDFOREACH()

ADD_LIBRARY(vtkFullMonte SHARED ${MODULE_CPP_FILES} vtkHelperFunctions.cpp)
TARGET_LINK_LIBRARIES(vtkFullMonte ${VTK_LIBRARIES} FullMonteLogging FullMonteData FullMonteGeometry)

#### Tcl wrapping
IF(WRAP_TCL AND WRAP_VTK)
	SET(VTK_INSTALL_LIBRARY_DIR ${CMAKE_INSTALL_PREFIX}/lib)
	
	INCLUDE(${VTK_DIR}/vtkModuleMacros.cmake)
	INCLUDE(${VTK_DIR}/vtkTclWrapping.cmake)
	
	SET(vtkFullMonte_TCL_NAME vtkFullMonte)
	
	TARGET_LINK_LIBRARIES(vtkFullMonte FullMonteGeometryTCL FullMonteDataTCL)	
		
	VTK_ADD_TCL_WRAPPING(vtkFullMonte "${MODULE_H_FILES}" "") 
ENDIF()

#### Python wrapping ###### CURRENTLY NOT FULLY FUNCTIONAL <- TODO
IF(WRAP_PYTHON AND WRAP_VTK)
	SET(VTK_INSTALL_LIBRARY_DIR ${CMAKE_INSTALL_PREFIX}/lib/fmpy)
	
	INCLUDE(${VTK_DIR}/vtkModuleMacros.cmake)
	INCLUDE(${VTK_DIR}/vtkPythonWrapping.cmake)
	
	SET(vtkFullMonte_Python3_NAME vtkFullMonte)
	
	TARGET_LINK_LIBRARIES(vtkFullMonte _Geometry _Data)
		
	VTK_WRAP_PYTHON3(vtkFullMontePython vtkFullMontePython3_SRCS "${MODULE_H_FILES}")

	ADD_LIBRARY(vtkFullMontePythonD ${vtkFullMontePython3_SRCS})

	PYTHON_ADD_MODULE(vtkFullMontePython MODULE vtkFullMontePythonInit.cxx)
	TARGET_LINK_LIBRARIES(vtkFullMontePythonD vtkFullMonte vtkWrappingTools vtkWrappingPythonCore)
	SET_TARGET_PROPERTIES(vtkFullMontePythonD PROPERTIES LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib/fmpy)

	TARGET_LINK_LIBRARIES(vtkFullMontePython vtkFullMontePythonD)
	SET_TARGET_PROPERTIES(vtkFullMontePython PROPERTIES LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib/fmpy)

	
ENDIF()
	
#### Installation

INSTALL (TARGETS vtkFullMonte
	DESTINATION lib
)
    
