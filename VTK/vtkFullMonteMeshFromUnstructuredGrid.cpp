/*
 * vtkFullMonteMeshFromUnstructuredGrid.cpp
 *
 *  Created on: May 24, 2017
 *      Author: jcassidy
 */

#include <vtkPoints.h>
#include <vtkUnstructuredGrid.h>
#include <vtkObject.h>
#include <vtkCellData.h>
#include <vtkCellTypes.h>
#include <vtkObjectFactory.h>

class TetraMesh;

#include <FullMonteSW/Logging/FullMonteLogger.hpp>
#include <FullMonteSW/Geometry/PTTetraMeshBuilder.hpp>

#include "vtkFullMonteMeshFromUnstructuredGrid.h"

vtkStandardNewMacro(vtkFullMonteMeshFromUnstructuredGrid)

vtkFullMonteMeshFromUnstructuredGrid::vtkFullMonteMeshFromUnstructuredGrid()
{
}

vtkFullMonteMeshFromUnstructuredGrid::~vtkFullMonteMeshFromUnstructuredGrid()
{
}

void vtkFullMonteMeshFromUnstructuredGrid::unstructuredGrid(vtkUnstructuredGrid* ug)
{
	m_ug = ug;
}

void vtkFullMonteMeshFromUnstructuredGrid::regionLabelFieldName(const std::string fieldName)
{
	m_regionLabelFieldName = fieldName;
}

void vtkFullMonteMeshFromUnstructuredGrid::regionLabelFieldName(const char* fieldName)
{
	m_regionLabelFieldName = fieldName;
}


void vtkFullMonteMeshFromUnstructuredGrid::update()
{
	PTTetraMeshBuilder builder;

	bool hasReferenceToPointZero=false;
	bool hasZeroTetra=false;

	// check valid structure, and whether there's a zero tetra & zero point present
	// normally for FullMonte, tetra zero is a special dummy (0,0,0,0) and point zero is not part of any tetra

	if (!m_ug)
	{
		LOG_ERROR << "vtkFullMonteMeshFromUnstructuredGrid called with null data" << endl;
		return;
	}

	for(unsigned t=0;t<m_ug->GetNumberOfCells();++t)
	{
		vtkIdType* cellPts;
		vtkIdType  nCellPts;

		if (m_ug->GetCellType(t) != VTK_TETRA)
		{
			LOG_ERROR << "vtkFullMonteMeshFromUnstructuredGrid: non-tetra cell" << endl;
			return;
		}

		m_ug->GetCellPoints(t, nCellPts, cellPts);

		if (nCellPts != 4)
		{
			LOG_ERROR << "vtkFullMonteMeshFromUnstructuredGrid: tetra cell with nCellPts != 4" << endl;
			return;
		}
		/* This if/else statement checks if the input mesh already has a zero point and zero tetra
		 * and sets the variables hasZeroTetra and hasReferenceToPointZero. Our current policy is 
		 * that FullMonte always inserts an additional zero point and zero tetra for its internal 
		 * mesh represenation. This way, we are able to keep the input mesh and ouptut mesh always 
		 * consistent
		 */ 
		if (t==0)
		{
			hasZeroTetra = true;
			for(unsigned i=0;i<nCellPts;++i)
				hasZeroTetra &= cellPts[i] == 0;
		}
		else
			for(unsigned i=0;i<nCellPts;++i)
				hasReferenceToPointZero |= cellPts[i] == 0;
	}

	// Both variables are currently hard coded to true because we are inserting an additional zero point and 
	// zero tetra regardless if they have existed in the input mesh beforehand.
	// If for some reason, it turns out we have to check for the zero point and zero tetra again, just uncomment the
	// variable assignments after the true statements below.
	bool insertZeroPoint = true;//hasReferenceToPointZero;
	bool insertZeroTetra = true;//!hasZeroTetra;

	if (insertZeroPoint) {
		LOG_DEBUG << "\tFullMonte will insert an artificial zero point into the mesh in case the existing zero point is already referenced by a tetra" << endl
			        << "\t\tThis will not change the output mesh from FullMonte." << endl
			        << "\t\tThe additional zero point is internally required by FullMonte" << endl << endl;
    }
    
    if (insertZeroTetra) {
		LOG_DEBUG << "\tFullMonte will insert a zero cell (tetra) in case the existing zero cell (tetra) has nonzero point references." << endl
			        << "\t\tInternally, this means that the mesh FullMonte is operating on has an additional tetra." << endl
			        << "\t\tThis additional zero cell (tetra) will be removed when writing the output to a file." << endl << endl;
    }

	// extract the points

	if (!m_builder)
		m_builder = new PTTetraMeshBuilder();

	m_builder->setNumberOfPoints(m_ug->GetNumberOfPoints()+insertZeroPoint);

	if (insertZeroPoint)
	{
		Point<3,double> P(0,0,0);
		// This code sets the zero point to the coordinates of the first point of the mesh
		// I think the coordinates do not matter for the zero point in FullMonte but in the
		// output method, we would get odd bounding boxes when visualizing the mesh with the zero point inserted
		// Since we aren't writing the zero points to the output mesh, it is currently commented out
		//m_ug->GetPoint(0,P.data());
		m_builder->setPoint(0,P);
	}

	for(unsigned i=0;i<m_ug->GetNumberOfPoints();++i)
	{
		Point<3,double> P;
		m_ug->GetPoint(i,P.data());
		m_builder->setPoint(i+insertZeroPoint, P);
	}



	// extract tetra cells

	m_builder->setNumberOfTetras(m_ug->GetNumberOfCells()+insertZeroTetra);

	// If insertZeroTetra = true; (which it currently is hardcoded to be), we will insert
	// a zero tetra into the mesh where its 4 corners are assigned to the same point, essentially
	//  reating a tetra with 0 volume. It's material ID is also set to 0.
	if (insertZeroTetra)
		m_builder->setTetra(0,TetraByPointID{{0U,0U,0U,0U}},0U);

	for(unsigned i=0;i<m_ug->GetNumberOfCells(); ++i)
	{
		vtkIdType* p;
		vtkIdType Npc=0;

		m_ug->GetCellPoints(i,Npc,p);		// already checked they're tetras and have 4 points

		TetraByPointID IDps;

		for(unsigned j=0;j<4;++j)
			IDps[j] = p[j]+insertZeroPoint;

		m_builder->setTetra(i+insertZeroTetra, IDps);
	}



	// extract the partition if present

	vtkCellData* cd = m_ug->GetCellData();
	vtkDataArray* regions=nullptr;

	if (!cd)
	{
		LOG_WARNING << "vtkFullMonteMeshFromUnstructuredGrid: No cell data provided; assuming mesh is all region 1" << endl;
	}
	else if ((regions = cd->GetScalars(m_regionLabelFieldName.c_str())))
	{
	}
	else if ((regions = cd->GetScalars()))
	{
		const char *name = regions->GetName();
		if (name != m_regionLabelFieldName) {
			LOG_WARNING << "vtkFullMonteMeshFromUnstructuredGrid: Could not find specified region label field '" << m_regionLabelFieldName << "', using default field '" << name << "' instead" << endl;
        }
	}
	else
	{
		LOG_WARNING << "vtkFullMonteMeshFromUnstructuredGrid: Cell data without scalars provided; assuming mesh is all region 1" << endl;
		cd=nullptr;
	}

	if(regions)
	{
		if (regions->GetTuple1(0) != 0 && !insertZeroTetra) {
			LOG_WARNING << "vtkFullMonteMeshFromUnstructuredGrid: Cell 0 is not assigned to material 0" << endl;
        }

		for(unsigned i=0;i<m_ug->GetNumberOfCells();++i)
			m_builder->setTetraMaterial(i+insertZeroTetra, regions->GetTuple1(i));
	}
	else
		for(unsigned i=1;i<m_builder->getNumberOfTetras();++i)
			m_builder->setTetraMaterial(i,1);
}

TetraMesh* vtkFullMonteMeshFromUnstructuredGrid::mesh()
{
	m_builder->build();
	return m_builder->mesh();
}

#ifdef WRAP_VTK
#include "swig_traits.hpp"
extern template class swig_traits<TetraMesh*>;
#ifdef WRAP_TCL

void vtkFullMonteMeshFromUnstructuredGrid::mesh(const char* objName) const
{
	m_builder->build();
	TetraMesh* m = m_builder->mesh();

	createSwigObjectInstance(m,objName);
}
#endif

// TODO Support SWIG - VTK python interface
// #ifdef WRAP_PYTHON
// void vtkFullMonteMeshFromUnstructuredGrid::pymesh(const char* objName) const
// {
// 	m_builder->build();
// 	TetraMesh* m = m_builder->mesh();

// 	createSwigObjectInstance(m);
// }
// #endif
#endif
