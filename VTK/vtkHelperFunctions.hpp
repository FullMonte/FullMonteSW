/*
 * vtkHelperFunctions.hpp
 *
 *  Created on: Jun 6, 2017
 *      Author: jcassidy
 */

#ifndef VTK_VTKHELPERFUNCTIONS_HPP_
#define VTK_VTKHELPERFUNCTIONS_HPP_

class vtkPoints;
class vtkCellArray;
class vtkUnstructuredGrid;
class vtkUnsignedShortArray;
class vtkPolyData;

class TetraMesh;

void getVTKPoints(const TetraMesh& M,vtkPoints* P, bool zeroTetra_inserted);
bool getVTKTetraCells(const TetraMesh& M,vtkCellArray* ca);
void getVTKTetraRegions(const TetraMesh& M,vtkUnsignedShortArray* R, bool zeroTetra_inserted);
void getVTKTriangleCells(const TetraMesh& M,vtkCellArray* ca, bool zeroTetra_inserted);
void getVTKDirectedTriangleCells(const TetraMesh& M,vtkCellArray* ca, bool zeroTetra_inserted);

#endif /* VTK_VTKHELPERFUNCTIONS_HPP_ */
