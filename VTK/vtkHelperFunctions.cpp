/*
 * vtkHelperFunctions.cpp
 *
 *  Created on: Mar 8, 2018
 *      Author: jcassidy
 */

#include "vtkHelperFunctions.hpp"

#include <FullMonteSW/Logging/FullMonteLogger.hpp>
#include <FullMonteSW/Warnings/Push.hpp>
#include <FullMonteSW/Warnings/VTK.hpp>

#include <vtkUnsignedShortArray.h>
#include <vtkPoints.h>
#include <vtkUnstructuredGrid.h>
#include <vtkCellArray.h>
#include <vtkIdTypeArray.h>

#include <FullMonteSW/Warnings/Boost.hpp>
#include <boost/range/adaptor/indexed.hpp>

#include <FullMonteSW/Warnings/Pop.hpp>

#include <FullMonteSW/Geometry/TetraMesh.hpp>
#include <FullMonteSW/Geometry/Point.hpp>
#include <FullMonteSW/Geometry/Points.hpp>
#include <FullMonteSW/Geometry/FaceLinks.hpp>




/** Extract vtkPoints from a TetraMesh. 
 *
 * @param 	M		The mesh
 * @param	P		Destination point set (will be overwritten)
 */

void getVTKPoints(const TetraMesh& M,vtkPoints* P, bool zeroTetra_inserted)
{
	assert(P);
	if (!M.points())
	{
		LOG_ERROR << "getVTKPoints(M,P) provided empty mesh" << endl;
		P->SetNumberOfPoints(0);
		return;
	}

	unsigned Np = M.points()->size();
	
	if(zeroTetra_inserted == true)
	{
	// We inserted a zero point and now to have the input and the output be consistent,
	// we need to delete it again. This reduces the number of elements in the vtkPoints array by 1	
	P->SetNumberOfPoints(Np-1);
	
		// The first point element (index 0) is the zero point, thus we need to start at index 1
		// to get the first actual point of the mesh
		for(unsigned i=1;i<Np;++i)
		{
			Point<3,double> Pc = get(coords,M,TetraMesh::PointDescriptor(i));
			// Since the index of FullMontes internal point representation inlcude the zero point, we 
			// have an index mismatch for the output data. We need to subtract 1 in order to let the
			// vtkPoint array P also start at index 0 
			P->SetPoint(i-1,Pc.data());
		}
	}
	else
	{
		P->SetNumberOfPoints(Np);
		for(unsigned i=0;i<Np;++i)
		{
			Point<3,double> Pc = get(coords,M,TetraMesh::PointDescriptor(i));
			// Since the index of FullMontes internal point representation inlcude the zero point, we 
			// have an index mismatch for the output data. We need to subtract 1 in order to let the
			// vtkPoint array P also start at index 0 
			P->SetPoint(i,Pc.data());
		}
	}
	
	// This used to set the zero point to the coordinates of point 1.
	// Duplicating point 0 to point 1 was done to avoid goofy bounding boxes when the object is located far from the origin.
	// Now that we are deleting the zero point in the output, we do not need this line anymore.
	//P->SetPoint(0,P->GetPoint(1));
}



/** Copy the TetraMesh tetras, including the 0 element (dummy containing 0,0,0,0)
 *
 */

bool getVTKTetraCells(const TetraMesh& M,vtkCellArray* ca)
{
	bool zeroTetra_inserted = false;
	assert(ca);
	if (!M.tetraCells())
	{
		LOG_ERROR << "getVTKTetraCells(M,ca) provided empty mesh" << endl;
		ca->SetNumberOfCells(0);
		return zeroTetra_inserted;
	}
	
	size_t Nt=0;

	// Create tetra ID array
	vtkIdTypeArray *ids = vtkIdTypeArray::New();
	ids->SetNumberOfComponents(1);
	for(const auto t : M.tetras())
	{
		TetraByPointID IDps = get(points,M,t);
		if (t.value() == 0 && (IDps[0] == 0 && IDps[1] == 0 && IDps[2] == 0 && IDps[3] == 0))
		{
			// We ignore the first element, since it is the zero tetra and do not insert it
			zeroTetra_inserted = true;
		}
		else
		{
			// copy regular elements 1..Nt to tetras 1..Nt
			ids->InsertNextTuple1(4);
			for(unsigned k=0;k<4;++k)
			{
				// Since we also removed the zero point, we need to also change
				// the point assignment of the tetras
				// with zero point:
				// 			zero tetra points: 0 0 0 0 -> first tetra points: 1 2 3 4
				// without zero point:
				//          zero tetra points: X X X X -> first tetra points: 0 1 2 3
				if(zeroTetra_inserted)
					ids->InsertNextTuple1((vtkIdType)(IDps[k]-1));
				else
					ids->InsertNextTuple1((vtkIdType)(IDps[k]));
				assert(IDps[k] < get(num_points,M));
				
				
			}
			++Nt;
		}
	}
	ca->SetCells(Nt, ids);
	ids->Delete();
	return zeroTetra_inserted;
}


/** Get region codes for all tetra elements (assign tetra 0 -> region 0 always)
 * @param	M		The mesh
 * @param	R		vtkUnsignedShortArray to hold region codes (will be overwritten)
 */

void getVTKTetraRegions(const TetraMesh& M,vtkUnsignedShortArray* R, bool zeroTetra_inserted)
{
	assert(R);

	if (!M.regions())
	{
		LOG_ERROR << "getVTKTetraRegions(M,R) provided empty region partition" << endl;
		R->SetNumberOfTuples(0);
		return;
	}

	if(zeroTetra_inserted)
	{
		// Since we know that we have inserted a zero tetra, we also know the the first tetra region is assigned to the
		// zero tetra and we need to reduce the output size by one.
		R->SetNumberOfTuples(M.tetraCells()->size()-1);
		R->SetNumberOfComponents(1);

		for(const auto t : M.tetras() | boost::adaptors::indexed(0U))
		{
			if(t.index() == 0)
			{
				// The first entry is region 0 from the zero tetra, thus we will ignore it
			}
			else
			{
				// We need to account for the index mismatch between the internal Fullmonte mesh representation
				// of the regions and the original input/output mesh representation.
				// Since we ignored the first entry, we need to subtract the index of t by 1 
				R->SetValue(t.index()-1, get(region,M,t.value()));
			}
		}
	}
	else
	{
		R->SetNumberOfTuples(M.tetraCells()->size());
		R->SetNumberOfComponents(1);

		for(const auto t : M.tetras() | boost::adaptors::indexed(0U))
		{
			R->SetValue(t.index(), get(region,M,t.value()));
		}
	}
	

	// This used to set the first region entry for the zero tetra to region 0
	// Now that we are deleting the zero tetra in the output, we do not need this line anymore.
	// R->SetTuple1(0,0);
}

/** Copy the TetraMesh directed faces, including the zero element
 *
 */

void getVTKDirectedTriangleCells(const TetraMesh& M,vtkCellArray* ca, bool zeroTetra_inserted)
{
	assert(ca);

	if (!M.faceTetraLinks())
	{
		LOG_ERROR << "getVTKDirectedTriangleCells(M,ca) failed due to empty face list" << endl;
		ca->SetNumberOfCells(0);
		return;
	}

	size_t Nf=0;

	// Create triangle ID array
	vtkIdTypeArray *ids = vtkIdTypeArray::New();
	ids->SetNumberOfComponents(1);

	for(const auto f : M.directedFaces())
	{
		const auto IDps = get(points,M,f);
		if (f.value() == 0 && zeroTetra_inserted)
		{
			// We ignore the first element, since it is the zero face and do not insert it
		}
		else
		{
			ids->InsertNextValue(3);
			for(unsigned k=0;k<3;++k)
			{
				// Since we also removed the zero point, we need to also change
				// the point assignment of the tetra faces
				// with zero point:
				// 			zero tetra face points: 0 0 0 -> first tetra face points: 1 2 3
				// without zero point:
				//          zero tetra face points: X X X -> first tetra face points: 0 1 2 
				if (zeroTetra_inserted)
					ids->InsertNextValue((vtkIdType)(IDps[k].value()-1));
				else
					ids->InsertNextValue((vtkIdType)(IDps[k].value()));
			}
			++Nf;
		}
	}

	LOG_DEBUG << "getVTKDirectedTriangleCells returned " << Nf << " faces (" << get(num_faces,M) << " undirected, " << get(num_directed_faces,M) << " directed)" << endl;

	ca->SetCells(Nf, ids);
	ids->Delete();
}

/** Copy the TetraMesh faces, including the 0 element (dummy containing 0,0,0)
 *
 */

void getVTKTriangleCells(const TetraMesh& M,vtkCellArray* ca, bool zeroTetra_inserted)
{
	assert(ca);
	if (!M.faceTetraLinks())
	{
		LOG_ERROR << "getVTKTriangleCells(M,ca) failed due to empty face list" << endl;
		ca->SetNumberOfCells(0);
		return;
	}

	size_t Nf=0;

	// Create triangle ID array
	vtkIdTypeArray *ids = vtkIdTypeArray::New();
	ids->SetNumberOfComponents(1);

	for(const auto f : M.faces())
	{
		const auto IDps = get(points,M,f);
		if (f.value() == 0 && zeroTetra_inserted)
		{
			// We ignore the first element, since it is the zero face and do not insert it
		}
		else
		{
			// copy regular elements 1..Nt to tetras 1..Nt
			ids->InsertNextValue(3);
			for(unsigned k=0;k<3;++k)
			{
				// Since we also removed the zero point, we need to also change
				// the point assignment of the tetra faces
				// with zero point:
				// 			zero tetra face points: 0 0 0 -> first tetra face points: 1 2 3
				// without zero point:
				//          zero tetra face points: X X X -> first tetra face points: 0 1 2
				if (zeroTetra_inserted)
					ids->InsertNextValue((vtkIdType)(IDps[k].value()-1));
				else
					ids->InsertNextValue((vtkIdType)(IDps[k].value()));
			}
			++Nf;
		}
	}

	ca->SetCells(Nf, ids);
	ids->Delete();
}

