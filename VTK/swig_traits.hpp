/*
 * swig_traits.hpp
 *
 *  Created on: Mar 8, 2018
 *      Author: jcassidy
 */

#ifndef SWIG_TRAITS_HPP_
#define SWIG_TRAITS_HPP_

#include <iostream>

struct swig_type_info;

/** Traits class supporting template-based conversion of SWIG strings into typed pointers
 *
 * There should be a specialization within the SWIG interface %header section for every class you want to be able to pass to a
 * VTK-wrapped class.
 *
 * eg. 	template<>static const swig_type_info* 	swig_traits<TetraMesh*>::type_info(){ return SWIGTYPE_p_TetraMesh; }
 * 		template<>static const char*				swig_traits<TetraMesh*>::type_name = "TetraMesh*";
 *
 * Important notes:
 * 		* specialization must appear below %runtime section for the definition to be available
 * 		* SWIGTYPE_p_TetraMesh is a #define constant to an entry in an array of swig_type_info that gets populated during init so its
 * 			static value is not useful - that's why it's a static function
 */

template<typename T>class swig_traits
{
public:
	static const swig_type_info*	type_info();
	static const char*				type_name;
	typedef T						type;
};

struct Tcl_Interp;
struct Tcl_Obj;

//  Python interface vor SWIG to VTK conversion not supported -> commenting out for now
// #ifndef SWIGPYTHON
// #include <Python.h>
// #endif

/** The base case, using a swig_type_info.
 * Not meant for end-user (decodeSwigPointer<T> below preferred)
 *
 * 1. If the string is a SWIG-coded pointer, passes to SWIG for conversion
 * 2. If not a pointer but a command, uses "<obj> cget -this" to extract pointer and then converts
 */

void* decodeSwigPointerBase(const char* ptr,const swig_type_info*);

/** Decode a SWIG pointer into a C++ typed pointer.
 *
 * It uses the swig_traits<...> class so that the user does not need access to the internal swig_type_info* variable.
 * By packaging it up in the traits class, which is defined but not specialized here we can be satisfied with a forward
 * declaration of the struct and a specialization elsewhere.
 *
 */

template<typename T>T decodeSwigPointer(const char* swigPtr)
{
	void* p = decodeSwigPointerBase(swigPtr,swig_traits<T>::type_info());

	if (!p)
		std::cout << "decodeSwigPointer<" << swig_traits<T>::type_name << ">(" << swigPtr << ") returned nonzero exit code with pointer=" << p << std::endl;
	else
		std::cout << "decodeSwigPointer<" << swig_traits<T>::type_name << ">(" << swigPtr << ") decoded " << p << std::endl;

	return static_cast<T>(p);
}

/** Internal machinery (not intended for end-user; see createSwigObjectInstance<T> below)
 * Uses SWIG functions to create a Tcl object command with optional name.
 */

Tcl_Obj* createSwigObjectInstanceBase(void*,const swig_type_info* objSwigType, const char* objName);

/** Use SWIG functions to create a Tcl object command with optional name.
 *
 * Similar to decodeSwigPointer, uses the traits class to handle typing and to permit hiding of the details.
 * Specialization should be buried somewhere in the SWIG interface file.
 */


template<typename T>Tcl_Obj* createSwigObjectInstance(T in,const char* objName)
{
	Tcl_Obj* obj=nullptr;
	std::cout << "createSwigObjectInstance<" << swig_traits<T>::type_name << "> type=" << swig_traits<T>::type_info() << std::endl;
	if ( (obj = createSwigObjectInstanceBase(static_cast<void*>(in),swig_traits<T>::type_info(), objName) ) )
	{
		std::cout << "createSwigObjectInstance(" << swig_traits<T>::type_name << ",\"" << objName << "\") created object at " << obj << std::endl;
	}
	else
		std::cout << "createSwigObjectInstance(" << swig_traits<T>::type_name << ",\"" << objName << "\") failed with null pointer" << std::endl;

	return obj;
}

/**********************************************************************
 *  SWIG - VTK interface for python -> Not supported yet
***********************************************************************/
// void* decodeSwigPointerBase(PyObject* ptr,const swig_type_info*);

// template<typename T>T decodeSwigPointer(PyObject* swigPtr)
// {
// 	void* p = decodeSwigPointerBase(swigPtr,swig_traits<T>::type_info());

// 	if (!p)
// 		std::cout << "decodeSwigPointer<" << swig_traits<T>::type_name << ">(" << swigPtr << ") returned nonzero exit code with pointer=" << p << std::endl;
// 	else
// 		std::cout << "decodeSwigPointer<" << swig_traits<T>::type_name << ">(" << swigPtr << ") decoded " << p << std::endl;

// 	return static_cast<T>(p);
// }

// PyObject* createSwigObjectInstanceBase(void*,const swig_type_info* objSwigType);

// template<typename T>PyObject* createSwigObjectInstance(T in)
// {
// 	PyObject* obj=nullptr;
// 	std::cout << "createSwigObjectInstance<" << swig_traits<T>::type_name << "> type=" << swig_traits<T>::type_info() << std::endl;
// 	if ( (obj = createSwigObjectInstanceBase(static_cast<void*>(in),swig_traits<T>::type_info()) ) )
// 	{
// 		std::cout << "createSwigObjectInstance(" << swig_traits<T>::type_name << ") created object at " << obj << std::endl;
// 	}
// 	else
// 		std::cout << "createSwigObjectInstance(" << swig_traits<T>::type_name << ") failed with null pointer" << std::endl;

// 	return obj;
// }

#endif /* SWIG_TRAITS_HPP_ */
