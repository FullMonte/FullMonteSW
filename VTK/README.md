# VTK interfaces

This folder contains

No VTK-dependent code should be found outside here, with the possible exception of non-core test and/or visualization code that relies on VTK.


## Tcl wrapping

VTK has its own Tcl wrapping system. 
To avoid having FullMonte's core depend on a VTK install, we use SWIG to generate most of the Tcl wrapping. 
That means that the otherwise incompatible VTK and SWIG Tcl wrapping methods need to be bridged here.

FullMonte objects always interface with other FullMonte objects using the SWIG interface style.
``vtkXXX`` classes always interface only with other VTK-provided classes, using the VTK wrapping style.
``vtkFullMonteXXX`` classes will have inputs and outputs that are a mix of FullMonte and VTK classes. The VTK wrapping tools are used to generate the VTK interface automatically, while the SWIG interface code must be called manually.
  


### VTK style

VTK Tcl wrapping relies on custom tools build within VTK to extract information from the standard header file idioms employed by VTK (eg. type macros specifying superclasses).


### SWIG style


### Interfacing VTK Tcl wrapping with SWIG

All interfaces necessary should, like VTK-dependent code, be confined to the present directory so that any modules outside this directory need no particular knowledge of VTK wrapping.
Inputs and outputs that are expressed as FullMonte types should therefore be converted within the module so that the external interface is SWIG-type. 



#### Class inputs

FullMonte class inputs to be exposed to the scripting interface should have a variant that accepts a ``const char* swigPtr`` which performs the conversion to the underlying FullMonte type. 
Swig pointers are presented as text ``_XXXXXXXXXXXXXXXX_p_TTTTTTTT`` where the ``XXX`` represents a zero-padded little-endian 64-bit pointer value and ``TTT`` stands in for a type name such as ``TetraMesh``. If the appropriate Tcl module is linked, then the necessary SWIG runtime functions will be present to handle the conversion.


#### Class outputs

Methods that output FullMonte types will also need to generate the appropriate calls to set up SWIG-type references.
They may optionally also provide a name for the output SWIG object, eg. ``TetraMesh* vtkFullMonteMeshFromUnstructuredGrid::mesh(const char* objName=nullptr)`` which would result in an object with the specified name.