function vis(fn);


% Read mean-variance data

fid = fopen(fn,'r');
s = fscanf(fid,'%%%[^\n]\n',[1 Inf]);
%printf('Discarded %s',s);
dims = fscanf(fid,'%dx%d',2);
Nr = dims(1);
Nz = dims(2);
printf('Reading %d x %d data',Nr,Nz);
T = fscanf(fid,'%f',[4 Nr*Nz+1])';

E_mu = reshape(T(:,1),Nz,Nr);
E_sigma = reshape(T(:,2),Nz,Nr);
phi_ref = reshape(T(:,3),Nz,Nr);
phi_test = reshape(T(:,4),Nz,Nr);


%%%% Log-scale fluence
subplot(1,3,1);
imagesc(log10(phi_test(:,1:end-1)));
colorbar;
xlabel('Radial bin');
ylabel('Depth bin');
title('Fluence (log au)');

print(gcf,'fluence.png','-dpng','-color');



%%%% Relative difference
subplot(1,3,2);
% compute histogram to figure out scaling s.t. p% of histogram
% falls within colourmap
x = phi_test./phi_ref-1;
xs = sort(x(:));
N = length(xs);

if (exist('centre'))
elseif (mod(N,2)==1)
    centre = xs((N+1)/2);
else
    centre = (xs(N/2) + xs(N/2+1))/2;
end

printf('Centre is %f\n',centre);
p=0.0025;
l = (p/2)*N+1;
u = N*(1-p/2);

dxLower=0;
dxUpper=1;


%while(abs(dxLower) < abs(dxUpper))
%    dxLower = xs(l)-centre;
%    dxUpper = xs(u)-centre;
%
%    printf('Domain [%d,%d] range [%f,%f]\n',l,u,xs(l),xs(u));
%
%    if (abs(dxLower) < abs(dxUpper))
%        if (l > 1)
%            l=l-1;
%        end
%        
%        u=u-1;
%    end
%end
% diffRange = [xs(l) xs(u)];

diffRange = [-0.1 0.1];

imagesc(phi_test./phi_ref*100-100,100*diffRange);
colorbar;
xlabel('Radial bin');
ylabel('Depth bin');
title('% difference');

print(gcf,'relative_difference.png','-dpng','-color');



%%%%% Z Score difference
%figure;
subplot(1,3,3);

z = (phi_test-phi_ref)./phi_ref./(E_sigma ./ E_mu);
imagesc(z,[-3 3]);

xlabel('Radial bin')
ylabel('Depth bin');
title('Z score difference');
colorbar;

print(gcf,'z_difference.png','-dpng','-color');

figure;
zs = sort(z(:));

empirical_F = (1:length(zs))/length(zs);
empirical_x = zs;

theoretical_F = normcdf(zs,0,1);
plot(theoretical_F(:),empirical_F(:))
xlabel('Standard normal');
ylabel('Variance Z Score Quantile');

M = [
    min([theoretical_F(:);empirical_F(:)])
    max([theoretical_F(:);empirical_F(:)])];

line(M,M);

print(gcf,'pp.png','-dpng','-color');

figure;
theoretical_x = norminv(empirical_F,0,1);
plot(theoretical_x(:),empirical_x(:));
xlabel('Standard normal value for quantile');
ylabel('Difference Z Score');
xlim = get(gca,'XLim');
line([xlim(1) xlim(2)],[xlim(1) xlim(2)]);

print(gcf,'qq.png','-dpng','-color');
