% Flattens a Nx1 cell array of RxC matrices into a RC x N matrix
% Suitable for computing mean/standard deviation etc
%
% NOTE: Compresses RxC in row-major order (compatible with MCML & TextFileWriter readout order)

function F = flatten(S)

[R,C] = size(S{1});

F = zeros(R*C,length(S));

for i=1:length(S)
    F(:,i) = S{i}'(:);
end
