% writeMeshFile(fn,P,T) writes the mesh described by points P and tetras T to a plaintext format in file <fn>
% Tetra point indices are 1-based
%
% Format
% <Np>
% <Nt>
% { <x> <y> <z> }Np
% { <p0> <p1> <p2> <p3> <r>}Nt
%
% Np        Number of points
% Nt        Number of tetras
% x y z     Point coordinates
% p0..p3    Point indices for tetra
% r         Region assignment for tetra
%
% This is part of FullMonte
% (c) Jeffrey Cassidy, University of Toronto 2017

function writeMeshFile(fn,P,T)

fid = fopen(fn,'w');

[Np,Dp] = size(P);
if (Dp ~= 3)
    error('Incorrect dimensions for P array: should be Np x 3')
end

[Nt,Dt] = size(T);
if (Dt ~= 5)
    error('Incorrect dimensions for T array: should be Nt x 5')
end

fprintf(fid,'%d\n',Np);
fprintf(fid,'%d\n',Nt);

fprintf(fid,'%10.6f %10.6f %10.6f\n',P');
fprintf(fid,'%8d %8d %8d %8d %2d\n',T');

fclose(fid);

printf('Wrote %d tetras and %d points to file %s',Nt,Np,fn);
