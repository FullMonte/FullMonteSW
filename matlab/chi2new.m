% Uses a chi2 test of homogeneity between vectors A and B
%
%
% 
% Assumes a binomial distribution of photon weight deposition (same as physical process - no roulette/packetizing)
%
% For each bin in a binomial with arrival probability p
%   mu        = Np
%   sigma^2   = Np(1-p)
%
% N is number of packets
% Can estimate p as w/w_total for a given bin
% Can estimate sigma as sqrt(Np(1-p))
%
% If that is the case then (w - w_expected) / sqrt(Np(1-p)) should be iid N(0,1)

function [p,chi2,DF,emu,evar]=chi2new(Np,A,B);

printf('Data sums: %f, %f\n',sum(A(:)),sum(B(:)));

M = Np * [A(:)' ; B(:)'];

[R,C] = size(M)

% estimated parameters, mean, and variance
p = sum(M)/R/Np;
q = 1-p;
emu = Np*p;
evar = Np*p.*q;

% scaling constant to make output iid
k2 = 1./repmat(evar,[R 1]);
k  = repmat(evar,[R 1]).^(-1/2);


E = repmat(p*Np,[R 1]);
delta = k2 .* (M-E).^2;

chi2 = sum(delta(:));

% degrees of freedom
%   R-1     datasets since the p parameter is fit
%   C       samples because total is not fixed (exit is implicitly an option as well)
DF = (R-1)*C;

p = 1-chi2cdf(chi2,DF);
