% Uses the chi2 test of homogeneity between vectors A and B
%
%
% 
% Test statistic is (O-E)^2 / E
%
% Uses a categorical table with r rows and c columns
% Each row is a population (A,B), each c is a category (bin/element of A or B)
%
% Returns
%   p       P-value for test

function [p,chi2,DF]=chi2homogeneity(A,B);

M = [A(:)';B(:)'];
[R,C] = size(M);

printf('Table is %dx%d\n',R,C);

E_r = sum(M')';
E_c = sum(M);

E = repmat(E_r,[1 C]) .* repmat(E_c,[R 1]) / sum(E_r(:));

t = (M-E).^2 ./ E;

DF = (R-1)*(C-1);

chi2 = sum(t(:));

p = chi2cdf(chi2,DF);


