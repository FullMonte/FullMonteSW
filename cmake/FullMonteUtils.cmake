# CONVERT_TIMOS_MESH_TO_VTK(SOURCE DEST)
# 
#
# Arguments
#	SOURCE		Full path to source mesh file
#	DEST		Path to output VTK file, relative to CMAKE_CURRENT_BINARY_DIR
#
# Effects
#	mesh_vtk_<name>		Custom target to perform file conversion (<name> is SOURCE stripped of path/extension)

FUNCTION(CONVERT_TIMOS_MESH_TO_VTK SOURCE DEST)
    GET_FILENAME_COMPONENT(NAME ${SOURCE} NAME_WE)
    
    IF(NOT IS_ABSOLUTE ${SOURCE})
    	SET(SOURCE "${CMAKE_CURRENT_SOURCE_DIR}/${SOURCE}")
    ENDIF()

    ADD_CUSTOM_COMMAND(
        OUTPUT ${DEST}
        DEPENDS ${SOURCE}
        COMMAND ${CMAKE_BINARY_DIR}/bin/tclmonte.sh ${CMAKE_SOURCE_DIR}/Examples/mesh_convert_timos_to_vtk.tcl ${SOURCE} ${DEST}
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
    )

    ADD_CUSTOM_TARGET(mesh_vtk_${NAME} DEPENDS ${DEST})
ENDFUNCTION()






## LINK_MMC_MESH(PATH NAME)
#
# Symlinks an MMC path to the current binary dir (required because MMC apparently can't specify meshes outside cwd)
#
# Arguments
#	PATH	A path-like MMC mesh name (/PATH/ROOT) -> /PATH/{elem,...}_ROOT
#	NAME	A path-like name for output mesh
#
# Effects
#	link_mesh_mmc_${NAME}		A custom target that symlinks mesh from PATH to CMAKE_CURRENT_BINARY_DIR
# 								Adds the target to ALL
#

FUNCTION(LINK_MMC_MESH MESH NAME)

    GET_FILENAME_COMPONENT(OUTPUT_SUFFIX    ${NAME} NAME)    
    GET_FILENAME_COMPONENT(OUTPUT_NAME      ${NAME} NAME_WE)  
    GET_FILENAME_COMPONENT(INPUT_DIR        ${MESH} DIRECTORY)
    GET_FILENAME_COMPONENT(INPUT_SUFFIX     ${MESH} NAME)
    GET_FILENAME_COMPONENT(INPUT_NAME       ${MESH} NAME_WE)

    #MESSAGE("LINK_MMC_MESH linking ${INPUT_DIR}/{node,elem,velem,facenb}_${INPUT_SUFFIX} -> ${CMAKE_CURRENT_BINARY_DIR}/{node,elem,facenb,velem}_${OUTPUT_SUFFIX} and creating target link_mmc_mesh_${OUTPUT_NAME}")

    MMC_MESH_FILENAMES(INPUT  ${MESH})
    MMC_MESH_FILENAMES(OUTPUT ${NAME})

    ADD_CUSTOM_COMMAND(
        OUTPUT ${OUTPUT}
        DEPENDS ${INPUT}
        COMMAND ${CMAKE_COMMAND} -E create_symlink ${INPUT_DIR}/elem_${INPUT_SUFFIX}   elem_${OUTPUT_SUFFIX}
        COMMAND ${CMAKE_COMMAND} -E create_symlink ${INPUT_DIR}/node_${INPUT_SUFFIX}   node_${OUTPUT_SUFFIX}
        COMMAND ${CMAKE_COMMAND} -E create_symlink ${INPUT_DIR}/velem_${INPUT_SUFFIX}  velem_${OUTPUT_SUFFIX}
        COMMAND ${CMAKE_COMMAND} -E create_symlink ${INPUT_DIR}/facenb_${INPUT_SUFFIX} facenb_${OUTPUT_SUFFIX}
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})
        
    ADD_CUSTOM_TARGET(link_mesh_mmc_${OUTPUT_NAME} DEPENDS ${OUTPUT})
    ADD_DEPENDENCIES(link_mesh_mmc_${OUTPUT_NAME} mesh_mmc_${INPUT_NAME})
    ADD_DEPENDENCIES(testdata link_mesh_mmc_${OUTPUT_NAME})
ENDFUNCTION()



## LINK_MMC_OPTICAL(PATH NAME)
#
# Symlinks an MMC optical-property file to the current binary dir (required because MMC apparently can't specify meshes outside cwd)
#
# Arguments
#	PATH	An MMC optical-property file
#	NAME	A path-like name for output mesh
#
# Effects
#	link_opt_mmc_${NAME}		A custom target that symlinks mesh from PATH to CMAKE_CURRENT_BINARY_DIR
# 								Adds the target to ALL
#

FUNCTION(LINK_MMC_OPTICAL PATH NAME)
	SET(OUTPUT "${NAME}")
	
	GET_FILENAME_COMPONENT(OUTPUT_NAME ${NAME} NAME_WE)

    ADD_CUSTOM_COMMAND(
        OUTPUT ${OUTPUT}
        DEPENDS ${PATH}
        COMMAND ${CMAKE_COMMAND} -E create_symlink ${PATH}   ${OUTPUT}
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})

    ADD_CUSTOM_TARGET(link_opt_mmc_${OUTPUT_NAME} DEPENDS ${OUTPUT})
    ADD_DEPENDENCIES(testdata link_opt_mmc_${OUTPUT_NAME})
ENDFUNCTION()




## CONVERT_TIMOS_MESH_TO_MMC(MESHFILE DEST)
#
# Create a target to convert a TIM-OS text-format mesh to MMC format
#
# Arguments
#	MESHFILE		Path to TIM-OS style mesh file (absolute or relative to CMAKE_CURRENT_SOURCE_DIR)
#	DEST			A path-like MMC mesh file name
#
# Effects
#	mesh_mmc_<name>		Custom target that does the conversion
#							Invocation creates {elem,velem,node,facenb>_<name>.mmc.dat

FUNCTION(CONVERT_TIMOS_MESH_TO_MMC MESHFILE DEST)

    IF(NOT IS_ABSOLUTE ${MESHFILE})
    	SET(MESHFILE ${CMAKE_CURRENT_SOURCE_DIR}/${MESHFILE})
    ENDIF()

    MMC_MESH_FILENAMES(OUTPUT ${DEST})
    GET_FILENAME_COMPONENT(OFN ${DEST} NAME)
    GET_FILENAME_COMPONENT(NAME ${DEST} NAME_WE)
    
    #MESSAGE("CONVERT_TIMOS_MESH_TO_MMC output: ${OUTPUT}")

    ADD_CUSTOM_COMMAND(
        OUTPUT ${OUTPUT}
        DEPENDS ${MESHFILE}
        COMMAND ${CMAKE_BINARY_DIR}/bin/tclmonte.sh ${CMAKE_SOURCE_DIR}/Examples/mesh_convert_timos_to_mmc.tcl ${MESHFILE} ${OFN}
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
    )

    ADD_CUSTOM_TARGET(mesh_mmc_${NAME}
        DEPENDS ${OUTPUT})
    ADD_DEPENDENCIES(testdata mesh_mmc_${NAME})
    

ENDFUNCTION()



## CONVERT_TIMOS_MATERIALS_TO_MMC(SOURCE NAME)
#
# Creates a target opt_mmc_<name> that converts the specified TIM-OS optical property file into MMC format
#
# Arguments
# 	SOURCE		Path to TIM-OS format text file  (absolute or relative to CMAKE_CURRENT_SOURCE_DIR)
#	NAME		A path-like MMC mesh file name 
#
# Effects
#	opt_mmc_<root>		Custom target to do the conversion (root is ${NAME} without path/extension)

FUNCTION(CONVERT_TIMOS_MATERIALS_TO_MMC SOURCE NAME)

    IF(NOT IS_ABSOLUTE ${SOURCE})
    	SET(SOURCE ${CMAKE_CURRENT_SOURCE_DIR}/${SOURCE})
    ENDIF()
    
    MESSAGE("CONVERT_TIMOS_MATERIALS_TO_MMC: output prop_${NAME} \ 
    ")

    GET_FILENAME_COMPONENT(ROOT ${NAME} NAME_WE)
    ADD_CUSTOM_COMMAND(
        OUTPUT prop_${NAME}
        DEPENDS ${SOURCE}
        COMMAND ${CMAKE_BINARY_DIR}/bin/tclmonte.sh ${CMAKE_SOURCE_DIR}/Examples/opt_convert_timos_to_mmc.tcl ${SOURCE} prop_${NAME}
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
    )
    ADD_CUSTOM_TARGET(opt_mmc_${ROOT} DEPENDS prop_${NAME})
    ADD_DEPENDENCIES(testdata opt_mmc_${ROOT})
ENDFUNCTION()
