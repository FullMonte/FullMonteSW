## MMC_MESH_FILENAMES(VAR MMCMESH)
#
# A utility to generate the four MMC file names from a path-like MMC mesh name
# /PATH/ROOT -> /PATH/{elem,...}_ROOT
#
# Arguments
#	VAR		Output variable name
#	MMCMESH	A path-like MMC mesh name
#
# Effects
#	Sets ${VAR} to be a list containing /PATH/{elem,node,facenb,velem}_ROOT	
#

FUNCTION(MMC_MESH_FILENAMES VAR MMCMESH)
    GET_FILENAME_COMPONENT(DIR ${MMCMESH} DIRECTORY)
    GET_FILENAME_COMPONENT(SFX ${MMCMESH} NAME)

    IF(DIR)
        SET(DIR "${DIR}/")
    ENDIF()

    FOREACH(PFX elem node facenb velem)
        LIST(APPEND TMP ${DIR}${PFX}_${SFX})
    ENDFOREACH()
    SET(${VAR} "${TMP}" PARENT_SCOPE)
    #MESSAGE("Debug MMC_MESH_FILENAMES: ${TMP}")
ENDFUNCTION()

####
# Adds an MMC-based regression test
#
# Arguments:
#	NAME			Name of test case (must be same as SessionID in .json file)
#	INPUT_MESH		Path-like mesh argument*
# 	INPUT_OPT		Path to optical properties file*
#	INPUT_JSON		Path to JSON config file*
#	SWEEP_RUNS		Number of runs in variance sweep (default=64)
#	TEST_PACKETS	Number of packets to run in test set
#	DETAILS			Boolean - if present save per-run results
#
#	
# Requirements:
#	CMAKE_CURRENT_SOURCE_DIR has NAME.reference.mve.out reference mean-variance energy file 
#	* INPUT_JSON, INPUT_MESH, and INPUT_OPT are in CMAKE_CURRENT_BINARY_DIR (typically generated or symlinked)
#
# Test cases created:
#	reg_sim_NAME			Test case to run the simulation in FullMonte
#	reg_compare_NAME		Test case to compare simulation results with stored reference data
#
# Targets created:
#	sim_fullmonte_NAME		Target to run the simulation in FullMonte
#	sweep_fullmonte_NAME	Target to run a seed sweep in FullMonte
#	sim_mmc_NAME			Runs MMC simulation
#	regcheck_mmc_NAME		Checks MMC simulation results against the stored reference data

FUNCTION(ADD_MMC_REGRESSION_TEST)
	CMAKE_PARSE_ARGUMENTS("MY" "DETAILS;REGRESSION;FULLMONTE" "NAME;INPUT_MESH;INPUT_OPT;INPUT_JSON;SWEEP_RUNS;MMC_OUTPUT;TEST_PACKETS;TIMOS_MESH;TIMOS_SOURCE;TIMOS_OPT" "" ${ARGN})
	
	# Handle optional arguments that have defaults
	IF(NOT MY_SWEEP_RUNS)
		SET(MY_SWEEP_RUNS 64)
	ENDIF()
		
	# Check mandatory arguments are present	
	IF(NOT (MY_INPUT_JSON AND MY_NAME AND MY_INPUT_MESH AND MY_TEST_PACKETS))
		MESSAGE(FATAL_ERROR "ADD_MMC_REGRESSION_TEST required argument missing: must specify NAME, INPUT_JSON, INPUT_MESH, TEST_PACKETS")
	ENDIF()
	
	# Derive MMC file names from test case name	
	MMC_MESH_FILENAMES(INPUT_GEOM_FILES "${MY_INPUT_MESH}")
	SET(INPUT_OPT_FILE prop_${MY_NAME}.mmc.dat)
	
	IF (MY_DETAILS)
		SET(MY_DETAIL_ARG --details ${MY_NAME}.%04d.txt) 
	ENDIF()
	
	IF(MY_FULLMONTE)
	
	# Custom target for FullMonte single simulation	
	ADD_CUSTOM_COMMAND(
		COMMAND sim_fullmonte_mmc
			--json ${MY_INPUT_JSON}
			--packets ${MY_TEST_PACKETS}
		OUTPUT ${MY_NAME}.EV.txt ${MY_NAME}.ES.txt
		DEPENDS ${INPUT_GEOM_FILES} ${MY_INPUT_JSON} ${INPUT_OPT_FILE}
		WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})
	ADD_CUSTOM_TARGET(sim_fullmonte_${MY_NAME} DEPENDS ${MY_NAME}.EV.txt ${MY_NAME}.ES.txt sim_fullmonte_mmc)
	
	# Custom target for FullMonte seed sweep	
	ADD_CUSTOM_COMMAND(
		COMMAND sim_fullmonte_mmc
			--json ${MY_INPUT_JSON}
			--packets ${MY_TEST_PACKETS}
			${MY_DETAIL_ARG}
			-r ${MY_SWEEP_RUNS}
		OUTPUT ${MY_NAME}.ES.mve.out ${MY_NAME}.EV.mve.out
		DEPENDS ${INPUT_GEOM_FILES} ${MY_INPUT_JSON} ${INPUT_OPT_FILE}
		WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})
	ADD_CUSTOM_TARGET(sweep_fullmonte_${MY_NAME} DEPENDS ${MY_NAME}.ES.mve.out ${MY_NAME}.EV.mve.out sim_fullmonte_mmc)
	
	ENDIF()
	
	IF(MY_FULLMONTE AND MY_REGRESSION)
	
	## TODO: target regcheck_mmc_${MY_NAME}
	## 
	
	# Test case for single FullMonte simulation
	ADD_TEST(
		NAME reg_sim_${MY_NAME}
		COMMAND sim_fullmonte_mmc
			--json ${MY_INPUT_JSON}
			--packets ${MY_TEST_PACKETS}
		WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})
		
	SET_TESTS_PROPERTIES(reg_sim_${MY_NAME}
		PROPERTIES
			FIXTURES_SETUP fixture_sim_${MY_NAME})
	
	# Test cases for surface & volume energy comparison
	ADD_TEST(
		NAME reg_compare_volume_${MY_NAME}
		COMMAND Compare
			--test ${MY_NAME}.EV.txt
			--test-packets ${MY_TEST_PACKETS}
			--ref ${CMAKE_CURRENT_SOURCE_DIR}/${MY_NAME}.EV.reference.mve.out
			--output ${MY_NAME}.EV.reg.chi2.txt
#			--extra-cv 0.001
#			--extra-stddev-mult 0.1
			--checkpoint-CE 0.95
			--checkpoint-CV 0.10
			--pcrit 0.95
		WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})
		
	SET_TESTS_PROPERTIES(reg_compare_volume_${MY_NAME}
		PROPERTIES
			REQUIRED_FILES "${CMAKE_CURRENT_SOURCE_DIR}/${MY_NAME}.EV.reference.mve.out;${MY_NAME}.EV.txt")
		
	ADD_TEST(
		NAME reg_compare_surface_${MY_NAME}
		COMMAND Compare
			--test ${MY_NAME}.ES.txt
			--test-packets ${MY_TEST_PACKETS}
			--ref ${CMAKE_CURRENT_SOURCE_DIR}/${MY_NAME}.ES.reference.mve.out
			--output ${MY_NAME}.ES.reg.chi2.txt
#			--extra-cv 0.001
#			--extra-stddev-mult 0.1
			--checkpoint-CE 0.95
			--checkpoint-CV 0.10
			--pcrit 0.95
		WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})
		
	SET_TESTS_PROPERTIES(reg_compare_surface_${MY_NAME}
		PROPERTIES
			REQUIRED_FILES "${CMAKE_CURRENT_SOURCE_DIR}/${MY_NAME}.ES.reference.mve.out;${MY_NAME}.ES.txt")
	
	SET_TESTS_PROPERTIES(reg_compare_surface_${MY_NAME} reg_compare_volume_${MY_NAME}
		PROPERTIES
			DEPENDS reg_sim_${MY_NAME}
			FIXTURES_REQUIRED fixture_sim_${MY_NAME})
			
	ENDIF()
	
	# Custom target for MMC simulation
	ADD_CUSTOM_COMMAND(
		COMMAND echo "MMC simulation of ${MY_INPUT_JSON} with ${MY_TEST_PACKETS} packets"
		COMMAND time ${MMC_EXECUTABLE} -f ${MY_INPUT_JSON} -n ${MY_TEST_PACKETS} -C 0 -b 1 -e 1e-5 -O E -U 0
		DEPENDS ${MY_INPUT_JSON} ${INPUT_GEOM_FILES} ${INPUT_OPT_FILE}
		OUTPUT ${MY_NAME}.mmc.dat
		WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})
	ADD_CUSTOM_TARGET(sim_mmc_${MY_NAME} DEPENDS ${MY_NAME}.mmc.dat)

	# Regression test check: MMC simulation vs. stored reference values
	# (NOTE: only volume-energy is produced from MMC so can't check surface fluence)
	
	ADD_CUSTOM_COMMAND(
		COMMAND Compare
			--test ${MY_NAME}.mmc.dat
			--test-packets ${MY_TEST_PACKETS}
			--ref ${CMAKE_CURRENT_SOURCE_DIR}/${MY_NAME}.EV.reference.mve.out
			--output ${MY_NAME}.EV.mmc.chi2.txt
			--checkpoint-CE 0.95
			--checkpoint-CV 0.10
			--pcrit 0.95
			--no-error-codes
		DEPENDS ${MY_NAME}.mmc.dat ${CMAKE_CURRENT_SOURCE_DIR}/${MY_NAME}.EV.reference.mve.out
		OUTPUT ${MY_NAME}.EV.mmc.chi2.txt
		WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})
	ADD_CUSTOM_TARGET(regcheck_mmc_${MY_NAME} DEPENDS ${MY_NAME}.EV.mmc.chi2.txt)
ENDFUNCTION()
