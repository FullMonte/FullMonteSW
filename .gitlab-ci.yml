stages:
  - build_dependencies
  - build
  - test
  - deploy
  - deploy_master

variables: 
  GIT_SUBMODULE_STRATEGY: recursive
  GIT_STRATEGY: fetch
  CI_BUILD_ROOT: /builds/FullMonte/FullMonteSW/Build
  CI_INSTALL_DIR: /usr/local/FullMonteSW

# Build the development environment
# On success, pushes to fullmonte-dev:pipeline-XXX
build_environment:
  stage: build_dependencies
  image: docker:20.10.3
  script:
    - docker login $CI_REGISTRY -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD
    - docker build 
        --tag $CI_REGISTRY_IMAGE/fullmonte-dev:pipeline-$CI_PIPELINE_ID
        --target fullmonte-dev
        /builds/FullMonte/FullMonteSW/Docker
    - docker push $CI_REGISTRY_IMAGE/fullmonte-dev:pipeline-$CI_PIPELINE_ID

# Use the pushed dev environment to build
build-Release:
  stage: build
  image: registry.gitlab.com/fullmonte/fullmontesw/fullmonte-dev:pipeline-$CI_PIPELINE_ID
  script:
    - $CI_PROJECT_DIR/Docker/build_release.sh
  artifacts:
    paths:
      - $CI_BUILD_ROOT/Release
    when: always

build-Debug:
  stage: build
  image: registry.gitlab.com/fullmonte/fullmontesw/fullmonte-dev:pipeline-$CI_PIPELINE_ID
  script:
    - $CI_PROJECT_DIR/Docker/build_debug.sh
  artifacts:
    paths:
      - $CI_BUILD_ROOT/Debug
    when: always

build-Release-AVX:
  stage: build
  image: registry.gitlab.com/fullmonte/fullmontesw/fullmonte-dev:pipeline-$CI_PIPELINE_ID
  script:
    - $CI_PROJECT_DIR/Docker/build_release_avx.sh
    - ninja -C $CI_BUILD_ROOT/Release-AVX testdata
    - ninja -C $CI_BUILD_ROOT/Release-AVX install
    - mkdir -p $CI_BUILD_ROOT/Install-AVX
    - mv $CI_INSTALL_DIR-AVX $CI_BUILD_ROOT/Install-AVX
  artifacts:
    paths:
      - $CI_BUILD_ROOT/Release-AVX
      - $CI_BUILD_ROOT/Install-AVX
    when: always

# Build using the P8 architecture switch, but not necessarily on an actual Power8 system
build-Release_P8:
  stage: build
  image: registry.gitlab.com/fullmonte/fullmontesw/fullmonte-dev:pipeline-$CI_PIPELINE_ID
  script:
    - $CI_PROJECT_DIR/Docker/build_release_P8.sh
  artifacts:
    paths:
      - $CI_BUILD_ROOT/Release_P8

test-Release:
  stage: test
  image: registry.gitlab.com/fullmonte/fullmontesw/fullmonte-dev:pipeline-$CI_PIPELINE_ID
  script:
     - cd $CI_PROJECT_DIR
     - git lfs install
     - git lfs pull
     - cd $CI_BUILD_ROOT/Release
     - ninja testdata
     - $CI_PROJECT_DIR/Docker/test_release.sh
     - ninja install
     - mkdir -p $CI_BUILD_ROOT/Install
     - mv $CI_INSTALL_DIR $CI_BUILD_ROOT/Install
  artifacts:
     paths:
     - $CI_BUILD_ROOT/Release/Testing/Temporary
     - $CI_BUILD_ROOT/Install
     when: always

test-Coverage:
  stage: test
  image: registry.gitlab.com/fullmonte/fullmontesw/fullmonte-dev:pipeline-$CI_PIPELINE_ID
  script:
     - rm -rf $CI_PROJECT_DIR/Build/Debug/code_coverage*
     - cd $CI_PROJECT_DIR
     - git lfs install
     - git lfs pull
     - cd $CI_BUILD_ROOT/Debug
     - ninja testdata
     - $CI_PROJECT_DIR/Docker/test_release.sh
     - lcov --rc lcov_branch_coverage=1 --directory $CI_BUILD_ROOT/Debug --capture --output-file $CI_BUILD_ROOT/Debug/code_coverage.info -rc lcov_branch_coverage=1
     - lcov --rc lcov_branch_coverage=1 --remove $CI_BUILD_ROOT/Debug/code_coverage.info '/usr/*' -o $CI_BUILD_ROOT/Debug/code_coverage_filter.info
     - lcov --rc lcov_branch_coverage=1 --remove $CI_BUILD_ROOT/Debug/code_coverage_filter.info "$CI_PROJECT_DIR/Build/*" -o $CI_BUILD_ROOT/Debug/code_coverage_filtered.info
     - genhtml $CI_BUILD_ROOT/Debug/code_coverage_filtered.info --branch-coverage --output-directory $CI_BUILD_ROOT/Coverage
  coverage: '/^\s*lines.+:\s*\d+.\d+\%/'
  artifacts:
     paths:
     - $CI_BUILD_ROOT/Coverage
     when: always

#test-ReleaseNonSSE:
#  stage: test
#  image: registry.gitlab.com/fullmonte/fullmontesw/fullmonte-dev:pipeline-$CI_PIPELINE_ID
#  script:
#     - cd $CI_PROJECT_DIR
#     - cd $CI_BUILD_ROOT/Release_P8
#     - ninja testdata
#     - $CI_PROJECT_DIR/Docker/test_release.sh
#  artifacts:
#     paths:
#     - $CI_BUILD_ROOT/Release_P8/Testing/Temporary
#     when: always

documentation:
    stage: deploy
    image: registry.gitlab.com/fullmonte/fullmontesw/fullmonte-dev:pipeline-$CI_PIPELINE_ID
    only:
      refs:
        - master
      changes:
        - Geometry/*
        - Kernels/*
        - Logging/*
        - OutputTypes/*
        - Queries/*
        - Storage/*
        - Utilities/*
        - VTK/*
        - Warnings/*
        - README.md
        - LICENSE.md
        - BUILDING.md
        - BUGREPORTS.md
        - CONTRIBUTING.md
        - .gitlab-ci.yml
    before_script:
      ##
      ## Install ssh-agent if not already installed, it is required by Docker.
      ## (change apt-get to yum if you use an RPM-based image)
      ##
      - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'
      ##
      ## Run ssh-agent (inside the build environment)
      ##
      - eval $(ssh-agent -s)
      ##
      ## Add the SSH key stored in SSH_PRIVATE_KEY variable to the agent store
      ## We're using tr to fix line endings which makes ed25519 keys work
      ## without extra base64 encoding.
      ## https://gitlab.com/gitlab-examples/ssh-private-key/issues/1#note_48526556
      ##
      - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
      ##
      ## Create the SSH directory and give it the right permissions
      ##
      - mkdir -p ~/.ssh
      - chmod 700 ~/.ssh
      ##
      ## Use ssh-keyscan to scan the keys of your private server. Replace gitlab.com
      ## with your own domain name. You can copy and repeat that command if you have
      ## more than one server to connect to.
      ##
      - ssh-keyscan gitlab.com >> ~/.ssh/known_hosts
      - chmod 644 ~/.ssh/known_hosts

    script:
      - rm -rf $CI_PROJECT_DIR/External/Doxygen
      - mkdir -p $CI_PROJECT_DIR/External/Doxygen
      - cd $CI_PROJECT_DIR/External/Doxygen
      - git init
      - git config user.name "Gitlab-runner-savi1"
      - git config user.email "savi1-utoronto@gmail.com"
      - git remote add origin git@gitlab.com:FullMonte/FullMonteSW-doc.git
      - git pull -r origin master
      - cd $CI_PROJECT_DIR
      - doxygen Doxyfile
      - cd External/Doxygen
      - git add -A
      - git diff-index --quiet HEAD || (git commit -m "update doxygen website" && git push -u origin master)

pages:
  stage: deploy
  image: registry.gitlab.com/fullmonte/fullmontesw/fullmonte-dev:pipeline-$CI_PIPELINE_ID
  dependencies:
    - test-Coverage
  script:
    - mv $CI_BUILD_ROOT/Coverage/ public/
  artifacts:
    paths:
      - public
    expire_in: 30 days
  only:
    - master
    
#### Deploys to Docker registry as fullmonte-run
# Also pushes the dev image tag once we know the pipeline is successful
# Pushes two tags: one as :<pipeline-ID> and one as :<branch-name>
# The image expects that the build context will be the install dir
deploy:
  stage: deploy
  image: docker:20.10.3
  script:
# Main variant
    - docker login $CI_REGISTRY -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD
    - docker pull $CI_REGISTRY_IMAGE/fullmonte-dev:pipeline-$CI_PIPELINE_ID
    - docker tag $CI_REGISTRY_IMAGE/fullmonte-dev:pipeline-$CI_PIPELINE_ID fullmonte-dev:latest
    - docker tag $CI_REGISTRY_IMAGE/fullmonte-dev:pipeline-$CI_PIPELINE_ID fullmonte-dev:pipeline-$CI_PIPELINE_ID
    - cp $CI_PROJECT_DIR/Docker/Dockerfile.Install $CI_BUILD_ROOT
    - docker build 
        --build-arg fullmonte_dev_tag=:pipeline-$CI_PIPELINE_ID
        --tag $CI_REGISTRY_IMAGE/fullmonte-run:pipeline-$CI_PIPELINE_ID
        -f $CI_BUILD_ROOT/Dockerfile.Install
        $CI_BUILD_ROOT
    - docker tag $CI_REGISTRY_IMAGE/fullmonte-dev:pipeline-$CI_PIPELINE_ID $CI_REGISTRY_IMAGE/fullmonte-dev:$CI_COMMIT_REF_NAME
    - docker push $CI_REGISTRY_IMAGE/fullmonte-dev:$CI_COMMIT_REF_NAME
    - docker tag $CI_REGISTRY_IMAGE/fullmonte-run:pipeline-$CI_PIPELINE_ID $CI_REGISTRY_IMAGE/fullmonte-run:$CI_COMMIT_REF_NAME
    - docker push $CI_REGISTRY_IMAGE/fullmonte-run:pipeline-$CI_PIPELINE_ID
    - docker push $CI_REGISTRY_IMAGE/fullmonte-run:$CI_COMMIT_REF_NAME

# AVX (non-AVX2) variant
    - docker build 
        --build-arg fullmonte_dev_tag=:pipeline-$CI_PIPELINE_ID
        --build-arg is_avx=-AVX
        --tag $CI_REGISTRY_IMAGE/fullmonte-run-avx:pipeline-$CI_PIPELINE_ID
        -f $CI_BUILD_ROOT/Dockerfile.Install
        $CI_BUILD_ROOT
    - docker tag $CI_REGISTRY_IMAGE/fullmonte-run-avx:pipeline-$CI_PIPELINE_ID $CI_REGISTRY_IMAGE/fullmonte-run-avx:$CI_COMMIT_REF_NAME
    - docker push $CI_REGISTRY_IMAGE/fullmonte-run-avx:pipeline-$CI_PIPELINE_ID
    - docker push $CI_REGISTRY_IMAGE/fullmonte-run-avx:$CI_COMMIT_REF_NAME

deploy_master:
    stage: deploy_master
    image: docker:20.10.3
    only: 
      - master
    script:
      - docker login $CI_REGISTRY -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD
      - docker pull $CI_REGISTRY_IMAGE/fullmonte-run:pipeline-$CI_PIPELINE_ID
      - docker tag $CI_REGISTRY_IMAGE/fullmonte-run:pipeline-$CI_PIPELINE_ID $CI_REGISTRY_IMAGE/fullmonte-run:latest
      - docker push $CI_REGISTRY_IMAGE/fullmonte-run:latest

      - docker pull $CI_REGISTRY_IMAGE/fullmonte-run-avx:pipeline-$CI_PIPELINE_ID
      - docker tag $CI_REGISTRY_IMAGE/fullmonte-run-avx:pipeline-$CI_PIPELINE_ID $CI_REGISTRY_IMAGE/fullmonte-run-avx:latest
      - docker push $CI_REGISTRY_IMAGE/fullmonte-run-avx:latest
