ADD_CUSTOM_COMMAND(
    COMMAND 7z x -y ${CMAKE_CURRENT_SOURCE_DIR}/Bladder_108.7z
    OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/July2017BladderWaterMesh1.mesh.vtk
    DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/Bladder_108.7z
    WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})

ADD_CUSTOM_TARGET(testdata_bladder    
    DEPENDS July2017BladderWaterMesh1.mesh.vtk)

ADD_DEPENDENCIES(testdata testdata_bladder)

INSTALL(FILES
    ${CMAKE_CURRENT_BINARY_DIR}/July2017BladderWaterMesh1.mesh.vtk
    DESTINATION share/data/Bladder)
