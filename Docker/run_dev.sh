docker run  --gpus all  -it                 \
    -v $PWD:/builds/FullMonte/FullMonteSW \
    -v FMSWBuild:/builds/FullMonte \
    -e CI_PROJECT_PATH=FullMonte/FullMonteSW \
    -e CI_PROJECT_DIR=/builds/FullMonte/FullMonteSW \
    -e CI_BUILD_ROOT=/builds/FullMonte/FullMonteSW/Build \
    -e CI_INSTALL_DIR=/usr/local/FullMonteSW \
    ubuntu18
