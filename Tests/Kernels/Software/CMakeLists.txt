### MCML-like kernel with layered geometry and cylindrical scoring

ADD_BOOST_UNIT_TEST(Test_MCMLKernel Test_MCMLKernel.cpp)
TARGET_LINK_LIBRARIES(Test_MCMLKernel FullMonteSWKernel FullMonteData)

ADD_BOOST_UNIT_TEST(Test_RNG Test_RNG.cpp)
TARGET_LINK_LIBRARIES(Test_RNG ${Boost_UNIT_TEST_FRAMEWORK_LIBRARY} SFMT)

ADD_BOOST_UNIT_TEST(Test_Lambertian_Reflect Test_Lambertian_Reflect.cpp)

ADD_TEST(NAME reg_lambertian_reflect COMMAND Test_Lambertian_Reflect)

IF(VTK_FOUND)
    TARGET_LINK_LIBRARIES(Test_MCMLKernel vtkFullMonte)
ENDIF()

ADD_TEST(NAME Test_MCMLKernel_Sample
    COMMAND ${CMAKE_BINARY_DIR}/bin/Test_MCMLKernel
    WORKING_DIRECTORY ${CMAKE_BINARY_DIR}/bin/test)

ADD_BOOST_UNIT_TEST(Test_BlockRandomDistribution Test_BlockRandomDistribution.cpp)
TARGET_LINK_LIBRARIES(Test_BlockRandomDistribution ${Boost_UNIT_TEST_FRAMEWORK_LIBRARY} FullMonteSWKernel)

IF(WRAP_VTK)
    #ADD_BOOST_UNIT_TEST(Test_InternalKernel Test_InternalKernel.cpp)
    #TARGET_LINK_LIBRARIES(Test_InternalKernel FullMonteSWKernel FullMonteData FullMonteGeometry vtkFullMonte FullMonteTIMOS)
    TARGET_LINK_LIBRARIES(Test_MCMLKernel ${VTK_LIBRARIES} vtkFullMonte)

    #INCLUDE(${VTK_USE_FILE})
    ADD_BOOST_UNIT_TEST(Test_Mouse Test_Mouse.cpp)
    TARGET_LINK_LIBRARIES(Test_Mouse vtkFullMonte ${VTK_LIBRARIES} FullMonteGeometry FullMonteKernelBase FullMonteSWKernel FullMonteTIMOS FullMonteQueries)

    ## Paraview visualization file
    FILE(COPY VisualizeMouse.pvsm DESTINATION ${CMAKE_CURRENT_BINARY_DIR})
ENDIF()

MESSAGE("${Blue}Entering subdirectory Tests/Kernels/Software/Emitters${ColourReset}")
ADD_SUBDIRECTORY(Emitters)
MESSAGE("${Blue}Entering subdirectory Tests/Kernels/Software/Logger${ColourReset}")
ADD_SUBDIRECTORY(Logger)
