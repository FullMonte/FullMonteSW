ADD_BOOST_UNIT_TEST(Test_DirectedSurfaceScorer 
	Test_DirectedSurfaceScorer.cpp
)
TARGET_LINK_LIBRARIES(Test_DirectedSurfaceScorer 
	${VTK_LIBRARIES} 
	FullMonteGeometry 
	FullMonteSWKernel 
    FullMonteTIMOS
)
ADD_TEST(NAME test_unit_directed_surface_scorer
	COMMAND Test_DirectedSurfaceScorer)

ADD_BOOST_UNIT_TEST(Test_SurfaceExitImageScorer Test_SurfaceExitImageScorer.cpp)
TARGET_LINK_LIBRARIES(Test_SurfaceExitImageScorer
	${VTK_LIBRARIES}
	FullMonteGeometry
	FullMonteSWKernel
	FullMonteData
	FullMonteTIMOS
)
ADD_TEST(NAME test_camera_sim
	COMMAND Test_SurfaceExitImageScorer)