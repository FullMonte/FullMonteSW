/*
 * Test_SurfaceExitImageScorer.cpp
 *
 *  Created on: Sep 12, 2022
 *      Author: Shuran Wang
 */

#include <vtkPoints.h>
#include <vtkFloatArray.h>
#include <vtkPolyData.h>
#include <vtkPointData.h>
#include <vtkPolyDataWriter.h>
#include <vtkCellArray.h>
#include <vtkCellData.h>

#include <FullMonteSW/Storage/TIMOS/TIMOSMeshReader.hpp>

#include <FullMonteSW/Kernels/Software/TetraSurfaceKernel.hpp>

#include <FullMonteSW/Queries/EnergyToFluence.hpp>
#include <FullMonteSW/Queries/AbsorptionSum.hpp>

#include <FullMonteSW/Config.h>

#include <FullMonteSW/Geometry/FaceLinks.hpp>

#include <FullMonteSW/Geometry/TetraMesh.hpp>
#include <FullMonteSW/Geometry/Point.hpp>
#include <FullMonteSW/Geometry/UnitVector.hpp>

#include <FullMonteSW/OutputTypes/OutputDataCollection.hpp>
#include <FullMonteSW/OutputTypes/OutputDataType.hpp>
#include <FullMonteSW/OutputTypes/PhotonData.hpp>

#include <boost/test/unit_test.hpp>
#include <boost/test/test_tools.hpp>

#include <vector>

using namespace std;

/** Run a simulation with 1e6 packets and a circle monitor looking down from the z axis.
 * Assume air everywhere. Should be isotropical radiation from point source.
 */


BOOST_AUTO_TEST_CASE(Test_CameraSim)
{
	TIMOSMeshReader MR;
	MR.filename(FULLMONTE_DATA_DIR "/TIM-OS/cube_5med/cube_5med.mesh");
	MR.read();
	TetraMesh* M = MR.mesh();

	Source::Point src;
	src.position({{0.0f, 0.0f, 4.9f}});

	Material* air = new Material();
	air->scatteringCoeff(0.0f);
	air->absorptionCoeff(0.0f);
	air->refractiveIndex(1.0f);
	air->anisotropy(0.0f);

	// Material* largeCube = new Material();
	// largeCube->scatteringCoeff(10.0f);
	// largeCube->absorptionCoeff(100.0f);
	// largeCube->refractiveIndex(1.37f);
	// largeCube->anisotropy(0.9f);

	// Material* smallCube = new Material();
	// smallCube->scatteringCoeff(0.1f);
	// smallCube->absorptionCoeff(0.01f);
	// smallCube->refractiveIndex(1.37f);
	// smallCube->anisotropy(0.8f);

	MaterialSet* MS = new MaterialSet();
	MS->set(0,air);
	// MS->set(1,largeCube);
	// MS->set(2,smallCube);
	// MS->set(3,largeCube);
	// MS->set(4,largeCube);
	// MS->set(5,largeCube);
	MS->set(1,air);
	MS->set(2,air);
	MS->set(3,air);
	MS->set(4,air);
	MS->set(5,air);


	TetraSurfaceKernel K;
	K.geometry(M);
	K.monitor({{0,0,-1}},{{0,0,5.9}},2.5f);
	K.source(&src);
//#ifdef DEBUG_MODE
	K.packetCount(10000);
	K.energyPowerValue(10000); // Legacy requirement: photon weight = energy e.g no scaling should be done
// #else
// 	K.packetCount(1000000);
// 	K.energyPowerValue(1000000); // Legacy requirement: photon weight = energy e.g no scaling should be done
// #endif
	K.materials(MS);

	cout << "Starting" << endl;
	K.runSync();
	cout << "Done" << endl;

	OutputDataCollection* C = K.results();

	cout << "Produced " << C->size() << " result items" << endl;
	for(unsigned i=0;i<C->size();++i)
		cout << "  [" << i << "] name '" << C->getByIndex(i)->name() << "' type '" << C->getByIndex(i)->type()->name() << '\'' << endl;

	// Extract photon weights
	PhotonData* photonData;
	SpatialMap<float>* weight = nullptr;
	typedef vector<pair<array<float,3>, array<float,3> > > rvec;
	rvec* ray;

	BOOST_REQUIRE(photonData = dynamic_cast<PhotonData*>(C->getByName("SurfaceExitPhotonData")));
	BOOST_REQUIRE(weight = dynamic_cast<SpatialMap<float>*>(photonData->weight()));
	BOOST_REQUIRE(ray = dynamic_cast<rvec*>(photonData->ray()));
	
	// Print photons
	cout << "Total " << ray->size() << " photons scored.\n";

	BOOST_REQUIRE_EQUAL (ray->size(), 3151u);
	// for (unsigned i = 0; i < ray->size(); i++) {
	// 	array<float,3> pos = ray->at(i).first;
	// 	array<float,3> dir = ray->at(i).second;
	// 	cout << "Photon " << i << ": weight " << weight->get(i) << " position " << pos[0] << " " << pos[1] << " " << pos[2] << " direction " << dir[0] << " " << dir[1] << " " << dir[2] << endl;
	// }
}

