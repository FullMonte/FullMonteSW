//BOOST libraries
#include <boost/test/included/unit_test.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/align/aligned_allocator.hpp>
#include <boost/range.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/adaptor/indexed.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/random/variate_generator.hpp>

//includes configurations for non-host related paths
#include <FullMonteSW/Config.h>

#include <iostream>
#include <array>
#include <math.h>
#include <cmath>
#include <ctime>
#include <chrono>
#include <stdio.h>
#include <numeric>
#include <vector>
#include <random>

//Sources
#include <FullMonteSW/Geometry/Sources/Cylinder.hpp>
#include <FullMonteSW/Geometry/Sources/CylDetector.hpp>
#include <FullMonteSW/Kernels/Software/Emitters/CylDetector.hpp>


#include <FullMonteSW/Geometry/Queries/TetraEnclosingPointByLinearSearch.hpp>
#include <FullMonteSW/Geometry/TetraMesh.hpp>
#include <FullMonteSW/Storage/TIMOS/TIMOSMeshReader.hpp>



//Factories
#include <FullMonteSW/Kernels/Software/Emitters/TetraMeshEmitterFactory.hpp>
//

//Emitters
#include <FullMonteSW/Kernels/Software/Emitters/Base.hpp>
#include <FullMonteSW/Kernels/Software/Packet.hpp>
#include <FullMonteSW/Kernels/Software/Emitters/FiberConeEmitter.hpp>


//SFMT AVX Random number generator 
#include <FullMonteSW/Kernels/Software/RNG_SFMT_AVX.hpp>

using namespace std;
//Source namespace
using namespace Source;
//Emitter namespace
using namespace Emitter;

BOOST_AUTO_TEST_SUITE(Test_CylinderDetector)

BOOST_AUTO_TEST_CASE( CylindricalDetector )

{

	//number of launch packet
	const size_t Ndw=1024;
	
    //TIMOSMeshReader Class to read the mesh
	TIMOSMeshReader R;
	//load mesh
	R.filename(FULLMONTE_DATA_DIR "/TIM-OS/cube_5med/cube_5med.mesh");
	//R.filename(FULLMONTE_DATA_DIR "/Colin27/Colin27.mesh");
	R.read();
	//TetraMesh class stores the mesh
	TetraMesh* M = R.mesh();

    //Define MaterialSet to hold all materials for the supported meshes
    MaterialSet MS;
	//Material definitions
	Material bigcube(0.05,20.0,0.9,1.3);
	Material smallcube1(0.1,10.0,0.7,1.1);
	Material smallcube2(0.2,20.0,0.8,1.2);
	Material smallcube3(0.1,10.0,0.9,1.4);
	Material smallcube4(0.2,20.0,0.9,1.5);

	MS.append(&bigcube);
	MS.append(&smallcube1);
	MS.append(&smallcube2);
	MS.append(&smallcube3);
	MS.append(&smallcube4);

	//factory creates an emitter from the source light . It can generate photon packets 
	auto factory = new Emitter::TetraEmitterFactory<RNG_SFMT_AVX>(M, &MS);

	//Point source
	auto point = new Source::Point(1.0f);
    point->position({0,-2,0});

    // cylindrical detector
    auto detector = new Source::CylDetector(1.0f); 
    detector->radius(1.0); 
    detector->endpoint(0, {1, -2, 1});
    detector->endpoint(1, {1, 2, 1});
    detector->numericalAperture(0.22); 
    
    // add source and detector  to factory
    point->acceptVisitor(factory);
    detector->acceptVisitor(factory); 
	auto S = factory->cemitters();

    auto D = factory->detectors(); 

    std::cout << "Number of detectors: " << D.size() << endl;

    const std::array<float,3> de0 = detector->endpoint(0);
    const std::array<float,3> de1 = detector->endpoint(1);
    const std::array<float,3> dDir = {
        de1[0] - de0[0],
        de1[1] - de0[1],
        de1[2] - de0[2]
    };
    const std::array<float,3> dDirNorm = normalize(dDir);
    const float dHeight = norm(dDir);
    const float dRadius = detector->radius();

    Emitter::Detector<RNG_SFMT_AVX, Emitter::CylDetector<RNG_SFMT_AVX>> * Ddetector = 
                    dynamic_cast<Emitter::Detector<RNG_SFMT_AVX, Emitter::CylDetector<RNG_SFMT_AVX>>*>(D[0]);

    BOOST_CHECK(Ddetector != nullptr); 

    // Check valuse are correct 
    BOOST_REQUIRE(abs(dRadius - Ddetector->detector().radius()) < 1e-5);
    BOOST_REQUIRE(abs(dHeight - Ddetector->detector().height()) < 1e-5);
    std::array<float, 3> diffNorm = {dDirNorm[0] - Ddetector->detector().dirNormal()[0], 
                dDirNorm[1] - Ddetector->detector().dirNormal()[1],
                dDirNorm[2] - Ddetector->detector().dirNormal()[2]};
    BOOST_REQUIRE(norm(diffNorm) < 1e-5);
    BOOST_REQUIRE(abs(dRadius - Ddetector->detector().radius()) < 1e-5);
    

    // Now launch some photons and try to detect them by manually setting the packet position to be 
    // 1- outside the detector ==> detetion = 0; 
    // 2- Inside the detector ==> detection = packet weight
    
    // the RNG
	RNG_SFMT_AVX rng;
	rng.seed(1);
    
	//randLaunch holds the launch packets generated 
	vector<LaunchPacket> randLaunch(Ndw);
    std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();
    // launch all of the packets
	for(unsigned i = 0; i < Ndw; ++i) {
		//fill the randLaunch vector with packets generated
		randLaunch[i]= S[0]->emit(rng, Ndw, i);

	}
    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    std::cout << "Launching " << Ndw << " packets took " << std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count() << "ms" << std::endl;

    float n = 1.3;

    // analyze the launch packets
    float weightDetected = 0.0;
    for(unsigned i = 0; i < Ndw; ++i) {
        LaunchPacket lpkt = randLaunch[i];

        // trying to detect any of the packets should return zero detected weight
        Packet pkt(lpkt);
        Ddetector->detect(rng, pkt, n); // assuming n=1.3 
        weightDetected += (1-pkt.w); 
    }
    cout << "Detected packets: " << weightDetected << endl;
    cout << "Total detected weight: " << Ddetector->detector().detectedWeight() << endl;
    BOOST_REQUIRE(Ddetector->detector().detectedWeight() < 1e-5);
   
    // Now move the point source inside the detected so that we have packets inside to be detected
    // Depending on the packet direction, it may or may not be detected
    point->position({0, -2, 0});  

    // delete the factory and reinitialize
    delete factory;
    factory = new Emitter::TetraEmitterFactory<RNG_SFMT_AVX>(M, &MS);

    // add source and detector  to factory
    point->acceptVisitor(factory);
    detector->acceptVisitor(factory); 
	S = factory->cemitters();

    D = factory->detectors();

    start = std::chrono::steady_clock::now();
    // launch all of the packets
	for(unsigned i = 0; i < Ndw; ++i) {
		//fill the randLaunch vector with packets generated
		randLaunch[i]= S[0]->emit(rng, Ndw, i);
	}
    end = std::chrono::steady_clock::now();
    std::cout << "Launching " << Ndw << " packets took " << std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count() << "ms" << std::endl;

    // analyze the launch packets
    Ddetector->detector().setDetectionType(Source::CylDetector::DetectionType::FULL); 
    Ddetector->detector().resetDetectedWeight(); 
    weightDetected = 0.0;
    float shouldBeDetected = 0.0;
    SSE::UnitVector3 detDir(Ddetector->detector().dirNormal());

    srand(0); 
    for(unsigned i = 0; i < Ndw; ++i) {
        LaunchPacket lpkt = randLaunch[i];

        // Move the packets randomly 
        Packet pkt(lpkt);
        
        SSE::Point3 newpos = SSE::Point3(pkt.oldP) + pkt.direction()*SSE::Scalar((1.0*rand()/RAND_MAX));
#ifdef USE_SSE
        pkt.p = __m128(newpos);
#else
        pkt.p = m128(newpos); 
#endif

       float angle = std::acos(float(dot(detDir*SSE::Scalar(-1.0), pkt.direction())));
        if ( angle <= std::asin(Ddetector->detector().numericalAperture()/n))
            shouldBeDetected += pkt.w;

        Ddetector->detect(rng, pkt, n); // assuming n=1.3 
        weightDetected += (1-pkt.w); 
    }
    cout << "Detected packets: " << weightDetected << endl;
    cout << "Total detected weight: " << Ddetector->detector().detectedWeight() << endl;

    BOOST_REQUIRE(abs(shouldBeDetected - Ddetector->detector().detectedWeight()) < 1e-5);


    // Test detection with probability now 
    Ddetector->detector().setDetectionType(Source::CylDetector::DetectionType::PROBABILITY); 
    Ddetector->detector().resetDetectedWeight(); 
    // analyze the launch packets
    weightDetected = 0.0;
    shouldBeDetected = 0.0;
    for(unsigned i = 0; i < Ndw; ++i) {
        LaunchPacket lpkt = randLaunch[i];

        // Move the packets randomly 
        Packet pkt(lpkt);
        
        SSE::Point3 newpos = SSE::Point3(pkt.oldP) + pkt.direction()*SSE::Scalar((1.0*rand()/RAND_MAX));
#ifdef USE_SSE
        pkt.p = __m128(newpos);
#else
        pkt.p = m128(newpos); 
#endif
        float old_weight = pkt.w;
        float angle = std::acos(float(dot(detDir*SSE::Scalar(-1.0), pkt.direction())));
        if ( angle <= std::asin(Ddetector->detector().numericalAperture()/n)) {
            shouldBeDetected += pkt.w*std::exp((std::log(0.01)/
                (0.9*Ddetector->detector().height()))*2); 
        }
        Ddetector->detect(rng, pkt, n); // assuming n=1.3 
        if (pkt.w < old_weight)
            weightDetected += (old_weight-pkt.w); 
    }
    cout << "Detected packets: " << weightDetected << endl;
    cout << "Total detected weight: " << Ddetector->detector().detectedWeight() << endl;

    BOOST_REQUIRE(abs(shouldBeDetected - Ddetector->detector().detectedWeight()) < 1e-5);

    // Test detection with ODE now 
    Ddetector->detector().setDetectionType(Source::CylDetector::DetectionType::ODE); 
    Ddetector->detector().resetDetectedWeight(); 
    // analyze the launch packets
    weightDetected = 0.0;
    shouldBeDetected = 0.0;
    for(unsigned i = 0; i < Ndw; ++i) {
        LaunchPacket lpkt = randLaunch[i];

        // Move the packets randomly 
        Packet pkt(lpkt);
        
        SSE::Point3 newpos = SSE::Point3(pkt.oldP) + pkt.direction()*SSE::Scalar((1.0*rand()/RAND_MAX));
#ifdef USE_SSE
        pkt.p = __m128(newpos);
#else
        pkt.p = m128(newpos); 
#endif
        float old_weight = pkt.w;
        float angle = std::acos(float(dot(detDir*SSE::Scalar(-1.0), pkt.direction())));
        if ( angle <= std::asin(Ddetector->detector().numericalAperture()/n)) {
            shouldBeDetected += pkt.w*std::exp((-10.0/7)*(std::exp(7.0*(2.0-0.9*Ddetector->detector().height()))
                        - std::exp(-7.0*0.9*Ddetector->detector().height())));
        }
        Ddetector->detect(rng, pkt, n); // assuming n=1.3 
        if (pkt.w < old_weight)
            weightDetected += (old_weight-pkt.w); 
    }
    cout << "Detected packets: " << weightDetected << endl;
    cout << "Total detected weight: " << Ddetector->detector().detectedWeight() << endl;

    BOOST_REQUIRE(abs(shouldBeDetected - Ddetector->detector().detectedWeight()) < 1e-5);

}

BOOST_AUTO_TEST_SUITE_END()









