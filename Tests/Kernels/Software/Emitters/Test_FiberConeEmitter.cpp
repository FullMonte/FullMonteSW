/*
 *  * Test_FiberConeEmitter.c
 *   *
 *    *  Created on: May 20, 2018
 *     *      Author: Yasmin Afsharnejad
 *      */

//Test_FiberConeEmitter generates launch packets from a fiber. 
//It checks for the direction of launch packets to make sure their "alphaWithin" angle relative to z-axis is selected uniformly random within the alphaof aperture
//It also checks for the position of launch packets to be selected randomly uniform within the surface of the cut-end fiber end point with radius r.     
//The test reads a mesh file and pass it to the emiter factory to generate the position and direction of launch packets. So, make sure the correct path is given to the file.   
//The test generates 65536 packets 
//It verifies the uniform distribution in 20 bins for both direction and position. 
//

//boost unit test 
#include <boost/test/included/unit_test.hpp>

//includes configurations for non-host related paths
#include <FullMonteSW/Config.h>

#include <iostream>
#include <array>
#include <math.h>
#include <cmath>
#include <stdio.h>
#include <numeric>
#include <vector>

//boost

//BOOST libraries
#include <boost/random/mersenne_twister.hpp>
#include <boost/align/aligned_allocator.hpp>
#include <boost/range.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/adaptor/indexed.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/random/variate_generator.hpp>
#include <random>


//Sources
#include <FullMonteSW/Geometry/Sources/Fiber.hpp>


#include <FullMonteSW/Geometry/Queries/TetraEnclosingPointByLinearSearch.hpp>
#include <FullMonteSW/Geometry/TetraMesh.hpp>
#include <FullMonteSW/Geometry/MaterialSet.hpp>
#include <FullMonteSW/Storage/TIMOS/TIMOSMeshReader.hpp>



//Factories
#include <FullMonteSW/Kernels/Software/Emitters/TetraMeshEmitterFactory.hpp>
//Emitters
#include <FullMonteSW/Kernels/Software/Emitters/Base.hpp>
#include <FullMonteSW/Kernels/Software/Packet.hpp>
#include <FullMonteSW/Kernels/Software/Emitters/FiberConeEmitter.hpp>


//SFMT AVX Random number generator 
#include <FullMonteSW/Kernels/Software/RNG_SFMT_AVX.hpp>


//random generator from boost library using mt19937: mersenne_twister_engine
typedef boost::random::mt19937 base_generator_type;
base_generator_type rng_f;

//Random number generator with Uniform distribution between -1 and 1    
boost::uniform_real<> uni_dist_11(-1,1);
boost::variate_generator<base_generator_type&, boost::uniform_real<> > uni(rng_f, uni_dist_11);

//Random number generator with Uniform distribution between 0 and 1 
boost::uniform_real<> uni_dist_01(0,1);
boost::variate_generator<base_generator_type&, boost::uniform_real<> > uni01(rng_f, uni_dist_01);

//random generator for generating integer numbers 

std::random_device rd;
std::mt19937 gen(rd());

//
struct RNG2 {

	float rand0;
	float rand1;

};

//RNG3 is sent to the emitter to generate launch packets.
// It holds random numbers btween -1 and 1 in rand0, rand1, rand2
// And holds random numbers between 0 and 1 in rand_U01_0, rand_U01_1, and rand_U01_2
struct RNG3 {

	float rand0;
	float rand1;
	float rand2;
	float rand_U01_0;
	float rand_U01_1;
	float rand_U01_2;
};



using namespace std;
//Source namespace
using namespace Source;
//Emitter namespace
using namespace Emitter;

BOOST_AUTO_TEST_SUITE(Test_FiberConeEmitter)

BOOST_AUTO_TEST_CASE( example )

{

	//number of launch packet
	const size_t Ndw=1024*64;




	//TIMOSMeshReader Class to read the mesh
	TIMOSMeshReader R;
	//load mesh
	R.filename(FULLMONTE_DATA_DIR "/TIM-OS/cube_5med/cube_5med.mesh");
	//R.filename(FULLMONTE_DATA_DIR "/Colin27/Colin27.mesh");
	R.read();
	//TetraMesh class stores the mesh
	TetraMesh* M = R.mesh();
	M->rtree()->create();
	//randLaunch holds the launch packets generated 
	vector<LaunchPacket> randLaunch(Ndw);

	//Define MaterialSet to hold all materials for the supported meshes
    MaterialSet MS;
	//Material definitions
	Material bigcube(0.05,20.0,0.9,1.3);
	Material smallcube1(0.1,10.0,0.7,1.1);
	Material smallcube2(0.2,20.0,0.8,1.2);
	Material smallcube3(0.1,10.0,0.9,1.4);
	Material smallcube4(0.2,20.0,0.9,1.5);

	MS.append(&bigcube);
	MS.append(&smallcube1);
	MS.append(&smallcube2);
	MS.append(&smallcube3);
	MS.append(&smallcube4);

	//factory creates an emitter from the source light . It can generate photon packets 
	auto myFactory = new Emitter::TetraEmitterFactory<RNG_SFMT_AVX>(M, &MS);



	std::array<float,3> fiberDirection={{0,0,1}};//direction of fiber axis
	float fiberEndRadius=0.2;// radius of fiber end point
	float numAperture= sin(PI/6); //numerical aperture
	std::array<float,3> fiberEndPosition= {0,0,0};//position of fiber end

	//fiber source
	auto mySourceFs= new Source::Fiber(1.0f, numAperture, fiberDirection,fiberEndPosition , fiberEndRadius);
	mySourceFs->acceptVisitor(myFactory);
	auto S = myFactory->cemitters();

	//generates a random number to pick between one of the emitters in the factory
	std::uniform_int_distribution<> dis(0,S.size()-1);

	RNG_SFMT_AVX rng;
	rng.seed(1);
	
	for(unsigned i=0;i<Ndw;++i)
	{
		//Uniform random numbers passed to emitters
		//which emitter to pick (when more than one emitter is created (i.e. when emitter is Ball or Line) 
		int emitNum =  dis(gen) ;
		//cout << emitNum << endl;  

		//emit a packet from the emitter created and store it in lpkt
		LaunchPacket lpkt = S[emitNum]->emit(rng,Ndw,i);

		/* print out the position and direction of the launch packets along with their auxilary vectors a, and b  */
		//std::array<float,3> pos = lpkt.pos.array();
		//std::array<float,3> d = lpkt.dir.d.array();

		/*cout << std::fixed << std::setprecision(4) <<
			std::setw(9) << pos[0] << ',' << std::setw(9) << pos[1] << ',' << std::setw(9) << pos[2] << "  dir " <<
			std::setw(7) << d[0]   << ',' << std::setw(7) << d[1]   << ',' << std::setw(7) << d[2]   << endl;*/

		//fill the randLaunch vector with packets generated
		randLaunch[i]=(lpkt);

	}	

	//Pick 20 bins to verify the distribution of launch packets is uniform within those bins (both for direction and poistion)
	int numBins = 20;

	//divide the circle of endpoint with radius of "fiberEndRadius" into multiple (=numBins) rings. Division should be in a way that the area of rings would be equal. 
	float radiusDiv[numBins+1] ;

	//cout << "radius of divisions" << endl;

	for (int j=0; j < (numBins+1); j++)
	{
		float b = j;
		radiusDiv[j] = fiberEndRadius * sqrt (b/numBins);
		//cout << radiusDiv[j] << endl; 

	}

	//Now count the number of packets launched from within any of those rings to verify the distribution is uniform within the surface of fiber end-point.
	vector<int> radiusCount(numBins);

	//cout << "radius of packets" << endl;

	for (unsigned i=0; i<Ndw; i++)
	{
		//compute the radius of the position of packet on a circle of radius 'fiberEndRadius'
		std::array<float,3> pos= randLaunch[i].pos.array();
		float r = sqrt(pos[0]*pos[0] + pos[1]*pos[1]) ;
		//cout << r << endl;

		//which ring does the packet belong to?
		for (int k=0; k < numBins ; k++)
		{
			if ( radiusDiv[k] <r  && r < radiusDiv[k+1] )
				radiusCount[k] = radiusCount[k] +1;
		}
	}

	/*cout << "distribution of radius" << endl;

	for (int j=0; j < numBins; j++)
	{
		cout << radiusCount[j] << endl ;
	}*/





	//divide the alpha of aperture into multiple (=numBins) bins 
	float alphaDiv[numBins+1] ;

	//perform search using RTree (which falls back to linear search) 
	TetraMesh::TetraDescriptor el = M->rtree()->search(fiberEndPosition);
	//determine the refractive index of the material from which the photon is launched
	unsigned region_index = get(region,*M, el);
	float refractiveIndex = MS.get(region_index)->refractiveIndex();
	//alpha of aperture
	float alpha = asin (numAperture/refractiveIndex);

	//	cout << "alpha of divisions" << endl;


	//Now count the number of packets launched from within any of those bins within an alpha of aperture
	vector<int> alphaCount(numBins);

	//alpha range of each bin
	for (int j=0; j < (numBins+1); j++)
	{
		float b = j;
		alphaDiv[j] = alpha * (b/numBins);
		//        cout << alphaDiv[j] << endl; 
	}


	//generate launch packets
	for (unsigned i=0; i<Ndw; i++)
	{

		std::array<float,3> d= randLaunch[i].dir.d.array();//lpkt.dir.d.array();
		float alphaWithin = acos (d[2]);
		//      cout << alphaWithin << endl; 

		//which bin does the packet belong to?
		for (int k=0; k < numBins ; k++)
		{
			if ( alphaDiv[k] < alphaWithin  && alphaWithin < alphaDiv[k+1] )
				alphaCount[k] = alphaCount[k] +1;
		}


	}

	/*cout << "distribution of alpha" << endl;

	for (int j=0; j < numBins; j++)
	{
		cout << alphaCount[j] << endl ;
	}*/



	//For alpha distribution in the bins , find the standard deviation and mean
	double sum = std::accumulate(alphaCount.begin(), alphaCount.end(), 0.0);
	double mean = sum / alphaCount.size();

	double sq_sum = std::inner_product(alphaCount.begin(), alphaCount.end(), alphaCount.begin(), 0.0);
	double stdev = std::sqrt(sq_sum / alphaCount.size() - mean * mean);

	//cout << "mean of 'alphaWithin' angle distribution within alpha of aperture: " << mean<< endl; 
	//cout << "standard deviation of 'alphaWithin' angle distribution within alpha of aperture: " << stdev << endl;

	//boost checks if the standard deviation is less than 25% of mean (experimentally set based on number of packets in the test. )
	BOOST_REQUIRE( stdev < 0.25*mean );

	//For radius distribution in the bins, find the standard deviation and mean
	sum = std::accumulate(radiusCount.begin(), radiusCount.end(), 0.0);
	mean = sum / radiusCount.size();

	sq_sum = std::inner_product(radiusCount.begin(), radiusCount.end(), radiusCount.begin(), 0.0);
	stdev = std::sqrt(sq_sum / radiusCount.size() - mean * mean);

	//cout << "mean of position distribution in bins of equal area within fiber end-point: " << mean<< endl; 
	//cout << "standard deviation of position distribution in bins of equal area within fiber end-point:: " << stdev << endl;

	//boost checks if the standard deviation is less than 25% of mean (experimentally set based on number of packets in the test. )
	BOOST_REQUIRE( stdev < 0.25*mean );
}

BOOST_AUTO_TEST_SUITE_END()









