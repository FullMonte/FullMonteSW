/*
 * Test_Runtime.cpp
 *
 *  Created on: Nov 16, 2018
 *      Author: fynns
 */

#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include <FullMonteSW/Config.h>

#include <iostream>
#include <fstream>
#include <string>

// check the runtime of the current branch against the golden runtime
BOOST_AUTO_TEST_CASE(Runtime_check)
{
	std::string reg_line, gold_line;
	std::ifstream gold_file, reg_file;
	std::string filename_reg(FULLMONTE_BINARY_DIR);
	filename_reg.append("/Tests/SystemLevelTests/Runtime.txt");
	reg_file.open(filename_reg);
	std::string filename_gold(FULLMONTE_SOURCE_DIR);
#ifdef DEBUG_MODE
	filename_gold.append("/Tests/SystemLevelTests/gold_DebugRuntime.txt");
#else
	filename_gold.append("/Tests/SystemLevelTests/gold_ReleaseRuntime.txt");
#endif
	gold_file.open(filename_gold);
	if (gold_file.is_open() && reg_file.is_open())
	{
		getline(reg_file,reg_line);
		float reg_runtime = std::stof(reg_line);
		
		getline(gold_file, gold_line);
		float gold_runtime = std::stof(gold_line);

    	reg_file.close();
		gold_file.close();

		BOOST_CHECK_CLOSE(reg_runtime, gold_runtime, 20);


	}
	else 
	{	
		if (!gold_file.is_open()) std::cout << "[debug] unable to open " << filename_gold << std::endl;
		if (!reg_file.is_open()) std::cout << "[debug] unable to open " << filename_reg << std::endl;
		
		BOOST_FAIL("Unable to open files");
	}
}
