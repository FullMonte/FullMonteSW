import sys
import filecmp
import os

import fmpy

T = fmpy.Logging.FullMonteTimer()

print("Reading mesh")
MR = fmpy.TIMOS.TIMOSMeshReader()
meshpath = os.path.join(fmpy.datadir,'TIM-OS/cube_5med/cube_5med.mesh')
MR.filename(meshpath)
MR.read()
mesh = MR.mesh()

# Set up materials
air = fmpy.Geometry.Material()
air.scatteringCoeff(0)
air.absorptionCoeff(0)
air.refractiveIndex(1.0)
air.anisotropy(0.0)
air.refractiveIndex(1.3)

big_cube = fmpy.Geometry.Material() 
big_cube.scatteringCoeff(20)
big_cube.absorptionCoeff(0.05)
big_cube.anisotropy(0.9)
big_cube.refractiveIndex(1.3)

small_cube1 = fmpy.Geometry.Material() 
small_cube1.scatteringCoeff(10)
small_cube1.absorptionCoeff(0.1)
small_cube1.anisotropy(0.7)
small_cube1.refractiveIndex(1.1)


small_cube2 = fmpy.Geometry.Material() 
small_cube2.scatteringCoeff(20)
small_cube2.absorptionCoeff(0.2)
small_cube2.anisotropy(0.8)
small_cube2.refractiveIndex(1.2)


small_cube3 = fmpy.Geometry.Material() 
small_cube3.scatteringCoeff(10)
small_cube3.absorptionCoeff(0.1)
small_cube3.anisotropy(0.9)
small_cube3.refractiveIndex(1.4)


small_cube4 = fmpy.Geometry.Material() 
small_cube4.scatteringCoeff(20)
small_cube4.absorptionCoeff(0.2)
small_cube4.anisotropy(0.9)
small_cube4.refractiveIndex(1.5)

MS = fmpy.Geometry.MaterialSet()
MS.exterior(air)
MS.append(big_cube)
MS.append(small_cube1)
MS.append(small_cube2)
MS.append(small_cube3)
MS.append(small_cube4)


# Set up all available sources (Cut-end Fiber will be added once it is finished) 
P = fmpy.Geometry.Point()
P.position((2.1, 2.2, 2.35))

PB = fmpy.Geometry.PencilBeam()
PB.position((-2.1, 2.2, 2.35))
PB.direction((0, 0, -1))

V = fmpy.Geometry.Volume()
V.elementID(11166)

B = fmpy.Geometry.Ball()
B.centre((-2.1, -2.2, -2.35))
B.radius(0.5)

L = fmpy.Geometry.Line()
L.endpoint(0, (2.1, -2.2, -2.35))
L.endpoint(1, (-2.1, 2.2, 2.35))

F = fmpy.Geometry.Fiber()
F.radius(0.5)
F.numericalAperture(0.5)
F.fiberDir((-1.0, 0.0, 0.0))
F.fiberPos((4.15, 0.0, 0.0))

C = fmpy.Geometry.Composite()
C.add(P)
C.add(PB)
C.add(V)
C.add(B)
C.add(L)
C.add(F)

# Set up scoring region (score photons entering/exiting region 2)
vol = fmpy.Geometry.VolumeCellInRegionPredicate()
vol.setRegion(2)

# Configure kernel
    
K = fmpy.SWKernel.TetraInternalKernel()

K.materials(MS)
K.source(C)
scorer = K.directedSurfaceScorer()
scorer.addScoringRegionBoundary(vol)
K.geometry(mesh)
K.threadCount(1)

if fmpy.buildtype == "Release":
    print("Build type of FullMonte is Release. Using large photon count for simulation.")
    K.packetCount(1000000)
else:
    print("Build type of FullMonte is Debug. Using small photon count for simulation.")
    K.packetCount(2000)

print("Running")
K.runSync()
print("Done")

results = K.results()

directedSurfE = results.getByType("DirectedSurface")
surfaceExitE = results.getByName("SurfaceExitEnergy")
volumeE = results.getByName("VolumeEnergy")

EF = fmpy.Queries.EnergyToFluence()
EF.kernel(K)
EF.inputEnergy()
EF.outputFluence()


# We are only writing out the fluence values as .txt file since we want to verify the correct functionality of the simulator
# The mesh creation should have been verified by another test

# Write out the directed surface fluence
EF.data(directedSurfE)
EF.update()

TW = fmpy.TextFile.TextFileMatrixWriter()
TW.filename("DirectedSurfaceFluence.txt")
TW.source(EF.result())
TW.write()

# Write out the exterior surface fluence
EF.data(surfaceExitE)
EF.update()


TW.filename("SurfaceExitFluence.txt")
TW.source(EF.result())
TW.write()  

# Write out the exterior volume fluence
EF.data(volumeE)
EF.update()


TW.filename("VolumeFluence.txt")
TW.source(EF.result())
TW.write()  

Runtime = T.elapsed_sec()
Memory = T.max_rss_mib()

print("Runtime =", Runtime, "s, peak memory usage =", Memory, "MiB.")

RuntimeFile = open('Runtime.txt', 'w')
RuntimeFile.write(str(Runtime))
RuntimeFile.close()

summ = fmpy.Data.OutputDataSummarize()
summ.visit(results.getByName("EventCounts"))
summ.visit(results.getByName("ConservationCounts"))

# Open all generated files for comparison with golden files
# RegTestDirected = open('DirectedSurfaceFluence.vtk', 'r')
with open('DirectedSurfaceFluence.txt') as f1:
    next(f1) # Skip first line of file
    next(f1) # Skip second line file
    RegTestDirected = [float(val) for line in f1 for i, val in enumerate(line.split()) if i == 2]
    # Code below does the same as the above line, it might just be more readable than the one-liner :-)
    #dirSurf = []
    #for line in f:
    #    for i, val in enumerate(line.split()):
    #        if i == 2:
    #            dirSurf.append(float(val))


# RegTestSurface = open('SurfaceExitFluence.txt', 'r')
with open('SurfaceExitFluence.txt') as f2:
    next(f2)
    RegTestSurface = [float(val) for line in f2 for val in line.split()]

# RegTestVolume = open('VolumeFluence.txt', 'r')
with open('VolumeFluence.txt') as f3:
    next(f3)
    RegTestVolume = [float(val) for line in f3 for val in line.split()]

if fmpy.buildtype == "Release":
    f1GoldPath = os.path.join(fmpy.sourcedir, 'Tests/SystemLevelTests/gold_DirectedSurfaceFluence.txt')
    with open(f1GoldPath) as f1Gold:
        next(f1Gold)
        next(f1Gold)
        GoldTestDirected = [float(val) for line in f1Gold for i, val in enumerate(line.split()) if i == 2]

    f2GoldPath = os.path.join(fmpy.sourcedir, 'Tests/SystemLevelTests/gold_SurfaceExitFluence.txt')
    with open(f2GoldPath) as f2Gold:
        next(f2Gold)
        GoldTestSurface = [float(val) for line in f2Gold for val in line.split()]

    f3GoldPath = os.path.join(fmpy.sourcedir, 'Tests/SystemLevelTests/gold_VolumeFluence.txt')
    with open(f3GoldPath) as f3Gold:
        next(f3Gold)
        GoldTestVolume = [float(val) for line in f3Gold for val in line.split()]
    
    ### Test if the results are within the expected error ranges
    sumGoldDirected = sum(GoldTestDirected)
    absErrorDirected = sum([abs(GoldTestDirected[i] - RegTestDirected[i]) for i in range(len(GoldTestDirected))])
    print("DirectedSurfaceTotalEnergy - Normalized L1-Norm of errors: ", absErrorDirected/sumGoldDirected)
    if absErrorDirected/sumGoldDirected < 0.02:
        print("DirectedSurface results are within the expected statistical range of the golden sample!")
    else:
        raise Exception("Absolute sum of errors for the DirectedSurface results are higher than expected!")

    sumGoldSurface = sum(GoldTestSurface)
    absErrorSurface = sum([abs(GoldTestSurface[i] - RegTestSurface[i]) for i in range(len(GoldTestSurface))])
    print("SurfaceExitEnergy - Normalized L1-Norm of errors: ", absErrorSurface/sumGoldSurface)
    if absErrorSurface/sumGoldSurface < 0.08:
        print("SurfaceExit results are within the expected statistical range of the golden sample!")
    else:
        raise Exception("Absolute sum of errors for the SurfaceExit results are higher than expected!")

    sumGoldVolume = sum(GoldTestVolume)
    absErrorVolume = sum([abs(GoldTestVolume[i] - RegTestVolume[i]) for i in range(len(GoldTestVolume))])
    print("VolumeEnergy - Normalized L1-Norm of errors: ",absErrorVolume/sumGoldVolume)
    if absErrorVolume/sumGoldVolume < 0.03:
        print("Volume results are within the expected statistical range of the golden sample!")
    else:
        raise Exception("Absolute sum of errors for the Volume results are higher than expected!")

else :
    ### It does not make any sense to statistically test the results in Debug mode because of the small amount of 
    ### packets that are being simulated
    print("In Debug mode, this script always completes successfully. We only simulate 2000 packets to keept runtime low. Statistical comparisons of the results won't be accurate.")
    # GoldTestDirected = os.path.join(fmpy.sourcedir, 'Examples/SystemLevelTests/gold_DirectedSurfaceFluenceDebug.vtk.reg')
    # GoldTestSurface = os.path.join(fmpy.sourcedir, 'Examples/SystemLevelTests/gold_SurfaceExitFluenceDebug.txt')
    # GoldTestVolume = os.path.join(fmpy.sourcedir, 'Examples/SystemLevelTests/gold_VolumeFluenceDebug.txt')


 



