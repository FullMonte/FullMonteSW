package require FullMonte

puts "Reading mesh"
VTKMeshReader MR
MR filename "$FullMonte::datadir/Bladder/July2017BladderWaterMesh1.mesh.vtk"
MR read
set mesh [MR mesh]


# Set up source
Point P
P position "-13.7 -3.3 687"


# Set up materials
MaterialSet MS

Material air
air scatteringCoeff 0
air absorptionCoeff 0
air refractiveIndex 1.37

Material void
void scatteringCoeff 0.1
void absorptionCoeff 0.01
void refractiveIndex 1.37
void anisotropy 0.9

Material surround
surround scatteringCoeff 100.0
surround absorptionCoeff 0.5
surround refractiveIndex 1.39
surround anisotropy 0.9

MS set 0 air
MS set 1 surround
MS set 2 void


# Set up scoring region (score photons entering/exiting region 2)
VolumeCellInRegionPredicate vol
vol setRegion 2



# Configure kernel

TetraInternalKernel K
set isRelease [string equal $FullMonte::buildtype "Release"]

if {$isRelease} {
    K packetCount 1000000
} else {
    # speed up reg test in debug mode
    K packetCount 1000
}
K materials MS
K source P
[K directedSurfaceScorer] addScoringRegionBoundary vol
K geometry $mesh


puts "Running"
K runSync
puts "Done"

set results [K results]

set directedSurfE [$results getByType "DirectedSurface"]

EnergyToFluence EF
EF data $directedSurfE
EF kernel K
EF inputEnergy
EF outputFluence
EF update

## Resulting VTK dataset has two components: entering (0) and exiting (1)
VTKSurfaceWriter W
W filename "directed_surface.vtk"
W mesh $mesh
W addData "Fluence" [EF result]
W write

