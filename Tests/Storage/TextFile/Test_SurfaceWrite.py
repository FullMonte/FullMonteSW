import os
import fmpy

## Delete files if exist
if os.path.exists("colin27.EV.txt"):
    os.remove("colin27.EV.txt")
if os.path.exists("colin27.ES.txt"):
    os.remove("colin27.ES.txt")
if os.path.exists("colin27_dirsurf.txt"):
    os.remove("colin27_dirsurf.txt")


## Read VTK mesh

fn = os.path.join(fmpy.datadir, 'Colin27/Colin27.vtk')

R = fmpy.VTKFiles.VTKMeshReader()
R.filename(fn)
R.read()

M = R.mesh()

## create material set

MS = fmpy.Geometry.MaterialSet()

air = fmpy.Geometry.Material()
air.scatteringCoeff(0)
air.absorptionCoeff(0)
air.refractiveIndex(1.0)
air.anisotropy(0.0)

skull = fmpy.Geometry.Material()
skull.scatteringCoeff(7.80)
skull.absorptionCoeff(0.019)
skull.anisotropy(0.89)
skull.refractiveIndex(1.37)

csf = fmpy.Geometry.Material()
csf.scatteringCoeff(0.009)
csf.absorptionCoeff(0.004)
csf.anisotropy(0.89)
csf.refractiveIndex(1.37)

gray = fmpy.Geometry.Material()
gray.scatteringCoeff(9.00)
gray.absorptionCoeff(0.02)
gray.anisotropy(0.89)
gray.refractiveIndex(1.37)

white = fmpy.Geometry.Material()
white.scatteringCoeff(40.90)
white.absorptionCoeff(0.08)
white.anisotropy(0.89)
white.refractiveIndex(1.37)

MS.exterior(air)
MS.append(skull)
MS.append(csf)
MS.append(gray)
MS.append(white)

# Create source
P = fmpy.Geometry.Point()
P.position((71.2,149.8,127.5))

# Set up kernel

vol = fmpy.Geometry.VolumeCellInRegionPredicate()
vol.setRegion(2)

k = fmpy.SWKernel.TetraInternalKernel()
k.packetCount(100)
k.source(P)
k.geometry(M)
k.materials(MS)
k.directedSurfaceScorer().addScoringRegionBoundary(vol)

isRelease = (fmpy.buildtype == "Release")
if isRelease:
    k.packetCount(1000000)
else:
    k.packetCount(100)

# Run and wait until completes
k.runSync()

# Get results OutputDataCollection
ODC = k.results()

# Convert energy absorbed per volume element to volume average fluence
# Use DirectedSurface data instead of SurfaceExitEnergy
EF = fmpy.Queries.EnergyToFluence()
EF.kernel(k)
EF.inputEnergy()
EF.outputFluence()
EF.data(ODC.getByName("VolumeEnergy"))
EF.update()

# Write the surface mesh with fluence appended
# Test if VTKSurfaceWriter can still write out external surfaces
W_vol = fmpy.TextFile.TextFileMatrixWriter()
W_vol.filename("colin27.EV.txt")
W_vol.source(EF.result())
W_vol.write()

if os.path.exists("colin27.EV.txt") == False:
    raise Exception("Mesh file not written for VolumeEnergy.")

EF.data(ODC.getByName("SurfaceExitEnergy"))
EF.update()

# Write the surface mesh with fluence appended
# Test if VTKSurfaceWriter can still write out external surfaces
W_surf = fmpy.TextFile.TextFileMatrixWriter()
W_surf.filename("colin27.ES.txt")
W_surf.source(EF.result())
W_surf.write()

if os.path.exists("colin27.ES.txt") == False:
    raise Exception("Mesh file not written for SurfaceExitEnergy.")

EF.data(ODC.getByType("DirectedSurface"))
EF.update()

# Write the surface mesh with fluence appended
W_dirsurf = fmpy.TextFile.TextFileMatrixWriter()
W_dirsurf.filename("colin27_dirsurf.txt")
W_dirsurf.source(EF.result())
W_dirsurf.write()

if os.path.exists("colin27_dirsurf.txt") == False:
    raise Exception("Mesh file not written for DirectedSurface.")
