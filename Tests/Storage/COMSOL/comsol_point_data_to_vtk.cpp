/*
 * comsol_point_data_to_vtk.cpp
 *
 *  Created on: Apr 27, 2018
 *      Author: jcassidy
 */

#include <FullMonteSW/Storage/COMSOL/COMSOLDataReader.hpp>
#include <FullMonteSW/Storage/COMSOL/COMSOLReader.hpp>

#include <FullMonteSW/Geometry/Points.hpp>
#include <FullMonteSW/Geometry/TetraMesh.hpp>

#include <FullMonteSW/OutputTypes/SpatialMap.hpp>

#include <FullMonteSW/Queries/PointMatcher.hpp>
#include <FullMonteSW/Geometry/Permutation.hpp>

#include <iostream>
#include <iomanip>
#include <array>

using namespace std;

#include <FullMonteSW/VTK/vtkFullMonteTetraMeshWrapper.h>

#include <FullMonteSW/Warnings/Push.hpp>
#include <FullMonteSW/Warnings/VTK.hpp>
#include <vtkFloatArray.h>
#include <vtkPoints.h>
#include <vtkIdTypeArray.h>
#include <vtkCellArray.h>
#include <vtkUnstructuredGrid.h>
#include <vtkPointData.h>
#include <vtkCellData.h>
#include <vtkPolyData.h>
#include <vtkPolyDataWriter.h>
#include <vtkUnstructuredGridWriter.h>
#include <vtkPointDataToCellData.h>
#include <FullMonteSW/Warnings/Pop.hpp>

#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/value_semantic.hpp>


#include <FullMonteSW/Warnings/Push.hpp>
#include <FullMonteSW/Warnings/Boost.hpp>
#include <boost/filesystem.hpp>
#include <FullMonteSW/Warnings/Pop.hpp>

#include <string>
using namespace std;

int main(int argc,char **argv)
{
	boost::program_options::options_description cmdline;
	cmdline.add_options()
			("sparse_point_output",boost::program_options::value<string>(),"List (point ID, fluence) pairs to text file")
			("dense_tetra_output",boost::program_options::value<string>(),"Write a vector of fluence elements to a text file")
			("point_output,p",boost::program_options::value<string>(),"Point cloud output file")
			("surface_output,s",boost::program_options::value<string>(),"Surface output .vtk file")
			("volume_output,v",boost::program_options::value<string>(),"Volume output .vtk file")
			("pointdata,p",boost::program_options::value<string>()->required(),"Point data input file")
			("mesh,m",boost::program_options::value<string>()->required(),"Input mesh file")
			("help,h","Print help and exit");

	boost::program_options::variables_map vm;
	boost::program_options::store(
			boost::program_options::parse_command_line(argc,argv,cmdline),
			vm);
	vm.notify();

	// Check for help option
	if (vm.count("help"))
	{
		cout << cmdline << endl;
		return 0;
	}


	// Get the mesh
	TetraMesh* M=nullptr;

	boost::filesystem::path meshPath = vm["mesh"].as<string>();

	if(meshPath.extension()==".mesh")
		cout << "TIM-OS mesh files aren't supported in this utility" << endl;
	else if (meshPath.extension()==".vtk")
		cout << "VTK mesh files aren't supported in this utility" << endl;
	else if (meshPath.extension()==".mphtxt")
	{
		COMSOLReader		MR;
		MR.filename(meshPath.native());
		MR.read();

		M = MR.mesh();

		if (!MR.tetRegionsReadOK())
		{
			cout << " failed to read tetra regions. terminating." << endl;
			return 1;
		}

	}

	if (!M)
	{
		cout << "No mesh provided" << endl;
		return 1;
	}



	////// Get the data

	boost::filesystem::path dataPath = vm["pointdata"].as<string>();

	COMSOLDataReader 	DR;
	DR.filename(dataPath.native());
	DR.read();
	Points* pts = DR.points();
	SpatialMap<float>* data=DR.data();

	cout << "Read " << pts->size() << " data points" << endl;

	if (data->dim() != pts->size())
	{
		cout << "  Data array size (" << data->dim() << ") doesn't match point array size (" << pts->size() << ")" << endl;
		cout << "  Terminating" << endl;
		return 1;
	}







	////// Match points from data against mesh

	Points* MP = M->points();

	PointMatcher matcher;
	matcher.reference(MP);
	matcher.query(pts);
	matcher.tolerance(1e-5f);
	matcher.update();

	if (matcher.unmatchedCount() > 0)
		cout << "WARNING: There are " << matcher.unmatchedCount() << " points with a match distance that exceeds tolerance" << endl;

	Permutation* match = matcher.result();




	////// Prepare common data elements

	vtkFullMonteTetraMeshWrapper* VTKM = nullptr;

	if (vm.count("surface_output") || vm.count("volume_output"))
	{
		VTKM = vtkFullMonteTetraMeshWrapper::New();

		VTKM->mesh(M);
		// VTKM->update();
	}



	////// VTKify the point cloud

	if (vm.count("point_output"))
	{
		// Points
		vtkPoints* points = vtkPoints::New();
		points->SetNumberOfPoints(pts->size());

		// Fluence data array
		vtkFloatArray* phi = vtkFloatArray::New();
		phi->SetNumberOfTuples(pts->size());

		// IDs for cells
		vtkIdTypeArray* ids = vtkIdTypeArray::New();
		ids->SetNumberOfTuples(2*pts->size());

		for(unsigned i=0;i<pts->size();++i)
		{
			Point<3,double> p = pts->get(i);
			array<double,3> pa = p.as_array_of<double>();
			points->SetPoint(i,pa.data());

			phi->SetTuple1(i,data->get(i));

			ids->SetValue(2*i,1);
			ids->SetValue(2*i+1,i);
		}

		vtkCellArray* ca = vtkCellArray::New();
		ca->SetCells(pts->size(),ids);

		vtkPolyData* pointUG = vtkPolyData::New();
		pointUG->SetPoints(points);
		pointUG->SetVerts(ca);
		pointUG->GetCellData()->SetScalars(phi);

		vtkPolyDataWriter* W = vtkPolyDataWriter::New();
		W->SetFileName(vm["point_output"].as<string>().c_str());
		W->SetInputData(pointUG);
		W->Update();
		W->Delete();
	}

	cout << "Point cloud VTK" << endl;

	////// Create VTK point data for mesh if needed

	vtkFloatArray* pointPhi=nullptr;

	if (vm.count("surface_output") || vm.count("volume_output"))
	{
		pointPhi = vtkFloatArray::New();
		pointPhi->SetName("Point Data");

		pointPhi->SetNumberOfTuples(VTKM->regionMesh()->GetNumberOfPoints());

		for(unsigned i=0;i<VTKM->regionMesh()->GetNumberOfPoints();++i)
			pointPhi->SetValue(i,0.0f);
		for(unsigned i=0;i<match->dim();++i)
		{
			unsigned idx = match->get(i);
			if (idx)
				pointPhi->SetValue(idx,data->get(i));
			else
				cout << "  WARNING: No match for input point " << i << endl;
		}
	}

	cout << "Point data mesh" << endl;

	////// VTKify the matched surface data set into a polydata
	if (vm.count("surface_output"))
	{
		vtkPolyData* pd = VTKM->faces();
		pd->GetPointData()->SetScalars(pointPhi);

		// create cell (triangle) data from point data
		vtkPointDataToCellData* pcd = vtkPointDataToCellData::New();
		pcd->SetInputData(pd);
		pcd->PassPointDataOn();
		pcd->Update();
		pcd->GetOutput()->GetCellData()->GetScalars()->SetName("Converted surface data");

		vtkPolyDataWriter* W = vtkPolyDataWriter::New();
		W->SetFileName(vm["surface_output"].as<string>().c_str());
		W->SetInputData(pcd->GetOutput());
		W->Update();

		pcd->Delete();
		W->Delete();
	}

	cout << "surface data to poly data" << endl;
	////// VTKify into a full mesh with cell data and point data

	if (vm.count("volume_output"))
	{
		vtkUnstructuredGrid* ug = VTKM->regionMesh();
		ug->GetPointData()->SetScalars(pointPhi);

		vtkPointDataToCellData* pcd = vtkPointDataToCellData::New();
		pcd->SetInputData(ug);
		pcd->Update();

		vtkUnstructuredGrid* ugOut = vtkUnstructuredGrid::SafeDownCast(pcd->GetOutput());

		vtkDataArray* phiV = ugOut->GetCellData()->GetScalars();
		phiV->SetName("Converted volume data");
		ug->GetCellData()->AddArray(phiV);

		vtkUnstructuredGridWriter* UGW = vtkUnstructuredGridWriter::New();
		UGW->SetFileName(vm["volume_output"].as<string>().c_str());
		UGW->SetInputData(ug);
		UGW->Update();

		UGW->Delete();
		pcd->Delete();
	}

	cout << "full mesh" << endl;

	////// Sparse text output for points

	if(vm.count("sparse_point_output"))
	{
		ofstream os(vm["sparse_point_output"].as<string>());

		// write header: <# nonzeros>/<array dim>
		os << match->dim() << '/' << MP->size() << endl;

		// copy (point ID, fluence) pairs to vector and sort by ascending tetra ID
		vector<pair<unsigned,float>> v(match->dim());
		for(unsigned i=0;i<match->dim();++i)
			v[i] = make_pair(match->get(i),data->get(i));

		sort(v.begin(),v.end(),[](const pair<unsigned,float>& lhs,const pair<unsigned,float>& rhs){ return lhs.first < rhs.first; });

		// write
		for(const auto e : v)
			os << e.first << ' ' << e.second << endl;
	}

	if (vm.count("dense_tetra_output"))
	{
		ofstream os(vm["dense_tetra_output"].as<string>());

		os << M->tetraCells()->size() << endl;
		os << fixed << setprecision(5);
		for(unsigned i=0; i<M->tetraCells()->size(); ++i)
			os << endl;
	}
}
