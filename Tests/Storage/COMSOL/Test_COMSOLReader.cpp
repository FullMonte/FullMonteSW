/*
 * Test_COMSOLReader.cpp
 *
 *  Created on: Oct 16, 2017
 *      Author: jcassidy
 */

#include <boost/test/unit_test.hpp>

#include <FullMonteSW/Storage/COMSOL/COMSOLReader.hpp>
#include <FullMonteSW/Storage/COMSOL/COMSOLWriter.hpp>
#include <FullMonteSW/Config.h>

BOOST_AUTO_TEST_CASE(HNC)
{
	COMSOLReader R;

	R.filename(FULLMONTE_SOURCE_DIR "/Tests/Storage/COMSOL/HeadAndNeckModel.mphtxt");
	R.read();

	R.vtkExport("tets.vtk");

	COMSOLWriter W;
	W.filename("HeadAndNeckModel_COMSOLWriter.mphtxt");
	W.mesh(R.mesh());
	W.write();

	R.filename("HeadAndNeckModel_COMSOLWriter.mphtxt");
	R.read();

	R.vtkExport("tets_COMSOLWriter.vtk");
}

BOOST_AUTO_TEST_CASE(Colin27_Write)
{
	//COMSOLWriter W;
}
