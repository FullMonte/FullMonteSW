/*
 * Test_CommentFilter.cpp
 *
 *  Created on: Oct 10, 2017
 *      Author: jcassidy
 */

#include <boost/test/unit_test.hpp>

#include <FullMonteSW/Warnings/Push.hpp>
#include <FullMonteSW/Warnings/Boost.hpp>
#include <boost/iostreams/device/file.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <FullMonteSW/Warnings/Pop.hpp>

#include <FullMonteSW/Storage/Common/CommentFilter.hpp>
#include <FullMonteSW/Storage/Common/FlattenWhitespace.hpp>

#include <iostream>

using namespace std;

BOOST_AUTO_TEST_CASE(readMesh)
{
	boost::iostreams::basic_file_source<char> is("HeadAndNeckModel.mphtxt");

	single_delim_comment_filter CF;
	flatten_whitespace fw;

	boost::iostreams::filtering_stream<boost::iostreams::input,char> f;

	f.push(fw,4);
	f.push(CF);
	f.push(is);

	ofstream os("out.txt");

	while(!f.eof())
	{
		string s;
		getline(f,s);
		os << s << endl;
	}
}

