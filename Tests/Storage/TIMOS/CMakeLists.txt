ADD_BOOST_UNIT_TEST(TIMOSReaderWriter Test.cpp)
TARGET_LINK_LIBRARIES(TIMOSReaderWriter 
	FullMonteGeometry 
	FullMonteTIMOS 
	FullMonteLogging
)

#ADD_TEST(TIMOSReaderWriter
#	COMMAND ${CMAKE_BINARY_DIR}/bin/TIMOSReaderWriter
#	COMMAND diff ${CMAKE_CURRENT_BINARY_DIR}/TIMOSMeshWriter.mesh ${CMAKE_CURRENT_SOURCE_DIR}/TIMOSMeshWriter.expected.mesh)

ADD_BOOST_UNIT_TEST(Test_TIMOSSource Test_TIMOSSource.cpp)
TARGET_LINK_LIBRARIES(Test_TIMOSSource FullMonteGeometry FullMonteTIMOS)