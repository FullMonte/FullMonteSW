/*
 * Test_GenericGeometryReader.cpp
 *
 *  Created on: Nov 1, 2017
 *      Author: jcassidy
 */

#include <boost/test/unit_test.hpp>
#include <FullMonteSW/Storage/GenericGeometryReader.hpp>

BOOST_AUTO_TEST_CASE(reader9)
{
	GenericGeometryReader R;

	R.filename("/blah/blah/bloop/digimouse.mesh");
	R.read();

	R.filename("/a/b/c/d/sample.mci");
	R.read();

	R.filename("bleep.vtk.xz");
	R.read();
}
