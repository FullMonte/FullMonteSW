/*
 * Test_MMCJSONReader.cpp
 *
 *  Created on: Nov 21, 2017
 *      Author: jcassidy
 */

#include <boost/test/unit_test.hpp>
#include <FullMonteSW/Storage/MMC/MMCJSONReader.hpp>
#include <FullMonteSW/Config.h>

using namespace std;

BOOST_AUTO_TEST_CASE(readOneCube)
{
	MMCJSONReader R;
	R.filename(FULLMONTE_SOURCE_DIR "/Tests/Storage/MMC/TestData/onecube.expected.json");
	R.read();
}
