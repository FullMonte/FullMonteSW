/*
 * Test_MMCMeshWriter.cpp
 *
 *  Created on: Sep 13, 2017
 *      Author: jcassidy
 */

#include <boost/test/unit_test.hpp>
#include <FullMonteSW/Storage/TIMOS/TIMOSMeshReader.hpp>
#include <FullMonteSW/Storage/MMC/MMCMeshWriter.hpp>
#include <FullMonteSW/Config.h>

BOOST_AUTO_TEST_CASE(onecube)
{
	TIMOSMeshReader R;
	R.filename(FULLMONTE_SOURCE_DIR "/Tests/Storage/MMC/onecube.mesh");
	R.read();

	TetraMesh* M = R.mesh();

	BOOST_REQUIRE(M);

	MMCMeshWriter W;
	W.filename("onecube.mmc.dat");
	W.mesh(M);
	W.write();
}
