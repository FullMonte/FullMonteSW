## diff_mmc_mesh_text.sh
#
# Compares MMC mesh files for identical text
#
# diff_mmc_mesh_text.sh /path/to/reference a /path/to/test b
#
# This will compare /path/to/reference/{elem,node,velem,facenb}_a with /path/to/test/{...}_b
# Returns nonzero if any fails
#
# NOTE: This is a text diff so not very clever (ie. 1.0 != 1.00)

LPATH=$1
LPFX=$2
RPATH=$3
RPFX=$4

diff $LPATH/elem_$LPFX $RPATH/elem_$RPFX &&
    diff $LPATH/node_$LPFX $RPATH/node_$RPFX &&
    diff $LPATH/velem_$LPFX $RPATH/velem_$RPFX &&
    diff $LPATH/facenb_$LPFX $RPATH/facenb_$RPFX
