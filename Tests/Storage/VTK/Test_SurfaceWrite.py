import os
import fmpy

## Delete files if exist
if os.path.exists("colin27_surf.out.vtk"):
    os.remove("colin27_surf.out.vtk")
if os.path.exists("colin27_dirsurf.out.vtk"):
    os.remove("colin27_dirsurf.out.vtk")

## Read VTK mesh

fn = os.path.join(fmpy.datadir, 'Colin27/Colin27.vtk')

R = fmpy.VTKFiles.VTKMeshReader()
R.filename(fn)
R.read()

M = R.mesh()

## create material set

MS = fmpy.Geometry.MaterialSet()

air = fmpy.Geometry.Material()
air.scatteringCoeff(0)
air.absorptionCoeff(0)
air.refractiveIndex(1.0)
air.anisotropy(0.0)

skull = fmpy.Geometry.Material()
skull.scatteringCoeff(7.80)
skull.absorptionCoeff(0.019)
skull.anisotropy(0.89)
skull.refractiveIndex(1.37)

csf = fmpy.Geometry.Material()
csf.scatteringCoeff(0.009)
csf.absorptionCoeff(0.004)
csf.anisotropy(0.89)
csf.refractiveIndex(1.37)

gray = fmpy.Geometry.Material()
gray.scatteringCoeff(9.00)
gray.absorptionCoeff(0.02)
gray.anisotropy(0.89)
gray.refractiveIndex(1.37)

white = fmpy.Geometry.Material()
white.scatteringCoeff(40.90)
white.absorptionCoeff(0.08)
white.anisotropy(0.89)
white.refractiveIndex(1.37)

MS.exterior(air)
MS.append(skull)
MS.append(csf)
MS.append(gray)
MS.append(white)

# Create source
P = fmpy.Geometry.Point()
P.position((71.2,149.8,127.5))

# Set up kernel

vol = fmpy.Geometry.VolumeCellInRegionPredicate()
vol.setRegion(2)

k = fmpy.SWKernel.TetraInternalKernel()
k.packetCount(100)
k.source(P)
k.geometry(M)
k.materials(MS)
k.directedSurfaceScorer().addScoringRegionBoundary(vol)

isRelease = (fmpy.buildtype == "Release")
if isRelease:
    k.packetCount(1000000)
else:
    k.packetCount(100)

# Run and wait until completes
k.runSync()

# Get results OutputDataCollection
ODC = k.results()

# Convert energy absorbed per volume element to volume average fluence
# Use SurfaceExitEnergy
EF = fmpy.Queries.EnergyToFluence()
EF.kernel(k)
EF.data(ODC.getByName("SurfaceExitEnergy"))
EF.inputEnergy()
EF.outputFluence()
EF.update()

# Write the surface mesh with fluence appended
# Test if VTKSurfaceWriter can still write out external surfaces
W_surf = fmpy.VTKFiles.VTKSurfaceWriter()
W_surf.filename("colin27_surf.out.vtk")
W_surf.addData("Fluence", EF.result())
W_surf.mesh(M)
W_surf.write()

if os.path.exists("colin27_surf.out.vtk") == False:
    raise Exception("Mesh file not written for SurfaceExitEnergy.")

# Convert energy absorbed per volume element to volume average fluence
# Use DirectedSurface data instead of SurfaceExitEnergy
EF = fmpy.Queries.EnergyToFluence()
EF.kernel(k)
EF.data(ODC.getByType("DirectedSurface"))
EF.inputEnergy()
EF.outputFluence()
EF.update()

# Write the surface mesh with fluence appended
W_dirsurf = fmpy.VTKFiles.VTKSurfaceWriter()
W_dirsurf.filename("colin27_dirsurf.out.vtk")
W_dirsurf.addData("Fluence", EF.result())
W_dirsurf.mesh(M)
W_dirsurf.write()

if os.path.exists("colin27_dirsurf.out.vtk") == False:
    raise Exception("Mesh file not written for DirectedSurface.")
