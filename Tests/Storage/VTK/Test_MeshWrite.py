import os
import fmpy

## Delete files if exist
if os.path.exists("colin27.test.vtk"):
    os.remove("colin27.test.vtk")

R = fmpy.TIMOS.TIMOSMeshReader()
meshpath = os.path.join(fmpy.datadir, 'Colin27/Colin27.mesh')
R.filename(meshpath)
R.read()

M = R.mesh()

W = fmpy.VTKFiles.VTKMeshWriter()
W.mesh(M)
W.filename("colin27.test.vtk")
W.write()

if os.path.exists("colin27.test.vtk") == False:
    raise Exception("Mesh file not written due to error in VTKMeshwriter")
