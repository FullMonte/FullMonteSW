ADD_BOOST_UNIT_TEST(Test_Layered Test_Layered.cpp)
TARGET_LINK_LIBRARIES(Test_Layered FullMonteGeometry) 
ADD_TEST(NAME Test_Layered_Basic 
	COMMAND Test_Layered)

ADD_BOOST_UNIT_TEST(Test_Unit_Spec Test_Unit_Spec.cpp)
TARGET_LINK_LIBRARIES(Test_Unit_Spec FullMonteGeometryReader FullMonteVTKFiles FullMonteGeometryPredicates FullMonteSWKernel FullMonteGeometry FullMonteKernelBase)
ADD_TEST(NAME reg_unit_spec COMMAND Test_Unit_Spec)

MESSAGE("${Blue}Entering subdirectory Tests/Geometry/Modifers${ColourReset}")
ADD_SUBDIRECTORY(Modifiers)
MESSAGE("${Blue}Entering subdirectory Tests/Geometry/Placement${ColourReset}")
ADD_SUBDIRECTORY(Placement)
MESSAGE("${Blue}Entering subdirectory Tests/Geometry/Predicates${ColourReset}")
ADD_SUBDIRECTORY(Predicates)
MESSAGE("${Blue}Entering subdirectory Tests/Geometry/Queries${ColourReset}")
ADD_SUBDIRECTORY(Queries)
MESSAGE("${Blue}Entering subdirectory Tests/Geometry/Sources${ColourReset}")
ADD_SUBDIRECTORY(Sources)

