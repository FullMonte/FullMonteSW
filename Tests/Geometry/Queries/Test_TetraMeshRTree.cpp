#include <FullMonteSW/Config.h>

#include <FullMonteSW/Logging/FullMonteLogger.hpp>

#include <FullMonteSW/Geometry/TetraMesh.hpp>
#include <FullMonteSW/Geometry/Queries/TetraEnclosingPointByLinearSearch.hpp>

#include <FullMonteSW/Warnings/Push.hpp>
#include <FullMonteSW/Warnings/Boost.hpp>
#include <FullMonteSW/Warnings/Pop.hpp>

#include <FullMonteSW/Storage/TIMOS/TIMOSMeshReader.hpp>

#include <boost/range/adaptor/indexed.hpp>
#include <boost/test/unit_test.hpp>
#include <boost/test/unit_test.hpp>
#include <boost/test/unit_test_log.hpp>

#include <ctime>
#include <chrono>
#include <iostream>
#include <random>

/**
 * This test both checks the validity of the TetraMeshRTree query and benchmarks its performance against 
 * the linear lookup using TetraEnclosingPointByLinearSearch (actually, it validates both the linear 
 * search and RTree search)
 *
 * The general idea is this:
 *  1)  load the mesh and randomly pick POINTS_TO_TEST tetrahedrons
 *  2)  get the centroid of that tetrahedron and store the {tetra ID, centroid} pair
 *  3)  for each {tetra id, centroid} pair, use a linear search to find the tetrahedron containing 
 *      the centroid check that the found tetra equals the original
 *  4)  do the same as (3) but using the RTree search
 *
 *  In steps (3) and (4) we time each lookup and compute the average lookup for linear rtree search
 */

using namespace std;

BOOST_AUTO_TEST_CASE(TetraMeshRTreeValidityAndPerformance)
{
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //// Setup test
    // read in the mesh (HeadNeck)
    string meshfn(FULLMONTE_DATA_DIR "/TIM-OS/cube_5med/cube_5med.mesh");

    TIMOSMeshReader R;
    R.filename(meshfn);
    R.read();
    // get the tetra mesh
    const TetraMesh* M = R.mesh();

    BOOST_TEST_MESSAGE("Mesh loaded");

    // number of tetras in mesh
    const unsigned int Nt = get(num_tetras,*M);

    // number of points to test
    const unsigned int POINTS_TO_TEST=200;

    // tracking data
    vector<unsigned int> tetraIDs(POINTS_TO_TEST);
    vector<std::array<float,3>> tetraCentroids(POINTS_TO_TEST);

    // random number generator stuff for generating random numbers from [1,Nt)
    std::mt19937 rng(777);
    std::uniform_int_distribution<> dist(1, Nt);

    // first, randomly select POINTS_TO_TEST tetrahedrons and use their centroids as the points we will search for in the mesh
    for(unsigned int i = 0; i < POINTS_TO_TEST; i++) {
        // grab a random tetra ID
        unsigned int IDt = dist(rng);
        BOOST_REQUIRE(IDt >= 1 && IDt < Nt);

        // grab this tetra IDs centroid
        Point<3,double> C = get(centroid, *M, TetraMesh::TetraDescriptor(IDt));

        // track this data
        tetraIDs[i] = IDt;
        tetraCentroids[i] = std::array<float,3>{ (float)C[0], (float)C[1], (float)C[2] };
    }

	BOOST_TEST_MESSAGE("Random points generated");

    // timing data (gathered below)
    vector<std::chrono::duration<double,std::milli>> linearTimes(POINTS_TO_TEST);
    vector<std::chrono::duration<double,std::milli>> rtreeTimes(POINTS_TO_TEST);
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //// Linear
    // First use the linear search method to find each tetra
    TetraEnclosingPointByLinearSearch L;
    L.mesh(M);

    auto linear_start = std::chrono::high_resolution_clock::now();
    for(unsigned int i = 0; i < POINTS_TO_TEST; i++) {
        // update the search point and perform the linear search (time it)
        auto start = std::chrono::high_resolution_clock::now();
        L.point(Point<3,double>(tetraCentroids[i]));
        L.update();
        auto end = std::chrono::high_resolution_clock::now();
        
        // track how long it took
        linearTimes[i] = std::chrono::duration_cast<std::chrono::milliseconds>(end-start);
        
        // check the result
        const unsigned int IDt = L.tetraID();
        BOOST_REQUIRE_EQUAL(IDt, tetraIDs[i]);
    }
    auto linear_end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double,std::milli> linear_diff = std::chrono::duration_cast<std::chrono::milliseconds>(linear_end-linear_start);
    
	BOOST_TEST_MESSAGE("Located all points with linear search");
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
   
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //// RTree
    // logging for RTree
    logAddFilter("WARNING");
    
    // Now use the RTree method to do the same thing
    TetraMesh::TetraMeshRTree rtree;
    rtree.mesh(M);
    
    auto create_start = std::chrono::high_resolution_clock::now();
    rtree.create();
    auto create_end = std::chrono::high_resolution_clock::now();

    std::chrono::duration<double,std::milli> create_diff = std::chrono::duration_cast<std::chrono::milliseconds>(create_end-create_start);
    cout << "rtree took " << create_diff.count() << "ms to create\n";

    auto rtree_start = std::chrono::high_resolution_clock::now();
    for(unsigned int i = 0; i < POINTS_TO_TEST; i++) {
        // update the search point and perform the search with an RTree (time it)
        auto start = std::chrono::high_resolution_clock::now();
        const unsigned int IDt = rtree.search(tetraCentroids[i]).value();
        auto end = std::chrono::high_resolution_clock::now();
        
        // track how long it took
        rtreeTimes[i] = std::chrono::duration_cast<std::chrono::milliseconds>(end-start);

        // check the result
        BOOST_REQUIRE_EQUAL(IDt, tetraIDs[i]);
    }
    auto rtree_end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double,std::milli> rtree_diff = std::chrono::duration_cast<std::chrono::milliseconds>(rtree_end-rtree_start);
	
    BOOST_TEST_MESSAGE("Located all points with rtree search");
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //// analyzing timing data
    // compute average lookup times
    std::chrono::duration<double,std::milli> linearAvg = linearTimes[0];
    std::chrono::duration<double,std::milli> rtreeAvg = linearTimes[0];

    for(unsigned int i = 1; i < POINTS_TO_TEST; i++) {
        linearAvg += linearTimes[i];
        rtreeAvg += rtreeTimes[i];
    }

    linearAvg /= (double)(POINTS_TO_TEST);
    rtreeAvg /= (double)(POINTS_TO_TEST);

    cout << "\n";
    cout << "======================================================\n";
    cout << "Located points: " << POINTS_TO_TEST << ", Tetras: " << Nt << "\n";
    cout << "Total lookup time:\n";
    cout << "\tlinear: " << linear_diff.count() << " ms\n";
    cout << "\trtree: " << rtree_diff.count() << " ms\n";
    cout << "Average lookup time:\n";
    cout << "\tlinear: " << linearAvg.count() << " ms\n";
    cout << "\trtree: " << rtreeAvg.count() << " ms\n";
    cout << "======================================================\n";
    cout << "\n";
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
