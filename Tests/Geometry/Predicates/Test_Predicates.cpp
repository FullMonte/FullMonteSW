/*
 * Test_Predicates.cpp
 *
 *  Created on: May 17, 2018
 *      Author: jcassidy
 */

#include <boost/test/unit_test.hpp>
#include <boost/test/test_tools.hpp>

#include <FullMonteSW/Geometry/TetraMesh.hpp>
#include <FullMonteSW/Geometry/Partition.hpp>
#include <FullMonteSW/Geometry/Permutation.hpp>
#include <FullMonteSW/Geometry/FaceLinks.hpp>

#include <FullMonteSW/Geometry/Predicates/ArrayPredicateEvaluator.hpp>

#include <FullMonteSW/Geometry/Predicates/VolumeCellInRegionPredicate.hpp>
#include <FullMonteSW/Geometry/Predicates/SurfaceOfRegionPredicate.hpp>

BOOST_AUTO_TEST_CASE(RegionPredicate)
{
	Partition* P = new Partition();
	P->resize(5);

	P->assign(0,0);
	P->assign(1,11);
	P->assign(2,12);
	P->assign(3,11);
	P->assign(4,14);

	Geometry* G = new TetraMesh();
	G->regions(P);

	VolumeCellInRegionPredicate RP;
	RP.setRegion(11);

	ArrayPredicateEvaluator* eval = RP.bind(G);

	// check individual tests
	BOOST_CHECK( !(*eval)(0) );
	BOOST_CHECK(  (*eval)(1) );
	BOOST_CHECK( !(*eval)(2) );
	BOOST_CHECK(  (*eval)(3) );
	BOOST_CHECK( !(*eval)(4) );
	BOOST_CHECK_THROW( (*eval)(5), std::out_of_range);

	Permutation* Psubset = eval->matchingElements();

	BOOST_CHECK_EQUAL(Psubset->dim(), 2U);
	BOOST_CHECK_EQUAL(Psubset->get(0), 1U);
	BOOST_CHECK_EQUAL(Psubset->get(1), 3U);
}

#include <FullMonteSW/Storage/TIMOS/TIMOSMeshReader.hpp>

BOOST_AUTO_TEST_CASE(mouse_brain)
{
	const unsigned brain=2;
	const unsigned N=2836;

	TIMOSMeshReader R;
	R.filename(FULLMONTE_SOURCE_DIR "/data/TIM-OS/mouse/mouse.mesh");
	R.read();

	TetraMesh* M = R.mesh();

	VolumeCellInRegionPredicate RP;
	RP.setRegion(brain);

	SurfaceOfRegionPredicate surf;
	surf.setRegionPredicate(&RP);

	ArrayPredicateEvaluator* eval = surf.bind(M);

	Permutation* Pbrainsurf = eval->matchingElements();

	// Output permutation should record the size of the set it's permuting from (ie. all faces)
	BOOST_CHECK_EQUAL(Pbrainsurf->sourceSize(), M->faceTetraLinks()->size());

	// check size, first, and last element of permutation, as well as max size
	BOOST_CHECK_EQUAL(Pbrainsurf->dim(), 	N);
	BOOST_CHECK_EQUAL(Pbrainsurf->get(0), 	2085U);
	BOOST_CHECK_EQUAL(Pbrainsurf->get(N-1), 	585741U);
	BOOST_CHECK_THROW(Pbrainsurf->get(N), std::out_of_range);
}
