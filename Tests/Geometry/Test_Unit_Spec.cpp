/*
 * Test_Unit_Spec.cpp
 *
 *  Created on: June 20, 2021
 *      Author: Shuran Wang
 */

#include <boost/test/unit_test.hpp>

#include <FullMonteSW/Config.h>
#include <FullMonteSW/Geometry/Geometry.hpp>
#include <FullMonteSW/Geometry/Material.hpp>
#include <FullMonteSW/Geometry/MaterialSet.hpp>
#include <FullMonteSW/Geometry/TetraMesh.hpp>
#include <FullMonteSW/Geometry/Sources/Point.hpp>
#include <FullMonteSW/Geometry/Predicates/VolumeCellInRegionPredicate.hpp>
#include <FullMonteSW/Kernels/Software/TetraInternalKernel.hpp>
#include <FullMonteSW/Storage/VTK/VTKMeshReader.hpp>

#include <string>
#include <stdexcept>

using namespace std;

BOOST_AUTO_TEST_CASE(Test_Unit_Spec) {

    // Read VTK mesh
    string filename(FULLMONTE_DATA_DIR);
    filename.append("/Colin27/Colin27.vtk");

    VTKMeshReader R;
    R.filename(filename);
    R.read();
    TetraMesh* M = R.mesh();

    // Create material set
    MaterialSet MS;

    Material air(0, 0, 0.0, 1.0);
    Material skull(0.019, 7.80, 0.89, 1.37);
    Material csf(0.004, 0.009, 0.89, 1.37);
    Material gray(0.02, 9.00, 0.89, 1.37);
    Material white(0.08, 40.90, 0.89, 1.37);

    // Set units of mesh and material
    M->unitDimension("mm");

    air.unitDimension("mm");
    skull.unitDimension("mm");
    csf.unitDimension("mm");
    gray.unitDimension("mm");
    white.unitDimension("mm");

    MS.exterior(&air);
    MS.append(&skull);
    MS.append(&csf);
    MS.append(&gray);
    MS.append(&white);

    // Create source	
    Source::Point P;
	P.position(array<float,3>{{ 71.2f, 149.8f, 127.5f }});

    // Set up kernel
    VolumeCellInRegionPredicate vol;
    vol.setRegion(2);

    TetraInternalKernel k;
    k.packetCount(100);
    k.source(&P);
    k.geometry(M);
    k.materials(&MS);
    k.directedSurfaceScorer().addScoringRegionBoundary(&vol);

    // Kernel energy or power not specified
    BOOST_CHECK_THROW(k.runSync(), logic_error);

    k.isEnergy_or_Power("w");

    // Should be able to run with no error
    BOOST_CHECK_NO_THROW(k.runSync());

    k.isEnergy_or_Power("j");

    // Should be able to run with no error
    BOOST_CHECK_NO_THROW(k.runSync());

    // Adding unit inconsistency to mesh
    M->unitDimension("m");
    k.geometry(M);

    BOOST_CHECK_THROW(k.runSync(), logic_error);
    
    // Adding unit inconsistency to material set
    M->unitDimension("mm");
    gray.unitDimension("m");
    MaterialSet newMS;

    newMS.exterior(&air);
    newMS.append(&skull);
    newMS.append(&csf);
    newMS.append(&gray);
    newMS.append(&white);
    k.geometry(M);
    k.materials(&newMS);

    BOOST_CHECK_THROW(k.runSync(), logic_error);

    // Check other units
    M->unitDimension("cm");
    MaterialSet cmMS;

    air.unitDimension("cm");
    skull.unitDimension("cm");
    csf.unitDimension("cm");
    gray.unitDimension("cm");
    white.unitDimension("cm");

    cmMS.exterior(&air);
    cmMS.append(&skull);
    cmMS.append(&csf);
    cmMS.append(&gray);
    cmMS.append(&white);
    k.geometry(M);
    k.materials(&cmMS);

    BOOST_CHECK_NO_THROW(k.runSync());

    M->unitDimension("m");
    MaterialSet mMS;

    air.unitDimension("m");
    skull.unitDimension("m");
    csf.unitDimension("m");
    gray.unitDimension("m");
    white.unitDimension("m");

    mMS.exterior(&air);
    mMS.append(&skull);
    mMS.append(&csf);
    mMS.append(&gray);
    mMS.append(&white);
    k.geometry(M);
    k.materials(&mMS);

    BOOST_CHECK_NO_THROW(k.runSync());
}
