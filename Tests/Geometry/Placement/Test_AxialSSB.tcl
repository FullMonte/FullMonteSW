package require FullMonte

file delete "cube1.test.txt"

FullMonte::close_window_if_present

VTKMeshReader R
    R filename "$FullMonte::datadir/TIM-OS/cube_1med/cube_1med.vtk"
    R read

set M [R mesh]

## create material set

MaterialSet MS

Material external 
    external scatteringCoeff 0
    external absorptionCoeff 0
    external refractiveIndex 1.0
    external anisotropy      0

Material internal
    internal scatteringCoeff 0
    internal absorptionCoeff 0.1
    internal refractiveIndex 1.0
    internal anisotropy      0

MS exterior external
MS append internal

# set the region in the mesh which you want to emit light
VolumeCellInRegionPredicate vol
vol setRegion 0

# setup the SurfaceSourceBuilder
AxialSurfaceSourceBuilder SSB
# the mesh geometry
SSB mesh $M
# the region of the mesh to use (VolumeCellInRegionPredicate)
SSB setRegion vol
# the TOTAL power of the composite source (each TetraFace power is proportional to its surface area) - must be >1
SSB power 1.0
# if true, emits in the unit hemisphere centered around the TetraFace normal
# if false, always emit with the same direction as the TetraFace normal
SSB emitHemiSphere false
# if emitHemiSphere is true, select the theta angle distribution to use (currently: UNIFORM, CUSTOM or LAMBERT)
SSB hemiSphereEmitDistribution "LAMBERT"
# if hemiSphereEmitDistribution is set to CUSTOM, you have to define the numerical aperture of the source
SSB numericalAperture 0.6
# if checkDirection is 1 (true), enable checking for emission direction of the source, defaulted to false
# emitDirection sets the vector representing the direction photons will be emitted
#SSB checkDirection 1
#SSB emitDirection "0 1 0"
# set irradiance
SSB endpoint 0 "0 0 0"
SSB endpoint 1 "0 3 0"
SSB emitProfile "$FullMonte::sourcedir/Tests/Geometry/Placement/AxialSSB_profile.txt"
# update the output composite source
SSB update

# the output of the SurfaceSourceBuilder is a Composite source you can use in your kernel
set C [SSB output]

TetraInternalKernel k
    k packetCount 1000000
    k source $C
    k geometry $M
    k materials MS
   [k directedSurfaceScorer] addScoringRegionBoundary vol

# Run and wait until completes
    k runSync

# Get results OutputDataCollection
set ODC [k results]

# Convert energy absorbed per volume element to volume average fluence
# Use SurfaceExitEnergy
EnergyToFluence EF
    EF kernel k
    EF data [$ODC getByName "VolumeEnergy"]
    EF inputEnergy
    EF outputFluence
    EF update

set f [EF result]

#VTKMeshWriter W
#    W filename "/sims/axialssb_out.vtk"
#    W addData "Fluence" [EF result]
#    W mesh $M
#    W write

TextFileMatrixWriter TW
    TW filename "cube1.test.txt"
    TW source [EF result]
    TW write

set GoldTest [open "$FullMonte::sourcedir/Tests/Geometry/Placement/gold_AxialSSB_test_Fluence.txt" r]
set RegTest [open "cube1.test.txt" r]
set GoldTestData [read $GoldTest]
set RegTestData [read $RegTest]
close $GoldTest
close $RegTest
set GoldData [split $GoldTestData " \n"]
set RegData [split $RegTestData " \n"]
set column 0
set DirErr 0
set GoldDir 0
for { set i 10 } { $i < [expr [llength $GoldData] -1] } { incr i } {
    if {$column == 2} {
        set DirErr [expr $DirErr + [expr abs([lindex $GoldData $i] - [lindex $RegData $i])]]
        set GoldDir [expr $GoldDir + [lindex $GoldData $i]]
        set column 0
    } else {
        incr column
    }
}
set L1Dir [expr $DirErr/$GoldDir]
puts [concat "Fluence - Normalized L1-Norm of errors: " $L1Dir]
if {$L1Dir < 0.02} {
    puts "SSB Fluence results are within the expected statistical range of the golden sample!"
} else {
    error "Absolute sum of errors for the SSB Fluence results are higher than expected!"
}

