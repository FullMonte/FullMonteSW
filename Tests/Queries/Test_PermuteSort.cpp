/*
 * Test_PermuteSort.cpp
 *
 *  Created on: Sep 11, 2017
 *      Author: jcassidy
 */

#include <boost/test/unit_test.hpp>
#include <boost/test/test_tools.hpp>

#include <FullMonteSW/Geometry/Permutation.hpp>
#include <FullMonteSW/Queries/Permute.hpp>
#include <FullMonteSW/Queries/Sort.hpp>
#include <FullMonteSW/OutputTypes/SpatialMap.hpp>

#include <cmath>
#include <limits>

using namespace std;

/** NOTE: std::sort can't handle NaN
 *
 */

BOOST_AUTO_TEST_CASE(SortPermute)
{
	vector<float> i {
		-1.0,										// 0
		std::numeric_limits<float>::quiet_NaN(),
		-std::numeric_limits<float>::infinity(),	// 2
		10.0,
		-3.0,										// 4
		11.0,
		4.0,										// 6
		5.2,
		std::numeric_limits<float>::quiet_NaN(),	// 8
		std::numeric_limits<float>::infinity() };

	vector<float> o_expect { -std::numeric_limits<float>::infinity(), -3.0, -1.0, 4.0, 5.2, 10.0, 11.0, std::numeric_limits<float>::infinity(),
		std::numeric_limits<float>::quiet_NaN(),std::numeric_limits<float>::quiet_NaN()
		};

	vector<unsigned> perm_expect { 2,4,0,6,7,3,5,9,1,8 };

	SpatialMap<float>* f = new SpatialMap<float>(i,AbstractSpatialMap::UnknownSpaceType,AbstractSpatialMap::Scalar);

	// set up pipeline
	Sort* S = new Sort();
	S->input(f);
	S->setAscending();
	S->excludeNaNs(false);
	S->update();

	// Check permutation against expected
	Permutation* Sp = S->output();

	vector<unsigned> perm_test;
	perm_test.resize(Sp->dim());
	for(unsigned j=0;j<Sp->dim();++j)
		perm_test[j]=Sp->get(j);

	BOOST_REQUIRE_EQUAL(Sp->dim(), i.size());

	BOOST_REQUIRE(perm_test.size() == perm_expect.size());

	//// Drop the two NaNs at the end!!
	BOOST_CHECK_EQUAL_COLLECTIONS(perm_test.begin(),perm_test.end(),perm_expect.begin(),perm_expect.end());

	// Apply the permutation and check output vector
	Permute* P = new Permute();
	P->input(f);
	P->permutation(Sp);

	BOOST_TEST_CHECKPOINT("preupdate");
	P->update();
	BOOST_TEST_CHECKPOINT("postupdate");

	OutputData* d = P->output();
	SpatialMap<float>* df=nullptr;

	BOOST_REQUIRE( (df = dynamic_cast<SpatialMap<float>*>(d)));

	vector<float> o_test(df->dim());
	for(unsigned j=0;j<o_test.size();++j)
		o_test[j] = df->get(j);

	BOOST_CHECK_EQUAL(df->dim(), Sp->dim());
	BOOST_CHECK_EQUAL_COLLECTIONS(o_test.begin(),o_test.end()-2,o_expect.begin(),o_expect.end()-2);

	BOOST_CHECK(isnan(*(o_test.end()-2)));
	BOOST_CHECK(isnan(*(o_test.end()-1)));
}

BOOST_AUTO_TEST_CASE(PermuteInverse)
{
	Permute forward;
	Permute inverse;

	// 0, 5 not sent to output
	// 2 duplicated
	Permutation P;
	P.resize(6);
	P.set(0,4);
	P.set(1,3);
	P.set(2,2);
	P.set(3,2);
	P.set(4,1);
	P.set(5,6);
	P.sourceSize(7);

	vector<unsigned> in{{100,110,120,130,140,150,160}};
	vector<unsigned> expect{{140,130,120,120,110,160}};
	vector<bool> present(in.size(),false);

	// copy input data
	SpatialMap<float> v;
	v.dim(in.size());
	for(unsigned i=0;i<v.dim();++i)
		v.set(i,in[i]);

	// do the forward permutation
	forward.permutation(&P);
	forward.input(&v);
	forward.update();
	SpatialMap<float>* fo = static_cast<SpatialMap<float>*>(forward.output());

	BOOST_REQUIRE(fo);


	// check forward permutation against expected
	for(unsigned i=0;i<P.dim();++i)
		present[P.get(i)]=true;

	BOOST_REQUIRE_EQUAL(fo->dim(), expect.size());
	for(unsigned i=0;i<expect.size();++i)
		BOOST_CHECK(fo->get(i) == expect[i]);

	// run the inverse permutation on the forward output

	inverse.permutation(&P);
	inverse.input(fo);
	inverse.inverseMode(true);
	inverse.update();
	SpatialMap<float>* io = static_cast<SpatialMap<float>*>(inverse.output());

	BOOST_REQUIRE_EQUAL(io->dim(), in.size());

	// check that inverse matches input where it should (data dropped in the forward pass can't be recovered)
	for(unsigned i=0;i<expect.size();++i)
	{
		BOOST_CHECK(!present[i] || io->get(i)==in[i]);
		//cout << "[" << i << "] io=" << io->get(i) << " in=" << in[i] << endl;
	}
}
