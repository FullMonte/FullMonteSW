/*
 * Test_AccumulationProbes.cpp
 *
 *  Created on: Sep 9, 2020
 *      Author: fynns
 */

#include <boost/test/unit_test.hpp>

#include <FullMonteSW/Geometry/TetraMesh.hpp>
#include <FullMonteSW/Geometry/Layered.hpp>

#include <FullMonteSW/OutputTypes/OutputDataType.hpp>
#include <FullMonteSW/OutputTypes/OutputData.hpp>
#include <FullMonteSW/OutputTypes/SpatialMap.hpp>

#include <FullMonteSW/Queries/VolumeAccumulationProbe.hpp>
#include <FullMonteSW/Queries/SurfaceAccumulationProbe.hpp>
#include <FullMonteSW/Queries/LayeredAccumulationProbe.hpp>

#include <FullMonteSW/Geometry/Points.hpp>
#include <FullMonteSW/Geometry/Partition.hpp>
#include <FullMonteSW/Geometry/FaceLinks.hpp>
#include <FullMonteSW/Geometry/PTTetraMeshBuilder.hpp>

#include <iomanip>
#include <iostream>
#include <fstream>
#include <vector>
using namespace std;

TetraMesh* 		fixture_geom=nullptr;


struct Accumulation_Fixture
{
	Accumulation_Fixture();
	~Accumulation_Fixture();

};

Accumulation_Fixture::Accumulation_Fixture()
{
	// construct a tetramesh cube
	PTTetraMeshBuilder builder;
	builder.setNumberOfTetras(6);
	builder.setNumberOfPoints(8);
	
	Points* P = new Points();
	P->resize(8);
	P->set(0, Point<3,double>{.0,.0,.0});
	P->set(1, Point<3,double>{1.,.0,.0});
	P->set(2, Point<3,double>{.0,1.,.0});
	P->set(3, Point<3,double>{.0,.0,1.});
	P->set(4, Point<3,double>{1.,1.,.0});
	P->set(5, Point<3,double>{1.,.0,1.});
	P->set(6, Point<3,double>{.0,1.,1.});
	P->set(7, Point<3,double>{1.,1.,1.});

	for (unsigned i=0; i<P->size();i++)
	{
		builder.setPoint(i, P->get(i));
	}
	/*
	+y
	^ / +z
	|/
	+--> +x
	
	  6 ------ 7
	 /|       /|
	2 ------ 4 |
 	| |      | |
	| 3 -----|-5
	|/       |/
	0 ------ 1

	total distinct edges: 8C2 = 8!/2!/6! = 28

	cube outer edges (12): 		01 02 03 14 15 24 26 35 36 47 57 67   	differ in only 1 component
	cube face diagonals (12): 	04 05 06 12 13 17 23 27 37 45 46 56		differ in only 2 components
	cube interior diagonals (4):	07 16 25 34								differ in all 3  components

	faces (6): 0142 0351 0362 7635 7426 7415
	            XX   XX   XX   XX   XX   XX

	outer edges all mutually compatible
	face diagonals conflict in pairs: 04-12 05-13 06-23 17-45 27-46 37-56
	interior diagonals all mutually conflict

	outer edges (12) appear only once, interior diagonals don't appear at all (0), and rest appear twice (2x12) -> 36
	
	*/
	vector<TetraByPointID> TPointID{
									// the dummy tetra as always (V=0)
									TetraByPointID{{0,0,0,0}},
									// four corner tetras anchored at 0, 4, 5, 6 (V=1/6)
									TetraByPointID{{0,1,2,3}}, // 01 02 03 12 13 23  --> !04 !05 !06
									TetraByPointID{{1,2,4,7}}, // 12 14 17 24 27 47  --> !04 !45 !46
									TetraByPointID{{1,3,5,7}}, // 13 15 17 35 37 57  --> !05 !45 !56
									TetraByPointID{{2,3,6,7}}, // 23 26 27 35 37 57  --> !06 !46 !56
									// interior wedge (V=2/6)
									TetraByPointID{{1,2,3,7}}
									};
	
	Partition* Mat = new Partition();
	Mat->resize(6);
	Mat->assign(0, 0); // TetraID 0 -> MatID 0
	Mat->assign(1, 1); // TetraID 1 -> MatID 1
	Mat->assign(2, 1); // TetraID 2 -> MatID 1
	Mat->assign(3, 1); // TetraID 3 -> MatID 1
	Mat->assign(4, 2); // TetraID 4 -> MatID 2
	Mat->assign(5, 1); // TetraID 5 -> MatID 1

	for(unsigned i=0; i<TPointID.size(); i++)
	{
		builder.setTetra(i, TPointID[i], Mat->get(i));
	}
	builder.build();
	fixture_geom = builder.mesh();
	/***********************************************************************************************************
	 *  Mesh components:
	 *  8 points
	 *	6 tetrahedrons - 5 "real" + 1 "0" tetrahderon
	 *  17 faces - 16 "real" (4 shared faces) + 1 "0" face
	 *********************************************************************************************************** 
	 * | TetraID |  Points   |    Faces    | Directed Faces |
	 * |----------------------------------------------------|
	 * |    0    |  0,0,0,0  |   0,0,0,0   |    0,0,0,0     |
	 * |    1    |  0,1,2,3  |   1,2,3,4   |    2,5,6,9     |
	 * |    2    |  1,2,4,7  |   5,6,7,8   |  10,13,15,16   |
	 * |    3    |  1,3,5,7  |  9,10,11,12 |  18,21,22,25   |
	 * |    4    |  2,3,6,7  | 13,14,15,16 |  26,29,31,32   |
	 * |    5    |  1,2,3,7  |  4,5,10,13  |   8,11,20,27   |
	 * 
	 * | FaceID  |  Directed FaceID |   Points   | Associated Tetras (inside/outside) |
	 * |------------------------------------------------------------------------------|
	 * |    0    |         0        |    0,0,0   |               0/0                  |
	 * |    1    |         2        |    0,1,2   |               1/0                  |
	 * |    2    |         5        |    0,1,3   |               0/1                  |
	 * |    3    |         6        |    0,2,3   |               1/0                  |
	 * |    4    |         9        |    1,2,3   |               5/1                  |
	 * |    5    |        10        |    1,2,7   |               2/5                  |
	 * |    6    |        13        |    1,2,4   |               0/2                  |
	 * |    7    |        15        |    1,4,7   |               0/2                  |
	 * |    8    |        16        |    2,4,7   |               2/0                  |
	 * |    9    |        18        |    1,3,5   |               3/0                  |
	 * |   10    |        21        |    1,3,7   |               5/3                  |
	 * |   11    |        22        |    1,5,7   |               3/0                  |
	 * |   12    |        25        |    3,5,7   |               0/3                  |
	 * |   13    |        26        |    2,3,7   |               4/5                  |
	 * |   14    |        29        |    2,3,6   |               0/4                  |
	 * |   15    |        31        |    2,6,7   |               0/4                  |   
	 * |   16    |        32        |    3,6,7   |               4/0                  |
	 * 
	 * The Directed FaceID is constructed based on the FaceID and the normal of the
	 * face is pointing to. If it points to the inside of the tetrahedron it is considered to belong 
	 * to the respective tetrahedron. If the normal of the face points outside of the tetrahedron, then
	 * it belongs the the tetrahedron on the other side of the face. There are always 2 faces pointing
	 * inwards of the tetrahedron and 2 faces pointing outwards of the tetrahedron.
	 * 
	 * If face points inwards (even Directed FaceID number):
	 * Directed FaceID = FaceID << 1
	 * 
	 * If face points outwards (odd Directed FaceID number):
	 * Directed FaceID = (FaceID << 1) | 1
	 * 
	 ***********************************************************************************************************/
}

Accumulation_Fixture::~Accumulation_Fixture()
{
	delete fixture_geom->points();
	delete fixture_geom->tetraCells();
	delete fixture_geom->regions();
	delete fixture_geom;
}

BOOST_GLOBAL_FIXTURE(Accumulation_Fixture);

BOOST_AUTO_TEST_CASE(energy_VolumeAccumulationProbe)
{
	// Energy values for our artificial mesh. We are ignoring the 0 Tetra entry
	SpatialMap<float> phi(
			vector<float>{
				11.0f, // TetraID 1
				22.0f, // TetraID 2
				43.0f, // TetraID 3
				14.0f, // TetraID 4
				05.0f  // TetraID 5
			},
			AbstractSpatialMap::Volume,
			AbstractSpatialMap::Scalar,
			AbstractSpatialMap::PhotonWeight);

	VolumeAccumulationProbe VAP;

	VAP.update();
	BOOST_CHECK(VAP.total() == -1 && "Expectation: m_total= -1 because the first thing we check is if an energy/fluence vector is provided ");
	LOG_INFO << "Error Check passed!" << endl;

	SpatialMap<unsigned> test_1;
	VAP.source(&test_1);
	VAP.update();
	BOOST_CHECK(VAP.total() == -2 && "Expectation: m_total= -2 because we expect a SpatialMap<float> as input");
	LOG_INFO << "Error Check passed!" << endl;

	SpatialMap<float> test_2;
	test_2.spatialType(AbstractSpatialMap::Surface);
	VAP.source(&test_2);
	VAP.update();
	BOOST_CHECK(VAP.total() == -3 && "Expectation: m_total= -3 because the SpatialMap<float> needs to have the spatial type Volume");
	LOG_INFO << "Error Check passed!" << endl;

	test_2.spatialType(AbstractSpatialMap::Volume);
	VAP.update();
	BOOST_CHECK(VAP.total() == -4 && "Expectation: m_total= -4 because the SpatialMap<float> needs to be of output type Energy, Fluence or PhotonWeight");
	LOG_INFO << "Error Check passed!" << endl;

	test_2.outputType(AbstractSpatialMap::Energy);
	VAP.update();
	BOOST_CHECK(VAP.total() == -5 && "Expectation: m_total= -5 because no geometry has been provided");
	LOG_INFO << "Error Check passed!" << endl;

	Layered* geom = new Layered();
	VAP.geometry(geom); // Provide an input for the geomtry, but is the wrong format;
	VAP.update();
	delete geom;
	BOOST_CHECK(VAP.total() == -6 && "Expectation: m_total= -6 because no TetraMesh geometry has been provided");
	LOG_INFO << "Error Check passed!" << endl;

	VAP.geometry(fixture_geom);
	VAP.update();
	BOOST_CHECK(VAP.total() == -7 && "Expectation: m_total= -7 because the dimensions of the geometry and the input do not match");
	LOG_INFO << "Error Check passed!" << endl;

	VAP.source(&phi);
	VAP.update();
	BOOST_CHECK(VAP.total() == -8 && "Expectation: m_total= -8 because no origin point has been defined");
	LOG_INFO << "Error Check passed!" << endl;

	VAP.includePartialTetras(true);
	VAP.origin({0.0f,1.0f,1.0f}); // Point 6 in mesh
	VAP.radius(0.5f);
	VAP.setsphere();
	VAP.update();

	BOOST_CHECK_EQUAL(VAP.total(),phi[3]);

	// Test the result of the VolumeAccumulationProbe when the radius is 0
	VAP.radius(0);
	VAP.origin({0.5f,0.2f,0.2f}); // This point should be in TetraID 1
	VAP.update();

	BOOST_CHECK_EQUAL(VAP.total(),phi[0]);

	//  Test the result of the VolumeAccumulationProbe when we are not considering partial tetrahedrons
	VAP.includePartialTetras(false);
	VAP.radius(1.0f);
	VAP.origin({0.0f,0.0f,0.0f}); // Only all points of TetraID 1 are included here
	VAP.update();

	BOOST_CHECK_EQUAL(VAP.total(),phi[0]);

	// revert back to including partial tetras
	VAP.includePartialTetras(true);
	// Test the cylinder option of the VolumeAccumulationProbe
	VAP.setcylinder();
	VAP.radius(0.5f);
	VAP.origin({0.0f,1.0f,1.0f}); // Point 6 in mesh
	VAP.point1({1.1f,1.0f,0.0f}); // Point 4 in mesh
	VAP.update();

	BOOST_CHECK_EQUAL(VAP.total(),phi[3]+phi[1]);

	// If radius is 0, we should automatically revert back to sphere probe
	VAP.radius(0.0f);
	VAP.origin({-0.1f,1.0f,1.0f}); // Not in mesh -> error
	VAP.update();

	BOOST_CHECK(VAP.total() == -9 && "Should output negative value since no Tetra could be enclosing that point");
	LOG_INFO << "Error Check passed!" << endl;
}

BOOST_AUTO_TEST_CASE(fluence_VolumeAccumulationProbe)
{
	float V=.0f;

	for(const auto t : fixture_geom->tetras())
	{
		V += get(volume,*fixture_geom,t);
		cout << "TetraID " << get(id,*fixture_geom,t) << " has volume " << get(volume,*fixture_geom,t) << endl;
	}
	cout << "Total volume " << V << endl;
	// Check if the volume of the mesh is actually 1.0
	// If that is not the case, either the mesh building failed
	// or changes to this simple mesh have introduced this error
	BOOST_CHECK_EQUAL(V,1.0f);

	// Fluence values for our artificial mesh. We are ignoring the 0 Tetra entry
	SpatialMap<float> phi(
			vector<float>{
				11.0f, // TetraID 1
				22.0f, // TetraID 2
				43.0f, // TetraID 3
				14.0f, // TetraID 4
				05.0f  // TetraID 5
			},
			AbstractSpatialMap::Volume,
			AbstractSpatialMap::Scalar,
			AbstractSpatialMap::Fluence);

	VolumeAccumulationProbe VAP;
	VAP.geometry(fixture_geom);
	VAP.source(&phi);
	VAP.includePartialTetras(true);
	VAP.origin({0.0f,0.5f,1.0f}); // Point 6 in mesh
	VAP.radius(0.5f);
	VAP.setsphere();
	VAP.update();

	float vol_1 = get(volume,*fixture_geom,TetraMesh::TetraDescriptor(1));
	float vol_2 = get(volume,*fixture_geom,TetraMesh::TetraDescriptor(2));
	float vol_3 = get(volume,*fixture_geom,TetraMesh::TetraDescriptor(3));
	float vol_4 = get(volume,*fixture_geom,TetraMesh::TetraDescriptor(4));
	float vol_5 = get(volume,*fixture_geom,TetraMesh::TetraDescriptor(5));
	float expected_fluence = (phi[3]*vol_4+phi[4]*vol_5+phi[2]*vol_3+phi[0]*vol_1) / (vol_4+vol_5+vol_3+vol_1);

	BOOST_CHECK_CLOSE(VAP.total(),expected_fluence,1e-4f);
	// Test the result of the VolumeAccumulationProbe when the radius is 0
	VAP.radius(0);
	VAP.origin({0.5f,0.2f,0.2f}); // This point should be in TetraID 1
	VAP.update();

	BOOST_CHECK_EQUAL(VAP.total(),phi[0]);

	// Test the cylinder option of the VolumeAccumulationProbe
	VAP.setcylinder();
	VAP.radius(0.5f);
	VAP.origin({0.0f,1.0f,1.0f}); // Point 6 in mesh
	VAP.point1({1.1f,1.0f,0.0f}); // Point 4 in mesh
	VAP.update();
	expected_fluence = (phi[3]*vol_4+phi[1]*vol_2) / (vol_4+vol_2);
	BOOST_CHECK_CLOSE(VAP.total(),expected_fluence,1e-4f);
}

BOOST_AUTO_TEST_CASE(energy_SurfaceAccumulationProbe)
{

	// Surface energy values for our artificial mesh. We are ignoring the 0 face entry
	SpatialMap<float> phi(
			vector<float>{
				00.0f, // FaceID 0
				11.0f, // FaceID 1
				22.0f, // FaceID 2
				43.0f, // FaceID 3
				14.0f, // FaceID 4
				05.0f, // FaceID 5
				16.0f, // FaceID 6
				03.0f, // FaceID 7
				56.0f, // FaceID 8
				33.0f, // FaceID 9
				87.0f, // FaceID 10
				21.0f, // FaceID 11
				67.0f, // FaceID 12
				19.0f, // FaceID 13
				92.0f, // FaceID 14
				12.0f, // FaceID 15
				01.0f  // FaceID 16
			},
			AbstractSpatialMap::Surface,
			AbstractSpatialMap::Scalar,
			AbstractSpatialMap::Energy);

	SurfaceAccumulationProbe SAP;

	LOG_INFO << "Empty geometry check" << endl;
	SAP.update();
	BOOST_CHECK(SAP.total() == -1 && "Expectation: m_total= -1 because the first thing we check is if an energy/fluence vector is provided ");

	SpatialMap<unsigned> test_1;
	SAP.source(&test_1);
	SAP.update();
	BOOST_CHECK(SAP.total() == -2 && "Expectation: m_total= -2 because we expect a SpatialMap<float> as input");
	LOG_INFO << "Error Check passed!" << endl;

	SpatialMap<float> test_2;
	test_2.spatialType(AbstractSpatialMap::Volume);
	SAP.source(&test_2);
	SAP.update();
	BOOST_CHECK(SAP.total() == -3 && "Expectation: m_total= -3 because the SpatialMap<float> needs to have the spatial type Volume");
	LOG_INFO << "Error Check passed!" << endl;

	test_2.spatialType(AbstractSpatialMap::Surface);
	SAP.update();
	BOOST_CHECK(SAP.total() == -4 && "Expectation: m_total= -4 because the SpatialMap<float> needs to be of output type Energy, Fluence or PhotonWeight");
	LOG_INFO << "Error Check passed!" << endl;

	test_2.outputType(AbstractSpatialMap::Energy);
	SAP.update();
	BOOST_CHECK(SAP.total() == -5 && "Expectation: m_total= -5 because no geometry has been provided");
	LOG_INFO << "Error Check passed!" << endl;

	Layered* geom = new Layered();
	SAP.geometry(geom); // Provide an input for the geomtry, but is the wrong format;
	SAP.update();
	delete geom;
	BOOST_CHECK(SAP.total() == -6 && "Expectation: m_total= -6 because no TetraMesh geometry has been provided");
	LOG_INFO << "Error Check passed!" << endl;

	SAP.geometry(fixture_geom);
	SAP.update();
	BOOST_CHECK(SAP.total() == -7 && "Expectation: m_total= -7 because the dimensions of the geometry and the input do not match");
	LOG_INFO << "Error Check passed!" << endl;

	SAP.source(&phi);
	SAP.includePartialSurfaces(false);
	SAP.origin({0.0f,0.0f,0.0f}); // Point 0 in mesh
	SAP.radius(1.0f);
	SAP.update();
	// We should include all points from face 1, 2 and 3 in the sphere
	BOOST_CHECK_EQUAL(SAP.total(),phi[1]+phi[2]+phi[3]);

	// Turn includePartialSurfaces on and check if the values are the expected outputs
	SAP.includePartialSurfaces(true);
	SAP.origin({1.0f,0.0f,0.0f}); // Point 0 in mesh
	SAP.radius(0.1f); // radius <1 means we are only including Point 1 in the sphere
	SAP.update();
	// We should include faces 1, 2, 6, 7, 9 and 11 in the sphere
	BOOST_CHECK_EQUAL(SAP.total(),phi[1]+phi[2]+phi[6]+phi[7]+phi[9]+phi[11]);

	SAP.origin({0.5f,0.5f,0.5f}); // Point should be in TetraID 5
	SAP.radius(0.0f); // radius 0 means we searching for the tetra enclosing the point
	SAP.update();
	// Only face 13 of TetraID 5 should be included
	BOOST_CHECK_EQUAL(SAP.total(),phi[13]);

	SAP.origin({-0.1f,0.0f,0.0f});
	SAP.update();
	// Value should be 0
	BOOST_CHECK_EQUAL(SAP.total(),0.0f);
}

BOOST_AUTO_TEST_CASE(fluence_SurfaceAccumulationProbe)
{
	SpatialMap<float>* areas;
	areas = fixture_geom->surfaceAreas();
	float A = .0f;

	for(unsigned i=0; i<areas->dim(); i++)
	{
		A += areas->get(i);
		cout << "FaceID " << i << " has area " << areas->get(i) << endl;
	}
	cout << "Total area " << A << endl;

	// Surface energy values for our artificial mesh. We are ignoring the 0 face entry
	SpatialMap<float> phi(
			vector<float>{
				00.0f, // FaceID 0
				11.0f, // FaceID 1
				22.0f, // FaceID 2
				43.0f, // FaceID 3
				14.0f, // FaceID 4
				05.0f, // FaceID 5
				16.0f, // FaceID 6
				03.0f, // FaceID 7
				56.0f, // FaceID 8
				33.0f, // FaceID 9
				87.0f, // FaceID 10
				21.0f, // FaceID 11
				67.0f, // FaceID 12
				19.0f, // FaceID 13
				92.0f, // FaceID 14
				12.0f, // FaceID 15
				01.0f  // FaceID 16
			},
			AbstractSpatialMap::Surface,
			AbstractSpatialMap::Scalar,
			AbstractSpatialMap::Fluence);

	SurfaceAccumulationProbe SAP;
	SAP.geometry(fixture_geom);
	SAP.source(&phi);
	SAP.includePartialSurfaces(false);
	SAP.origin({0.0f,0.0f,0.0f}); // Point 0 in mesh
	SAP.radius(1.0f);
	SAP.update();
	// We should include all points from face 1, 2 and 3 in the sphere
	float expected_fluence = (phi[1]*areas->get(1)+phi[2]*areas->get(2)+phi[3]*areas->get(3)) / (areas->get(1)+areas->get(2)+areas->get(3));
	BOOST_CHECK_CLOSE(SAP.total(),expected_fluence, 1e-4f);

	// Turn includePartialSurfaces on and check if the values are the expected outputs
	SAP.includePartialSurfaces(true);
	SAP.origin({1.0f,0.0f,0.0f}); // Point 0 in mesh
	SAP.radius(0.1f); // radius <1 means we are only including Point 1 in the sphere
	SAP.update();
	// We should include faces 1, 2, 6, 7, 9 and 11 in the sphere
	expected_fluence = (phi[1]*areas->get(1)+phi[2]*areas->get(2)+phi[6]*areas->get(6)+phi[7]*areas->get(7)+phi[9]*areas->get(9)+phi[11]*areas->get(11)) / (areas->get(1)+areas->get(2)+areas->get(6)+areas->get(7)+areas->get(9)+areas->get(11));
	BOOST_CHECK_CLOSE(SAP.total(),expected_fluence,1e-4f);

	SAP.origin({0.5f,0.5f,0.5f}); // Point should be in TetraID 5
	SAP.radius(0.0f); // radius 0 means we searching for the tetra enclosing the point
	SAP.update();
	// Only face 13 of TetraID 5 should be included
	BOOST_CHECK_EQUAL(SAP.total(),phi[13]);

	SAP.origin({-0.1f,0.0f,0.0f}); // Point should be in TetraID 5
	SAP.update();
	// Value should be 0
	BOOST_CHECK_EQUAL(SAP.total(),0.0f);
}

BOOST_AUTO_TEST_CASE(energy_LayeredAccumulationProbe)
{
	ifstream infile;
	char tetraData[150];
	float read_value;

	// Fluence values for our artificial mesh. We are ignoring the 0 Tetra entry
	SpatialMap<float> phi(
			vector<float>{
				11.0f, // TetraID 1
				22.0f, // TetraID 2
				43.0f, // TetraID 3
				14.0f, // TetraID 4
				05.0f  // TetraID 5
			},
			AbstractSpatialMap::Volume,
			AbstractSpatialMap::Scalar,
			AbstractSpatialMap::Energy);

	float vol_1 = get(volume,*fixture_geom,TetraMesh::TetraDescriptor(1));
	float vol_2 = get(volume,*fixture_geom,TetraMesh::TetraDescriptor(2));
	float vol_3 = get(volume,*fixture_geom,TetraMesh::TetraDescriptor(3));
	float vol_4 = get(volume,*fixture_geom,TetraMesh::TetraDescriptor(4));
	float vol_5 = get(volume,*fixture_geom,TetraMesh::TetraDescriptor(5));

	LayeredAccumulationProbe LAP;

	LOG_INFO << "Undefined output file check" << endl;
	LAP.update();

	infile.open("error.log");
	infile >> tetraData;
	infile >> read_value;
	infile.close();
	BOOST_CHECK((read_value == 0) && strcmp(tetraData,"No_Output_File_Error") == 0);

	LOG_INFO << "Empty input check" << endl;
	LAP.output_filename("reg_test.txt");
	LAP.update();

	infile.open("reg_test.txt");
	infile >> tetraData;
	infile >> read_value;
	infile.close();
	BOOST_CHECK((read_value == -1) && strcmp(tetraData,"No_Input_Error") == 0);
	
	ofstream os("phi.txt");
	os << "Unknown (?) 1 5 float\n5.5 13.4 6.7 3.14 5.92" << endl;
	os.close();
	LAP.input_filename("phi.txt");

	infile.open("error.log");
	infile >> tetraData;
	infile >> read_value;
	infile.close();
	BOOST_CHECK((read_value == -9) && strcmp(tetraData,"Wrong_OutputType_Error") == 0);

	os.open("phi.txt");
	os << "Fluence (J/mm^2) 1 7 float\n5.5 13.4 6.7 3.14 5.92" << endl;
	os.close();

	LAP.input_filename("phi.txt");

	infile.open("error.log");
	infile >> tetraData;
	infile >> read_value;
	infile.close();
	BOOST_CHECK((read_value == -10) && strcmp(tetraData,"Input_File_Error") == 0);
	os.open("phi.txt");
	os << "Energy (J;mm) 1 5 float\n5.5 13.4 6.7 3.14 5.92";
	os.close();

	LAP.input_filename("phi.txt");

	LOG_INFO << "Wrong input type check 1" << endl;
	SpatialMap<unsigned> test_1;
	LAP.source(&test_1);
	LAP.update();
	
	infile.open("reg_test.txt");
	infile >> tetraData;
	infile >> read_value;
	infile.close();
	BOOST_CHECK((read_value == -2) && strcmp(tetraData,"SpatialMap_Error") == 0);
	
	LOG_INFO << "Wrong input type check 2" << endl;
	SpatialMap<float> test_2;
	test_2.spatialType(AbstractSpatialMap::Surface);
	LAP.source(&test_2);
	LAP.update();

	infile.open("reg_test.txt");
	infile >> tetraData;
	infile >> read_value;
	infile.close();
	BOOST_CHECK((read_value == -3) && strcmp(tetraData,"SpatialMap_Volume_Error") == 0);

	LOG_INFO << "Wrong input type check 3" << endl;
	test_2.spatialType(AbstractSpatialMap::Volume);
	LAP.update();

	infile.open("reg_test.txt");
	infile >> tetraData;
	infile >> read_value;
	infile.close();
	BOOST_CHECK((read_value == -4) && strcmp(tetraData,"SpatialMap_OutputType_Error") == 0);

	LOG_INFO << "No geometry check" << endl;
	test_2.outputType(AbstractSpatialMap::Energy);
	LAP.update();

	infile.open("reg_test.txt");
	infile >> tetraData;
	infile >> read_value;
	infile.close();
	BOOST_CHECK((read_value == -5) && strcmp(tetraData,"No_Geometry_Error") == 0);

	LOG_INFO << "Wrong geometry check" << endl;
	Layered* geom = new Layered();
	LAP.geometry(geom); // Provide an input for the geomtry, but is the wrong format;
	LAP.update();
	delete geom;

	infile.open("reg_test.txt");
	infile >> tetraData;
	infile >> read_value;
	infile.close();
	BOOST_CHECK((read_value == -6) && strcmp(tetraData,"TetraMesh_Error") == 0);

	LOG_INFO << "Input vs geometry check" << endl;
	LAP.geometry(fixture_geom);
	LAP.update();

	infile.open("reg_test.txt");
	infile >> tetraData;
	infile >> read_value;
	infile.close();
	BOOST_CHECK((read_value == -7) && strcmp(tetraData,"Size_Mismatch_Error") == 0);

	LOG_INFO << "Defined origin check" << endl;
	LAP.source(&phi);
	LAP.update();

	infile.open("reg_test.txt");
	infile >> tetraData;
	infile >> read_value;
	infile.close();
	BOOST_CHECK((read_value == -8) && strcmp(tetraData,"No_Origin_Error") == 0);
	
	///////////////////              Done Error checking       //////////////////////////////////////
	LAP.includePartialTetras(true);
	LAP.origin({0.0f,0.0f,0.0f}); // Point 0 in mesh
	LAP.radius(-0.8f);
	LAP.setIncrement(-0.4);
	LAP.setsphere();
	LAP.update();

	infile.open("reg_test.txt");

	infile.getline(tetraData,150);
	infile >> read_value;
	BOOST_CHECK_EQUAL(read_value,0);

	infile >> read_value;
	BOOST_CHECK_EQUAL(read_value,phi[0]);
	infile.close();

	LAP.includePartialTetras(true);
	LAP.origin({0.5f,0.5f,0.5f}); // Point is in TetraID 5
	LAP.radius(0.0f);
	LAP.update();

	infile.open("reg_test.txt");

	infile.getline(tetraData,150);
	infile >> read_value;
	BOOST_CHECK_EQUAL(read_value,phi[4]);
	infile.close();

	LAP.includePartialTetras(false);
	LAP.origin({0.0f,0.0f,0.0f}); // Point is in TetraID 5
	LAP.radius(2.0f);
	LAP.setIncrement(0.4);
	LAP.update();

	infile.open("reg_test.txt");

	infile.getline(tetraData,150);

	infile >> read_value;
	BOOST_CHECK_EQUAL(read_value,0);

	infile >> read_value;
	BOOST_CHECK_EQUAL(read_value,phi[0]);

	infile >> read_value;
	BOOST_CHECK_EQUAL(read_value,phi[4]);

	infile >> read_value;
	BOOST_CHECK_EQUAL(read_value,phi[1]+phi[2]+phi[3]);
	infile.close();

	LAP.setcylinder(); // Cylinder probe will exclude TetraID 2 and TetraID 4
	LAP.point1({1.0f,0.0f,1.0f});
	LAP.radius(1.0f);
	LAP.setIncrement(0.5);
	LAP.update();

	infile.open("reg_test.txt");

	infile.getline(tetraData,150);

	infile >> read_value;
	BOOST_CHECK_EQUAL(read_value,phi[0]);

	infile >> read_value;
	BOOST_CHECK_EQUAL(read_value,phi[4]);

	infile >> read_value;
	BOOST_CHECK_EQUAL(read_value,phi[2]);
	infile.close();

	// Cylinder test with radius 0
	LAP.point1({1.0f,0.0f,1.0f});
	LAP.radius(0.0f);
	LAP.setIncrement(0.5);
	LAP.update();

	infile.open("reg_test.txt");

	infile.getline(tetraData,150);

	infile >> read_value;
	BOOST_CHECK_EQUAL(read_value,0);

	infile >> read_value;
	BOOST_CHECK_EQUAL(read_value,phi[0]+phi[4]);

	infile >> read_value;
	BOOST_CHECK_EQUAL(read_value,phi[2]);
	infile.close();

	// Cylinder Test with partial tetras enabled
	LAP.includePartialTetras(true);
	LAP.point1({1.0f,1.0f,1.0f});
	LAP.radius(0.1f);
	LAP.update();

	infile.open("reg_test.txt");

	infile.getline(tetraData,150);

	infile >> read_value;
	BOOST_CHECK_EQUAL(read_value,phi[0]);

	infile >> read_value;
	BOOST_CHECK_EQUAL(read_value,phi[4]);

	infile >> read_value;
	BOOST_CHECK_EQUAL(read_value,phi[1]+phi[2]+phi[3]);

	infile >> read_value;
	BOOST_CHECK_EQUAL(read_value,0.0f);
	infile.close();
}

BOOST_AUTO_TEST_CASE(fluence_LayeredAccumulationProbe)
{
	ifstream infile;
	char tetraData[150];

	// Fluence values for our artificial mesh. We are ignoring the 0 Tetra entry
	SpatialMap<float> phi(
			vector<float>{
				11.0f, // TetraID 1
				22.0f, // TetraID 2
				43.0f, // TetraID 3
				14.0f, // TetraID 4
				05.0f  // TetraID 5
			},
			AbstractSpatialMap::Volume,
			AbstractSpatialMap::Scalar,
			AbstractSpatialMap::Fluence);

	float vol_1 = get(volume,*fixture_geom,TetraMesh::TetraDescriptor(1));
	float vol_2 = get(volume,*fixture_geom,TetraMesh::TetraDescriptor(2));
	float vol_3 = get(volume,*fixture_geom,TetraMesh::TetraDescriptor(3));
	float vol_4 = get(volume,*fixture_geom,TetraMesh::TetraDescriptor(4));
	float vol_5 = get(volume,*fixture_geom,TetraMesh::TetraDescriptor(5));

	LayeredAccumulationProbe LAP;
	LAP.geometry(fixture_geom);
	LAP.source(&phi);
	LAP.output_filename("reg_test.txt");
	LAP.includePartialTetras(true);
	LAP.origin({0.0f,0.0f,0.0f}); // Point 0 in mesh
	LAP.radius(0.8f);
	LAP.setIncrement(0.4);
	LAP.setsphere();
	LAP.update();

	infile.open("reg_test.txt");

	infile.getline(tetraData,150);
	float read_value;
	infile >> read_value;
	BOOST_CHECK_EQUAL(read_value,0);

	infile >> read_value;
	BOOST_CHECK_EQUAL(read_value,phi[0]);
	infile.close();

	LAP.includePartialTetras(true);
	LAP.origin({0.5f,0.5f,0.5f}); // Point is in TetraID 5
	LAP.radius(0.0f);
	LAP.update();

	infile.open("reg_test.txt");

	infile.getline(tetraData,150);
	infile >> read_value;
	BOOST_CHECK_EQUAL(read_value,phi[4]);
	infile.close();

	LAP.includePartialTetras(false);
	LAP.origin({0.0f,0.0f,0.0f}); // Point is in TetraID 5
	LAP.radius(2.0f);
	LAP.setIncrement(0.4);
	LAP.update();

	infile.open("reg_test.txt");

	infile.getline(tetraData,150);

	infile >> read_value;
	BOOST_CHECK_EQUAL(read_value,0);

	infile >> read_value;
	BOOST_CHECK_EQUAL(read_value,phi[0]);

	infile >> read_value;
	BOOST_CHECK_EQUAL(read_value,phi[4]);

	infile >> read_value;
	float expected_fluence = (phi[1]*vol_2+phi[2]*vol_3+phi[3]*vol_4) / (vol_2+vol_3+vol_4);
	BOOST_CHECK_CLOSE(read_value,expected_fluence,1e-3f);
	infile.close();

	LAP.setcylinder(); // Cylinder probe will exclude TetraID 2 and TetraID 4
	LAP.point1({1.0f,0.0f,1.0f});
	LAP.radius(1.0f);
	LAP.setIncrement(0.5);
	LAP.update();

	infile.open("reg_test.txt");

	infile.getline(tetraData,150);

	infile >> read_value;
	BOOST_CHECK_EQUAL(read_value,phi[0]);

	infile >> read_value;
	BOOST_CHECK_EQUAL(read_value,phi[4]);

	infile >> read_value;
	BOOST_CHECK_EQUAL(read_value,phi[2]);
	infile.close();

	// Cylinder test with radius 0
	LAP.point1({1.0f,0.0f,1.0f});
	LAP.radius(0.0f);
	LAP.setIncrement(0.5);
	LAP.update();

	infile.open("reg_test.txt");

	infile.getline(tetraData,150);

	infile >> read_value;
	BOOST_CHECK_EQUAL(read_value,0);

	infile >> read_value;
	expected_fluence = (phi[0]*vol_1+phi[4]*vol_5) / (vol_1+vol_5);
	BOOST_CHECK_CLOSE(read_value,expected_fluence,1e-4f);

	infile >> read_value;
	BOOST_CHECK_EQUAL(read_value,phi[2]);
	infile.close();

	// Cylinder Test with partial tetras enabled
	LAP.includePartialTetras(true);
	LAP.point1({1.0f,1.0f,1.0f});
	LAP.radius(0.1f);
	LAP.update();

	infile.open("reg_test.txt");

	infile.getline(tetraData,150);

	infile >> read_value;
	BOOST_CHECK_EQUAL(read_value,phi[0]);

	infile >> read_value;
	BOOST_CHECK_EQUAL(read_value,phi[4]);

	infile >> read_value;
	expected_fluence = (phi[1]*vol_2+phi[2]*vol_3+phi[3]*vol_4) / (vol_2+vol_3+vol_4);
	BOOST_CHECK_CLOSE(read_value,expected_fluence,1e-3f);

	infile >> read_value;
	BOOST_CHECK_EQUAL(read_value,0.0f);
	infile.close();
}