# Convert cube_5med mesh to .vtk format so Python scripts can read it

package require FullMonte

###### Set file names and load files

set CI_PROJECT_DIR [array get env CI_PROJECT_DIR]

set datapath [file join $FullMonte::datadir TIM-OS cube_5med]
puts $datapath
set meshfn "$datapath/cube_5med.mesh"

set out_mesh_fn "cube_5med.vtk"

TIMOSMeshReader R
R filename $meshfn
R read
set mesh [R mesh]

VTKMeshWriter W
W filename $out_mesh_fn
W mesh $mesh
W write
