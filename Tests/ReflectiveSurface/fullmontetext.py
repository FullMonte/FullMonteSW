import re
import numpy as np
import math

"""
Python module for manipulating FullMonte text-format files

See format definition in FullMonteSW/Storage/TextFile/TextFileMatrixWriter.cpp
"""

def _prod(value_sequence):
  temp = list(value_sequence)
  product = 1
  for x in temp:
    product *= x
  return product

header_re = re.compile(r'(?P<name>[A-Za-z_0-9]+)\s+(?P<unit>([(A-Z]+[\/;]+[a-z^0-9)]+|[(?)]+)+)\s+(?P<n_dim>[0-9]+)\s+(?P<dims>([0-9]+\s+)+)(?P<type>[a-zA-Z0-9_]+)$')

def read_matrix(file_name):
  """
  Return the N-D FullMonte fluence data from the specified file as an numpy.ndarray

  NOTE: Tested only for 1D
  """

  with open(file_name,'r') as f:
    header = next(f)

    m = header_re.match(header)

    if m:
      n_dim = int(m.group('n_dim'))
      dims = [int(d) for d in m.group('dims').split()]
      data_type = m.group('type')

      x = np.zeros(_prod(dims),dtype=np.float,order='C')
    else:
      raise Exception('Header line failed to match expected format')

    n=0

    for (line_num,line) in enumerate(f,2):
      data = [float(x) for x in line.split()]
      x[n:n+len(data)] = data
      n += len(data)

  return x.reshape(dims)
