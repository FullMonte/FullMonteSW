/*
 * Test_OutputTypes.cpp
 *
 *  Created on: Jun 29, 2017
 *      Author: jcassidy
 */


#include <boost/test/unit_test.hpp>

#include <iostream>

#include <FullMonteSW/OutputTypes/SpatialMap2D.hpp>
#include <FullMonteSW/OutputTypes/MCEventCounts.hpp>

#include <FullMonteSW/OutputTypes/OutputDataType.hpp>

BOOST_AUTO_TEST_CASE(derived)
{
	vector<float> v(100);
	SpatialMap2D<float> m2d(10,10,std::move(v),AbstractSpatialMap::Volume,AbstractSpatialMap::Scalar,AbstractSpatialMap::UnknownOutputType);

	SpatialMap2D<float>* pm2=&m2d;
	SpatialMap<float>* 	pf=&m2d;
	OutputData*			d=&m2d;

	// make sure there's an associated type
	BOOST_REQUIRE(m2d.type());

	// make sure type association is correct
	BOOST_CHECK_EQUAL( m2d.type()->name(),"SpatialMap2D<float>");
	BOOST_CHECK_EQUAL(pm2->type()->name(),"SpatialMap2D<float>");
	BOOST_CHECK_EQUAL(  d->type()->name(),"SpatialMap2D<float>");

	BOOST_CHECK_EQUAL( pm2->type(), SpatialMap2D<float>::staticType());
	BOOST_CHECK_EQUAL(  pf->type(), SpatialMap2D<float>::staticType());
	BOOST_CHECK_EQUAL(   d->type(), SpatialMap2D<float>::staticType());


	// check derivation up the tree
	BOOST_CHECK(pm2->type()->derivedFrom(OutputData::staticType()));
	BOOST_CHECK(pm2->type()->derivedFrom(SpatialMap<float>::staticType()));
	BOOST_CHECK(pm2->type()->derivedFrom(SpatialMap2D<float>::staticType()));

	// check not derived from unrelated type
	BOOST_CHECK(!pm2->type()->derivedFrom(MCEventCountsOutput::staticType()));
	BOOST_CHECK(pf->type()->derivedFrom(SpatialMap2D<float>::staticType()));
	BOOST_CHECK( d->type()->derivedFrom(SpatialMap2D<float>::staticType()));





	vector<float> t(200);
	SpatialMap<float> *smf = new SpatialMap<float>(t,AbstractSpatialMap::Surface,AbstractSpatialMap::Scalar,AbstractSpatialMap::UnknownOutputType);

	BOOST_REQUIRE(smf->type());
	BOOST_CHECK_EQUAL(smf->type()->name(), "SpatialMap<float>");
	BOOST_CHECK(smf->type()->derivedFrom(OutputData::staticType()));
	BOOST_CHECK(smf->type()->derivedFrom(SpatialMap<float>::staticType()));

	// check not derived from child
	BOOST_CHECK(!smf->type()->derivedFrom(SpatialMap2D<float>::staticType()));

	// check not derived from cousin
	BOOST_CHECK(!smf->type()->derivedFrom(MCEventCountsOutput::staticType()));
}
