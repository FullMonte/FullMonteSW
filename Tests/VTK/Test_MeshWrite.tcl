package require vtk
package require FullMonte

puts "Reading"
TIMOSMeshReader R
    R filename "$FullMonte::datadir/Colin27/Colin27.mesh"
    R read

set mesh [R mesh]

puts "Converting from mesh=$mesh"

vtkFullMonteTetraMeshWrapper vtkm
    vtkm mesh $mesh
    vtkm update

puts "Writing"

vtkUnstructuredGridWriter W
    W SetFileName "colin27.blank.vtk"
    W SetInputData [vtkm blankMesh]
    W Update

exit
