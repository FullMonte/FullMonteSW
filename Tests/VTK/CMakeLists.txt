ADD_BOOST_UNIT_TEST(TestVTK TestVTK.cpp)
TARGET_LINK_LIBRARIES(TestVTK vtkFullMonte FullMonteTIMOS)

ADD_TEST(NAME unit_vtk_readwrite_mesh
	COMMAND ${CMAKE_BINARY_DIR}/bin/TestVTK)	
	
IF(WRAP_TCL AND WRAP_VTK)
	# Test reading from a .vtk mesh and converting to TetraMesh
	ADD_TEST(NAME unit_vtktcl_read_mesh
		COMMAND ${CMAKE_BINARY_DIR}/bin/tclmontevtk.sh ${CMAKE_CURRENT_SOURCE_DIR}/Test_MeshRead.tcl)
		
	# Test converting a TetraMesh to a VTK mesh and writing 		
	ADD_TEST(NAME unit_vtktcl_write_mesh
		COMMAND ${CMAKE_BINARY_DIR}/bin/tclmontevtk.sh ${CMAKE_CURRENT_SOURCE_DIR}/Test_MeshWrite.tcl)
		
	# Test creation of an empty mesh 		
	ADD_TEST(NAME unit_vtktcl_empty_mesh
		COMMAND ${CMAKE_BINARY_DIR}/bin/tclmontevtk.sh ${CMAKE_CURRENT_SOURCE_DIR}/Test_MeshEmpty.tcl)
ENDIF()