#ifdef CMAKE_COMPILER_GCC
#pragma GCC diagnostic ignored "-Wunused-parameter"
#endif

#ifdef CMAKE_COMPILER_CLANG
#pragma clang diagnostic ignored "-Wunused-parameter"
#pragma clang diagnostic ignored "-Winconsistent-missing-override"
#endif
