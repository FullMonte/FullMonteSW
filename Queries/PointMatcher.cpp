/*
 * PointMatcher.cpp
 *
 *  Created on: Apr 25, 2018
 *      Author: jcassidy
 */

#include <FullMonteSW/Queries/PointMatcher.hpp>

#include <FullMonteSW/Geometry/Vector.hpp>

#include <FullMonteSW/Geometry/Points.hpp>
#include <FullMonteSW/Geometry/Permutation.hpp>

using namespace std;

PointMatcher::PointMatcher()
{

}

PointMatcher::~PointMatcher()
{

}

void PointMatcher::reference(Points* P0)
{
	m_reference=P0;
}

void PointMatcher::query(Points* P)
{
	m_query=P;
}

void PointMatcher::tolerance(float eps)
{
	m_eps=eps;
}

void PointMatcher::update()
{
	createRTree();
	match();
}

void PointMatcher::createRTree()
{
	if (!m_reference)
	{
		cout << "PointMatcher::createRTree() failed due to lack of points provided" << endl;
		return;
	}

	m_pointIndex.clear();

	// insert reference points into the RTree for later query
	// skip dummy point at index zero (its coordinates are not significant)
	for(unsigned i=1;i<m_reference->size();++i)
	{
		Point<3,double> P = m_reference->get(i);
		m_pointIndex.insert(make_pair(PointCoord{P[0],P[1],P[2]},i-1));
	}
}

void PointMatcher::match()
{
	m_permutation = new Permutation();
	m_unmatched=0;

	if (!m_query)
	{
		cout << "PointMatcher::match() failed due to lack of query points" << endl;
		return;
	}
	else if (m_pointIndex.empty())
	{
		cout << "PointMatcher::match() failed due to empty reference point set" << endl;
		return;
	}

	m_permutation->resize(m_query->size());

	for(unsigned i=0;i<m_query->size();++i)
	{
		// set up and run the k-nearest query with k=1
		Point<3,double> P = m_query->get(i);
		const auto nearestQuery = boost::geometry::index::nearest<PointCoord>(
						PointCoord{P[0],P[1],P[2]},
						1);

		PointPair result;
		m_pointIndex.query(nearestQuery,&result);
		Point<3,double> Q(array<double,3>{{result.first.get<0>(), result.first.get<1>(), result.first.get<2>()}});

		// compute distance and check against threshold
		double d = norm_l2(P-Q);

		if (d < m_eps)
			m_permutation->set(i,result.second);
		else
		{
			m_permutation->set(i,0);					// no match
			++m_unmatched;
		}
	}
}

unsigned PointMatcher::unmatchedCount() const
{
	return m_unmatched;
}

Permutation* PointMatcher::result()
{
	return m_permutation;
}
