#ifndef OUTPUTTYPES_ACCUMULATIONLINEQUERY_HPP_
#define OUTPUTTYPES_ACCUMULATIONLINEQUERY_HPP_

#include <array>
#include <boost/iterator/iterator_categories.hpp>

#include <FullMonteSW/Warnings/Push.hpp>
#include <FullMonteSW/Warnings/Boost.hpp>
#include <boost/range/any_range.hpp>
#include <FullMonteSW/Warnings/Pop.hpp>

#include <FullMonteSW/Geometry/TetraMesh.hpp>
#include <FullMonteSW/Geometry/RayWalk.hpp>

#include <FullMonteSW/OutputTypes/SpatialMap.hpp>
#include <FullMonteSW/OutputTypes/OutputData.hpp>


class TetraMesh;

/**
 * Probes a tetrahedral volume distribution for an accumulation value (e.g. Energy or Fluence) along a line.
 */
class AccumulationLineQuery
{
public:
    // update calculates the total requested
    void update();

    // returns the total data
    float total() 
    { 
       if (m_volume == 0)
            return m_total;
        else
            return m_total/m_volume;
    }

    // the geometry of mesh (must be a TetraMesh)
    const Geometry* geometry() const { return m_geometry; }
    void geometry(const Geometry* m) { m_geometry = m; }

    // the input data
    // NOTE: must be a SpatialMap<float> with 1 entry per tetrahedron in the mesh
    void source(OutputData* M) { m_input = M; }
    const OutputData* source() const { return m_input; }

    // probe endpoints setter/getter
    std::array<float,3> endpoint(unsigned i) const { return m_endpoint[i]; }
	void endpoint(unsigned i,std::array<float,3> p) { m_endpoint[i]=p; }

private:
    // the input data 
    // NOTE: must be a SpatialMap<float> with 1 entry per tetra (e.g. energy or fluence)
    const OutputData* m_input=nullptr;

    // the geometry (must be a TetraMesh)
    const Geometry* m_geometry=nullptr;
	
    // endpoints of the line probe
    std::array<float,3> m_endpoint[2];    
	
    // total accumulation (sum of m_values)
    float m_total=0.0;
    float m_volume=0.0;
};

#endif
