/*
 * EventCountComparison.cpp
 *
 *  Created on: Jun 27, 2017
 *      Author: jcassidy
 */

#include "EventCountComparison.hpp"

#include <FullMonteSW/OutputTypes/MCEventCounts.hpp>

#include <iostream>

#include <FullMonteSW/Warnings/Push.hpp>
#include <FullMonteSW/Warnings/Boost.hpp>
#include <boost/math/distributions/chi_squared.hpp>
#include <FullMonteSW/Warnings/Pop.hpp>

using namespace std;



StatisticalTest::StatisticalTest()
{
}

StatisticalTest::~StatisticalTest()
{
}

float StatisticalTest::statistic() const
{
	return m_statistic;
}

void StatisticalTest::statistic(float s)
{
	m_statistic=s;
}

const std::string& StatisticalTest::name() const
{
	return m_name;
}

void StatisticalTest::name(std::string n)
{
	m_name=n;
}

void StatisticalTest::print(std::ostream& os) const
{
	os << "Statistical test '" << setw(20) << m_name << "'" << endl;
	os << "  Statistic value: " << m_statistic << endl;
	os << "  p=" << pValue() << endl;
	os << "  cutoff for 5% significance: " << cutoff(0.05) << endl;
}


Chi2StatisticalTest::Chi2StatisticalTest()
{
}

Chi2StatisticalTest::~Chi2StatisticalTest()
{
}

unsigned Chi2StatisticalTest::df() const
{
	return m_df;
}

void Chi2StatisticalTest::df(unsigned df)
{
	m_df = df;
}

// probability of getting a larger statistic value given the null hypothesis
float Chi2StatisticalTest::pValue() const
{
	boost::math::chi_squared_distribution<double> chi2dist(m_df);
	return 1.0f-cdf(chi2dist,statistic());
}

float Chi2StatisticalTest::cutoff(float p) const
{
	boost::math::chi_squared_distribution<double> chi2dist(m_df);
	return quantile(chi2dist,1-p);
}

EventCountComparison::EventCountComparison()
{
}

EventCountComparison::~EventCountComparison()
{

}

void EventCountComparison::left(MCEventCountsOutput* lhs)
{
	m_lhs=lhs;
}

void EventCountComparison::right(MCEventCountsOutput* rhs)
{
	m_rhs=rhs;
}

void EventCountComparison::update()
{
//    m_os << "Launched: " << ec->Nlaunch << endl;
//
//    m_os << "Boundary (same):      " << ec->Nbound << endl;
	Chi2StatisticalTest bound = poissonCompare(&MCEventCountsOutput::Nbound);
	bound.name("Boundary (same material)");
	//bound.print();

	Chi2StatisticalTest absorb = poissonCompare(&MCEventCountsOutput::Nabsorb);
	absorb.name("Absorb");
	//absorb.print();

	Chi2StatisticalTest exit = binomialCompare(&MCEventCountsOutput::Nexit);
	exit.name("Exit");
	//exit.print();

	cout << setw(20) << bound.pValue() << setw(20) << absorb.pValue() << setw(20) << exit.pValue() << endl;

//    m_os << "Boundary (different): " << ec->Ninterface << endl;

//    m_os << "  TIR:     " << ec->Ntir << endl;

//    m_os << "  Fresnel: " << ec->Nfresnel << endl;

//    m_os << "  Refract: " << ec->Nrefr << endl;
//    m_os << "  Balance (bound - [TIR + fresnel + refract]): " << ec->Ninterface-ec->Ntir-ec->Nfresnel-ec->Nrefr << endl;
//
//    m_os << "Absorption: " << ec->Nabsorb << endl;
//    m_os << "Scatter:    " << ec->Nscatter << endl;
//
//    m_os << "Roulette results" << endl;
//    m_os << "  Win:  " << ec->Nwin << endl;
//    m_os << "  Lose: " << ec->Ndie << endl;
//
//    m_os << "End results" << endl;
//    m_os << "Died:       " << ec->Ndie << endl;
//    m_os << "Exited:     " << ec->Nexit << endl;
//    m_os << "Abnormal:   " << ec->Nabnormal << endl;
//    m_os << "Time gated: " << ec->Ntime << endl;
//    m_os << "No hit:     " << ec->Nnohit << endl;
//    m_os << "Balance ([launch] - [die + exit]): " << ec->Nlaunch-ec->Ndie-ec->Nexit-ec->Ntime-ec->Nabnormal-ec->Nnohit << endl;



}



/** Compares the two samples from a binomial distribution, with x successes in N trials.
 *
 * Null hypothesis: both samples come from a binomial distribution with parameter p=(x0+x1)/(N0+N1)
 *
 * That binomial has mean mu=(N0+N1)p and variance sigma2=(N0+N1)p(1-p)
 *
 * Under the null hypothesis, the chi2 statistic will be chi2 distributed with 1 df (2 observations with 1 fitted parameter)
 *
 *     | event0=E0  	| event1=E1  	|   -> expect p(N0+N1)
 *     | !event0=N0-E0 	| !event1=N1-E1 |	-> expect (1-p)(N0-N1)
 *
 *        N0         N1
 *
 *
 *  Column sum: (x0-pN0)^2/(pN0) + (N0-x0 - (1-p)N0)^2/((1-p)N0) = (x0-pN0)^2/(pN0) + (pN0-x0)^2/((1-p)N0) = (x0-pN0)^2 / (p(1-p)N0)
 *
 *  Intuitively, under the null hypothesis, x-np is binomial with zero mean and variance np(1-p).
 *  Therefore E[(X-np)^2]=var(X-np)=np(1-p) so dividing that quantity by np(1-p) yields a chi2 distribution with k=1
 *
 */

Chi2StatisticalTest EventCountComparison::binomialCompare(unsigned long long MCEventCountsOutput::* stat)
{
	return binomialCompare(m_lhs->Nlaunch,m_lhs->*stat,m_rhs->Nlaunch,m_rhs->*stat);
}

Chi2StatisticalTest EventCountComparison::poissonCompare(unsigned long long MCEventCountsOutput::* stat)
{
	return poissonCompare(m_lhs->Nlaunch,m_lhs->*stat,m_rhs->Nlaunch,m_rhs->*stat);
}

Chi2StatisticalTest EventCountComparison::binomialCompare(float N0,float x0,float N1,float x1)
{
	double p = (x0+x1)/(N0+N1);

	double chi2stat =
			((x0-p*N0)*(x0-p*N0)/N0 +
			 (x1-p*N1)*(x1-p*N1)/N1)
			/(p*(1.0-p));

	Chi2StatisticalTest result;
	result.df(1);
	result.statistic(chi2stat);

	return result;
}


/** Compare two samples and test if they come from a single Poisson distribution with rate parameter lambda.
 *
 * For N packets, mu = lambda*N sigma^2 = lambda*N
 *
 * The ML estimate for lambda is x/N
 * Very similar to binomial for small p s.t. p*(1-p) -> p as 1-p -> 0
 *
 * 1 df since 2 measurements and 1 estimated parameter
 */


Chi2StatisticalTest EventCountComparison::poissonCompare(float N0,float x0,float N1,float x1)
{
	double lambda = (x0+x1)/(N0+N1);
//
//	cout << "Poisson test " << endl;
//	cout << "  x0=" << setw(20) << x0 << " N0=" << setw(20) << N0 << "  lambda0=" << x0/N0 << endl;
//	cout << "  x1=" << setw(20) << x1 << " N1=" << setw(20) << N1 << "  lambda1=" << x1/N1 << endl;
//	cout << "  lambda=" << lambda << endl;

	double chi2stat =
			((x0-lambda*N0)*(x0-lambda*N0)/N0 +
			 (x1-lambda*N1)*(x1-lambda*N1)/N1)/lambda;

	Chi2StatisticalTest result;
	result.statistic(chi2stat);
	result.df(1);
	return result;
}

void EventCountComparison::print(ostream&) const
{
}
