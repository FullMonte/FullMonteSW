/*
 * PercentDifference.cpp
 *
 *  Created on: Jun 29, 2017
 *      Author: jcassidy
 */

#include "PercentDifference.hpp"

bool PercentDifference::dimCheck() const
{
	return left()->dim() == right()->dim();
}

void PercentDifference::useAverageForReference()
{
	m_denominatorMode = Average;
}

PercentDifference::PercentDifference()
{
}

PercentDifference::~PercentDifference()
{
}

void PercentDifference::doUpdate()
{
	SpatialMap<float>* o = left()->clone();

	if (m_denominatorMode == Average)
		for(unsigned i=0;i<left()->dim();++i)
		{
			float l = left()->get(i);
			float r = right()->get(i);
			o->set(i,2.*(l-r)/(l+r));
		}
	else
	{
		cout << "PercentDifference::doUpdate() called with unrecognized denominator mode" << endl;
		for(unsigned i=0;i<left()->dim();++i)
			o->set(i,0);
	}

	m_output=o;
}


