#include <FullMonteSW/Queries/VolumeAccumulationProbe.hpp>

#include <FullMonteSW/OutputTypes/SpatialMap.hpp>

#include <FullMonteSW/Logging/FullMonteLogger.hpp>

#include <FullMonteSW/Geometry/Points.hpp>
#include <FullMonteSW/Geometry/TetraMesh.hpp>
#include <FullMonteSW/Geometry/FaceLinks.hpp>
#include <FullMonteSW/Geometry/MaterialSet.hpp>
#include <FullMonteSW/Geometry/Material.hpp>

#include <FullMonteSW/Geometry/Queries/TetraEnclosingPointByLinearSearch.hpp>

#include <FullMonteSW/OutputTypes/OutputDataType.hpp>

#include <functional>
#include <vector>

/**
 * Updates the 'total' of the probe based on the input parameters
 */
void VolumeAccumulationProbe::update() {
    //// checking inputs to make sure they are valid
    
    // input data must be SpatialMap<float>
    if(!m_input) {
        LOG_ERROR << "VolumeAccumulationProbe::update() no input data provided\n";
        m_total = -1; // negative m_total value is an error code that tells us what went wrong in the update function
        return;
    }
    
    // get SpatialMap<float> data
    const SpatialMap<float>* in_smap = dynamic_cast<const SpatialMap<float>*>(m_input);

    // check we can get this data
    if(!in_smap) {
        LOG_ERROR << "VolumeAccumulationProbe::update() input data could not be cast to SpatialMap<float>\n";
         m_total = -2; // negative m_total value is an error code that tells us what went wrong in the update function
        return;
    } else if(in_smap->spatialType() != AbstractSpatialMap::Volume) {
        LOG_ERROR << "VolumeAccumulationProbe::update() input data must be AbstractSpatialMap::Volume\n";
         m_total = -3; // negative m_total value is an error code that tells us what went wrong in the update function
        return;
    }
    if (in_smap->outputType() == AbstractSpatialMap::UnknownOutputType)
    {
        LOG_ERROR << "Could not identify type of data. This probe expects fluence, energy or photon weight data!" << endl;
         m_total = -4; // negative m_total value is an error code that tells us what went wrong in the update function
        return;
    }

    // ensure geometry is defined
    if(!m_geometry) {
        LOG_ERROR << "VolumeAccumulationProbe::update() no geometry provided\n";
         m_total = -5; // negative m_total value is an error code that tells us what went wrong in the update function
        return;
    }

    // get the TetraMesh geometry
    const TetraMesh* mesh = dynamic_cast<const TetraMesh*>(m_geometry);

    // geometry must be TetraMesh
    if(!mesh) {
        LOG_ERROR << "VolumeAccumulationProbe::update() could not convert geometry to TetraMesh\n";
         m_total = -6; // negative m_total value is an error code that tells us what went wrong in the update function
        return;
    }

    // must have 1 input value per tetra (-1 is for the 0 tetra automatically inserted by FullMonte, which is not present in the data)
    if((mesh->tetraCells()->size() - 1) != in_smap->dim()) {
        LOG_ERROR << "VolumeAccumulationProbe::update() input must have same number of entries as tetras in the mesh\n";
         m_total = -7; // negative m_total value is an error code that tells us what went wrong in the update function
        return;
    }

    if(m_p0_init == false)
    {
        LOG_ERROR << "VolumeAccumulationProbe::update() origin point not defined\n";
         m_total = -8; // negative m_total value is an error code that tells us what went wrong in the update function
        return;
    }

    if(m_p1_init == false && m_shape == Cylinder)
    {
        LOG_WARNING << "VolumeAccumulationProbe::update() second point for cylinder not defined. Reverting to spherical shape\n";
        m_shape = Sphere;
    }
    
    if(m_r <= 0.0) {
        LOG_DEBUG << "VolumeAccumulationProbe::update() radius of <=0 gets data from a single tetra. Taking absolute and reverting to spherical shape\n";
        m_r = abs(m_r);
        m_shape = Sphere;
    }

    Vector<3, float> slope;
    // Calculate the slope the two point define to create the line that later forms the cylinder
    if(m_shape == Cylinder)
    {
        slope[0] = m_p0[0] - m_p1[0];
        slope[1] = m_p0[1] - m_p1[1];
        slope[2] = m_p0[2] - m_p1[2];
    }
    
    // reset the total
    m_total = 0.0;
    m_volume = 0.0;

    // Get all volume data for each tetrahedron of the mesh
    const SpatialMap<float>* tetraVolumes = mesh->elementVolumes();
    if(m_r > 0) {
        //// Spherical probe ////
        // Accumulate all data in tetras in a spherical probe (either fully or partially)
        for(unsigned t = 1; t < mesh->tetraCells()->size(); t++) {
            // get the points that make up the tetra
            TetraByPointID IDps = mesh->tetraCells()->get(t);

            // determine if tetras is in (entirely or partially) in probe sphere
            // iterate over each point of the tetra
            unsigned points_in_probe = 0;
            for(unsigned i = 0; i < 4; i++) {
                // current point
                Point<3,double> p = mesh->points()->get(IDps[i]);
                float dist;
                switch(m_shape)
                {
                    //// Spherical probe ////
                    case Sphere:
                        // distance to center of sphere probe
                        dist = sqrt(pow(p[0]-m_p0[0], 2) + pow(p[1]-m_p0[1], 2) + pow(p[2]-m_p0[2], 2));
                        // if distance to point is less than the spheres radius, the probe contains the tetra
                        if(dist <= m_r) {
                            points_in_probe++;
                        }
                    break;
                    //// Cylindrical probe ////
                    case Cylinder:
                        // Calculate the vector between the point in the mesh and the point that defines the 1st end of cylinder plane
                        Vector<3, float> p_p0;
                        p_p0[0] = (float) m_p0[0]-p[0];
                        p_p0[1] = (float) m_p0[1]-p[1];
                        p_p0[2] = (float) m_p0[2]-p[2];

                        // Calculate the vector between the point in the mesh and the point that defines the 2nd end of cylinder plane
                        Vector<3, float> p_p1;
                        p_p1[0] = (float) m_p1[0]-p[0];
                        p_p1[1] = (float) m_p1[1]-p[1];
                        p_p1[2] = (float) m_p1[2]-p[2];

                        // Calculate the dot-product of slope and the plane-point vector
                        // this calculates the cos(theta) between the plane normal and the point-planepoint
                        float updownPlane_p0 = dot(slope, p_p0);
                        float updownPlane_p1 = dot(slope, p_p1);

                        // we need to check if the current point in the mesh lies between the two planes defined by the two points of the cylinder
                        // We are using the same normal for both cylinder plane definitions. This means that a point is inside the cylinder planes, if the
                        // point is on the same side as the normal vector for one plane (> 0) and on the opposite side of the normal vector for the other plane (< 0)
                        // < 0 opposite side of normal vector
                        // > 0 same side of normal vector
                        // = 0 on the plane
                        // p0 < 0 && p1 <  0 -> not valid,    *  <--|    <--| point is on the opposite side of both planes  
                        // p0 <= 0 && p1 >= 0 -> valid,        |-->  *    <--| point is on the opposite side of p0 and on the same side of p1
                        // p0 >=  0 && p1 <=  0 -> valid,        |-->  *    <--| point is on the same side of p0 and on the opposite side of p1
                        // p1 >  0 && p1 > 0 -> not valid,    |-->    |-->  * point is on the same side of both planes
                        if( (updownPlane_p0 <= 0 && updownPlane_p1 >= 0) || (updownPlane_p0 >= 0 && updownPlane_p1 <= 0) )
                        {
                            // The cross product between the slope of the line and the vector between the current point and the origin point of the cylinder
                            // the cross product of these two vectors is equal to the area of the parallelogram created by these vectors
                            // A = cross(p_p0, slope)
                            //
                            //        slope
                            //      _________
                            //     /        /
                            //    /    A   /  p_p0
                            //   /________/
                            Vector<3, float> crossprod = cross(p_p0, slope);
                            // distance to center line of cylinder probe
                            // The area A can also be calculated by the length of one side * the height (distance) to the opposite parallel line of the parallelogram
                            //
                            //        slope
                            //      _________
                            //     /     |  /
                            //    /    d | /  vector p_p0
                            //   /_______|/
                            // A = |slope| * distance
                            // In order to calcualte the distance, we need to solve for the distance d
                            // d = A / |slope|
                            dist = sqrt(dot(crossprod, crossprod)) / sqrt(dot(slope, slope));
                            // if distance to point is less than the spheres radius, the probe contains the tetra
                            if(dist <= m_r) {
                                points_in_probe++;
                            }
                        }
                    break;
                }
                    
            }

            // add the data of this tetra based on number of points in probe sphere
            // 2 cases:
            //  1) all points of tetras are in the probe
            //  2) any point of tetra is in probe sphere and we count partial tetras
            if((points_in_probe == 4) || (points_in_probe > 0 && m_partialTets)) {
                if (in_smap->outputType() == AbstractSpatialMap::Fluence)
                {
                    // the -1 is because the mesh has the 0 tetra added but the volume data does not have this tetra
                    float fluence = in_smap->get(t - 1);
                    float volume = tetraVolumes->get(t);
                    float fluence_x_volume = fluence * volume;
                    m_total += fluence_x_volume;
                    m_volume += volume;
                }
                else  // data is energy
                {
                    // the -1 is because the mesh has the 0 tetra added but the volume data does not have this tetra
                    m_total += in_smap->get(t - 1);
                }
            }
        }
    } else {
        //// Single point probe (i.e. single tetra) ////
        // Locate the tetra enclosing the point
        TetraMesh::TetraDescriptor t = mesh->rtree()->search(m_p0);
        
        // data for this tetra is all we want (single point probe)
        // the -1 is because the mesh has the 0 tetra added but the volume data does not have this tetra
        if(t.value() != 0)
            m_total = in_smap->get(t.value() - 1);
        else
        {
            LOG_ERROR << "VolumeAccumulationProbe::update() RTree did not find any Tetra enclosing the requested point.\n";
            m_total = -9;
        }
        
    }
}

