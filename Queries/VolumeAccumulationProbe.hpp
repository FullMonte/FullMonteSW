#ifndef QUERIES_FLUENCEPROBE_HPP_
#define QUERIES_FLUENCEPROBE_HPP_

#include <FullMonteSW/Geometry/TetraMesh.hpp>

#include <FullMonteSW/OutputTypes/SpatialMap.hpp>
#include <FullMonteSW/OutputTypes/OutputData.hpp>

#include <array>

class Geometry;
class MaterialSet;
class OutputData;
class AbstractSpatialMap;
template<class T>class SpatialMap;

/**
 * Probes a tetrahedral volume distribution for an accumulation value (e.g. Energy or Fluence).
 * If r > 0:
 *      Accumulates some data (e.g. energy or fluence) of all tetras entirely or partially (partialTets)
 *      inside the spherical probe (center = p0, radius = r).
 * If r == 0:
 *      A point probe which can be used to determine the data of a
 *      tetra at a specific position. Find the tetra containing point p0
 *      return that data.
 */
class VolumeAccumulationProbe {
public:
    // update calculates the total requested
    void update();

    // returns the total data
    float total() 
    {
        if (m_volume == 0)
            return m_total;
        else
            return m_total/m_volume;
        
    }
    
    // the geometry of mesh (must be a TetraMesh)
    const Geometry* geometry() const { return m_geometry; }
    void geometry(const Geometry* m) { m_geometry = m; }

    void setcylinder() { m_shape=Cylinder; }
    void setsphere() { m_shape=Sphere; }

    // the input data
    // NOTE: must be a SpatialMap<float> with 1 entry per tetrahedron in the mesh
    void source(OutputData* M) { m_input = M; }
    const OutputData* source() const { return m_input; }

    // the origin of the spherical probe or the first point of the cylider probe
    std::array<float,3> origin() { return m_p0; }
    void origin(std::array<float,3> p) { m_p0 = p; m_p0_init = true; }

    // the second point for the cylinder probe
    std::array<float,3> point1() { return m_p1; }
    void point1(std::array<float,3> p) { m_p1 = p; m_p1_init = true; }

    // the radius of the spherical probe. Set to 0 to get a single tetra
    float radius() { return m_r; }
    void radius(const float r) { m_r = r; }

    // whether to include partial tetras (1 or more points in the probe) in the accumulation
    void includePartialTetras(bool inc) { m_partialTets = inc; }
private:
    // the input data 
    // NOTE: must be a SpatialMap<float> with 1 entry per tetra (e.g. energy or fluence)
    const OutputData*       m_input=nullptr;

    // the geometry (must be a TetraMesh)
    const Geometry*         m_geometry=nullptr;

    // the center of the spherical probe
    std::array<float,3>     m_p0;
    bool                    m_p0_init = false;
    std::array<float,3>     m_p1;
    bool                    m_p1_init = false;

    // the radius of the probe. If 0, querying 1 tetra
    float                   m_r=0.0;

    // whether to include partial tetras in the spherical probe
    // false = all points of tetra must be in sphere
    // true = 1 or more points of tetra must be in sphere
    bool                    m_partialTets=true;

    // the output data
    float                   m_total=0.0;
    float                   m_volume=0.0;

    enum GeometryShape{Sphere, Cylinder};

    GeometryShape   m_shape=Sphere;
};

#endif

