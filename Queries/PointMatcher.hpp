/*
 * PointMatcher.hpp
 *
 *  Created on: Apr 25, 2018
 *      Author: jcassidy
 */

#ifndef QUERIES_POINTMATCHER_HPP_
#define QUERIES_POINTMATCHER_HPP_

class Points;
class Permutation;

#include <FullMonteSW/Warnings/Push.hpp>
#include <FullMonteSW/Warnings/Boost.hpp>
#include <boost/geometry/index/rtree.hpp>
#include <FullMonteSW/Warnings/Pop.hpp>

/** Matches each point within a query (Q) set to its closest match within another reference (R) set.
 *
 * It returns a Permutation of size N = |Q| such that:
 * 		R[P[i]] is the closest match to Q[i] for i=0..N-1
 * 		or P[i]=0 (dummy element) if the point is not matched
 *
 * Matching tolerance can be specified. If no reference point is available within that distance, it goes unmatched.
 *
 */

class PointMatcher
{
public:
	PointMatcher();
	~PointMatcher();

	void			reference(Points* P0);		///< Set the reference set to be matched against
	void 		query(Points* P);			///< Set of points to match

	void 		update();					///< Compute the matching

	unsigned		unmatchedCount() const;		///< Query how many point have gone unmatched

	void			tolerance(float eps);		///< Set the tolerance (Euclidean distance) for matching

	Permutation*	result();					///< Get the result

private:
	Points*			m_reference=nullptr;
	Points*			m_query=nullptr;

	Permutation*		m_permutation=nullptr;
	unsigned			m_unmatched=0;

	float			m_eps=1e-5f;

	// Boost Geometry RTree for point queries
	void createRTree();

	static constexpr std::size_t s_maxElementsPerNode=10;
	typedef boost::geometry::model::point<
			double,
			3,
			boost::geometry::cs::cartesian> PointCoord;

	typedef std::pair<PointCoord,unsigned> PointPair;

	boost::geometry::index::rtree<
		PointPair,
		boost::geometry::index::rstar<s_maxElementsPerNode>> m_pointIndex;

	// Match the points
	void match();
};



#endif /* QUERIES_POINTMATCHER_HPP_ */
