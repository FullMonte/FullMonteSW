/*
 * DoseHistogram.cpp
 *
 *  Created on: Apr 20, 2017
 *      Author: jcassidy
 */

#ifndef OUTPUTTYPES_EMPIRICALCDF_HPP_
#define OUTPUTTYPES_EMPIRICALCDF_HPP_

#include <vector>
#include <functional>
#include <utility>
#include <cmath>

#include <FullMonteSW/Warnings/Push.hpp>
#include <FullMonteSW/Warnings/Boost.hpp>
#include <boost/range/algorithm.hpp>
#include <FullMonteSW/Warnings/Pop.hpp>


#include "DoseHistogram.hpp"

#include <FullMonteSW/OutputTypes/OutputDataType.hpp>

#include <iostream>
#include <iomanip>
using namespace std;

const ordered_t ordered;

DoseHistogram::DoseHistogram()
{

}

DoseHistogram::~DoseHistogram()
{
	
}

DoseHistogram::const_iterator DoseHistogram::begin() const
{
	return m_histogram.begin();
}

DoseHistogram::const_iterator DoseHistogram::end() const
{
	return m_histogram.end();
}

float DoseHistogram::totalMeasure() const
{
	return m_histogram.empty() ? 0.0f : m_histogram.back().cmeasure;
}

std::size_t DoseHistogram::dim() const
{
	return m_histogram.size();
}

DoseHistogram::Element	DoseHistogram::get(unsigned i) const
{
	return m_histogram[i];
}

DoseHistogram::const_iterator DoseHistogram::doseAtPercentile(float) const
{
	throw std::logic_error("unimplemented");
	return m_histogram.begin();
}

DoseHistogram::const_iterator DoseHistogram::percentileOfDose(float) const
{
	throw std::logic_error("unimplemented");
	return m_histogram.begin();
}

const OutputDataType* pts[] = { OutputData::staticType(), nullptr };

const OutputDataType outputDataTypeInfo{ "DoseHistogram", pts };

const OutputDataType* DoseHistogram::s_type = &outputDataTypeInfo;

const OutputDataType* DoseHistogram::staticType()
{
	return s_type;
}

const OutputDataType* DoseHistogram::type() const
{
	return s_type;
}


#endif /* OUTPUTTYPES_EMPIRICALCDF_HPP_ */


