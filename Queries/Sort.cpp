/*
 * Sort.cpp
 *
 *  Created on: Sep 11, 2017
 *      Author: jcassidy
 */

#include "Sort.hpp"

#include <algorithm>
#include <FullMonteSW/Geometry/Permutation.hpp>
#include <vector>
#include <FullMonteSW/Geometry/Permutation.hpp>
#include <FullMonteSW/OutputTypes/SpatialMap.hpp>

#include <cmath>

using namespace std;

Sort::Sort()
{
}

Sort::~Sort()
{
}

void Sort::input(OutputData* d)
{
	m_input=d;
}

void Sort::setAscending()
{
	m_descending=false;
}

void Sort::setDescending()
{
	m_descending=true;
}

void Sort::excludeNaNs(bool e)
{
	m_excludeNaNs = e;
}

void Sort::update()
{
	vector<unsigned> idx;

	unsigned N=0;

	if (!m_input)
		cout << "Sort::update() failed for lack of input data" << endl;
	else if (SpatialMap<float>* f=dynamic_cast<SpatialMap<float>*>(m_input))
	{
		N=f->dim();
		idx.resize(f->dim());

		// create permutation indices with NaNs in reversed order at end
		vector<unsigned>::iterator validEnd=idx.begin();		// points to one-past-end of valid values, ie valid are [begin,validEnd)
		vector<unsigned>::iterator nanBegin=idx.end();			// points to one-past-end of nan values, ie. nan are [nanBegin,end)

		for(unsigned i=0;i<idx.size();++i)
			if (isnan(f->get(i)))
				*(--nanBegin) = i;
			else
				*(validEnd++) = i;

		assert(nanBegin == validEnd);

		// sort valid values by the field they refer to
		sort(idx.begin(), validEnd, [this,f](unsigned lhs,unsigned rhs){ return (f->get(lhs) < f->get(rhs))^m_descending; });

		if (m_excludeNaNs)
			idx.resize(validEnd-idx.begin());	// drop the NaNs off the end
		else
			reverse(nanBegin, idx.end());		// reverse NaN values to get back to stable order
	}
	else
		cout << "Sort::update() failed because input type can't be cast" << endl;

	if (idx.size() > 0)
	{
		Permutation* o = new Permutation();
		o->resize(idx.size());
		o->sourceSize(N);
		for(unsigned i=0;i<idx.size();++i)
			o->set(i,idx[i]);
		m_output = o;
	}
	else
		cout << "Sort::update() failed to produce output" << endl;
}

Permutation* Sort::output() const
{
	return m_output;
}
