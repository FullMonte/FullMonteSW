/*
 * PercentDifference.hpp
 *
 *  Created on: Jun 29, 2017
 *      Author: jcassidy
 */

#ifndef QUERIES_PERCENTDIFFERENCE_HPP_
#define QUERIES_PERCENTDIFFERENCE_HPP_

#include "DataDifference.hpp"
#include <FullMonteSW/OutputTypes/SpatialMap.hpp>

class PercentDifference : public DataDifference<SpatialMap<float>>
{
public:
	PercentDifference();
	virtual ~PercentDifference();


	/// Computes as 2(A-B)/(A+B) to avoid zero denominator (default)
	void useAverageForReference();

protected:
	virtual bool dimCheck() const override;
	virtual void doUpdate() override;

private:
	enum Mode { Average, A, B };

	Mode m_denominatorMode=Average;
};



#endif /* QUERIES_PERCENTDIFFERENCE_HPP_ */
