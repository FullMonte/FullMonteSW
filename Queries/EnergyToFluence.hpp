/*
 * FluenceConverter.hpp
 *
 *  Created on: Feb 20, 2016
 *      Author: jcassidy
 */

#ifndef OUTPUTTYPES_FLUENCECONVERTER_HPP_
#define OUTPUTTYPES_FLUENCECONVERTER_HPP_

class Geometry;
class MaterialSet;
class OutputData;
class MCKernelBase;
class AbstractSpatialMap;
template<class T>class SpatialMap;

#include <vector>

class EnergyToFluence
{
public:
	EnergyToFluence();
	~EnergyToFluence();

	void							data(OutputData* M);
	const OutputData*				data() const;

	void							kernel(const MCKernelBase* K);
	const MCKernelBase*				kernel() const;

	void 							update();

	OutputData*						result() const;

	void								inputPhotonWeight(); 	
	void								inputEnergy(); 			
	void								inputFluence();			
	void								inputEnergyPerVolume(); 

	void								outputPhotonWeight();	// PhotonWeight (PW) is the raw results from the simulation - Unit: unit-less values
	void								outputFluence();		// Fluence (Phi) = Energy / Area or = Energy / Volume / muA - Unit: W/m^2 or J/m^2
	void								outputEnergy();			// Energy (E) = PhotonWeight * TotalEnergy/TotalPackets - Unit: Joules (J) or Watt (W)
	void								outputEnergyPerVolume();// Energy / Volume - Unit: W/m^3 or J/m^3 

private:

	/// Holds the signed integer powers in a field. eg. fluence Phi = PW/V/mu_a*TotalEnergy/TotalPackets -> measure=-1, muA=-1, energyUnit=1
	struct Powers
	{
		int muA;
		int measure;
		int energyUnit;

		Powers operator-(const Powers rhs) const { return Powers{muA-rhs.muA, measure-rhs.measure, energyUnit-rhs.energyUnit}; }
	};


	enum Field 			{ UnknownField, Energy, EnergyPerVolume, Fluence, PhotonWeight };
	enum MeasureType		{ UnknownMeasureType, Area, Volume };

	static Powers powersForField(Field f,MeasureType mt);
	static const char* fieldName(Field f);
	static const char* measureTypeName(MeasureType mt);

	void updateMeasures();
	void updateMaterials();

	/// Copies input and applies muA and measure (area/volume) multipliers as needed
	SpatialMap<float>* calculate(const SpatialMap<float>* input,Powers op);

	/// Multiplies two fields elementwise x[i] = x[i]*k[i]^(power)  with power = -1, 0 1
	void multiplyFieldBy(SpatialMap<float>* x,const SpatialMap<float>* k,int power);

	const Geometry*						m_geometry=nullptr;
	const MaterialSet*					m_materials=nullptr;
	const MCKernelBase*					m_kernel=nullptr;

	const OutputData*					m_input=nullptr;
	OutputData*							m_output=nullptr;

	Field								m_requestedInputField=UnknownField;
	Field								m_requestedOutputField=UnknownField;

	float								m_simulatedEnergy=0;
	unsigned long long					m_simulatedPackets=0;

	SpatialMap<float>*					m_muA=nullptr;

	SpatialMap<float>*					m_measure=nullptr;
	MeasureType							m_measureType=UnknownMeasureType;
};

#endif /* OUTPUTTYPES_FLUENCECONVERTER_HPP_ */

