/*
 * Chi2SetFixture.hpp
 *
 *  Created on: Jul 14, 2017
 *      Author: jcassidy
 */

#ifndef QUERIES_CHI2SETFIXTURE_HPP_
#define QUERIES_CHI2SETFIXTURE_HPP_

class OutputDataCollection;
#include <FullMonteSW/Queries/Chi2Homogeneity.hpp>
#include <vector>

struct Chi2SetFixture
{
	void 		leftSet(OutputDataCollection*);

	void		run();

	Chi2Homogeneity			m_tester;
	OutputDataCollection*	m_leftSet=nullptr;
	std::vector<float>		m_pCrit{ 0.1f, 0.05f, 0.01f, 0.001f };
	std::vector<unsigned>	m_rejects;
	unsigned				m_runs=0;
};

// generate BOOST_TEST_MESSAGE with results of Chi2 test

#define BOOST_TEST_CHI2_PRINT(msg,T)	{ stringstream ss; \
	ss << msg << " vs. " << T.m_tester.rightRuns() << 'x' << T.m_tester.rightPackets() << endl; \
	for(unsigned i=0;i<T.m_pCrit.size();++i) \
		ss << "  Rejected " << fixed << setprecision(4) << T.m_rejects[i] \
		<< "/" << T.m_runs << " (" << float(T.m_rejects[i])/float(T.m_runs) << ") at p=" << T.m_pCrit[i] << endl; \
	BOOST_TEST_MESSAGE(ss.str()); \
}




#endif /* QUERIES_CHI2SETFIXTURE_HPP_ */
