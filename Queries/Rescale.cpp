/*
 * Rescale.cpp
 *
 *  Created on: Jun 15, 2017
 *      Author: jcassidy
 */

#include "Rescale.hpp"

#include <FullMonteSW/OutputTypes/SpatialMap.hpp>

Rescale::Rescale()
{

}

Rescale::~Rescale()
{

}

void Rescale::factor(float f)
{
	m_factor=f;
}

void Rescale::source(OutputData* d)
{
	m_data=d;
}

void Rescale::update()
{
//	if (m_output)
//		delete m_output;

	if (!m_data)
	{

	}

	const SpatialMap<float>* f = dynamic_cast<const SpatialMap<float>*>(m_data);
	if (!f)
	{

	}

	SpatialMap<float>* o = new SpatialMap<float>(f->dim()-1);
	o->spatialType(f->spatialType());
	for(unsigned i=0;i<o->dim();++i)
		o->set(i,f->get(i+1)*m_factor);

	m_output=o;
}

OutputData* Rescale::output() const
{
	return m_output;
}


