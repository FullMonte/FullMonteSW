/*
 * BasicStats.cpp
 *
 *  Created on: Jun 21, 2017
 *      Author: jcassidy
 */

#include "BasicStats.hpp"

#include <vector>
#include <limits>

#include <FullMonteSW/OutputTypes/OutputDataCollection.hpp>
#include <FullMonteSW/OutputTypes/OutputData.hpp>
#include <FullMonteSW/OutputTypes/SpatialMap.hpp>
#include <FullMonteSW/OutputTypes/SpatialMap2D.hpp>

#include <FullMonteSW/Warnings/Push.hpp>
#include <FullMonteSW/Warnings/Boost.hpp>
#include <boost/range/algorithm.hpp>
#include <FullMonteSW/Warnings/Pop.hpp>


#include <cmath>


using namespace std;

BasicStats::BasicStats()
{

}

BasicStats::~BasicStats()
{

}

void BasicStats::clear()
{
	m_sum_x.clear();
	m_sum_xx.clear();
	m_min.clear();
	m_max.clear();
	m_N=0;
}

void BasicStats::doUpdate(unsigned Nds,SpatialMap<float>* v0)
{
	m_N = Nds;

	m_nnz.resize(v0->dim());
	boost::fill(m_nnz, 0);

	m_sum_x.resize(v0->dim());
	boost::fill(m_sum_x,0.0);

	m_sum_xx.resize(v0->dim());
	boost::fill(m_sum_xx,0.0);

	m_min.resize(v0->dim());
	boost::fill(m_min, std::numeric_limits<float>::infinity());

	m_max.resize(v0->dim());
	boost::fill(m_max, -std::numeric_limits<float>::infinity());

	for(unsigned r=0;r<Nds;++r)
	{
		const SpatialMap<float>* v = static_cast<const SpatialMap<float>*>(input()->getByIndex(r));

		for(unsigned c=0;c<v0->dim();++c)
		{
			float x = v->get(c);
			m_sum_x[c] += x;
			m_sum_xx[c] += x*x;
			m_min[c] = std::min(x,m_min[c]);
			m_max[c] = std::max(x,m_max[c]);

			if (x > 0)
				m_nnz[c]++;
		}
	}
}

OutputData* BasicStats::min() const
{
	SpatialMap<float>* o = static_cast<SpatialMap<float>*>(input()->getByIndex(0)->clone());
	for(unsigned i=0;i<o->dim();++i)
		o->set(i,m_min[i]);
	return o;
}

OutputData* BasicStats::max() const
{
	SpatialMap<float>* o = static_cast<SpatialMap<float>*>(input()->getByIndex(0)->clone());
	for(unsigned i=0;i<o->dim();++i)
		o->set(i,m_max[i]);
	return o;
}

OutputData* BasicStats::mean() const
{
	SpatialMap<float>* o = static_cast<SpatialMap<float>*>(input()->getByIndex(0)->clone());
	float k = 1.0f/float(m_N);
	for(unsigned i=0;i<o->dim();++i)
		o->set(i,m_sum_x[i]*k);
	return o;
}

OutputData* BasicStats::sum() const
{
	SpatialMap<float>* o = static_cast<SpatialMap<float>*>(input()->getByIndex(0)->clone());

	for(unsigned i=0;i<o->dim();++i)
		o->set(i,m_sum_x[i]);

	return o;
}

OutputData* BasicStats::stddev() const
{
	SpatialMap<float>* o = static_cast<SpatialMap<float>*>(input()->getByIndex(0)->clone());

	for(unsigned i=0;i<o->dim();++i)
	{
		o->set(i, sqrt((m_sum_xx[i] - m_sum_x[i]*m_sum_x[i]/double(m_N))/double(m_N-1)));
	}

	return o;
}

OutputData* BasicStats::variance() const
{
	SpatialMap<float>* o = static_cast<SpatialMap<float>*>(input()->getByIndex(0)->clone());
	for(unsigned i=0;i<o->dim();++i)
		o->set(i,(m_sum_xx[i] - m_sum_x[i]*m_sum_x[i]/double(m_N))/double(m_N-1));
	return o;
}

OutputData* BasicStats::nnz() const
{
	AbstractSpatialMap* i = static_cast<AbstractSpatialMap*>(input()->getByIndex(0));
	OutputData* o=nullptr;

	if (const auto i2 = dynamic_cast<const SpatialMap2D<float>*>(i))
		o = new SpatialMap2D<unsigned>(
			i2->dims()[0],
			i2->dims()[1],
			m_nnz,
			i->spatialType(),
			AbstractSpatialMap::Scalar);
	else
		o = new SpatialMap<unsigned>(m_nnz,i->spatialType(),AbstractSpatialMap::Scalar);

	return o;
}

OutputData* BasicStats::cv() const
{
	SpatialMap<float>* o = static_cast<SpatialMap<float>*>(input()->getByIndex(0)->clone());
	for(unsigned i=0;i<o->dim();++i)
	{
		float cv=0;
		if (m_sum_x[i] != 0)
		{
			float sigma = sqrt((m_sum_xx[i] - m_sum_x[i]*m_sum_x[i]/double(m_N))/double(m_N-1));
			float mu = m_sum_x[i]/double(m_N);
			cv=sigma/mu;
		}
		o->set(i,cv);

	}
	return o;
}



