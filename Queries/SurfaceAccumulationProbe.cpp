#include <FullMonteSW/Queries/SurfaceAccumulationProbe.hpp>

#include <FullMonteSW/OutputTypes/SpatialMap.hpp>

#include <FullMonteSW/Logging/FullMonteLogger.hpp>

#include <FullMonteSW/Geometry/Points.hpp>
#include <FullMonteSW/Geometry/TetraMesh.hpp>
#include <FullMonteSW/Geometry/FaceLinks.hpp>
#include <FullMonteSW/Geometry/MaterialSet.hpp>
#include <FullMonteSW/Geometry/Material.hpp>
#include <FullMonteSW/Kernels/Software/Material.hpp>
#include <FullMonteSW/Geometry/Partition.hpp>

#include <FullMonteSW/Geometry/Queries/TetraEnclosingPointByLinearSearch.hpp>

#include <FullMonteSW/OutputTypes/OutputDataType.hpp>

#include <functional>
#include <vector>

/**
 * Updates the 'total' of the probe based on the input parameters
 */
void SurfaceAccumulationProbe::update() {
    //// checking inputs to make sure they are valid
    
    // input data must be SpatialMap<float>
    if(!m_input) {
        LOG_ERROR << "SurfaceAccumulationProbe::update() no input data provided\n";
        m_total = -1;
        return;
    }
    
    // get SpatialMap<float> data
    const SpatialMap<float>* in_smap = dynamic_cast<const SpatialMap<float>*>(m_input);

    // check we can get this data
    if(!in_smap) {
        LOG_ERROR << "SurfaceAccumulationProbe::update() input data could not be cast to SpatialMap<float>\n";
        m_total = -2;
        return;
    } else if(in_smap->spatialType() != AbstractSpatialMap::Surface) {
        LOG_ERROR << "SurfaceAccumulationProbe::update() input data must be AbstractSpatialMap::Surface\n";
        m_total = -3;
        return;
    }
    if (in_smap->outputType() == AbstractSpatialMap::UnknownOutputType)
    {
        LOG_ERROR << "Could not identify type of data. This probe expects fluence, energy or raw photon weight data!" << endl;
        m_total = -4;
        return;
    }

    // ensure geometry is defined
    if(!m_geometry) {
        LOG_ERROR << "SurfaceAccumulationProbe::update() no geometry provided\n";
        m_total = -5;
        return;
    }

    // get the TetraMesh geometry
    const TetraMesh* mesh = dynamic_cast<const TetraMesh*>(m_geometry);

    // geometry must be TetraMesh
    if(!mesh) {
        LOG_ERROR << "SurfaceAccumulationProbe::update() could not convert geometry to TetraMesh\n";
        m_total = -6;
        return;
    }

    // must have 1 input value per tetra
    if(mesh->faceTetraLinks()->size() != in_smap->dim()) {
        LOG_ERROR << "SurfaceAccumulationProbe::update() input must have same number of entries as tetra faces in the mesh\n";
        m_total = -7;
        return;
    }
    
    if(m_r == 0.0) {
        LOG_DEBUG << "SurfaceAccumulationProbe::update() radius of 0 gets data from a single face\n";
    }
    // Get the surface areas from the mesh (only needed for fluence data)
    SpatialMap<float>* areas;
    if (in_smap->outputType() == AbstractSpatialMap::Fluence)
    {
        areas = mesh->surfaceAreas();
    }
    // reset the outputs
    m_total = 0.0;
    m_surface = 0.0;

    if(m_r > 0) {
        //// Spherical probe ////
        // Accumulate all data in tetra faces in a spherical probe (either fully or partially)
        // we start at 1 because the the first entry (index 0) is the 0 face and that never scores energy/fluence
        for(unsigned f=1; f < mesh->faceTetraLinks()->size(); f++) {
            // get the points that make up the face
            FaceTetraLink FTL = mesh->faceTetraLinks()->get(f);
            // Get the materialIDs from both tetras that are connected by the face
            unsigned downMatID = get(region, *mesh, FTL.downTet().T);
            unsigned upMatID = get(region, *mesh, FTL.upTet().T);
            unsigned points_in_probe = 0;
            
            if(upMatID != downMatID || upMatID == 0 || downMatID == 0)
            {
                // determine if the tetra face is (entirely or partially) in probe sphere
                // iterate over each point of the tetra face 
                for(unsigned i = 0; i < 3; i++) {
                    // current point
                    Point<3,double> p = mesh->points()->get(FTL.pointIDs[i].value());

                    // distance to center of probe
                    float dist = sqrtf(pow(p[0]-m_p0[0], 2) + pow(p[1]-m_p0[1], 2) + pow(p[2]-m_p0[2], 2));

                    // if distance to point is less than the spheres radius, the probe contains the triangle
                    if(dist <= m_r) {
                        points_in_probe++;
                    }
                }
            }

            // add the data of this face based on number of points in probe sphere
            // 2 cases:
            //  1) all points of face are in the probe
            //  2) any point of face is in probe sphere and we count partial faces
            if((points_in_probe == 3) || (points_in_probe > 0 && m_partialSurfaces)) 
            {
                if (in_smap->outputType() == AbstractSpatialMap::Fluence)
                {
                    float area = areas->get(f);
                    float fluence = in_smap->get(f);
                    float fluence_x_area = fluence * area;
                    m_total += fluence_x_area;
                    m_surface += area;
                }
                else // data is energy
                {
                    m_total += in_smap->get(f);
                }
            }
        }
    } else {
        //// Single point probe (i.e. single face) ////
        // Locate the tetra enclosing the point
        TetraMesh::TetraDescriptor t = mesh->rtree()->search(m_p0);
        unsigned currMatID = get(region,*mesh,t);
        // get all faces belonging to the tetra
        std::array<TetraFaceLink, 4UL> TFL = mesh->tetraFaceLinks()->get(t.value());
        vector<unsigned> index;
        for(unsigned i=0; i <4; i++)
        {
            unsigned adjMatID = get(region,*mesh,TFL[i].tetraID);
            if(currMatID != adjMatID)
            {
                index.push_back(i);
            }
        }

        for(unsigned i=0; i<index.size(); i++)
        {
            if (in_smap->outputType() == AbstractSpatialMap::Fluence)
            {
                // data for the faces of this tetra is all we want (single point probe)
                float area = areas->get(TetraMesh::FaceDescriptor(TFL[index[i]].faceID).value());
                float fluence = in_smap->get(TetraMesh::FaceDescriptor(TFL[index[i]].faceID).value());
                float fluence_x_area = fluence * area;
                m_total += fluence_x_area;
                m_surface += area;
            }
            else
            {
                m_total += in_smap->get(TetraMesh::FaceDescriptor(TFL[index[i]].faceID).value());
            }
        }
    }
}

