/*
 * Layered.cpp
 *
 *  Created on: Jun 1, 2017
 *      Author: jcassidy
 */

#include "Layered.hpp"
#include "Layer.hpp"
#include "Partition.hpp"

#include <FullMonteSW/Logging/FullMonteLogger.hpp>
#include <FullMonteSW/Warnings/Push.hpp>
#include <FullMonteSW/Warnings/Boost.hpp>
#include <boost/math/constants/constants.hpp>
#include <FullMonteSW/Warnings/Pop.hpp>

#include <FullMonteSW/OutputTypes/SpatialMap2D.hpp>

using namespace std;

Layered::Layered()
{
}

Layered::~Layered()
{
}

SpatialMap<float>*		Layered::elementVolumes() const
{
	vector<float>	V(m_Nr*m_Nz+1,0.0f);

	V[0] = 0;

	for(unsigned ri=1;ri<=m_Nr;++ri)
		for(unsigned zi=1;zi<=m_Nz;++zi)
		{
			unsigned i = indexForBin(CylIndex { ri,zi });
			V[i] = binVolume(i);
		}

	return new SpatialMap2D<float>(m_Nr,m_Nz,std::move(V), AbstractSpatialMap::Volume, AbstractSpatialMap::Scalar, AbstractSpatialMap::UnknownOutputType);
}

SpatialMap<float>*		Layered::surfaceAreas() const
{
	LOG_WARNING << "Layered::surfaceAreas() is null" << endl;
	return nullptr;
}

SpatialMap<float>*		Layered::directedSurfaceAreas() const
{
	LOG_WARNING << "Layered::directedSurfaceAreas() is null" << endl;
	return nullptr;
}

void Layered::resolution(float dr,float dz)
{
	m_dr=dr;
	m_dz=dz;
}

CylCoord Layered::resolution() const
{
	return CylCoord { m_dr, m_dz };
}

CylIndex Layered::dims() const
{
	return CylIndex { m_Nr, m_Nz };
}

void Layered::dims(unsigned Nr,unsigned Nz)
{
	m_Nr = Nr;
	m_Nz = Nz;
}

void Layered::extent(float R,float Z)
{
	m_dr = R/float(m_Nr);
	m_dz = Z/float(m_Nz);
}

CylCoord Layered::extent() const
{
	return { m_dr*float(m_Nr), m_dz*float(m_Nz) };
}

unsigned Layered::layerCount() const
{
	return m_layers.size();
}

Layer* Layered::layer(unsigned i) const
{
	if (i==0)
		throw std::out_of_range("Layered::layer(i) starts at i=1");
	return m_layers.at(i-1);
}

void Layered::addLayer(Layer* l)
{
	m_layers.push_back(l);
}

void Layered::angularBins(unsigned Na)
{
	m_Na = Na;
}

unsigned Layered::angularBins() const
{
	return m_Na;
}

float Layered::angularResolution() const
{
	return boost::math::constants::pi<float>()/float(m_Na);
}


/** Compute the 1D index for a 2D bin.
 *
 * ri = 1..Nr
 * zi = 1..Nz
 *
 * i = 1..Nr*Nz (0 is special not-a-bin)
 */

unsigned			Layered::indexForBin(CylIndex ci) const
{
	if (ci.zi == 0U || ci.ri == 0U)
		return 0U;
	else
		return (ci.ri-1)*m_Nz + ci.zi;
}

CylIndex	Layered::binFromIndex(unsigned i) const
{
	CylIndex ci{0U,0U};
	if (i != 0)
	{
		ci.ri = ((i-1) / m_Nz) + 1;
		ci.zi = ((i-1) % m_Nz) + 1;
	}
	return ci;
}

float Layered::binVolume(unsigned i) const
{
	if (i==0)
		return 0.0f;
	else
	{
		CylIndex bin = binFromIndex(i);
		return binVolume(bin);
	}
}

float Layered::binVolume(CylIndex ci) const
{
	// radial & depth bins start at 1, so ri*dr is the outer diameter and (ri-1)*dr is inner
	// pi * (ro^2 - (ro-dr)^2) * dz
	return boost::math::constants::pi<float>()*m_dz*m_dr*m_dr*float(2*ci.ri-1);
}

std::ostream& operator<<(std::ostream& os,CylIndex ci)
{
	const auto w = os.width();
	return os << '[' << ci.ri << "][" << setw(w) << ci.zi << ']';
}


unsigned Layered::bins() const
{
	return m_Nr*m_Nz+1;
}


/** Build a regions map using the layer defs.
 *
 */

void Layered::buildRegions()
{
	Partition* p = new Partition();

	float dzBin=resolution().z;
	float zLayerMax=layer(1)->thickness();

	p->resize(bins());

	unsigned Nr = dims().ri;
	unsigned Nz = dims().zi;

	unsigned l=1;

	p->assign(0,0);

	for(unsigned zi=1;zi <= Nz;++zi)
	{
		float zBinMin = float(zi-1)*dzBin;

		while(zBinMin > zLayerMax-dzBin/2)
		{
			++l;
			zLayerMax += layer(l)->thickness();
		}
		for(unsigned ri=1;ri <= Nr; ++ri)
		{
			CylIndex c;
			c.ri = ri;
			c.zi = zi;

			p->assign(indexForBin(c),l);
		}
	}
	regions(p);
}
