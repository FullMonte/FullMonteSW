/*
 * DynamicIndexRelabel.cpp
 *
 *  Created on: May 28, 2018
 *      Author: jcassidy
 */

#include "DynamicIndexRelabel.hpp"
#include "Permutation.hpp"

void DynamicIndexRelabel::reserve(unsigned N)
{
	if (N > m_remap.size())
		m_remap.resize(N,-1U);
}

unsigned DynamicIndexRelabel::operator()(unsigned i)
{
	if (i >= m_remap.size())
		m_remap.resize(i+1,-1U);
	if (m_remap[i] == -1U)
		m_remap[i] = m_nextFreeIndex++;
	return m_remap[i];
}

Permutation* DynamicIndexRelabel::permutation() const
{
	Permutation* perm = new Permutation();
	perm->resize(m_nextFreeIndex);

	for(unsigned i=0;i<m_remap.size();++i)
		if (m_remap[i] != -1U)
			perm->set(m_remap[i],i);

	return perm;
}


unsigned DynamicIndexRelabel::outputCount() const
{
	return m_nextFreeIndex;
}
