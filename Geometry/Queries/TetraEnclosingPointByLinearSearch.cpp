/*
 * TetraEnclosingPointByLinearSearch.cpp
 *
 *  Created on: May 29, 2017
 *      Author: jcassidy
 */

#include "TetraEnclosingPointByLinearSearch.hpp"

#include <FullMonteSW/Logging/FullMonteLogger.hpp>
#include <FullMonteSW/Geometry/TetraMesh.hpp>
#include <FullMonteSW/Geometry/Vector.hpp>
#include <FullMonteSW/Geometry/StandardArrayKernel.hpp>
#include <FullMonteSW/Kernels/Software/Emitters/TetraLookupCache.hpp>
#include <iostream>

using namespace std;

#define NO		0
#define YES		1
#define MAYBE	2

TetraEnclosingPointByLinearSearch::TetraEnclosingPointByLinearSearch()
{
}

TetraEnclosingPointByLinearSearch::~TetraEnclosingPointByLinearSearch()
{
}

void TetraEnclosingPointByLinearSearch::mesh(const TetraMesh* M)
{
	m_mesh=M;
}

void TetraEnclosingPointByLinearSearch::point(Point<3,double> p)
{
	m_point=p;
}

int TetraEnclosingPointByLinearSearch::isPointInTetra(Point<3,double> p, unsigned int IDt) {
	return isPointInTetra(p, TetraMesh::TetraDescriptor(IDt));
}

int TetraEnclosingPointByLinearSearch::isPointInTetra(Point<3,double> p, TetraMesh::TetraDescriptor IDtd) {
	array<double,4> h;
	array<Point<3,double>,4> P = get(point_coords, *m_mesh, IDtd);

	// loop over all faces, computing height of the point over each face
	bool in = false;
	bool maybe = false;

	for(unsigned f = 0; f < 4; ++f) {
		const auto n = normalize(
			cross(
				P[tetra_face_opposite_point_indices[f].faceidx[0]],
				P[tetra_face_opposite_point_indices[f].faceidx[1]],
				P[tetra_face_opposite_point_indices[f].faceidx[2]]
			)
		);

		h[f] = dot(n, p - P[tetra_face_opposite_point_indices[f].faceidx[0]]);
	}

	in = h[0] >= 0 && h[1] >= 0 && h[2] >= 0 && h[3] >= 0;
	maybe = h[0] >= -m_heightEpsilon && h[1] >= -m_heightEpsilon && h[2] >= -m_heightEpsilon && h[3] >= -m_heightEpsilon;

	if(in) {
		return YES;
	} else if(maybe) {
		return MAYBE;
	} else {
		return NO;
	}
}

bool TetraEnclosingPointByLinearSearch::searchCache(TetraLookupCache* cache)
{
	m_inside.clear();
	m_maybe.clear();

	LOG_DEBUG << "Lookup in the cache - Searching for tetra to enclose " << m_point[0] << ' ' << m_point[1] << ' ' << m_point[2] << endl;

	auto cache_list = cache->getList();

	bool in=false;
	bool maybe=false;	

	for(auto  tp = cache_list->begin(); tp!= cache_list->end() ; tp++)  {
		TetraMesh::TetraDescriptor tetra = *tp;

		int status = isPointInTetra(m_point, tetra);

		in |= (status == YES);
		maybe |= (status == MAYBE);

		if(in) {
			m_inside.push_back(tetra);
		} else if(maybe) {
			m_maybe.push_back(tetra);
		}

		// early breakout
		if(in) {
			return true;
		} 
	}

	if (in|maybe) {
		return true;
	} else {
		//no tetra found. 
		LOG_DEBUG << "Could not find a tetra enclosing that point in the cache" << endl;
        return false;
	}
}

bool TetraEnclosingPointByLinearSearch::searchVector(std::vector<TetraMesh::TetraDescriptor>& vals) {
	m_inside.clear();
	m_maybe.clear();

	LOG_DEBUG << "Lookup in the vector - Searching for tetra to enclose " << m_point[0] << ' ' << m_point[1] << ' ' << m_point[2] << endl;

	bool in=false;
	bool maybe=false;	

	for(unsigned int i = 0; i < vals.size(); i++)  {
		TetraMesh::TetraDescriptor tetra = vals[i];

		int status = isPointInTetra(m_point, tetra);

		in |= (status == YES);
		maybe |= (status == MAYBE);

		if(in) {
			m_inside.push_back(tetra);
		} else if(maybe) {
			m_maybe.push_back(tetra);
		}

		// early breakout
		if(in) {
			return true;
		} 
	}

	if (in|maybe) {
		return true;
	} else {
		//no tetra found. 
		LOG_DEBUG << "Could not find a tetra enclosing that point in the cache" << endl;
        return false;
	}
}

void TetraEnclosingPointByLinearSearch::update()
{
	m_inside.clear();
	m_maybe.clear();

    LOG_DEBUG << "TetraEnclosingPointByLinearSearch::update - Searching for tetra to enclose " << m_point[0] << ' ' << m_point[1] << ' ' << m_point[2] << endl;

	for(const auto t : m_mesh->tetras())
	{
		int status = isPointInTetra(m_point, t);
		
		bool in = (status == YES);
		bool maybe = (status == MAYBE);

		if(in) {
			m_inside.push_back(t);
		} else if(maybe) {
			m_maybe.push_back(t);
		}

		if (in | maybe) {
		    LOG_DEBUG << "  Tetra " << t.value() << " " << (in ? "encloses" : "may enclose") << " point" << endl;;
		}
	}
}

TetraMesh::TetraDescriptor TetraEnclosingPointByLinearSearch::tetra() const
{
	if (m_inside.size() > 0)
	{
		if (m_inside.size() > 1) {
			LOG_WARNING << "TetraEnclosingPointByLinearSearch::tetra() found multiple (" << m_inside.size() << ") tetras; returning first" << endl;
        }

		return m_inside.front();
	}
	else if (m_maybe.size() > 1)
	{
		LOG_WARNING << "TetraEnclosingPointByLinearSearch::tetra() found no tetras but " << m_maybe.size() << " near-matches; returning first" << endl;
		return m_maybe.front();
	}
	else
	{
		LOG_ERROR << "TetraEnclosingPointByLinearSearch::tetra() no enclosing tetra found for point at (" << m_point[0] << "," << m_point[1] << "," << m_point[2] << ")" << endl;
	}
	
	return TetraMesh::TetraDescriptor();
}

unsigned TetraEnclosingPointByLinearSearch::tetraID() const
{
	return get(id,*m_mesh,tetra());
}
