/*
 * RemapRegions.hpp
 *
 *  Created on: Feb 9, 2018
 *      Author: jcassidy
 */

#ifndef GEOMETRY_REMAPREGIONS_HPP_
#define GEOMETRY_REMAPREGIONS_HPP_

class Partition;
class RemapTable;

/** Remaps (changes the ID of) entire subregions within a Partition
 *
 */

class RemapRegions
{
public:
	RemapRegions();
	virtual ~RemapRegions();

	void partition(Partition* p);

	void addMapping		(unsigned from,unsigned to);		/// Add a mapping from -> to
	void keepUnchanged	(unsigned from);					/// Mark a region to be preserved
	void removeMapping	(unsigned from);					/// Remove a mapping

	/// Indicate that fields without a remapping should be left as-is
	void leaveUnspecifiedAsIs();

	/// Remap anything not explicitly specified to a given value
	void remapUnspecifiedTo(unsigned to);

	void update();

	Partition* result();

private:
	Partition*	m_inputPartition=nullptr;
	Partition*	m_result=nullptr;

	/// y[i] = m_remapTable[x[i]]
	RemapTable*	m_remapTable=nullptr;
};



#endif /* GEOMETRY_REMAPREGIONS_HPP_ */
