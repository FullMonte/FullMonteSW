#ifndef GEOMETRY_PLACEMENT_CYLINDERSURFACESOURCEBUILDER_HPP_
#define GEOMETRY_PLACEMENT_CYLINDERSURFACESOURCEBUILDER_HPP_


#include <FullMonteSW/Geometry/Placement/SurfaceSourceBuilder.hpp>

#include <array>

/**
 * A specific instance of a SurfaceSourceBuilder for a cylinder surface
 */
class CylinderSurfaceSourceBuilder : public SurfaceSourceBuilder {
    public:
        virtual bool includeFace(TetraMesh::FaceDescriptor IDfud);
        virtual void update();

        std::array<float,3> endpoint(unsigned i) const { return m_endpoint[i]; }
        void endpoint(unsigned i,std::array<float,3> p) { m_endpoint[i] = p; }

        void emitFromEnds(bool en) { m_emitFromEnds = en; }
        bool emitFromEnds() const { return m_emitFromEnds; }

        void orthogonalTolerance(double t) { m_orthogonalTolerance = t; }
        float orthogonalTolerance() const { return m_orthogonalTolerance; }

    protected:
        // the two endpoints of the cylinder which do not have to be in the middle,
        // but must be parallel to the 'height' of the cylinder
        std::array<float,3> m_endpoint[2];
        
        // the normalized direction vector of the cylinder
        Vector<3,double> m_cylinderDirNorm;
        
        // if true, emit from the top and bottom circles of the cylinder, if false, try not to ...
        bool m_emitFromEnds=false;

        // the tolerance of orthogonality for checking if a tetra face is on the ends of the cylinder
        // default of 0.5 allows for a up to ~30 degrees of error (90 +- 30) to be counted as orthogonal
        float m_orthogonalTolerance=0.5;
};

#endif

