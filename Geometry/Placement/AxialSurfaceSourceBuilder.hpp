#ifndef GEOMETRY_PLACEMENT_AXIALSURFACESOURCEBUILDER_HPP_
#define GEOMETRY_PLACEMENT_AXIALSURFACESOURCEBUILDER_HPP_


#include <FullMonteSW/Geometry/Placement/SurfaceSourceBuilder.hpp>

#include <array>

/**
 * A specific instance of a SurfaceSourceBuilder for a radially symmetrical emission pattern
 */
class AxialSurfaceSourceBuilder : public SurfaceSourceBuilder {
    public:
        virtual bool includeFace(TetraMesh::FaceDescriptor IDfud);
        virtual void update();

        std::array<double,3> endpoint(unsigned i) const { return m_endpoint[i]; }
        void endpoint(unsigned i,std::array<double,3> p) { m_endpoint[i] = p; }

        void emitProfile (std::string fn);

    protected:
        // the two endpoints of the axis defining emission power
        std::array<double,3> m_endpoint[2];
        
        // the normalized direction vector of the cylinder
        Vector<3,double> m_axisDirNorm;
        
        // array storing power along axis
        std::vector<double> m_powers;

        // number of power segments, default 1 (homogeneous surface emission)
        unsigned m_numSegments = 1;
        double m_intervalSize = 1;
        double m_axisLength = 1;
};

#endif

