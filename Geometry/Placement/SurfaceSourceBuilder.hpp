#ifndef GEOMETRY_PLACEMENT_SURFACESOURCEBUILDER_HPP_
#define GEOMETRY_PLACEMENT_SURFACESOURCEBUILDER_HPP_

#include <FullMonteSW/Geometry/TetraMesh.hpp>
#include <FullMonteSW/Geometry/FaceLinks.hpp>
#include <FullMonteSW/Geometry/Permutation.hpp>
#include <FullMonteSW/Geometry/Predicates/GeometryPredicate.hpp>
#include <FullMonteSW/Geometry/Predicates/VolumeCellPredicate.hpp>
#include <FullMonteSW/Geometry/Predicates/SurfaceCellPredicate.hpp>

#include <FullMonteSW/Geometry/Sources/Composite.hpp>
#include <FullMonteSW/Geometry/Sources/TetraFace.hpp>

#include <vector>

/**
 * A class that takes a mesh and a volume region predicate within that mesh
 * and creates a composite source out of the tetrahedral faces on the surface
 * of the region.
 */
class SurfaceSourceBuilder {
    public:
        virtual ~SurfaceSourceBuilder() {}

        void mesh(TetraMesh* mesh) { m_mesh = mesh; }
        TetraMesh* mesh() const { return m_mesh; }

        void setRegion(VolumeCellPredicate* r) { m_region = r; };
        VolumeCellPredicate* surfaceRegion() const { return m_region; }

        void emitHemiSphere(bool en) { m_emitHemiSphere = en; }
        bool emitHemiSphere() { return m_emitHemiSphere; }

        void numericalAperture(float NA) { m_NA = abs(NA); }
        float numericalAperture() const { return m_NA; }

        void hemiSphereEmitDistribution(std::string str) { m_hemiSphereEmitDistribution = str; }
        std::string hemiSphereEmitDistribution() const { return m_hemiSphereEmitDistribution; }

        void power(float p);
        float power() const { return m_power; }

        void checkDirection(bool checkDirection) { m_checkDirection = checkDirection; }
        bool checkDirection() const { return m_checkDirection; }

        void emitDirection(std::array <double, 3> emitDirection);
        UnitVector<3, double> emitDirection() const { return m_emitDirection; }

        virtual bool includeFace(TetraMesh::FaceDescriptor IDfud);// { return true; }

        virtual void update();
        Source::Composite output();

    protected:
        // the geometry (has to be a TetraMesh)
        TetraMesh* m_mesh=nullptr;

        // the volume region predicate
        VolumeCellPredicate* m_region=nullptr;

        // if true, emit from the tetrhedral faces randomly in the unit hemisphere
        // if false, emit in the direction of the normal of the tetrahedral face
        bool m_emitHemiSphere=false;

        // the list of sources we have made
        std::vector<Source::TetraFace*> m_sources;

        // the distribution type for a hemisphere (see Kernels/Software/Emitters/HemiSphere.hpp)
        // currently should be "UNIFORM" or "LAMBERT" using a string in case we want to extend these distributions in the future
        std::string m_hemiSphereEmitDistribution="UNIFORM";

        // the total power of the output composite source
        float m_power=1.0;

        // defining the photon spread angle of a tetrahedral face (default value emits in a hemisphere)
        float m_NA=1.0;

        // if true, include only those faces emitting towards the specified direction
        // if false, include all faces
        bool m_checkDirection=false;
        // specified direction
        UnitVector <3, double> m_emitDirection=UnitVector<3, double>();

        // Error margin for dot product, kept as internal variable for now
        // currently set to be less than 25.842 degrees from normal
        float m_directionTolerance=0.9;
};

#endif

