#include <FullMonteSW/Geometry/Placement/AxialSurfaceSourceBuilder.hpp>

#include <FullMonteSW/Geometry/Predicates/VolumeCellPredicate.hpp>
#include <FullMonteSW/Geometry/Predicates/SurfaceCellPredicate.hpp>
#include <FullMonteSW/Geometry/Predicates/SurfaceOfRegionPredicate.hpp>
#include <FullMonteSW/Geometry/Predicates/ArrayPredicateEvaluator.hpp>

#include <FullMonteSW/Geometry/TetraMesh.hpp>
#include <FullMonteSW/Geometry/FaceLinks.hpp>

#include <FullMonteSW/Logging/FullMonteLogger.hpp>

#include <fstream>
#include <iomanip>

/**
 * Override the base method from SurfaceSourceBuilder to do the same thing
 * but remove tetras that may be on the ends of the cylinder if we don't want to emit from there.
 */
void AxialSurfaceSourceBuilder::update() {
    // check if emission profile is set
    if(m_powers.empty()) {
        LOG_ERROR << "AxialSurfaceSourceBuilder::update no emission profile is defined" << std::endl;
    }

    // compute direction vector for the axis
    Vector<3,double> axisDir;
    axisDir[0] = m_endpoint[1][0] - m_endpoint[0][0];
    axisDir[1] = m_endpoint[1][1] - m_endpoint[0][1];
    axisDir[2] = m_endpoint[1][2] - m_endpoint[0][2];

    // normalize the direction vector
    m_axisDirNorm = normalize(axisDir);

    // set interval size
    m_axisLength = sqrt(axisDir[0] * axisDir[0] + axisDir[1] * axisDir[1] + axisDir[2] * axisDir[2]);
    m_intervalSize = m_axisLength/m_numSegments;
    
    // copy SurfaceSourceBuilder update to create Composite source from surface tetras    
    //// pre processing ////
    if(!m_mesh) {
        LOG_ERROR << "SurfaceSourceBuilder::update no mesh is defined" << std::endl;
    }

    if(!m_region) {
        LOG_ERROR << "SurfaceSourceBuilder::update no region is  defined" << std::endl;
    }

    if(m_emitHemiSphere) {
        LOG_DEBUG << "SurfaceSourceBuilder::update I will create a surface source which emits randomly on a hemisphere centered on the normal of each tetrahedral face" << std::endl;
    } else {
        LOG_DEBUG << "SurfaceSourceBuilder::update I will create a surface source which emits in the direction of the face normal (pointing outside of the surface)" << std::endl;
    }

    //// create the composite source ////
    // get the surface region of the given volume region
    SurfaceOfRegionPredicate* s= new SurfaceOfRegionPredicate();
    s->setRegionPredicate(m_region);
    const SurfaceCellPredicate* surfaceRegion = s;

    // evaluators for the surface and volume
    ArrayPredicateEvaluator* surEval = surfaceRegion->bind(m_mesh);
    ArrayPredicateEvaluator* volEval = m_region->bind(m_mesh);

    // first find the region surface area
    double regionSurfaceArea = 0.0;
    double totalPower = 0.0;
    for(unsigned i = 0; i < m_mesh->faceTetraLinks()->size(); i++) {
        // is the face of the surface of the region
        if((*surEval)(i) && includeFace(TetraMesh::FaceDescriptor(i))) {
            regionSurfaceArea += get(area, *m_mesh, TetraMesh::FaceDescriptor(i));
            const Point<3,double> sc = get(centroid, *m_mesh, TetraMesh::FaceDescriptor(i));
            const Point<3,double> ep (m_endpoint[0]);
            // LOG_INFO << "sc " << sc[0] << " " << sc[1] << " " << sc[2] << std::endl;
            double projection = dot(sc-ep, m_axisDirNorm);
            // LOG_INFO << "projection " << i << ": " << projection << std::endl;

            const unsigned power_index = (unsigned) (projection / m_intervalSize + 0.5);
            totalPower += m_powers[power_index];
        }
    }

    // loop over all undirected faces
    for(unsigned i = 0; i < m_mesh->faceTetraLinks()->size(); i++) {
        // is the face of the surface of the region
        if((*surEval)(i) && includeFace(TetraMesh::FaceDescriptor(i))) {
            // if the tetra above the face (i.e. the one along the normal, i.e. 'upTet') is
            // INSIDE the region interior (the volume evaluator 'volEval' tells us this),
            // this means that the normal points inside the volume region and,
            // therefore, we need to flip the normal so it points outside the volume region
            bool flip = (*volEval)(m_mesh->faceTetraLinks()->get(i).upTet().T.value());

            // surface area of this triangle face
            // power of this face emitter is the fraction of the total power (m_power) based on the
            // surface are of this triangular face compared to the surface area of the whole region (sum of all face areas)
            const double sa = get(area, *m_mesh, TetraMesh::FaceDescriptor(i));

            // emission profile changes begin
            // project face centroid onto axis
            const Point<3,double> sc = get(centroid, *m_mesh, TetraMesh::FaceDescriptor(i));
            const Point<3,double> ep (m_endpoint[0]);
            // LOG_INFO << "sc " << sc[0] << " " << sc[1] << " " << sc[2] << std::endl;
            double projection = dot(sc-ep, m_axisDirNorm);
            // LOG_INFO << "projection " << i << ": " << projection << std::endl;

            const unsigned power_index = (unsigned) (projection / m_intervalSize + 0.5);
            
            const float face_power = (float)(sa / regionSurfaceArea) * m_powers[power_index] * m_power / totalPower;

            // create a TetraFace source using the info we just gathered
            // TODO: WE ARE LEAKING THIS MEMORY - NO GREAT WAY TO DELETE IT!
            Source::TetraFace* tf_src = new Source::TetraFace(face_power, i);   // (power, undirected face ID)
            tf_src->emitNormal(!flip);                                          // if flip, emit anti-normal, otherwise emit normal
            tf_src->emitHemiSphere(m_emitHemiSphere);                           // whether to emit in a hemi-sphere
            tf_src->hemiSphereEmitDistribution(m_hemiSphereEmitDistribution);   // what distribution to use for the theta angle IF emitting in a hemi-sphere
            tf_src->numericalAperture(m_NA);

            // add the source we created to our list
            m_sources.push_back(tf_src);
        }
    }
}

/**
 * Set the axial emission profile with the file containing powers along its axis.
 */
void AxialSurfaceSourceBuilder::emitProfile(std::string fn) {
    m_powers.clear(); // ensure vector is empty

    ifstream is (fn.c_str());

	if (!is.good())
	{
		LOG_ERROR << "AxialSurfaceSourceBuilder::emitProfile() failed to open " << fn << endl;
		return;
	}

    double data_in;

    while (is >> data_in) {
        m_powers.push_back(data_in);
    }

    m_numSegments = m_powers.size();
}

/**
 * Override this method to determine if a tetra face should be included in the surface
 *
 * @param IDfud the undirected face descriptor for the face to check
 * @return whether the face should be included in the surface
 */
bool AxialSurfaceSourceBuilder::includeFace(TetraMesh::FaceDescriptor IDfud) {

    const Point<3,double> sc = get(centroid, *m_mesh, TetraMesh::FaceDescriptor(IDfud));
    const Point<3,double> ep (m_endpoint[0]);
    // LOG_INFO << "sc " << sc[0] << " " << sc[1] << " " << sc[2] << std::endl;
    double projection = dot(sc-ep, m_axisDirNorm);
    // LOG_INFO << "projection " << i << ": " << projection << std::endl;
    if (projection >= 0 && projection <= m_axisLength) {
        // whatever the base class implemented
        return SurfaceSourceBuilder::includeFace(IDfud);
    } else return false;
}