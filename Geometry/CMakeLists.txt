ADD_LIBRARY(FullMonteGeometry SHARED
	Geometry.cpp Points.cpp ErrorChecker.cpp TetraMesh.cpp Partition.cpp RayWalk.cpp Basis.cpp MaterialSet.cpp Material.cpp 
	Queries/TetrasNearPoint.cpp Queries/TetraEnclosingPointByLinearSearch.cpp Queries/PartitionRelabel.cpp
	Queries/DynamicIndexRelabel.cpp
	Sources/Abstract.cpp Sources/Print.cpp 
	TetraMeshBuilder.cpp PTTetraMeshBuilder.cpp Layer.cpp Layered.cpp
	Permutation.cpp
	RemapRegions.cpp
	NonSSE.cpp)

TARGET_LINK_LIBRARIES(FullMonteGeometry ${Boost_TIMER_LIBRARIES} ${Boost_SYSTEM_LIBRARIES} FullMonteLogging FullMonteData)

IF (WRAP_TCL)
    SET_SOURCE_FILES_PROPERTIES(FullMonteGeometry.i PROPERTIES CPLUSPLUS ON)
	SWIG_ADD_LIBRARY(FullMonteGeometryTCL 
		TYPE SHARED 
		LANGUAGE tcl 
		SOURCES FullMonteGeometry.i
	)
	SWIG_LINK_LIBRARIES(FullMonteGeometryTCL 
		${TCL_LIBRARY} 
		FullMonteGeometryPredicates
	)
    
	INSTALL(TARGETS FullMonteGeometryTCL LIBRARY 
		DESTINATION lib
	)
ENDIF()

IF (WRAP_PYTHON)
	SET_SOURCE_FILES_PROPERTIES(FullMonteGeometry.i PROPERTIES CPLUSPLUS ON)
	SWIG_ADD_LIBRARY(Geometry 
		TYPE SHARED 
		LANGUAGE python 
		SOURCES FullMonteGeometry.i
	)
	SWIG_LINK_LIBRARIES(Geometry 
		${Python3_LIBRARIES} 
		FullMonteGeometryPredicates
	)
    SET_TARGET_PROPERTIES(_Geometry PROPERTIES LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib/fmpy)
ENDIF()


INSTALL(TARGETS FullMonteGeometry LIBRARY
	DESTINATION lib
)	

IF(WRAP_VTK)
	INCLUDE_DIRECTORIES(${VTK_INCLUDE_DIRS})
	LINK_DIRECTORIES(${VTK_LIBRARY_DIRS} ${ANTLR3_LIB_DIR})
ENDIF()

MESSAGE("${Blue}Entering subdirectory Geometry/Placement${ColourReset}")
ADD_SUBDIRECTORY(Placement)
MESSAGE("${Blue}Entering subdirectory Geometry/Predicates${ColourReset}")
ADD_SUBDIRECTORY(Predicates)
MESSAGE("${Blue}Leaving subdirectory Geometry${ColourReset} \ 
")
