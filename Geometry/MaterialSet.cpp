/*
 * MaterialSet.cpp
 *
 *  Created on: May 29, 2017
 *      Author: jcassidy
 */

#include "MaterialSet.hpp"
#include "Material.hpp"

MaterialSet::MaterialSet()
{

}

MaterialSet::~MaterialSet()
{
}

MaterialSet* MaterialSet::clone() const
{
	MaterialSet* MS = new MaterialSet();

	for(unsigned i=0;i<size();++i)
	{
		Material* M = new Material(*get(i));
		MS->append(M);
	}
	return MS;
}

bool MaterialSet::matchedBoundary() const
{
	return m_matched;
}

void MaterialSet::matchedBoundary(bool m)
{
	m_matched=m;
}

Material* MaterialSet::exterior()
{
	return get(0);
}

void MaterialSet::exterior(Material* m)
{
	set(0,m);
}

unsigned MaterialSet::append(Material* m)
{
	unsigned i=size();
	resize(i+1);

	WrappedVector<Material*,unsigned>::set(i,m);
	return i;
}

void MaterialSet::set(unsigned i,Material* m)
{
	if (i >= size())
		resize(i+1);

	WrappedVector<Material*,unsigned>::set(i,m);
}

Material* MaterialSet::material(unsigned i) const
{
	return get(i);
}
