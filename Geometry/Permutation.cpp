/*
 * Permutation.cpp
 *
 *  Created on: Sep 11, 2017
 *      Author: jcassidy
 */

#include "Permutation.hpp"
#include <FullMonteSW/OutputTypes/OutputDataType.hpp>

Permutation::Permutation()
{
}

Permutation::~Permutation()
{
}

void Permutation::sourceSize(unsigned n)
{
	m_sourceSize=n;
}

unsigned Permutation::sourceSize() const
{
	return m_sourceSize;
}



void Permutation::resize(unsigned n)
{
	m_perm.resize(n,0);
}

unsigned Permutation::dim() const
{
	return m_perm.size();
}

void	Permutation::set(unsigned i,unsigned j)
{
	m_perm.at(i) = j;
}

unsigned Permutation::get(unsigned i) const
{
	return m_perm.at(i);
}

OutputDataType s_PermutationType{ "Permutation" };

const OutputDataType* Permutation::type() const
{
	return &s_PermutationType;
}

Permutation* Permutation::clone() const
{
	return new Permutation(*this);
}
