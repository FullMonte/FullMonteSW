/*
 * Material.hpp
 *
 *  Created on: May 29, 2017
 *      Author: jcassidy
 */

#ifndef GEOMETRY_MATERIAL_HPP_
#define GEOMETRY_MATERIAL_HPP_

#include "UnitDimension.hpp"
#include <string>

/** Very simple material description
 *
 */

class Material
{
public:
	Material();
	Material(float muA,float muS,float g,float n);
	~Material();

	/**
     * Four dimension that can be set for the material optical properties
     */
	

	void unitDimension(std::string str);
	DimensionUnit getUnit();
	std::string unitDimension();

	/// Sets the scattering coefficient such that the reduced scatter (1-g)mu_s equals the provided value using the existing g
	void reducedScatteringCoeff(float mu_s_prime);

	/// Sets the scattering coefficient and shape parameter to match the specified reduced scattering coefficient
	void reducedScatteringCoeffWithG(float mu_s_prime,float g);

	/// Get the reduced scattering coefficent \f$ (1-g)\cdot\mu_s \f$
	float reducedScatteringCoeff() const;

	/// Get/set the scattering coefficient
	void scatteringCoeff(float mu_s);
	float scatteringCoeff() const;

	/// Get/set the absorption coefficient
	void absorptionCoeff(float mu_a);
	float absorptionCoeff() const;

	/// Get/set the Henyey-Greenstein g parameter
	void anisotropy(float g);
	float anisotropy()			const;

	/// Get/set the refractive index
	void refractiveIndex(float n);
	float refractiveIndex()		const;

private:
	/// scattering coefficient \f$ \mu_s \f$
	float 	mu_s=0.0f;

	/// absoprtion coefficient \f$ \mu_a \f$
	float 	mu_a=0.0f;
	
	/// anisotropy g (Henyey-Greenstein parameter)
	float	g=0.0f;

	/// refractive index n
	float	n=0.0f;

	DimensionUnit unit=DimensionUnit::NONE;
};




#endif /* GEOMETRY_MATERIAL_HPP_ */
