/*
 * ArrayPredicate.hpp
 *
 *  Created on: May 17, 2018
 *      Author: jcassidy
 */

#ifndef GEOMETRY_PREDICATES_ARRAYPREDICATE_HPP_
#define GEOMETRY_PREDICATES_ARRAYPREDICATE_HPP_


/** Abstract base class for predicates that can be applied over an array of elements of a subject.
 *
 * ie. where evaluate(pred,subj,i) makes sense
 *
 * for instance
 *
 * evaluate(cellPredicate, geom, i)
 *
 * to evaluate some cell predicate on element i of a geometry
 */

class ArrayPredicate
{
public:
	virtual ~ArrayPredicate(){};

protected:
	ArrayPredicate(){};

private:
};



#endif /* GEOMETRY_PREDICATES_ARRAYPREDICATE_HPP_ */
