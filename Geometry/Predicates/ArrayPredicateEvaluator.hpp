/*
 * ArrayPredicateEvaluator.hpp
 *
 *  Created on: May 17, 2018
 *      Author: jcassidy
 */

#ifndef GEOMETRY_PREDICATES_ARRAYPREDICATEEVALUATOR_HPP_
#define GEOMETRY_PREDICATES_ARRAYPREDICATEEVALUATOR_HPP_

class Permutation;

/** Abstract base class for evaluating array predicates.
 *
 * Array predicate evaluation requires both a subject (underlying array-like data) and a predicate.
 * This class should be returned by a predicate's bind(Subject*) operation for some appropriate class Subject.
 *
 */

class ArrayPredicateEvaluator
{
public:
	ArrayPredicateEvaluator();
	virtual ~ArrayPredicateEvaluator();

	virtual unsigned			size() const=0;						///< Size of the underlying array

	virtual bool 			operator()(unsigned i) const=0;		///< Evaluate the predicate at a specific index
	virtual Permutation*		matchingElements() const;			///< Extract the subset that match
};

#endif /* GEOMETRY_PREDICATES_ARRAYPREDICATEEVALUATOR_HPP_ */
