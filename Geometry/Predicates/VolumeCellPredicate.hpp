/*
 * VolumeCellPredicate.hpp
 *
 *  Created on: May 17, 2018
 *      Author: jcassidy
 */

#ifndef GEOMETRY_PREDICATES_VOLUMECELLPREDICATE_HPP_
#define GEOMETRY_PREDICATES_VOLUMECELLPREDICATE_HPP_

#include "../Predicates/GeometryPredicate.hpp"

class VolumeCellPredicate : public GeometryPredicate
{
};

#endif /* GEOMETRY_PREDICATES_VOLUMECELLPREDICATE_HPP_ */
