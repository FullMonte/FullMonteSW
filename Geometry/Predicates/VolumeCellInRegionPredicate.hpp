/*
 * VolumeCellInRegionPredicate.hpp
 *
 *  Created on: May 17, 2018
 *      Author: jcassidy
 */

#ifndef GEOMETRY_PREDICATES_VOLUMECELLINREGIONPREDICATE_HPP_
#define GEOMETRY_PREDICATES_VOLUMECELLINREGIONPREDICATE_HPP_

#include "../Predicates/VolumeCellPredicate.hpp"

class Geometry;
class ArrayPredicateEvaluator;

/** Predicate returning true if the argument volume cell is within the specified region
 */

class VolumeCellInRegionPredicate : public VolumeCellPredicate
{
public:
	VolumeCellInRegionPredicate();
	~VolumeCellInRegionPredicate();

	void setRegion(unsigned r);				///< Set the region
	unsigned region() 			const;		///< Get the region

	virtual ArrayPredicateEvaluator*		bind(const Geometry* G) const override;

private:
	unsigned			m_region=-1U;
};



#endif /* GEOMETRY_PREDICATES_VOLUMECELLINREGIONPREDICATE_HPP_ */
