/*
 * GeometryPredicate.hpp
 *
 *  Created on: May 17, 2018
 *      Author: jcassidy
 */

#ifndef GEOMETRY_PREDICATES_GEOMETRYPREDICATE_HPP_
#define GEOMETRY_PREDICATES_GEOMETRYPREDICATE_HPP_

#include "ArrayPredicate.hpp"

class Geometry;
class ArrayPredicateEvaluator;

/** Abstract base class for geometry queries.
 *
 */

class GeometryPredicate : public ArrayPredicate
{
public:
	virtual ArrayPredicateEvaluator*		bind(const Geometry* G) const=0;
};

#endif /* GEOMETRY_PREDICATES_GEOMETRYPREDICATE_HPP_ */
