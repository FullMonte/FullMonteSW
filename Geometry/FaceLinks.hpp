/*
 * FaceLinks.hpp
 *
 *  Created on: May 17, 2017
 *      Author: jcassidy
 */

#ifndef GEOMETRY_FACELINKS_HPP_
#define GEOMETRY_FACELINKS_HPP_

#include "WrappedVector.hpp"
#include "TetraMesh.hpp"

#include <FullMonteSW/Geometry/UnitVector.hpp>

/** FaceTetraLink specifies a face that links two tetras.
 *
 * The point IDs (A,B,C) should always be given in ascending index order.
 * The normal is defined by AB x AC / |AB x AC|.
 *
 * upTet/downTet are specified so that the point of upTet that is not in the face is above the face.
 *
 * NOTE: point IDs in ascending order is required to provide simple equality checking and ordering by ensuring a canonical form
 */

struct FaceTetraLink
{
	struct TetraLink
	{
		TetraMesh::TetraDescriptor T;
		TetraMesh::PointDescriptor vertexPoint;
	};

	std::array<TetraMesh::PointDescriptor,3>		pointIDs;	///< The points in this face in ascending index order

	TetraLink&		upTet(){ return tets[0]; }
	TetraLink& 		downTet(){ return tets[1]; }

	array<TetraLink,2>	tets;								///< Tetras incident to this face (Mesh exterior is tetra 0)
															///< Normal points into tets[0] aka "upTet".
};

typedef WrappedVector<FaceTetraLink,unsigned> FaceTetraLinks;




/** TetraFaceLink specifies links for each face in a tetra: which directed face ID the link passes through, and which tetra it
 * arrives in.
 *
 * TetraFaceLinks is just an array of four of these.
 */

struct TetraFaceLink
{
	TetraMesh::DirectedFaceDescriptor 	faceID;			///< Directed face ID (see TetraMesh.hpp)
	TetraMesh::TetraDescriptor			tetraID;			///< Tetra ID of the adjacent tetra (0 for mesh exterior)
};

typedef WrappedVector<array<TetraFaceLink,4>,unsigned> TetraFaceLinks;





inline std::array<TetraMesh::PointDescriptor,3> get(points_tag,const TetraMesh& M,TetraMesh::FaceDescriptor F)
{
	return M.faceTetraLinks()->get(F.value()).pointIDs;
}

inline std::array<TetraMesh::PointDescriptor,3> get(points_tag,const TetraMesh& M,TetraMesh::DirectedFaceDescriptor F)
{
	std::array<TetraMesh::PointDescriptor,3> Ps = M.faceTetraLinks()->get(F.value() >> 1).pointIDs;
	if (F.value() & 1)
		std::swap(Ps[1],Ps[2]);
	return Ps;
}

inline std::array<TetraMesh::FaceDescriptor,4> get(faces_tag,const TetraMesh& M,TetraMesh::TetraDescriptor T)
{
	const auto L = M.tetraFaceLinks()->get(T.value());
	std::array<TetraMesh::FaceDescriptor,4> Fu;
	for(unsigned i=0;i<4;++i)
		Fu[i] = get(face,M,L[i].faceID);
	return Fu;
}

inline std::array<TetraMesh::DirectedFaceDescriptor,4> get(directed_faces_tag,const TetraMesh& M,TetraMesh::TetraDescriptor T)
{
	const auto L = M.tetraFaceLinks()->get(T.value());
	std::array<TetraMesh::DirectedFaceDescriptor,4> F;
	for(unsigned i=0;i<4;++i)
		F[i] = L[i].faceID;
	return F;
}

template<typename FaceProp>struct face_prop { constexpr face_prop(){} };

/** Get a point property for all point on a face
 */


template<typename PointProp>
	std::array<decltype(get(PointProp(),declval<const TetraMesh&>(),TetraMesh::PointDescriptor())),3>
	get(
		point_prop<PointProp>,
		const TetraMesh& M,
		typename TetraMesh::FaceDescriptor IDf)
	{
	typedef decltype(get(PointProp(),M,std::declval<TetraMesh::PointDescriptor>())) result_type;
	std::array<result_type,3> res;
	std::array<TetraMesh::PointDescriptor,3> IDps = get(points,M,IDf);

	for(unsigned i=0;i<3;++i)
		res[i] = get(PointProp(),M,TetraMesh::PointDescriptor(IDps[i]));

	return res;
	}

template<typename PointProp>
	std::array<decltype(get(PointProp(),declval<const TetraMesh&>(),TetraMesh::PointDescriptor())),3>
	get(
		point_prop<PointProp>,
		const TetraMesh& M,
		typename TetraMesh::DirectedFaceDescriptor IDfd)
	{
	typedef decltype(get(PointProp(),M,std::declval<TetraMesh::PointDescriptor>())) result_type;
	std::array<result_type,3> res;
	std::array<TetraMesh::PointDescriptor,3> IDps = get(points,M,IDfd);

	for(unsigned i=0;i<3;++i)
		res[i] = get(PointProp(),M,TetraMesh::PointDescriptor(IDps[i]));

	return res;
	}

template<typename FaceProp>
	std::array<decltype(get(FaceProp(),declval<const TetraMesh&>(),TetraMesh::FaceDescriptor())),4>
	get(
			face_prop<FaceProp>,
			const TetraMesh& M,
			typename TetraMesh::TetraDescriptor IDt)
	{
		typedef decltype(get(FaceProp(),M,std::declval<TetraMesh::FaceDescriptor>())) result_type;
		std::array<result_type,4> res;
		std::array<TetraMesh::FaceDescriptor,4> IDfus = get(faces,M,IDt);

		for(unsigned i=0;i<4;++i)
			res[i] = get(FaceProp(),M,IDfus[i]);

		return res;
	}
//
//template<typename Prop>
//	std::array<decltype(get(Prop(),declval<const TetraMesh&>(),TetraMesh::PointDescriptor())),3>
//	get(
//		Prop p,
//		const TetraMesh& M,
//		typename TetraMesh::FaceDescriptor IDf)
//	{
//	typedef decltype(get(p,M,std::declval<TetraMesh::PointDescriptor>())) result_type;
//	std::array<result_type,3> res;
//	FaceByPointID IDps = get(points,M,IDf);
//
//	for(unsigned i=0;i<3;++i)
//		res[i] = get(p,M,TetraMesh::PointDescriptor(IDps[i]));
//
//	return res;
//	}

inline TetraMesh::DirectedFaceDescriptor get(directed_face_tag,const TetraMesh& /*M*/,TetraMesh::FaceDescriptor F,bool down=false)
{
	return TetraMesh::DirectedFaceDescriptor((F.value() << 1) | down);
}

FaceTetraLink get(face_link_tag,const TetraMesh& M,TetraMesh::FaceDescriptor IDf);
inline unsigned get(num_faces_tag,const TetraMesh& M){ return M.faceTetraLinks()->size(); }
inline unsigned get(num_directed_faces_tag,const TetraMesh& M){ return M.faceTetraLinks()->size()*2; }

inline unsigned get(id_tag,const TetraMesh& /*M*/,TetraMesh::DirectedFaceDescriptor Fd)
{ return Fd.value(); }

inline unsigned get(id_tag,const TetraMesh& /*M*/,TetraMesh::FaceDescriptor Fu)
{ return Fu.value(); }

inline std::pair<UnitVector<3,double>,double> get(face_plane_tag,const TetraMesh& M,TetraMesh::DirectedFaceDescriptor IDfd)
{
	std::array<Point<3,double>,3> P = get(point_coords,M,IDfd);
	UnitVector<3,double> n = normalize(cross(P[0],P[1],P[2]));

	double c = dot(n,Vector<3,double>(P[0]));
	return std::make_pair(n,c);
}

inline std::pair<UnitVector<3,double>,double> get(face_plane_scaled_tag,const TetraMesh& M,TetraMesh::DirectedFaceDescriptor IDfd, const double scale_parameter, const std::array<double,3> translation_vector)
{
	std::array<Point<3,double>,3> P = get(point_coords,M,IDfd);
	std::array<Point<3,double>,3> P_scale = P;

	for(int i=0; i<3; i++)
	{
		for(int j=0; j<3; j++)
		{
			P_scale[i][j] = (P[i][j] - translation_vector[j]) / scale_parameter;
		}
	}

	UnitVector<3,double> n_scale = normalize(cross(P_scale[0],P_scale[1],P_scale[2]));

	double c_scale = dot(n_scale,Vector<3,double>(P_scale[0]));
	return std::make_pair(n_scale,c_scale);
}

inline UnitVector<3,double> get(face_normal_tag,const TetraMesh& M,TetraMesh::DirectedFaceDescriptor IDfd)
{
	std::array<Point<3,double>,3> P = get(point_coords,M,IDfd);
	return normalize(cross(P[0],P[1],P[2]));
}

inline UnitVector<3,double> get(face_normal_tag,const TetraMesh& M,TetraMesh::FaceDescriptor IDfu)
{
	std::array<Point<3,double>,3> P = get(point_coords,M,IDfu);
	return normalize(cross(P[0],P[1],P[2]));
}

// TODO: ensure anti-normal is correct
inline UnitVector<3,double> get(face_anti_normal_tag,const TetraMesh& M,TetraMesh::DirectedFaceDescriptor IDfd)
{
	std::array<Point<3,double>,3> P = get(point_coords,M,IDfd);
	return normalize(cross(P[2],P[1],P[0]));
}

inline UnitVector<3,double> get(face_anti_normal_tag,const TetraMesh& M,TetraMesh::FaceDescriptor IDfu)
{
	std::array<Point<3,double>,3> P = get(point_coords,M,IDfu);
	return normalize(cross(P[2],P[1],P[0]));
}

inline Point<3,double> get(centroid_tag,const TetraMesh& M,TetraMesh::FaceDescriptor IDfu)
{
	std::array<Point<3,double>,3> P = get(point_coords,M,IDfu);
	return Point<3,double>{
		(P[0][0] + P[1][0] + P[2][0])/3.0,
		(P[0][1] + P[1][1] + P[2][1])/3.0,
		(P[0][2] + P[1][2] + P[2][2])/3.0,
	};
}

inline Point<3,double> get(centroid_tag,const TetraMesh& M,TetraMesh::DirectedFaceDescriptor IDfd)
{
	return get(centroid,M,TetraMesh::FaceDescriptor(IDfd));
}


inline std::pair<UnitVector<3,double>,double> get(face_plane_tag,const TetraMesh& M,TetraMesh::FaceDescriptor IDfu)
{
	std::array<Point<3,double>,3> P = get(point_coords,M,IDfu);
	UnitVector<3,double> n = normalize(cross(P[0],P[1],P[2]));

	double c = dot(n,Vector<3,double>(P[0]));
	return std::make_pair(n,c);
}

inline std::pair<UnitVector<3,double>,double> get(face_plane_scaled_tag,const TetraMesh& M,TetraMesh::FaceDescriptor IDfu, const double scale_parameter, const std::array<double,3> translation_vector)
{
	std::array<Point<3,double>,3> P = get(point_coords,M,IDfu);
	std::array<Point<3,double>,3> P_scale = P;
	
	for(int i=0; i<3; i++)
	{
		for(int j=0; j<3; j++)
		{
			P_scale[i][j] = (P[i][j] - translation_vector[j]) / scale_parameter;
		}
	}

	UnitVector<3,double> n_scale = normalize(cross(P_scale[0],P_scale[1],P_scale[2]));

	double c_scale = dot(n_scale,Vector<3,double>(P_scale[0]));
	return std::make_pair(n_scale,c_scale);
}

#endif /* GEOMETRY_FACELINKS_HPP_ */
