/*
 * NonSSE.cpp
 *
 *  Created on: Oct 2, 2018
 *      Author: fynns
 *
 */

#include <iostream>
#include <array>

#include <boost/iterator.hpp>
#include <boost/iterator/iterator_facade.hpp>

#include <boost/range/any_range.hpp>

typedef std::array<float,4> m128;

//
/**************************************/
//The following functions replace SSE instruction with non-SSE instructions

//cmpeq128 is the non-SSE version of {__m128 _mm_cmpeq_ps(__m128 a, __m128 b);} 
//Compares for equality
m128 cmpeq128(m128 const &a, m128 const &b);

//movemask128 is the non-SSE version of {int _mm_movemask_ps(__m128 a);}
//Creates a 4-bit mask from the most significant bits of the four SP FP values.
int movemask128( m128 const &a );

//getMinIndex128 is the non-SSE version of {std::pair<unsigned,m128> getMinIndex4p(m128 v);} which is declared in sse.hpp 
std::pair<unsigned,m128> getMinIndex128(m128 const &v);

//max128 is the non-SSE version of {__m128 _mm_max_ps(__m128 a , __m128 b );}
///computes the maximums of the four single-precision, floating-point values of a and b.
m128 max128(m128 const &a, m128 const &b);

//setzero128  is the non-SSE version of {__m128 _mm_setzero_ps (void)}
//Return array of type m128 with all elements set to zero.
m128 set_zero128();

//blend128  is the non-SSE version of {__m128 _mm_blendv_ps (__m128 a, __m128 b, m128 mask)}
//Blend packed single-precision (32-bit) floating-point elements from a and b using control mask , and return the result.
m128 blend128(m128 const &a, m128 const &b, m128 const &mask);

//shuffle128  is the non-SSE version of {__m128 _mm_shuffle_ps (__m128 lo, __m128 hi, unsigned int imm8)}
//Shuffle single-precision (32-bit) floating-point elements in lo and hi using the controls hi3, hi2, lo1 and lo0 and return the result.
m128 shuffle128(m128 const &lo, m128 const &hi, int hi3, int hi2, int lo1, int lo0);

//mult128  is the non-SSE version of {__m128 _mm_mul_ps (__m128 a, __m128 b)} 
//Multiply packed single-precision (32-bit) floating-point elements in a and b, and store the results in dst. 
m128 mult128(m128 const &mul1, m128 const &mul2);

//div128  is the non-SSE version of {__m128 _mm_div_ps (__m128 a, __m128 b)}
//Divide packed single-precision (32-bit) floating-point elements in a by packed elements in b, and store the results in dst.
m128 div128(m128 const &div1, m128 const &div2);

//add128  is the non-SSE version of {__m128 _mm_add_ps (__m128 a, __m128 b)}
//Add packed single-precision (32-bit) floating-point elements in a and b, and store the results in dst.
m128 add128(m128 const &add1, m128 const &add2);

//sub128  is the non-SSE version of {__m128 _mm_sub_ps (__m128 a, __m128 b)}
//Subtract packed single-precision (32-bit) floating-point elements in b from packed single-precision (32-bit) floating-point elements in a, and store the results in dst.
m128 sub128(m128 const &sub1, m128 const &sub2);

//min128  is the non-SSE version of {__m128 _mm_min_ps (__m128 a, __m128 b)}
//Compare packed single-precision (32-bit) floating-point elements in a and b, and return packed minimum values in dst. 
m128 min128(m128 const &a, m128 const &b);

//set1_ps128  is the non-SSE version of {__m128 _mm_set1_ps ( float  f) }
//Returns a vector of 4 SPFP values filled with the same SP value. 
m128 set1_ps128(float value);

//setr_ps128  is the non-SSE version of {__m128 _mm_setr_ps ( const float a,const float  b,const float  c,const float d) }
//returns a vector of 4 SPFP values with the lowest SPFP value set to f, and other values set to 0.0f. 
m128 setr_ps128(float z, float y, float x, float w);

//complo128  is the non-SSE version of {bool _mm_ucomilt_ss (__m128 a, __m128 b)}
//Compare the lower single-precision (32-bit) floating-point element in a and b for less-than, and return the boolean result (0 or 1).
bool complo128(m128 const &a, m128 const &b);

//loadu_ps128 is the non-SSE version of {__m128 _mm_loadu_ps (float const* mem_addr)}
//Load 128-bits (composed of 4 packed single-precision (32-bit) floating-point elements) from memory into dst. mem_addr does not need to be aligned on any particular boundary.
m128 loadu_ps128(float const* a);

//load_ps128 is the non-SSE version of {__m128 _mm_load_ps (float const* mem_addr)}
//Load 128-bits (composed of 4 packed single-precision (32-bit) floating-point elements) from memory into dst. mem_addr must be aligned on a 16-byte boundary or a general-protection exception may be generated.
m128 load_ps128(float const* a);

//dp_ps128 is the non-SSE version of {__m128 _mm_dp_ps (__m128 a, __m128 b, const int imm8)}
//Conditionally multiply the packed single-precision (32-bit) floating-point elements in a and b using the high 4 bits in imm8, sum the four products, and conditionally store the sum in dst using the low 4 bits of imm8.
m128 dp_ps128(m128 const &a, m128 const &b, const unsigned imm8);

//sqrt_ss128 is the non-SSE version of {__m128 _mm_sqrt_ss (__m128 a)}
//Compute the square root of the lower single-precision (32-bit) floating-point element in a, store the result in the lower element of dst, and copy the upper 3 packed elements from a to the upper elements of dst.
m128 sqrt_ss128(m128 const &a);

//rsqrt_ss is the non-SSE version of {__m128 _mm_rsqrt_ss (__m128 a)
//Compute the approximate reciprocal square root of the lower single-precision (32-bit) floating-point element in a, store the result in the lower element of dst, and copy the upper 3 packed elements from a to the upper elements of dst. The maximum relative error for this approximation is less than 1.5*2^-12.
m128 rsqrt_ss(m128 const &a);

//addsub128 is the non-SSE version of {__m128 _mm_addsub_ps (__m128 a, __m128 b)}
//Alternatively add and subtract packed single-precision (32-bit) floating-point elements in a to/from packed elements in b, and store the results in dst.
m128 addsub128(m128 const &a, m128 const &b);
