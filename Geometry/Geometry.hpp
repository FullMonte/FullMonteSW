/*
 * Geometry.hpp
 *
 *  Created on: May 29, 2017
 *      Author: jcassidy
 */

#ifndef GEOMETRY_GEOMETRY_HPP_
#define GEOMETRY_GEOMETRY_HPP_

#include "UnitDimension.hpp"
#include <string>

class OutputData;
class Partition;
template<class T>class SpatialMap;

/** Abstract base class for all geometry descriptions
 */

class Geometry
{
public:
	virtual ~Geometry();

	/// Get/set the unit dimension the mesh is in
	void unitDimension(std::string str);
	DimensionUnit getUnit() const;
	std::string unitDimension() const;

	/// Get/set the region partition (assignment of tetras to regions)
	void 			regions(Partition* p);
	Partition* 		regions() const;

	virtual SpatialMap<float>*	elementVolumes()		const=0;
	virtual SpatialMap<float>*	surfaceAreas() 			const=0;
	virtual SpatialMap<float>*	directedSurfaceAreas() 	const=0;

protected:
	Geometry();

private:
	Partition*		m_regions=nullptr;
	DimensionUnit	m_unit=DimensionUnit::NONE;
};

#endif /* GEOMETRY_GEOMETRY_HPP_ */
