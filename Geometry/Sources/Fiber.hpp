/*
 * Fiber.hpp
 *
 *  Created on: April 20, 2018
 *      Author: Yasmin Afsharnejad
 */

#ifndef GEOMETRY_SOURCES_FIBER_HPP_
#define GEOMETRY_SOURCES_FIBER_HPP_

/** Defines a conical beam emerging from a fiber source of a given numerical aperture, fiber axis direction, position and radius of end-point
 *
 */
#include "Abstract.hpp"

namespace Source {

	class Fiber  : public Abstract

	{
		public:

            Fiber(float w=1) : Abstract(w){}
			Fiber(float w,float NA ,  std::array<float,3> dir,  std::array<float,3> pos, float r) : Abstract(w),m_numericalAperture(NA), m_fiberDir(dir), m_fiberPos(pos), m_radius(r) {}

			DERIVED_SOURCE_MACRO(Abstract,Fiber)	

				float     radius()   const   { return m_radius; }
			void      radius(float r)    { m_radius=r; }


			float     numericalAperture()   const   { return m_numericalAperture; }
			void      numericalAperture(float NA)    { m_numericalAperture=NA; }

			std::array<float,3>  fiberDir()   const   { return m_fiberDir; }
			void      fiberDir(std::array<float,3>  dir)    { m_fiberDir=dir; }

			std::array<float,3>   fiberPos()   const   { return m_fiberPos; }
			void   fiberPos(std::array<float,3>  pos)    { m_fiberPos=pos; }


		private:
			float m_numericalAperture=0.0f;//numerical aperture
			std::array<float,3> m_fiberDir; //direction of fiber axis
			std::array<float,3> m_fiberPos; //position of center of fiber end-point
			float m_radius;//radius of fiber end-point 
	};

};


#endif /* GEOMETRY_SOURCES_FIBER_HPP_ */


