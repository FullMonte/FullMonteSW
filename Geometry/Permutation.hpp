/*
 * Permutation.hpp
 *
 *  Created on: Sep 11, 2017
 *      Author: jcassidy
 */

#ifndef QUERIES_PERMUTATION_HPP_
#define QUERIES_PERMUTATION_HPP_

#include <vector>
#include <FullMonteSW/OutputTypes/OutputData.hpp>

/** Holds a permutation (set of indices) into a vector
 *
 */

class Permutation : public OutputData
{
public:
	Permutation();
	virtual ~Permutation();

	virtual Permutation* 			clone() const override;
	virtual const OutputDataType* 	type() const override;

	void			sourceSize(unsigned n);			///< Set the size of the source (max index)
	unsigned		sourceSize() const;				///< Get the size of the source (max index)

	void 		resize(unsigned n);				/// Resize the permutation
	unsigned		dim() const;						/// Query permutation size

	void			set(unsigned i,unsigned j);		/// Set i'th output to come from j'th input
	unsigned		get(unsigned i) const;			/// Get the source index of the j'th output

private:
	std::vector<unsigned>		m_perm;
	unsigned						m_sourceSize=0;
};


#endif /* QUERIES_PERMUTATION_HPP_ */
