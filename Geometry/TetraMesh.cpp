#include <limits>

#include "StandardArrayKernel.hpp"
#include "BoundingBox.hpp"
#include "Point.hpp"
#include "Vector.hpp"
#include "Points.hpp"
#include "Partition.hpp"

#include <iomanip>

#include <FullMonteSW/OutputTypes/OutputData.hpp>
#include <FullMonteSW/OutputTypes/SpatialMap.hpp>

#include "TetraMesh.hpp"

#include "FaceLinks.hpp"

#include <array>

#include <FullMonteSW/Warnings/Push.hpp>
#include <FullMonteSW/Warnings/Boost.hpp>
#include <boost/range.hpp>
#include <boost/range/adaptor/transformed.hpp>
#include <FullMonteSW/Warnings/Pop.hpp>

#include "FaceLinks.hpp"

using namespace std;

TetraMesh::TetraMesh()
{
}

TetraMesh::~TetraMesh()
{
}

SpatialMap<float>* TetraMesh::elementVolumes() const
{
	vector<float> V(get(num_tetras,*this));
	boost::copy(
			tetras() | boost::adaptors::transformed([this](TetraDescriptor t){ return get(volume,*this,t); }),
			V.begin());
	return new SpatialMap<float>(move(V), AbstractSpatialMap::Volume,AbstractSpatialMap::Scalar,AbstractSpatialMap::UnknownOutputType);
}

SpatialMap<float>* TetraMesh::surfaceAreas() const
{
	vector<float> A(get(num_faces,*this));
	boost::copy(
				faces() | boost::adaptors::transformed([this](FaceDescriptor f){ return get(area,*this,f); }),
				A.begin());
	return new SpatialMap<float>(move(A), AbstractSpatialMap::Surface,AbstractSpatialMap::Scalar,AbstractSpatialMap::UnknownOutputType);
}

SpatialMap<float>* TetraMesh::directedSurfaceAreas() const
{
	vector<float> AD(get(num_directed_faces,*this));
	boost::copy(
			directedFaces() | boost::adaptors::transformed([this](DirectedFaceDescriptor df){ return get(area,*this,df); }),
			AD.begin());
	return new SpatialMap<float>(move(AD), AbstractSpatialMap::DirectedSurface,AbstractSpatialMap::Scalar,AbstractSpatialMap::UnknownOutputType);
}




void TetraMesh::points(Points* p)
{
	m_points = p;
}


Points* TetraMesh::points() const
{
	return m_points;
}

void TetraMesh::tetraCells(TetraCells* T)
{
	m_tetraCells=T;
}

TetraCells* TetraMesh::tetraCells() const
{
	return m_tetraCells;
}

void TetraMesh::rtree(TetraMesh::TetraMeshRTree* r)
{
	m_rtree=r;
}
	
TetraMesh::TetraMeshRTree*  TetraMesh::rtree() const
{
	return m_rtree;
}

/** get tetra volume: Calculate directly from point coordinates
 * 1/6 * AB x BC . AD
 */

double get(volume_tag,const TetraMesh& M,TetraMesh::TetraDescriptor T)
{
	std::array<Point<3,double>,4> Ps = get(point_coords,M,T);

	return abs(dot(
		Ps[3]-Ps[0],
		cross(Ps[0],Ps[1],Ps[2])
		)) /6.0;
}

TetraMesh::FaceDescriptor get(face_tag,const TetraMesh&,TetraMesh::DirectedFaceDescriptor F)
{
	return TetraMesh::FaceDescriptor(F.value()>>1);
}

double get(area_tag,const TetraMesh& M,TetraMesh::FaceDescriptor F)
{
	array<Point<3,double>,3> P=get(point_coords,M,F);
	return 0.5*norm_l2(cross(P[0],P[1],P[2]));
}

array<Point<3,double>,3> get(face_points_tag,const TetraMesh& M,TetraMesh::FaceDescriptor F)
{

	return get(point_coords,M,F);
}

double get(area_tag,const TetraMesh& M,TetraMesh::DirectedFaceDescriptor F)
{
	return get(area,
			M,
			get(face,M,F));		// return area of the undirected face descriptor
}


FaceTetraLink get(face_link_tag,const TetraMesh& M,TetraMesh::FaceDescriptor IDf)
{
	return M.faceTetraLinks()->get(IDf.value());
}

Point<3,double> get(coords_tag,const TetraMesh& M,TetraMesh::PointDescriptor IDp)
{
	return M.points()->get(IDp.value());
}

TetraMesh::PointDescriptor get(point_above_face_tag,const TetraMesh& M,TetraMesh::FaceDescriptor IDf)
{
	FaceTetraLink F = get(face_link, M, IDf);
	return TetraMesh::PointDescriptor(F.upTet().vertexPoint);
}

TetraMesh::PointDescriptor get(point_below_face_tag,const TetraMesh& M,TetraMesh::FaceDescriptor IDf)
{
	FaceTetraLink F = get(face_link, M, IDf);
	return TetraMesh::PointDescriptor(F.downTet().vertexPoint);
}


TetraMesh::TetraDescriptor get(tetra_above_face_tag,const TetraMesh& M,TetraMesh::FaceDescriptor IDf)
{
	FaceTetraLink F = get(face_link, M, IDf);
	return F.tets[0].T;
}

TetraMesh::TetraDescriptor get(tetra_below_face_tag,const TetraMesh& M,TetraMesh::FaceDescriptor IDf)
{
	FaceTetraLink F = get(face_link, M, IDf);
	return F.tets[1].T;
}

TetraMesh::TetraDescriptor get(tetra_above_face_tag,const TetraMesh& M,TetraMesh::DirectedFaceDescriptor IDf)
{
	FaceTetraLink F = get(face_link, M, get(face,M,IDf));

	return F.tets[IDf.value() & 1].T;
}

TetraMesh::TetraDescriptor get(tetra_below_face_tag,const TetraMesh& M,TetraMesh::DirectedFaceDescriptor IDf)
{
	FaceTetraLink F = get(face_link, M, get(face,M,IDf));

	return F.tets[!(IDf.value() & 1)].T;
}

unsigned get(region_tag,const TetraMesh& M,TetraMesh::TetraDescriptor IDt)
{
	return M.regions()->get(IDt.value());
}

//TetraMesh::FaceDescriptor 			get(face_tag,const TetraMesh&,TetraMesh::TetraDescriptor,unsigned i)
//{
//	//return get(face,M,T,i);
//}
//
//TetraMesh::DirectedFaceDescriptor 	get(face_tag,const TetraMesh&,TetraMesh::TetraDescriptor,unsigned i)
//{
//	//return get(face,M,T,i);
//}



TetraMesh::FaceRange TetraMesh::faces() const
{
	FaceTetraLinks* faces = faceTetraLinks();

	if (!faces)
		throw std::logic_error("TetraMesh::faces() called with no faces supplied");

	return FaceRange(
		FaceIterator(FaceDescriptor(0)),
		FaceIterator(FaceDescriptor(faces->size())));
}

TetraMesh::DirectedFaceRange TetraMesh::directedFaces() const
{
	FaceTetraLinks* faces = faceTetraLinks();

	if (!faces)
		throw std::logic_error("TetraMesh::directedFaces() called with no faces supplied");

	return DirectedFaceRange(
			DirectedFaceIterator(DirectedFaceDescriptor(0U)),
			DirectedFaceIterator(DirectedFaceDescriptor(faces->size()*2U)));
}

TetraMesh::TetraRange TetraMesh::tetras() const
{
	if (!m_tetraCells)
		throw std::logic_error("TetraMesh::tetras() called without any tetra cells");

	return TetraRange(
			TetraIterator(TetraDescriptor(0U)),
			TetraIterator(TetraDescriptor(tetraCells()->size())));
}

TetraMesh::PointRange TetraMesh::pointRange() const
{
	if (!m_points)
		throw std::logic_error("TetraMesh::pointRange() called without any points");

	return PointRange(
			PointIterator(PointDescriptor(0U)),
			PointIterator(PointDescriptor(points()->size())));
}


FaceTetraLinks* TetraMesh::faceTetraLinks() const
{
	return m_faceTetraLinks;
}

void TetraMesh::faceTetraLinks(FaceTetraLinks* f)
{
	m_faceTetraLinks=f;
}

void TetraMesh::tetraFaceLinks(TetraFaceLinks* t)
{
	m_tetraFaceLinks = t;
}

TetraFaceLinks* TetraMesh::tetraFaceLinks() const
{
	return m_tetraFaceLinks;
}

unsigned get(num_points_tag,const TetraMesh& M){ return M.points()->size(); }

/******************************************************************************
 *
 * Implememntation of TetraMeshRTree class functions
 * 
 ******************************************************************************
 */
#include <FullMonteSW/Geometry/Queries/TetraEnclosingPointByLinearSearch.hpp>
#include <FullMonteSW/Logging/FullMonteLogger.hpp>

// convenience namespaces
// NOTE: keep this in the CPP file. putting this in the HPP file breaks SWIG exports
namespace bgeo = boost::geometry;
namespace bgeoi = boost::geometry::index;

/**
 * Creates the RTree for the given mesh.
 * Must be called before calling update() after everytime the mesh (m_mesh) is changed.
 * If you don't update the mesh, no need to call create() again
 */
void TetraMesh::TetraMeshRTree::create() {
    // error checking
    if(m_mesh == nullptr) {
        LOG_ERROR << "TetraMeshRTree::create() mesh has not been defined\n";
        return;
    }

    // number of tetrahedrons
    unsigned Nt = get(num_tetras, *m_mesh);

    for(unsigned IDt = 1; IDt < Nt; IDt++) {
        TetraMesh::TetraDescriptor tetraD = TetraMesh::TetraDescriptor(IDt);
        
        // get bounding box of tetra
        array<::Point<3,double>,2> min_max = get(bounding_box, *m_mesh, tetraD);
        const RTreePoint min_p(min_max[0][0], min_max[0][1], min_max[0][2]);
        const RTreePoint max_p(min_max[1][0], min_max[1][1], min_max[1][2]);

        // create the RTree bounding box (a little waste of time)
        RTreeBox bb(min_p, max_p);

        // insert the {bounding box, Tetra Descriptor} pair into the RTree
        m_rtree.insert(std::make_pair(bb, tetraD));
    }

    // the tree has been created
    m_created = true;
}

TetraMesh::TetraDescriptor TetraMesh::TetraMeshRTree::search(const std::array<float,3> p, unsigned int K) {
    return search(::Point<3,double>(p), K);
}

TetraMesh::TetraDescriptor TetraMesh::TetraMeshRTree::search(const Point<3, double> p, unsigned int K) {
    // error checking
    if(m_mesh == nullptr) {
        LOG_ERROR << "TetraMeshRTree::update() mesh has not been defined\n";
        return TetraMesh::TetraDescriptor(0);
    }

    if(!m_created) {
        LOG_ERROR << "TetraMeshRTree::update() RTree has not yet been created, you must call create() before calling update\n";
        return TetraMesh::TetraDescriptor(0);
    }

    // find K closest tetras
    LOG_DEBUG << "TetraMeshRTree::update() querying tree to find the " << K << " closest tetras to point (" << p[0] << ", " << p[1] << ", " << p[2] << ")\n";
    std::vector<RTreeValue> results(K);
    m_rtree.query(bgeoi::nearest(RTreePoint(p[0],p[1],p[2]), K), std::back_inserter(results));

    // create vector of tetra ids found by RTree query (in update())
    std::vector<TetraMesh::TetraDescriptor> ids(results.size());
    for(unsigned i = 0; i < results.size(); i++) {
        ids[i] = results[i].second;
    }
    
    // search the results from the RTree query for a containing tetra
    TetraEnclosingPointByLinearSearch L;
    L.mesh(m_mesh);
	L.point(p);
    bool found = L.searchVector(ids);

    if(found) {
        // found a containing tetra in one of the tetras from the RTree query, return it
        LOG_DEBUG << "TetraMeshRTree::tetra() RTRee query found containing tetra\n";
        return L.tetra();
    } else {
        // bad result from RTree query, fall back to linear search
        LOG_WARNING << "TetraMeshRTree::tetra() RTree query did not provide any tetras containing the point, falling back to a linear search\n";
        L.update();
        return L.tetra();
    }
}

/**
 * This method returns all tetra that are within (or intersect) a bounding box 
 * @param min_p: bottom-left most point of the bounding box 
 * @param max_p: top-right most point of the bounding box
 * @return vector of all tetra descriptors that are within or the intersect the bound box
 */
std::vector<TetraMesh::TetraDescriptor> TetraMesh::TetraMeshRTree::getTetraInBoundingBox(const std::array<float, 3> min_p, 
                                const std::array<float, 3> max_p) 
{
    // error checking
    if(m_mesh == nullptr) {
        LOG_ERROR << "TetraMeshRTree::update() mesh has not been defined\n";
        return vector<TetraMesh::TetraDescriptor>();
    }

    if(!m_created) {
        LOG_ERROR << "TetraMeshRTree::update() RTree has not yet been created, you must call create() before calling update\n";
        return vector<TetraMesh::TetraDescriptor>();
    }

    
    // Create the bounding box 
    const RTreePoint min_rp(min_p[0], min_p[1], min_p[2]);
    const RTreePoint max_rp(max_p[0], max_p[1], max_p[2]);

    RTreeBox bb(min_rp, max_rp);

    // Query the tree
    LOG_DEBUG << "TetraMeshRTree::update() querying tree to find the tetra within bounding box (" << 
        min_p[0] << ", " << min_p[1] << ", " << min_p[2] << ") -----> (" << max_p[0] << ", " << max_p[1] << ", " << max_p[2] << ")\n";
    std::vector<RTreeValue> results;
    m_rtree.query(bgeoi::within(bb), std::back_inserter(results));
    m_rtree.query(bgeoi::intersects(bb), std::back_inserter(results));
    m_rtree.query(bgeoi::overlaps(bb), std::back_inserter(results));

    if (results.size() == 0) {
        LOG_WARNING << "TetraMeshRtree::tetra() Could not find tetra enclosed in bounding box (" << 
            min_p[0] << ", " << min_p[1] << ", " << min_p[2] << ") -----> (" << max_p[0] << ", " << max_p[1] << ", " << max_p[2] << ")\n";
        return vector<TetraMesh::TetraDescriptor>();
    }
    // create vector of tetra ids found by RTree query (in update())
    std::vector<TetraMesh::TetraDescriptor> ids(results.size());
    for(unsigned i = 0; i < results.size(); i++) {
        ids[i] = results[i].second;
    }

    return ids;
}
