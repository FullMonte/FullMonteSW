//
// This logger is used for outputting information to the user
// (i.e. error, warning, info, debug and status messages).
// Any file/class that wants to display a message to the user should
// use this logger (specifically use the FM_LOG and FM_LOG_NP macros).
//

#ifndef LOGGING_FULLMONTELOGGER_HPP_
#define LOGGING_FULLMONTELOGGER_HPP_

#include <iostream>
#include <bits/stdc++.h>
#include <FullMonteSW/Config.h>

//
// The logger logic for filtering output
// TODO: output to a file
// TODO: more Filter types?
//
class FullMonteLogger {
public:
    // the different types of messages
    enum Filter {
        DEBUG       = 1 << 0,
        INFO        = 1 << 1,
        WARNING     = 1 << 2,
        ERROR       = 1 << 3,
        TRACE       = DEBUG|INFO|WARNING|ERROR
    };

    bool isFilterActive(Filter f);
    std::ostream& getStream(Filter level);
    void addFilter(FullMonteLogger::Filter f);
    void addFilter(std::string fstr);
    void removeFilter(FullMonteLogger::Filter f);
    void removeFilter(std::string fstr);
    void clearFilters();

private:
    std::string filterToStr(Filter f);
    Filter strToFilter(std::string str);
    
    // the current filters for the logger
#ifdef DEBUG_MODE
    unsigned int filters = DEBUG|INFO|WARNING|ERROR;
#else
    unsigned int filters = INFO|WARNING|ERROR;
#endif
};

// the static logger for FullMonte (check FullMonteLogger.cpp)
extern FullMonteLogger logger;

// macro to log a certain level
// NOTE: Use this macro (or macros below) to log from anywhere in FullMonte
#define LOG(level) \
    if (!logger.isFilterActive(level)) ; \
    else logger.getStream(level)


// convenience macros for main logging macro above
#define LOG_TRACE       LOG(FullMonteLogger::Filter::TRACE)
#define LOG_DEBUG       LOG(FullMonteLogger::Filter::DEBUG)
#define LOG_INFO        LOG(FullMonteLogger::Filter::INFO)
#define LOG_WARNING     LOG(FullMonteLogger::Filter::WARNING)
#define LOG_ERROR       LOG(FullMonteLogger::Filter::ERROR)


// functions for TCL interface which doesn't seem to like the static variable
void logAddFilter(std::string fstr);
void logRemoveFilter(std::string fstr);
void logSetFilters(std::string fstr);
void logClearFilters();

#endif
