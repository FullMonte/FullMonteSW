/*
 * FullMonteTimer.hpp
 *  Useful timer that keeps track of the time elapsed and peak memory usage.
 *  Copied from VTR (url: https://github.com/verilog-to-routing/vtr-verilog-to-routing/blob/e5ff75cc76f83ee2a7a5c4bbda0a278e6980239c/libs/libvtrutil/src/vtr_time.h)
 *  
 *  Created on: September 4, 2021
 *      Author: Shuran Wang
 */

#ifndef LOGGING_FULLMONTETIMER_HPP_
#define LOGGING_FULLMONTETIMER_HPP_

#include "MemoryUsage.hpp"

#include <chrono>
#include <string>

///@brief Class for tracking time elapsed since construction
class FullMonteTimer {
  public:
    FullMonteTimer();
    virtual ~FullMonteTimer() = default;

    ///@brief No copy
    FullMonteTimer(FullMonteTimer&) = delete;
    FullMonteTimer& operator=(FullMonteTimer&) = delete;

    ///@brief No move
    FullMonteTimer(FullMonteTimer&&) = delete;
    FullMonteTimer& operator=(FullMonteTimer&&) = delete;

    ///@brief Return elapsed time in seconds
    float elapsed_sec() const;

    ///@brief Return peak memory resident set size (in MiB)
    float max_rss_mib() const;

    ///@brief Return change in peak memory resident set size (in MiB)
    float delta_max_rss_mib() const;

  private:
    using clock = std::chrono::steady_clock;
    std::chrono::time_point<clock> start_;

    size_t initial_max_rss_; //Maximum resident set size In bytes
    constexpr static float BYTE_TO_MIB = 1024 * 1024;
};

#endif // LOGGING_FULLMONTETIMER_HPP_