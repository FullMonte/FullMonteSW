#include <FullMonteSW/Logging/FullMonteLogger.hpp>

// the static instance of the FullMonteLogger
// the macros from FullMonteLogger.hpp operate on this
FullMonteLogger logger;

////////////////////////////////////////////////////////////////////
// For TCL interface which doesn't like the global variable
void logAddFilter(std::string fstr) {
    logger.addFilter(fstr);
}

void logRemoveFilter(std::string fstr) {
    logger.removeFilter(fstr);
}

void logSetFilters(std::string fstr) {
    std::istringstream iss(fstr);
    logger.clearFilters();

    for(std::string s; iss >> s; ) {
        logger.addFilter(s);
    }
}

void logClearFilters() {
    logger.clearFilters();
}
////////////////////////////////////////////////////////////////////

/**
 * Converts a Filter type to a string
 *
 * @param f the filter to convert
 * @return the string representation of f
 */
std::string FullMonteLogger::filterToStr(FullMonteLogger::Filter f) {
    switch(f) {
        case FullMonteLogger::TRACE: return "trace";
        case FullMonteLogger::DEBUG: return "debug";
        case FullMonteLogger::INFO: return "info";
        case FullMonteLogger::WARNING: return "warning";
        case FullMonteLogger::ERROR: return "error";
        default: return "unknown";
    }
}

/**
 * Converts a string to a Filter type
 *
 * @param str the filter string
 * @return the Filter for that string (defaults to TRACE)
 */
FullMonteLogger::Filter FullMonteLogger::strToFilter(std::string str) {
    transform(str.begin(), str.end(), str.begin(), ::tolower);

    if(str == "trace") {
        return FullMonteLogger::Filter::TRACE;
    } else if(str == "debug") {
        return FullMonteLogger::Filter::DEBUG;
    } else if(str == "info") {
       return FullMonteLogger::Filter::INFO;
    } else if(str == "warning") {
       return FullMonteLogger::Filter::WARNING;
    } else if(str == "error") {
       return FullMonteLogger::Filter::ERROR;
    } else {
       return FullMonteLogger::Filter::TRACE;
    }
}

/**
 * @return true if filter f is active in filters, false if not
 */
bool FullMonteLogger::isFilterActive(FullMonteLogger::Filter f) {
    return filters & f;
}

/**
 * @return the current output stream for the logger
 * TODO: support for more than just 'cout'
 */
std::ostream& FullMonteLogger::getStream(FullMonteLogger::Filter level) {
    return std::cout << "[" << filterToStr(level) << "] ";
}
    
/**
 * Add a filter to the filters of the logger
 * @param f the filter to add
 */
void FullMonteLogger::addFilter(FullMonteLogger::Filter f) {
    filters |= f;
}

/**
 * Add a filter (by string) to the logger
 *
 * @param f the string of the filter to add
 */
void FullMonteLogger::addFilter(std::string fstr) {
    addFilter(strToFilter(fstr));
}
    
/**
 * Remove a filter to the filters of the logger
 * @param f the filter to remove
 */
void FullMonteLogger::removeFilter(FullMonteLogger::Filter f) {
    filters &= ~f;
}

/**
 * Removes all filters
 * DANGER
 */
void FullMonteLogger::clearFilters() {
    filters = 0;
}

/**
 * Remove a filter (by string) to the logger
 *
 * @param f the string of the filter to remove
 */
void FullMonteLogger::removeFilter(std::string fstr) {
    removeFilter(strToFilter(fstr));
}


