/*
 * SpatialMap2D.cpp
 *
 *  Created on: Jun 29, 2017
 *      Author: jcassidy
 */

#include "OutputDataType.hpp"
#include "SpatialMap2D.hpp"

namespace detail {

const OutputDataType *pf[] = { SpatialMap<float>::staticType(), nullptr };
const OutputDataType tf{"SpatialMap2D<float>", pf};


const OutputDataType *pu[] = {SpatialMap<unsigned>::staticType(), nullptr };
const OutputDataType tu{"SpatialMap2D<unsigned>",pu};

};

template<>const OutputDataType* SpatialMap2D<float>::staticType(){ return &detail::tf; }
template<>const OutputDataType* SpatialMap2D<unsigned>::staticType(){ return &detail::tu; }
