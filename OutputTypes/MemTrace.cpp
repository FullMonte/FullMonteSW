/*
 * MemTrace.cpp
 *
 *  Created on: Nov 13, 2017
 *      Author: jcassidy
 */

#include "MemTrace.hpp"

using namespace std;

MemTrace::MemTrace()
{

}

MemTrace::~MemTrace()
{

}

MemTrace* MemTrace::clone() const
{
	return new MemTrace(*this);
}

unsigned MemTrace::size() const
{
	return m_trace.size();
}

void MemTrace::resize(unsigned N)
{
	m_trace.resize(N,MemTraceEntry());
}

MemTrace::MemTraceEntry MemTrace::get(unsigned i) const
{
	return m_trace.at(i);
}

void MemTrace::set(unsigned i,unsigned addr,unsigned count)
{
	m_trace.at(i) = MemTraceEntry { addr, count };
}

void MemTrace::set(unsigned i,MemTraceEntry e)
{
	m_trace.at(i) = e;
}



