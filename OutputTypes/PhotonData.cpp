/*
 * PhotonData.cpp
 *
 *  Created on: Sep 10, 2022
 *      Author: Shuran Wang
 */

#include "OutputDataType.hpp"
#include "PhotonData.hpp"
#include "SpatialMap.hpp"

#include <FullMonteSW/Geometry/Ray.hpp>

PhotonData::PhotonData()
{
}

PhotonData::~PhotonData()
{
	
}

PhotonData* PhotonData::clone() const
{
	PhotonData* n = new PhotonData();
	if (m_weight)
		n->weight(m_weight->clone());
	return n;
}

void PhotonData::weight(SpatialMap<float>* weight)
{
	m_weight=weight;
}

SpatialMap<float>* PhotonData::weight() const
{
	return m_weight;
}

void PhotonData::ray(std::vector<std::pair<std::array<float,3>, std::array<float,3>> >* r)
{
	m_ray=r;
}

std::vector<std::pair<std::array<float,3>, std::array<float,3>> >* PhotonData::ray() const
{
	return m_ray;
}