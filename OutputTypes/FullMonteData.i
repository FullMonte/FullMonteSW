#if defined(SWIGTCL)
%module FullMonteDataTCL
#elif defined(SWIGPYTHON)
%module Data
#else
	#warning Requested wrapping language not supported
#endif
%feature("autodoc", "3");

%begin %{
#include <FullMonteSW/Warnings/SWIG.hpp>
%}

%{
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include "clonable.hpp"
#include "visitable.hpp"
#include "OutputData.hpp"
#include "SpatialMap.hpp"
#include "OutputDataType.hpp"
#include "OutputDataSummarize.hpp"
#include "OutputDataCollection.hpp"
#include "OutputDataType.hpp"

#include <FullMonteSW/VTK/swig_traits.hpp>

template<>const swig_type_info* 	swig_traits<OutputData*>::type_info(){ return SWIGTYPE_p_OutputData; }
template<>const char*			swig_traits<OutputData*>::type_name = "OutputData*";
%}

#if defined(SWIGPYTHON)
    %rename("fm_%s", match$name="type") "";
	%rename("fm_%s", match$name="set") "";
	%rename("fm_%s", match$name="sum") "";
#endif

%include "std_string.i"

%include "OutputData.hpp"
%include "OutputDataType.hpp"
%include "AbstractSpatialMap.hpp"
%include "SpatialMap.hpp"
%include "OutputDataSummarize.hpp"
%include "OutputDataCollection.hpp"
%include "OutputDataType.hpp"

%template(SpatialMapF) SpatialMap<float>;

#if defined(SWIGPYTHON)
// TODO Support SWIG - VTK python interface
// %{
// #include <vtkPythonUtil.h>
// %}

// %typemap(out) OutputData*
// {
// 	PyImport_ImportModule("vtk");
// 	$result = vtkPythonUtil::GetObjectFromPointer((OutputData*)$1);
// }

// %typemap(in) OutputData*
// {
// 	$1 = (OutputData* ) vtkPythonUtil::GetPointerFromObject($input, "OutputData");
// 	if ($1 == NULL)
// 		SWIG_fail;
// }

#endif
