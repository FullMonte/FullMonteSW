/*
 * AccumulationEventSet.cpp
 *
 *  Created on: May 12, 2020
 *      Author: fynns
 */

#include "AccumulationEventSet.hpp"

using namespace std;

AccumulationEventSet::AccumulationEventSet()
{
}

AccumulationEventSet::~AccumulationEventSet()
{

}

AccumulationEventSet* AccumulationEventSet::clone() const
{
	return new AccumulationEventSet(*this);
}

void AccumulationEventSet::resize(unsigned N)
{
	m_traces.resize(N,nullptr);
}

unsigned AccumulationEventSet::size() const
{
	return m_traces.size();
}

AccumulationEvent* AccumulationEventSet::get(unsigned i) const
{
	return m_traces.at(i);
}

void AccumulationEventSet::set(unsigned i,AccumulationEvent* p)
{
	if (i >= m_traces.size())
		resize(i+1);

	m_traces.at(i) = p;
}

unsigned AccumulationEventSet::append(AccumulationEvent* p)
{
	m_traces.push_back(p);
	return m_traces.size()-1;
}


