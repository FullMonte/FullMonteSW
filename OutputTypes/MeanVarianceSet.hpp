/*
 * MeanVarianceSet.hpp
 *
 *  Created on: Oct 19, 2017
 *      Author: jcassidy
 */

#ifndef OUTPUTTYPES_MEANVARIANCESET_HPP_
#define OUTPUTTYPES_MEANVARIANCESET_HPP_

#include <FullMonteSW/OutputTypes/OutputData.hpp>
template<class T>class SpatialMap;


/** Dataset holding mean and variance for a SpatialMap.
 *
 */

class MeanVarianceSet : public OutputData
{
public:
	MeanVarianceSet();
	virtual ~MeanVarianceSet();

	virtual MeanVarianceSet*		clone() const override;

	void					mean(SpatialMap<float>* mu);
	SpatialMap<float>*		mean() 		const;

	void					variance(SpatialMap<float>* sigma2);
	SpatialMap<float>*		variance() 	const;

	unsigned				dim() const;

	void 					packetsPerRun(unsigned long long N);
	unsigned long long		packetsPerRun() const;

	void					runs(unsigned N);
	unsigned				runs() const;

private:
	SpatialMap<float>*		m_mu=nullptr;
	SpatialMap<float>*		m_sigma2=nullptr;

	unsigned long long		m_packets	=0ULL;
	unsigned 				m_runs		=0U;
};


#endif /* OUTPUTTYPES_MEANVARIANCESET_HPP_ */
