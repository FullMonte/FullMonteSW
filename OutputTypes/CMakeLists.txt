ADD_LIBRARY(FullMonteData SHARED
	AbstractSpatialMap.cpp
	OutputDataSummarize.cpp
	MCConservationCounts.cpp
	MCEventCounts.cpp 
	PacketPositionTrace.cpp
	PacketPositionTraceSet.cpp 
	OutputData.cpp
	OutputDataCollection.cpp
	PhotonData.cpp
	SpatialMap2D.cpp
	SpatialMap.cpp
	MemTrace.cpp
	MemTraceSet.cpp
	AccumulationEvent.cpp
	AccumulationEventSet.cpp
	MeanVarianceSet.cpp
	DirectedSurface.cpp)
TARGET_LINK_LIBRARIES(FullMonteData FullMonteLogging)

IF (WRAP_TCL)
    SET_SOURCE_FILES_PROPERTIES(FullMonteData.i PROPERTIES CPLUSPLUS ON)
    INCLUDE_DIRECTORIES(${CMAKE_CURRENT_SOURCE_DIR})
    SWIG_ADD_LIBRARY(FullMonteDataTCL 
		TYPE SHARED 
		LANGUAGE tcl
		SOURCES FullMonteData.i
	)
	SWIG_LINK_LIBRARIES(FullMonteDataTCL 
		FullMonteData 
		${TCL_LIBRARY}
	)

	INSTALL(TARGETS FullMonteDataTCL LIBRARY 
		DESTINATION lib
	)
ENDIF()

IF (WRAP_PYTHON)
    SET_SOURCE_FILES_PROPERTIES(FullMonteData.i PROPERTIES CPLUSPLUS ON)
    INCLUDE_DIRECTORIES(${CMAKE_CURRENT_SOURCE_DIR})
	SWIG_ADD_LIBRARY(Data 
		TYPE SHARED 
		LANGUAGE python 
		SOURCES FullMonteData.i
	)
	SWIG_LINK_LIBRARIES(Data 
		FullMonteData 
		${Python3_LIBRARIES}
	)
	SET_TARGET_PROPERTIES(_Data PROPERTIES LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib/fmpy)
ENDIF()

INSTALL(TARGETS FullMonteData LIBRARY
	DESTINATION lib
)
