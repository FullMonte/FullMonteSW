/*
 * AccumulationEventSet.hpp
 *
 *  Created on: May 12, 2020
 *      Author: fynns
 */

#ifndef OUTPUTTYPES_ACCUMULATIONEVENTSET_HPP_
#define OUTPUTTYPES_ACCUMULATIONEVENTSET_HPP_

#include <vector>
#include <FullMonteSW/OutputTypes/OutputData.hpp>

class AccumulationEvent;

class AccumulationEventSet : public OutputData
{
public:
	AccumulationEventSet();
	virtual ~AccumulationEventSet();

	virtual AccumulationEventSet* clone() const override;

	void		resize(unsigned N);
	unsigned 	size() const;

	AccumulationEvent*	get(unsigned i) const;
	void		set(unsigned i,AccumulationEvent* p);

	unsigned	append(AccumulationEvent* p);

private:
	std::vector<AccumulationEvent*>		m_traces;
};




#endif /* OUTPUTTYPES_ACCUMULATIONEVENTSET_HPP_ */
