/*
 * DirectedSurface.cpp
 *
 *  Created on: May 17, 2018
 *      Author: jcassidy
 */

#include "OutputDataType.hpp"
#include "DirectedSurface.hpp"
#include "SpatialMap.hpp"


DirectedSurface::DirectedSurface()
{
}

DirectedSurface::~DirectedSurface()
{
}

DirectedSurface* DirectedSurface::clone() const
{
	DirectedSurface* n = new DirectedSurface();
	if (m_entering)
		n->entering(m_entering->clone());
	if (m_exiting)
		n->exiting(m_exiting->clone());
	return n;
}

void DirectedSurface::entering(SpatialMap<float>* entering)
{
	m_entering=entering;
}

void DirectedSurface::exiting(SpatialMap<float>* exiting)
{
	m_exiting=exiting;
}


SpatialMap<float>* DirectedSurface::entering() const
{
	return m_entering;
}

SpatialMap<float>* DirectedSurface::exiting() const
{
	return m_exiting;
}

void DirectedSurface::permutation(Permutation* p)
{
	m_permutation=p;
}

Permutation* DirectedSurface::permutation() const
{
	return m_permutation;
}

SpatialMap<float>* DirectedSurface::calculateTotal() const
{
	SpatialMap<float>* o = m_exiting->clone();
	for(unsigned i=0;i<o->dim();++i)
		o->set(i,o->get(i) + m_entering->get(i));
	return o;
}

const OutputDataType* pts[] = { OutputData::staticType(), nullptr };

const OutputDataType outputDataTypeInfo{ "DirectedSurface", pts };

const OutputDataType* DirectedSurface::s_type = &outputDataTypeInfo;

const OutputDataType* DirectedSurface::staticType()
{
	return s_type;
}

const OutputDataType* DirectedSurface::type() const
{
	return s_type;
}

