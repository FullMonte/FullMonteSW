/*
 * MemTrace.hpp
 *
 *  Created on: Nov 13, 2017
 *      Author: jcassidy
 */

#ifndef OUTPUTTYPES_MEMTRACE_HPP_
#define OUTPUTTYPES_MEMTRACE_HPP_

#include <FullMonteSW/OutputTypes/OutputData.hpp>
#include <vector>

class MemTrace : public OutputData
{
public:
	MemTrace();
	virtual ~MemTrace();

	virtual MemTrace* clone() const;

	unsigned 	size() const;
	void 		resize(unsigned N);

	struct MemTraceEntry
	{
		MemTraceEntry(unsigned address_=0,unsigned count_=0) : address(address_), count(count_){}
		unsigned address;
		unsigned count;
	};

	MemTraceEntry	get(unsigned i) const;
	void			set(unsigned i,MemTraceEntry e);
	void			set(unsigned i,unsigned address,unsigned count=1);

private:
	std::vector<MemTraceEntry>		m_trace;
};





#endif /* OUTPUTTYPES_MEMTRACE_HPP_ */
