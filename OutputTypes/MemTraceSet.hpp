/*
 * MemTraceSet.hpp
 *
 *  Created on: Nov 13, 2017
 *      Author: jcassidy
 */

#ifndef OUTPUTTYPES_MEMTRACESET_HPP_
#define OUTPUTTYPES_MEMTRACESET_HPP_

#include <vector>
#include <FullMonteSW/OutputTypes/OutputData.hpp>

class MemTrace;

class MemTraceSet : public OutputData
{
public:
	MemTraceSet();
	virtual ~MemTraceSet();

	virtual MemTraceSet* clone() const override;

	void		resize(unsigned N);
	unsigned 	size() const;

	MemTrace*	get(unsigned i) const;
	void		set(unsigned i,MemTrace* p);

	unsigned	append(MemTrace* p);

private:
	std::vector<MemTrace*>		m_traces;
};




#endif /* OUTPUTTYPES_MEMTRACESET_HPP_ */
