/*
 * PhotonData.hpp
 *
 *  Created on: Sep 10, 2022
 *      Author: Shuran Wang
 */

#ifndef OUTPUTTYPES_PHOTONDATA_HPP_
#define OUTPUTTYPES_PHOTONDATA_HPP_

#include "OutputData.hpp"

#include <vector>

template<typename T>class SpatialMap;

template<size_t D, class T>class Point;
template<size_t D, class T>class UnitVector;

template<size_t D, class T>class Ray;

class PhotonData : public OutputData
{
public:
	typedef Ray<3,float> Posdir;

	PhotonData();
	virtual ~PhotonData();

	ACCEPT_VISITOR_METHOD(OutputData,PhotonData);

	virtual PhotonData* clone() const override;

	void				weight(SpatialMap<float>* w);

	SpatialMap<float>*	weight() const;

	void 				ray(std::vector<std::pair<std::array<float,3>, std::array<float,3>> >*);		///< Set the associated ray
	std::vector<std::pair<std::array<float,3>, std::array<float,3>> >* 	ray() const;				///< Get the associated ray


private:
	SpatialMap<float>*	m_weight=nullptr;
	std::vector<std::pair<std::array<float,3>, std::array<float,3>> >*	m_ray=nullptr;

	std::string 					m_name;

};


#endif /* OUTPUTTYPES_PHOTONDATA_HPP_ */
