/*
 * OutputDataType.hpp
 *
 *  Created on: May 31, 2017
 *      Author: jcassidy
 */

#ifndef OUTPUTTYPES_OUTPUTDATATYPE_HPP_
#define OUTPUTTYPES_OUTPUTDATATYPE_HPP_

#include <string>

class OutputDataType
{
public:
	OutputDataType(std::string typeName,const OutputDataType** parents=nullptr) :
		m_name(typeName),
		m_parentTypes(parents){};
	virtual ~OutputDataType(){}

	virtual const std::string& name() const { return m_name; }

	bool derivedFrom(std::string name) const;
	bool derivedFrom(const OutputDataType* t) const;

private:
	std::string				m_name;
	const OutputDataType**	m_parentTypes=nullptr;
};

inline bool OutputDataType::derivedFrom(std::string name) const
{
	return name == m_name;
}

inline bool OutputDataType::derivedFrom(const OutputDataType* t) const
{
	if (t == this)
		return true;

	if (m_parentTypes)
	{
		for(const OutputDataType** p = m_parentTypes; *p; ++p)
			if ((*p)->derivedFrom(t))
				return true;
	}

	return false;
}

#endif /* OUTPUTTYPES_OUTPUTDATATYPE_HPP_ */
