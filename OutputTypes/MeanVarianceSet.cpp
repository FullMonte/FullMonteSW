/*
 * MeanVarianceSet.cpp
 *
 *  Created on: Oct 26, 2017
 *      Author: jcassidy
 */

#include <FullMonteSW/OutputTypes/SpatialMap.hpp>
#include <FullMonteSW/Logging/FullMonteLogger.hpp>
#include "MeanVarianceSet.hpp"

MeanVarianceSet::MeanVarianceSet()
{

}

MeanVarianceSet::~MeanVarianceSet()
{

}

MeanVarianceSet* MeanVarianceSet::clone() const
{
	return new MeanVarianceSet(*this);
}

void MeanVarianceSet::mean(SpatialMap<float>* mu)
{
	m_mu=mu;
}

unsigned MeanVarianceSet::dim() const
{
	if (!m_mu || !m_sigma2 || m_mu->dim() != m_sigma2->dim())
	{
		LOG_ERROR << "MeanVarianceSet::dim() dimension mismatch" << endl;
		return 0;
	}
	return m_mu->dim();
}

void MeanVarianceSet::variance(SpatialMap<float>* sigma2)
{
	m_sigma2=sigma2;
}

unsigned long long MeanVarianceSet::packetsPerRun() const
{
	return m_packets;
}

unsigned MeanVarianceSet::runs() const
{
	return m_runs;
}

void MeanVarianceSet::packetsPerRun(unsigned long long N)
{
	m_packets=N;
}

void MeanVarianceSet::runs(unsigned N)
{
	m_runs=N;
}

SpatialMap<float>* MeanVarianceSet::variance() const
{
	return m_sigma2;
}

SpatialMap<float>* MeanVarianceSet::mean() const
{
	return m_mu;
}
