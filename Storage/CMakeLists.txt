MESSAGE("${Blue}Entering subdirectory Storage/TIMOS${ColourReset}")
ADD_SUBDIRECTORY(TIMOS)
MESSAGE("${Blue}Entering subdirectory Storage/TextFile${ColourReset}")
ADD_SUBDIRECTORY(TextFile)
MESSAGE("${Blue}Entering subdirectory Storage/MCML${ColourReset}")
ADD_SUBDIRECTORY(MCML)
MESSAGE("${Blue}Entering subdirectory Storage/MMC${ColourReset}")
ADD_SUBDIRECTORY(MMC)
MESSAGE("${Blue}Entering subdirectory Storage/COMSOL${ColourReset}")
ADD_SUBDIRECTORY(COMSOL)

IF(VTK_FOUND)
	MESSAGE("${Blue}Entering subdirectory Storage/VTK${ColourReset}")
	ADD_SUBDIRECTORY(VTK)
ENDIF()

ADD_LIBRARY(FullMonteGeometryReader SHARED GenericGeometryReader.cpp)

INSTALL(TARGETS FullMonteGeometryReader
	LIBRARY
	DESTINATION lib
)

MESSAGE("${Blue}Leaving subdirectory Storage${ColourReset} \ 
")
