/*
 * CommentFilter.hpp
 *
 *  Created on: Oct 10, 2017
 *      Author: jcassidy
 */

#ifndef STORAGE_COMSOL_COMMENTFILTER_HPP_
#define STORAGE_COMSOL_COMMENTFILTER_HPP_


#include <FullMonteSW/Warnings/Push.hpp>
#include <FullMonteSW/Warnings/Boost.hpp>
#include <boost/iostreams/categories.hpp>
#include <boost/iostreams/operations.hpp>
#include <FullMonteSW/Warnings/Pop.hpp>


/** Strips comments between '#' and end of line but leaves the newline character in place
 *
 * Also removes carriage returns \r
 * */

struct single_delim_comment_filter {
	typedef char                       	char_type;
	typedef boost::iostreams::multichar_input_filter_tag  category;

	template<typename Source>
	std::streamsize read(Source& src, char* const pBufBegin, const std::streamsize nBuf)
	{
		char *pDst=pBufBegin;
		const char* const pBufEnd = pBufBegin+nBuf;
		std::streamsize nRead,nReq;

		while(pDst != pBufEnd)
		{
			// read next chunk: from current destination to end of buffer
			char* const pChunkBegin = pDst;
			nReq = pBufEnd-pChunkBegin;
			nRead = boost::iostreams::read(src,pChunkBegin,nReq);

			if (nRead < 0)
				break;

			char* const pChunkEnd = pChunkBegin+nRead;
			const char* pSrc = pChunkBegin;

			// process chunk, stripping comments
			while(pSrc != pChunkEnd)
			{
				if (m_inComment)
				{
					// while in comment, skip all characters until end of line
					while(pSrc != pChunkEnd && *pSrc != '\n')
						++pSrc;

					// hit end of input -> still in a comment
					m_inComment = pSrc == pChunkEnd;
				}

				// while not in comment, copy all characters except carriage-return until comment delimiter
				if (!m_inComment)
				{
					while(pSrc != pChunkEnd && *pSrc != '#')
					{
						char c = *(pSrc++);
						if (c != '\r')
							*(pDst++) = c;
					}

					// did not hit end of input -> found a comment
					m_inComment = pSrc != pChunkEnd;
				}
			}

			// check for end of file
			if (nReq != nRead)
				break;
		}

		return pDst-pBufBegin;
	}

private:
	bool		m_inComment=false;
};

#endif /* STORAGE_COMSOL_COMMENTFILTER_HPP_ */
