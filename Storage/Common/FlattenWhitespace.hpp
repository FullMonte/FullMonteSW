/*
 * FlattenWhitespace.hpp
 *
 *  Created on: Oct 10, 2017
 *      Author: jcassidy
 */

#ifndef STORAGE_COMSOL_FLATTENWHITESPACE_HPP_
#define STORAGE_COMSOL_FLATTENWHITESPACE_HPP_

#include <boost/iostreams/categories.hpp>
#include <boost/iostreams/operations.hpp>

#include <iostream>
using namespace std;

/** Flattens whitespace:
 * multiple tabs/spaces -> single space
 * multiple tabs/spaces/newlines -> single newline
 *
 * removes trailing whitespace at end of file
 */

struct flatten_whitespace {
	typedef char                       	char_type;
	typedef boost::iostreams::multichar_input_filter_tag  category;

	template<typename Source>
	std::streamsize read(Source& src, char* const pBufBegin, const std::streamsize nBuf)
	{
		char *pDst=pBufBegin;
		const char* const pBufEnd = pBufBegin+nBuf;

		if(m_leftover)
			*(pDst++)=m_leftover;
		m_leftover='\0';

		while(pDst != pBufEnd)
		{
			// read next chunk: from current destination to end of buffer
			std::streamsize nReq = pBufEnd-pDst;

			char* const pChunkBegin = pDst;
			std::streamsize nRead = boost::iostreams::read(src,pChunkBegin,nReq);

			if(nRead <= 0)
				break;

			char* const pChunkEnd = pDst+nRead;

			// process chunk, stripping comments
			for(const char* pSrc = pChunkBegin; pSrc != pChunkEnd; ++pSrc)
			{
				bool newline = (*pSrc == '\n') | (*pSrc == '\r');
				bool space = (*pSrc == ' ') | (*pSrc == '\t');

				if (newline | space)
				{
					if (!m_newlines & !m_spaces)		// first whitespace -> leave space in output, else skip
						++pDst;

					m_newlines	|= newline;
					m_spaces 	|= space;
				}
				else
				{
					if (m_newlines)
						*(pDst-1) = '\n';
					else if (m_spaces)
						*(pDst-1) = ' ';

					*(pDst++)=*pSrc;
					m_newlines=m_spaces=false;
				}
			}

			// check for end of file
			if (nReq != nRead)
				return pDst-pBufBegin;
		}

		while(m_newlines | m_spaces)
		{
			auto c = boost::iostreams::get(src);

			if (c == '\n' || c == '\r')
				m_newlines=true;
			else if (c == ' ' || c == '\t')
				m_spaces=true;
			else
			{
				if (m_newlines)
					*(pDst-1) = '\n';
				else if (m_spaces)
					*(pDst-1) = ' ';

				m_newlines=m_spaces=false;

				if (c == EOF)
					m_leftover='\0';
				else
					m_leftover = c;
			}
		}

		return pDst-pBufBegin;
	}

private:
	bool m_newlines=false;
	bool m_spaces=false;
	char m_leftover='\0';
};




#endif /* STORAGE_COMSOL_FLATTENWHITESPACE_HPP_ */
