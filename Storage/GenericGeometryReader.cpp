/*
 * GenericGeometryReader.cpp
 *
 *  Created on: Nov 1, 2017
 *      Author: jcassidy
 */

#include "GenericGeometryReader.hpp"

#include <iostream>

using namespace std;

pair<string,string> GenericGeometryReader::split_at_last(string s,char delim)
{
	size_t pos = s.find_last_of(delim);

	if (pos == string::npos)
		return make_pair(s,string());
	else
		return make_pair(
				s.substr(0,pos),
				s.substr(pos+1));
}

GenericGeometryReader::GenericGeometryReader()
{

}

GenericGeometryReader::~GenericGeometryReader()
{

}

void GenericGeometryReader::filename(string fn)
{
	m_filename=fn;
}

void GenericGeometryReader::read()
{
	string base,sfx;
	tie(base,sfx) = split_at_last(m_filename,'.');

	if (sfx == "gz" || sfx == "xz" || sfx == "bz2")
	{
		if (sfx == "gz")
		{
			cout << "gzip ";

		}
		else if (sfx == "xz")
		{
			cout << "lzma ";

		}
		else if (sfx == "bz2")
		{
			cout << "bzip2 ";

		}
		tie(base,sfx) = split_at_last(base,'.');
	}

	if (sfx == "vtk")
	{
		cout << "VTK ";
	}
	else if (sfx == "mci")
	{

	}
	else if (sfx == "out")
	{
		tie(base,sfx) = split_at_last(sfx,'.');
		if (sfx == "mve")
		{
			cout << "mean-variance energy " << endl;
		}
	}
	else if (sfx == "mphtxt")
	{
		cout << "COMSOL text ";
	}
	else if (sfx == "txt")
	{

	}
	else if (sfx == "mesh")
	{
		cout << "TIM-OS mesh ";
	}
	cout << endl;
}


