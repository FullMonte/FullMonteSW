/*
 * MMCTextMeshReader.cpp
 *
 *  Created on: Sep 19, 2017
 *      Author: jcassidy
 */

#include "MMCTextMeshReader.hpp"
#include <iostream>
#include <fstream>
#include <FullMonteSW/Geometry/PTTetraMeshBuilder.hpp>

#include <FullMonteSW/Logging/FullMonteLogger.hpp>

#include <FullMonteSW/Warnings/Push.hpp>
#include <FullMonteSW/Warnings/Boost.hpp>
#include <boost/filesystem.hpp>
#include <FullMonteSW/Warnings/Pop.hpp>

using namespace std;

MMCTextMeshReader::MMCTextMeshReader()
{
	m_builder=new PTTetraMeshBuilder();
}

MMCTextMeshReader::~MMCTextMeshReader()
{
	delete m_builder;
}

void MMCTextMeshReader::read()
{
	//m_builder->clear();

	boost::filesystem::path P = (boost::filesystem::path(m_path) / string("node_" + m_fileprefix + ".dat"));

	ifstream is(P.native());

	if (!is.good())
	{
		LOG_ERROR << "MMCTextMeshReader::read() failed to open node file " << P.native() << endl;
		return;
	}

	unsigned junk;
	unsigned Np;
	is >> junk >> Np;

	m_builder->setNumberOfPoints(Np+1);

	for(unsigned i=1;i<=Np;++i)
	{
		unsigned IDt;
		is >> IDt;
		array<float,3> p;
		is >> p[0] >> p[1] >> p[2];
		m_builder->setPoint(i,p);

		if (is.fail())
		{
			LOG_ERROR << "MMCTextMeshReader::read() failure reading tetras at number " << i << endl;
			return;
		}
	}



	is.close();
	is.open((boost::filesystem::path(m_path) / string("elem_" + m_fileprefix + ".dat")).native());

	if (!is.good())
	{
		LOG_ERROR << "MMCTextMeshReader::read() failed to open element file" << endl;
		return;
	}

	unsigned Nt;
	is >> junk >> Nt;

	m_builder->setNumberOfTetras(Nt+1);
	for(unsigned i=1;i<=Nt;++i)
	{
		unsigned IDt;
		is >> IDt;
		array<unsigned,4> IDps;
		unsigned region;
		for(unsigned j=0;j<4;++j)
			is >> IDps[j];
		is >> region;
		m_builder->setTetra(i,IDps,region);

		if (is.fail())
		{
			LOG_ERROR << "MMCTextMeshReader::read() failure reading tetras at number " << i << endl;
			return;
		}
	}

	m_builder->build();
}

TetraMesh* MMCTextMeshReader::mesh()
{
	return m_builder->mesh();
}

TetraMeshBuilder* MMCTextMeshReader::builder()
{
	return m_builder;
}

void MMCTextMeshReader::path(string p)
{
	m_path=p;
}

void MMCTextMeshReader::prefix(string pfx)
{
	m_fileprefix=pfx;
}
