/*
 * MMCJSONReader.hpp
 *
 *  Created on: Nov 21, 2017
 *      Author: jcassidy
 */

#ifndef STORAGE_MMC_MMCJSONREADER_HPP_
#define STORAGE_MMC_MMCJSONREADER_HPP_

#include <string>
#include <array>

class MMCJSONCase;

class MMCJSONReader
{
public:
	MMCJSONReader();
	virtual ~MMCJSONReader();

	void filename(std::string fn);
	void read();

	MMCJSONCase*	result() const;

private:
	std::string		m_filename;

	MMCJSONCase*	m_case=nullptr;
};


#endif /* STORAGE_MMC_MMCJSONREADER_HPP_ */
