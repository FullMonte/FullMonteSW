/*
 * MMCJSONReader.cpp
 *
 *  Created on: Nov 21, 2017
 *      Author: jcassidy
 */

#include "MMCJSONReader.hpp"

extern "C" {
#include <FullMonteSW/External/cJSON/cJSON.h>
}

#include <iostream>
#include <fstream>
#include <memory>
#include <functional>

#include <FullMonteSW/Logging/FullMonteLogger.hpp>
#include <FullMonteSW/Warnings/Push.hpp>
#include <FullMonteSW/Warnings/Boost.hpp>
#include <boost/filesystem.hpp>
#include <FullMonteSW/Warnings/Pop.hpp>

#include "MMCJSONCase.hpp"

using namespace std;

bool getJSONValue(cJSON* node,int& x)
{
	if (!node)
		return false;

	x = node->valueint;
	return true;
}

bool getJSONValue(cJSON* node,unsigned long long& x)
{
	if(!node)
		return false;

	x = node->valueint;
	return true;
}

bool getJSONValue(cJSON* node,unsigned& x)
{
	int y;
	if (!getJSONValue(node,y))
		return false;
	else if (y < 0)
		return false;

	x=y;
	return true;
}

bool getJSONValue(cJSON* node,double& x)
{
	if (!node)
		return false;

	x = node->valuedouble;
	return true;
}

bool getJSONValue(cJSON* node,float& x)
{
	double y;
	if (!getJSONValue(node,y))
		return false;

	x=y;
	return true;
}

bool getJSONValue(cJSON* node,string& s)
{
	if (!node || !node->valuestring)
		return false;

	s = string(node->valuestring);
	return true;
}

template<typename T>bool getJSONValue(cJSON* node,string objName,T& x)
{
	cJSON* obj = cJSON_GetObjectItem(node,objName.c_str());
	if (!obj)
		return false;
	else
		return getJSONValue(obj,x);
}

template<typename T,size_t N>bool getJSONValue(cJSON* node,string objName,array<T,N>& a)
{
	cJSON* arr = cJSON_GetObjectItem(node,objName.c_str());

	if (!arr)
		return false;

	cJSON* elem = arr->child;

	if (!elem)
		return false;

	unsigned i;
	for(i=0;i<N && elem;++i, elem=elem->next)
		getJSONValue(elem,a[i]);

	return i==N;
}

using namespace std;

MMCJSONReader::MMCJSONReader()
{
}

MMCJSONReader::~MMCJSONReader()
{
	delete m_case;
}

void MMCJSONReader::filename(std::string fn)
{
	m_filename=fn;
}

void MMCJSONReader::read()
{
	// delete existing case
	if (m_case)
		delete m_case;

	m_case = new MMCJSONCase();

	// determine file size and slurp it into a unique_ptr<char>
	boost::filesystem::path path(m_filename);
	uintmax_t sz = file_size(path);

	if (sz == -1ULL)
	{
		LOG_ERROR << "MMCJSONReader::read() failed to get size for file " << m_filename << endl;
		return;
	}

	unique_ptr<char> t(new char[sz]);

	ifstream is(m_filename.c_str());

	is.read(t.get(), sz);

	// parse the JSON and automatically delete when it's done

	auto deleter = [](cJSON* p){ cJSON_Delete(p); };

	unique_ptr<cJSON,decltype(deleter)> root(
			cJSON_Parse(t.get()),
			deleter);

	if (!root)
	{
		LOG_ERROR << "MMCJSONReader::read(): cJSON_Parse failed" << endl;
		return;
	}

	cJSON* domain = cJSON_GetObjectItem(root.get(),"domain");
	if (!domain)
	{
		LOG_ERROR << "MMCJSONReader::read() file missing Domain object" << endl;
		return;
	}

	string meshId;
	if (!getJSONValue(domain,"meshid",meshId))
	{
		LOG_ERROR << "MMCJSONReader::read() file missing Domain.MeshID object" << endl;
		return;
	}
	m_case->meshId(meshId);

	unsigned elem=0;
	if (!getJSONValue(domain,"initelem",elem))
	{
		LOG_ERROR << "MMCJSONReader::read() file missing Domain.InitElem object" << endl;
		return;
	}
	else
	{
		LOG_DEBUG << "InitElem=" << elem << endl;
	}
	m_case->initElem(elem);


	cJSON* session = cJSON_GetObjectItem(root.get(),"session");
	if (!session)
	{
		LOG_ERROR << "MMCJSONReader::read() file missing Session object" << endl;
		return;
	}


	unsigned long long photons;

	if (!getJSONValue(session,"photons",photons))
	{
		LOG_ERROR << "MMCJSONReader::read() file missing Session.Photons object" << endl;
		return;
	}

	m_case->photons(photons);

	string sessionId;
	if(!getJSONValue(session,"ID",sessionId))
	{
		LOG_ERROR << "MMCJSONReader::read() file missing Session.ID object" << endl;
		return;
	}

	m_case->sessionId(sessionId);



	cJSON* forward = cJSON_GetObjectItem(root.get(),"forward");
	if (!forward)
	{
		LOG_ERROR << "MMCJSONReader::read() file missing Forward object" << endl;
		return;
	}


	cJSON* optode = cJSON_GetObjectItem(root.get(),"optode");
	if (!optode)
	{
		LOG_ERROR << "MMCJSONReader::read() file missing Optode object" << endl;
		return;
	}


	cJSON* source = cJSON_GetObjectItem(optode,"source");
	if (!source)
	{
		LOG_ERROR << "MMCJSONReader::read() file missing Optode.Source object" << endl;
		return;
	}


	array<float,3> srcPos;
	if (!getJSONValue(source,"pos",srcPos))
	{
		LOG_ERROR << "MMCJSONReader::read() file missing Optode.Source.Pos object" << endl;
		return;
	}

	string srcType;
	if (!getJSONValue(source,"type",srcType))
	{
		LOG_ERROR << "MMCJSONReader::read() file missing Optode.Source.Type object" << endl;
		return;
	}


	if(srcType == "pencil")
	{
		array<float,3> srcDir;

		if (!getJSONValue(source,"dir",srcDir))
		{
			LOG_ERROR << "MMCJSONReader::read() file missing Optode.Source.Dir object" << endl;
			return;
		}

		m_case->setPencilSource(srcPos,srcDir,elem);
	}
	else if (srcType == "isotropic")
	{
		m_case->setPointSource(srcPos,elem);
	}
}

MMCJSONCase* MMCJSONReader::result() const
{
	return m_case->clone();
}
