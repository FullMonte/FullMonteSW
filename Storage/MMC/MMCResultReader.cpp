/*
 * MMCResultReader.cpp
 *
 *  Created on: Sep 14, 2017
 *      Author: jcassidy
 */

#include "MMCResultReader.hpp"

#include <FullMonteSW/Logging/FullMonteLogger.hpp>

#include <fstream>
#include <FullMonteSW/OutputTypes/SpatialMap.hpp>
#include <vector>

using namespace std;

MMCResultReader::MMCResultReader()
{

}

MMCResultReader::~MMCResultReader()
{

}

void MMCResultReader::filename(const std::string fn)
{
	m_filename=fn;
}

void MMCResultReader::timestep(float dt)
{
	m_timestep=dt;
}

void MMCResultReader::read()
{
	// create vector of size 1 (0 dummy element)
	vector<float> d(1,0.0f);

	ifstream is(m_filename.c_str());

	if (!is.good())
	{
		LOG_ERROR << "MMCResultReader::read() failed to read from file " << m_filename << endl;
	}

	while(!is.fail() && !is.eof())
	{
		unsigned i;
		float v;

		is >> i >> v;
		if (is.fail())
		{
			LOG_ERROR << "MMCResultReader::read() IO failure reading next line at index " << i << " after " << d.size() << " elements" << endl;
		}
		else if (i != d.size())
		{
			LOG_ERROR << "MMCResultReader::read() unexpected index value (expecting " << d.size() << ", found " << i << ')'<< endl;
		}
		else
			d.push_back(v*m_timestep);
	}

	LOG_DEBUG << "  MMC-format fluence read with " << d.size() << " elements" << endl;

	m_result = new SpatialMap<float>(move(d), AbstractSpatialMap::Volume, AbstractSpatialMap::Scalar);
}

SpatialMap<float>* MMCResultReader::output() const
{
	return m_result;
}
