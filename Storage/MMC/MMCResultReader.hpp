/*
 * MMCResultReader.hpp
 *
 *  Created on: Sep 14, 2017
 *      Author: jcassidy
 */

#ifndef STORAGE_MMC_MMCRESULTREADER_HPP_
#define STORAGE_MMC_MMCRESULTREADER_HPP_

#include <string>

template<typename T>class SpatialMap;

/** Reads an MMC output file
 *
 * Simple format: N lines, each <index=1..N> <value>
 *
 * *** NOTE *** DOES NOT SUPPORT MULTIPLE TIMESTEPS
 *
 * Since MMC uses 1-based indexing, this reader prepends a zero element to all fields
 *
 * Output is normalized for unit energy, giving results in terms of flux (aka fluence rate) of 1/mm2/s
 *
 * If a timestep is provided, results are multiplied by the length of the timestep to get fluence (flux integrated over time).
 */

class MMCResultReader
{
public:
	MMCResultReader();
	virtual ~MMCResultReader();

	void filename(std::string fn);

	void read();

	void timestep(float dt);

	SpatialMap<float>* 	output() const;

private:
	std::string			m_filename;
	float				m_timestep=1.0f;
	SpatialMap<float>* 	m_result=nullptr;
};




#endif /* STORAGE_MMC_MMCRESULTREADER_HPP_ */
