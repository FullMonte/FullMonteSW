/*
 * MMCOpticalReader.hpp
 *
 *  Created on: Nov 21, 2017
 *      Author: jcassidy
 */

#ifndef STORAGE_MMC_MMCOPTICALREADER_HPP_
#define STORAGE_MMC_MMCOPTICALREADER_HPP_

#include <string>

class MaterialSet;

class MMCOpticalReader
{
public:
	MMCOpticalReader();
	virtual ~MMCOpticalReader();

	void filename(std::string fn);
	void read();

	MaterialSet* 	materials() const;

private:
	std::string		m_filename;
	MaterialSet*	m_materials=nullptr;
};



#endif /* STORAGE_MMC_MMCOPTICALREADER_HPP_ */
