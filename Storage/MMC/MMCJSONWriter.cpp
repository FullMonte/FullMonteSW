#include "MMCJSONWriter.hpp"
#include <fstream>
#include <iostream>
#include <iomanip>

#include <FullMonteSW/Logging/FullMonteLogger.hpp>
#include <FullMonteSW/Warnings/Push.hpp>
#include <FullMonteSW/Warnings/Boost.hpp>
#include <boost/filesystem.hpp>
#include <FullMonteSW/Warnings/Pop.hpp>

using namespace std;

MMCJSONWriter::MMCJSONWriter()
{
}

MMCJSONWriter::~MMCJSONWriter()
{
}

void MMCJSONWriter::meshfilename(std::string fn)
{
	m_meshfilename=fn;
}

void MMCJSONWriter::packets(unsigned long long N)
{
	m_packets=N;
}

void MMCJSONWriter::initelem(unsigned t)
{
	m_initelem=t;
}

void MMCJSONWriter::mmccommand(std::string path)
{
	m_mmccommand=path;
}

void MMCJSONWriter::seed(unsigned i)
{
	m_seed=i;
}

void MMCJSONWriter::sessionname(std::string fn)
{
	m_sessionname=fn;
}

void MMCJSONWriter::timestep(float t0,float t1,float dt)
{
	m_t0=t0;
	m_t1=t1;
	m_dt=dt;
}

void MMCJSONWriter::srcpos(std::array<float,3> p)
{
	m_srcpos = p;
}

void MMCJSONWriter::direction(std::array<float,3> d)
{
	m_direction = d;
}

void MMCJSONWriter::filename(string fn)
{
	m_filename=fn;
}


void MMCJSONWriter::scriptfilename(string fn)
{
	m_scriptfilename=fn;
}

void MMCJSONWriter::write() const
{
	ofstream os(m_filename.c_str());

	if (!os.good()) {
		LOG_ERROR << "MMCJSONWriter::write() failed to open file " << m_filename << " for writing" << endl;
	}

	os << "{" << endl;

	os << "\t\"Domain\": {" << endl;
	os << "\t\t\"MeshID\": \"" << m_meshfilename << "\"," << endl;
	os << "\t\t\"InitElem\": " << m_initelem  << endl;
	os << "\t}," << endl;

	os << "\t\"Session\": {" << endl;
	os << "\t\t\"Photons\": " << m_packets << ',' << endl;
	os << "\t\t\"Seed\": " << m_seed << ',' << endl;
	os << "\t\t\"ID\": \"" << m_sessionname << '\"' << endl;
	os << "\t}," << endl;

	os << "\t\"Forward\": {" << endl;
	os << "\t\t\"T0\": " << m_t0 << ',' << endl;
	os << "\t\t\"T1\": " << m_t1 << ',' << endl;
	os << "\t\t\"Dt\": " << m_dt << endl;
	os << "\t}," << endl;

	os << "\t\"Optode\": {" << endl;
	os << "\t\t\"Source\": {" << endl;
	os << "\t\t\t\"Type\" : \"isotropic\"," << endl;
	os << "\t\t\t\"Pos\" : [" << m_srcpos[0] << ',' << m_srcpos[1] << ',' << m_srcpos[2] << "]," << endl;

	// dir is required even if isotropic (why?)
	os << "\t\t\"Dir\" : [" << m_direction[0] << ',' << m_direction[1] << ',' << m_direction[2] << "]" << endl;

	// unknown params
//	os << "\t\t\"Param1\" : [0.0, 0.0, 0.0]," << endl << ',';
//	os << "\t\t\"Param2\" : [0.0, 0.0, 0.0]," << endl;

	os << "\t\t}" << endl;

	os << "\t}" << endl;

	os << "}" << endl;
}

void MMCJSONWriter::writeCmdLine() const
{
	ofstream os(m_scriptfilename.c_str());

	//os << "#!/bin/bash" << endl;
	os << m_mmccommand <<
			" -f " << m_filename <<		// .json file
			" -C 0" <<					// zero-order interpolation basis (report tetra volume absorption scores)
			" -b 1" <<					// handle internal boundaries
			" -O E" << 					// output energy instead of flux (flux is busted when used with -C 0)
			" -e 1e-5" <<				// roulette threshold to match FullMonte (MMC uses 1e-6)
			endl;

	os.close();

	// set ug+rwx permissions to allow script to run
	boost::filesystem::permissions(m_scriptfilename,
			boost::filesystem::add_perms |
				boost::filesystem::owner_exe | boost::filesystem::group_exe |
				boost::filesystem::owner_write | boost::filesystem::group_write |
				boost::filesystem::owner_read | boost::filesystem::group_read);

}
