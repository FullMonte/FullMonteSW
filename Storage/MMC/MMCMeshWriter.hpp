/*
 * MMCMeshWriter.hpp
 *
 *  Created on: Sep 13, 2017
 *      Author: jcassidy
 */

#ifndef STORAGE_MMC_MMCMESHWRITER_HPP_
#define STORAGE_MMC_MMCMESHWRITER_HPP_

#include <string>

class TetraMesh;

/** Writes text-based MMC mesh format
 *
 * For filename <fn>, writes
 *
 * velem_<fn>	Volume of each element (each line <tetra ID> <volume>)
 * node_<fn>	Point coordinates (each line <point ID> <x> <y> <z>)
 * elem_<fn>	Element indices (each line <tetra ID> <p0> <p1> <p2> <p3> <region ID>)
 * facenb_<fn>	Per tetra: neighbouring tetras at each of the four faces (each line <IDt0> <IDt1> <IDt2> <IDt3>)
 *
 * vnode_<fn>	** NOT GENERATED ** absorption volume assigned to each node?
 *
 * MMC elements start counting at 1, so the FullMonte dummy element 0 is dropped when writing.
 *
 *
 * *** IMPORTANT NOTES ON ORDERING CONVENTIONS ***
 *
 * 1) MMC appears to use a clockwise convention for faces, while FullMonte uses counterclockwise; both seem to use the
 * 		minimum oriented lexicographical ordering (ie. CW/CCW oriented, then rotated so min index first). Consequently,
 * 		tetrahedral elements ABCD with A < B < (C,D) in FullMonte are ABDC in MMC
 *
 * 2) MMC rotates face order relative to FullMonte. Given the lexicographically minimal rotation of CCW-oriented tetra
 * 		ABCD, FullMonte considers faces ABC, ABD, ACD, BCD. MMC uses DAB, DBC, CAD, BAC, ie. permuted (1,0,3,2).
 *
 *
 * These reorderings are done during the writing phase.
 *
 */

class MMCMeshWriter
{
public:
	MMCMeshWriter();
	virtual ~MMCMeshWriter();

	void filename(std::string fn);

	void write();

	void mesh(TetraMesh* M);

private:
	void writeTetrahedra(std::string);
	void writePoints(std::string);
	void writeFaces(std::string);
	void writeVolumes(std::string);

	TetraMesh*		m_mesh=nullptr;
	std::string		m_filename;
};


#endif /* STORAGE_MMC_MMCMESHWRITER_HPP_ */
