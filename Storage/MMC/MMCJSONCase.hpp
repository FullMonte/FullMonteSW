/*
 * MMCJSONCase.hpp
 *
 *  Created on: Nov 21, 2017
 *      Author: jcassidy
 */

#ifndef STORAGE_MMC_MMCJSONCASE_HPP_
#define STORAGE_MMC_MMCJSONCASE_HPP_

#include <string>
#include <array>

namespace Source { class Abstract; }

/** Class to hold the info from an MMC .json file.
 *
 */

class MMCJSONCase
{
public:
	MMCJSONCase();
	virtual ~MMCJSONCase();

	MMCJSONCase* clone() const;

	enum SourceType { Unknown=0, Point, Pencil };

	void				photons(unsigned long long N);
	unsigned long long 	photons() const;

	void				initElem(unsigned e);
	unsigned			initElem() const;

	void				sessionId(std::string s);
	std::string			sessionId() const;

	void				meshId(std::string s);
	std::string			meshId() const;

	void				setPointSource(std::array<float,3> pos,unsigned elem);
	void				setPencilSource(std::array<float,3> pos,std::array<float,3> dir,unsigned elem);

	Source::Abstract*	createSource() const;

	void				print(std::ostream& os) const;

private:
	unsigned long long	m_photons=0;

	std::string			m_sessionId;
	std::string			m_meshId;

	SourceType			m_srcType=Unknown;
	unsigned			m_initElem=0;
	std::array<float,3>	m_srcPos{{.0f,.0f,.0f}};
	std::array<float,3> m_srcDir{{.0f,.0f,.0f}};

	//std::array<float,3> m_srcParam1{{.0f,.0f,.0f}};
	//std::array<float,3> m_srcParam2{{.0f,.0f,.0f}};
};



#endif /* STORAGE_MMC_MMCJSONCASE_HPP_ */
