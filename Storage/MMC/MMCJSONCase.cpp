/*
 * MMCJSONCase.cpp
 *
 *  Created on: Nov 21, 2017
 *      Author: jcassidy
 */

#include "MMCJSONCase.hpp"

#include <FullMonteSW/Logging/FullMonteLogger.hpp>
#include <FullMonteSW/Geometry/Sources/PencilBeam.hpp>
#include <FullMonteSW/Geometry/Sources/Point.hpp>

#include <iostream>

using namespace std;

template<typename T,size_t N>ostream& operator<<(std::ostream& os,const array<T,N>& a)
{
	os << a[0];
	for(unsigned i=1;i<N;++i)
		os << ',' << a[i];
	return os;
}


MMCJSONCase::MMCJSONCase()
{
}

MMCJSONCase::~MMCJSONCase()
{

}

void MMCJSONCase::photons(unsigned long long N)
{
	m_photons=N;
}

unsigned long long MMCJSONCase::photons() const
{
	return m_photons;
}

void MMCJSONCase::initElem(unsigned e)
{
	m_initElem=e;
}

unsigned MMCJSONCase::initElem() const
{
	return m_initElem;
}

void MMCJSONCase::sessionId(string s)
{
	m_sessionId=s;
}

string MMCJSONCase::sessionId() const
{
	return m_sessionId;
}

void MMCJSONCase::meshId(string s)
{
	m_meshId=s;
}

string MMCJSONCase::meshId() const
{
	return m_meshId;
}

void MMCJSONCase::setPointSource(array<float,3> pos,unsigned elem)
{
	m_srcType = Point;
	m_srcPos = pos;
	m_initElem = elem;
}

void MMCJSONCase::setPencilSource(array<float,3> pos,array<float,3> dir,unsigned elem)
{
	m_srcType = Pencil;
	m_srcPos = pos;
	m_srcDir = dir;
	m_initElem = elem;
}

Source::Abstract* MMCJSONCase::createSource() const
{
	Source::PencilBeam* pb = nullptr;
	Source::Point* p = nullptr;

	switch(m_srcType)
	{
	case Pencil:
		pb = new Source::PencilBeam();
		pb->position(m_srcPos);
		pb->direction(m_srcDir);
		pb->elementHint(m_initElem);
		return pb;

	case Point:
		p = new Source::Point();
		p->position(m_srcPos);
		p->elementHint(m_initElem);
		return p;

	case Unknown:
		break;
	}

	LOG_ERROR << "MMCJSONCase::source() has unknown source type" << endl;
	return nullptr;
}

MMCJSONCase* MMCJSONCase::clone() const
{
	return new MMCJSONCase(*this);
}

void MMCJSONCase::print(std::ostream& os) const
{
	os << "Mesh ID: " << m_meshId << endl;
	os << "Session ID: " << m_sessionId << endl;
	os << "Packets: " << m_photons << endl;

	switch(m_srcType)
	{
	case Unknown:
		os << "Unknown source type position " << m_srcPos << endl;
		break;
	case Point:
		os << "Point source position " << m_srcPos << " element " << m_initElem << endl;
		break;
	case Pencil:
		os << "Pencil beam position " << m_srcPos << " direction " << m_srcDir << " element " << m_initElem << endl;
		break;
	}
}
