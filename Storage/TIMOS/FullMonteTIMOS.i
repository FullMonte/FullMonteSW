#if defined(SWIGTCL)
%module FullMonteTIMOSTCL
#elif defined(SWIGPYTHON)
%module TIMOS
#else
	#warning Requested wrapping language not supported
#endif
%feature("autodoc", "3");


%begin %{
#include <FullMonteSW/Warnings/SWIG.hpp>
%}

%include "std_string.i"

%include "std_vector.i"
%include "../../Geometry/FullMonteGeometry_types.i"

%{
#include <FullMonteSW/Geometry/Sources/Abstract.hpp>
#include <FullMonteSW/Geometry/TetraMesh.hpp>
#include <FullMonteSW/Storage/TIMOS/TextFileReader.hpp>
#include <FullMonteSW/Storage/TIMOS/TIMOSMeshReader.hpp>
#include <FullMonteSW/Storage/TIMOS/TIMOSSourceReader.hpp>
#include <FullMonteSW/Storage/TIMOS/TIMOSMeshWriter.hpp>
#include <FullMonteSW/Storage/TIMOS/TIMOSMaterialReader.hpp>
#include <FullMonteSW/Storage/TIMOS/TIMOSOutputReader.hpp>
#include <FullMonteSW/Storage/TIMOS/TIMOSSourceWriter.hpp>
#include <FullMonteSW/Storage/TIMOS/TIMOSMaterialWriter.hpp>

%}

%include "TextFileReader.hpp"
%include "TIMOSMaterialReader.hpp"
%include "TIMOSMeshReader.hpp"
%include "TIMOSMeshWriter.hpp"
%include "TIMOSSourceReader.hpp"
%include "TIMOSOutputReader.hpp"
%include "TIMOSSourceWriter.hpp"
%include "FullMonteSW/Storage/TIMOS/TIMOSMaterialWriter.hpp"