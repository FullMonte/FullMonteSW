/*
 * TIMOS.cpp
 *
 *  Created on: Jan 26, 2016
 *      Author: jcassidy
 */

#include <FullMonteSW/Geometry/Sources/Point.hpp>
#include <FullMonteSW/Geometry/Sources/Volume.hpp>
#include <FullMonteSW/Geometry/Sources/SurfaceTri.hpp>
#include <FullMonteSW/Geometry/Sources/PencilBeam.hpp>

Source::Abstract*	TIMOS::convertToSource(const TIMOS::SourceDef tSrc)
{
	Source::Abstract* src=nullptr;

	switch(tSrc.type)
	{
	case TIMOS::SourceDef::Types::Volume:
		src = new Source::Volume(tSrc.w,tSrc.details.vol.tetID);
		break;

	case TIMOS::SourceDef::Types::PencilBeam:
		src = new Source::PencilBeam(
					tSrc.w,
					tSrc.details.pencilbeam.pos,
					tSrc.details.pencilbeam.dir,
					tSrc.details.pencilbeam.tetID);
		break;

	case TIMOS::SourceDef::Types::Face:
		src = new Source::SurfaceTri(
				tSrc.w,
				tSrc.details.face.IDps);
		break;

	case TIMOS::SourceDef::Types::Point:
		src = new Source::Point(
				tSrc.w,
				tSrc.details.point.pos);
		break;

	default:
		throw std::logic_error("TIMOSReader::convertToSource - invalid source type");
	}

	return src;
}

TIMOS::SourceDef TIMOS::convertFromSource(const Source::Abstract* src)
{
	TIMOS::SourceDef tSrc;

	tSrc.w=src->power();

	if (const Source::PencilBeam *pb = dynamic_cast<const Source::PencilBeam*>(src))
	{
		tSrc.type=TIMOS::SourceDef::Types::PencilBeam;
		tSrc.details.pencilbeam.pos = pb->position();
		tSrc.details.pencilbeam.dir = pb->direction();
	}
	else if (const Source::SurfaceTri *face = dynamic_cast<const Source::SurfaceTri*>(src))
	{
		tSrc.type=TIMOS::SourceDef::Types::Face;
		tSrc.details.face.IDps = face->triPointIDs();
	}
	else if (const Source::Volume *vol = dynamic_cast<const Source::Volume*>(src))
	{
		tSrc.type=TIMOS::SourceDef::Types::Volume;
		tSrc.details.vol.tetID = vol->elementID();
	}
	else if (const Source::Point *pt = dynamic_cast<const Source::Point*>(src))
	{
		tSrc.type=TIMOS::SourceDef::Types::Point;
		tSrc.details.point.pos = pt->position();
	}
	else
		throw std::logic_error("TIMOSReader::convertFromSource - invalid source type");
	return tSrc;
}



