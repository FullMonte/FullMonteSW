/*
 * TIMOSMaterialWriter.cpp
 *
 *  Created on: Sep 26, 2017
 *      Author: jcassidy
 */

#include "TIMOSMaterialWriter.hpp"

#include <FullMonteSW/Logging/FullMonteLogger.hpp>
#include <FullMonteSW/Geometry/MaterialSet.hpp>
#include <FullMonteSW/Geometry/Material.hpp>

#include <fstream>
#include <iomanip>
#include <iostream>

using namespace std;

TIMOSMaterialWriter::TIMOSMaterialWriter()
{

}

TIMOSMaterialWriter::~TIMOSMaterialWriter()
{

}

void TIMOSMaterialWriter::write()
{
	ofstream os(m_filename.c_str());

	if (!m_materials) {
		LOG_ERROR << "TIMOSMaterialWriter::write() failed because it no MaterialSet was provided" << endl;
    } else if (!os.good()) {
		LOG_ERROR << "TIMOSMaterialWriter::write() failed because it couldn't open the destination file" << endl;
    } else if (m_materials->size() < 2) {
		LOG_ERROR << "TIMOSMaterialWriter::write() failed because <2 materials provided" << endl;
    } else {
		os << "1" << endl << m_materials->size()-1 << endl;
		os << fixed << setprecision(m_precision);
		for(unsigned i=1;i<m_materials->size();++i)
			os << m_materials->get(i)->absorptionCoeff() << ' ' << m_materials->get(i)->scatteringCoeff() << ' ' <<
				m_materials->get(i)->anisotropy() << ' ' << m_materials->get(i)->refractiveIndex() << endl;
		os << "1" << endl;
		os << m_materials->exterior()->refractiveIndex() << endl;


	}
}

void TIMOSMaterialWriter::filename(string fn)
{
	m_filename=fn;
}

void TIMOSMaterialWriter::materials(MaterialSet* MS)
{
	m_materials=MS;
}
