/*
 * GenericGeometryReader.hpp
 *
 *  Created on: Nov 1, 2017
 *      Author: jcassidy
 */

#ifndef STORAGE_GENERICGEOMETRYREADER_HPP_
#define STORAGE_GENERICGEOMETRYREADER_HPP_

#include <string>
#include <tuple>

class Geometry;

class GenericGeometryReader
{
public:
	GenericGeometryReader();
	virtual ~GenericGeometryReader();

	void filename(std::string fn);

	void 		read();

	Geometry*	geometry();

private:

	static std::pair<std::string,std::string> 	split_at_last(const std::string s,char delim);
	std::string		m_filename;
	Geometry* 		m_geometry=nullptr;
};



#endif /* STORAGE_GENERICGEOMETRYREADER_HPP_ */
