#if defined(SWIGTCL)
%module FullMonteCOMSOLFileTCL
#elif defined(SWIGPYTHON)
%module COMSOLFile
#else
	#warning Requested wrapping language not supported
#endif
%feature("autodoc", "3");

%include "std_string.i"

%header %{
#include <FullMonteSW/Warnings/SWIG.hpp>
#include <FullMonteSW/Storage/COMSOL/COMSOLReader.hpp>
#include <FullMonteSW/Storage/COMSOL/COMSOLWriter.hpp>
%}

%include "COMSOLReader.hpp"
%include "COMSOLWriter.hpp"