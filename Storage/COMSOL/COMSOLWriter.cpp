/*
 * COMSOLWriter.cpp
 *
 *  Created on: Nov 14, 2017
 *      Author: jcassidy
 */


#include <FullMonteSW/Geometry/TetraMesh.hpp>
#include <FullMonteSW/Geometry/Point.hpp>
#include <FullMonteSW/Geometry/Partition.hpp>
#include <boost/range/adaptor/indexed.hpp>
#include "COMSOLWriter.hpp"
#include <fstream>
#include <iostream>
#include <iomanip>

using namespace std;

COMSOLWriter::COMSOLWriter()
{

}

COMSOLWriter::~COMSOLWriter()
{

}

void COMSOLWriter::filename(string fn)
{
	m_filename=fn;
}

void COMSOLWriter::mesh(TetraMesh* M)
{
	m_mesh=M;
}

void COMSOLWriter::write()
{
	ofstream os(m_filename.c_str());

	// Comment line
	os << "# Created by FullMonteSW COMSOLWriter" << endl << endl << endl;

	// Version
	unsigned major = 0;
	unsigned minor = 1;
	os << "# Major & minor version" << endl;
	os << major << ' ' << minor << endl;

	unsigned nTags=1;
	unsigned nTypes=1;

	// Tags
	os << nTags << " # number of tags" << endl;

	string tag="mesh1";
	os << "# Tags" << endl;
	os << tag.size() << ' ' << tag << endl;


	// Types
	os << nTypes << " # number of types" << endl;
	string type="obj";
	os << "# Types" << endl;
	os << type.size() << ' ' << type << endl << endl;



	////// OBJECT
	os << "# --------- Object 0 ----------" << endl << endl;
	os << 0 << ' ' << 0 << ' ' << 1 << endl;	// purpose unknown
	os << "4 Mesh # class" << endl;				// purpose unknown
	os << "4 # version" << endl;				// purpose unknown
	os << "3 #sdim" << endl;					// spatial dimension
	os << get(num_points,*m_mesh)-1 << " # number of mesh points" << endl;	// number of points excluding zeroPoint
	os << "0 # lowest mesh point index"<< endl << endl;							// lowest point index
	os << "# Mesh point coordinates" << endl;
	os << setprecision(m_precision);
	os << fixed;

	for(const auto p : m_mesh->pointRange())
	{
		if (p.value() == 0)
		{

		}
		else
		{
			const auto P = get(coords,*m_mesh,p);

			for(unsigned i=0;i<3;++i)
				os << setw(m_width) << P[i] << ' ';
			os << endl;
		}
	}


	////// ELEMENTS

	os << endl << "1 # number of element types" << endl;	// 1 element type: tetras
	os << "# Type #0" << endl << endl;
	os << "3 tet # type name" << endl << endl << endl;						// tetras
	os << "4 # number of nodes per element" << endl;							// 4 nodes/element
	unsigned Nt = get(num_tetras,*m_mesh);	// number of tetrahedrons in mesh excluding zeroTetra
	os << Nt-1 << " # number of elements" << endl;							// number of tets
	os << "# Elements" << endl;

	for(const auto t : m_mesh->tetras())
	{
		if (t.value() == 0)
		{

		}
		else
		{
			const auto IDps = get(points,*m_mesh,t);
			for(unsigned i=0;i<4;++i)
				os << setw(m_tetraIDWidth) << IDps[i]-1 << ' ';
			os << endl;
		}
	}

	os << endl << endl;
	os << Nt-1 << " # number of geometric entity indices" << endl;
	os << "# Geometric entity indices" << endl;

	Partition* regions = m_mesh->regions();

	for(unsigned i=1;i<regions->size();++i)
		os << regions->get(i) << endl;
}



