/*
 * COMSOLDataReader.cpp
 *
 *  Created on: Apr 25, 2018
 *      Author: jcassidy
 */

#include <fstream>
#include <array>

#include "COMSOLDataReader.hpp"

#include <FullMonteSW/Logging/FullMonteLogger.hpp>
#include <FullMonteSW/Geometry/Points.hpp>
#include <FullMonteSW/OutputTypes/SpatialMap.hpp>

using namespace std;

COMSOLDataReader::COMSOLDataReader()
{
}

COMSOLDataReader::~COMSOLDataReader()
{
}

void COMSOLDataReader::filename(string fn)
{
	m_filename=fn;
}

void COMSOLDataReader::setZeroOffset(unsigned toggle)
{
	m_zeroOffset = toggle;
}

void COMSOLDataReader::read()
{
	ifstream is;

	m_points.clear();
	m_data.clear();

	is.open(m_filename);
	if (!is.good())
	{
		LOG_ERROR << "COMSOLDataReader::read() failed to open file '" << m_filename << "'" << endl;
		return;
	}

	string S;
	while(is.peek() == '%')
		getline(is,S);

	while(!is.fail())
	{
		array<double,3> P;
		double phi;
		is >> P[0] >> P[1] >> P[2] >> phi;
		m_points.emplace_back(P);
		m_data.emplace_back(phi);
	}

	LOG_DEBUG << "Read " << m_data.size() << " data points" << endl;
}

SpatialMap<float>* COMSOLDataReader::data() const
{
	SpatialMap<float>* d = new SpatialMap<float>(m_data.size()+m_zeroOffset, AbstractSpatialMap::Point, AbstractSpatialMap::Scalar);
	for(unsigned i=0;i<m_zeroOffset;++i)
		d->set(i,0.0f);
	for(unsigned i=0;i<m_data.size();++i)
		d->set(i+m_zeroOffset,m_data[i]);
	return d;
}

Points* COMSOLDataReader::points() const
{
	Points* P = new Points();

	P->resize(m_points.size()+m_zeroOffset);
	for(unsigned i=0;i<m_zeroOffset;++i)
		P->set(i,{0.0,0.0,0.0});
	for(unsigned i=0;i<m_points.size();++i)
		P->set(i+m_zeroOffset, Point<3,double>(m_points[i]));

	return P;
}
