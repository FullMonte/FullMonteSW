/*
 * VTKSurfaceWriter.hpp
 *
 *  Created on: May 31, 2018
 *      Author: jcassidy
 */

#ifndef STORAGE_VTK_VTKSURFACEWRITER_HPP_
#define STORAGE_VTK_VTKSURFACEWRITER_HPP_

#include <string>
#include <vector>
#include <map>
#include <vtkPolyDataWriter.h>

class TetraMesh;
class OutputData;

/** Writes surface data out to a .vtk file.
 * Only the subset of faces specified in the data are written. Points and faces are relabeled appropriately.
 *
 * When given a DirectedSurface input, it writes an array with three components: entering (0), exiting (1), and total (2)
 *
 */

class VTKSurfaceWriter
{
public:
	VTKSurfaceWriter();
	~VTKSurfaceWriter();


	void addData(const char* name,OutputData* d);
	void removeData(OutputData* d);
	void removeData(const char* name);

	void					mesh(const TetraMesh* M);
	const TetraMesh* 	mesh() const;

	void 				filename(const char* fn);
	const char* 			filename() const;

	void 				write();
	void 				addHeaderComment(const char* comment);

private:
	std::string				m_filename;
	const TetraMesh*		m_mesh=nullptr;
	std::map<std::string,OutputData*> m_arrays;
	vtkPolyDataWriter* 		surfWriter;

	bool					m_manualHeader=false;
};


#endif /* STORAGE_VTK_VTKSURFACEWRITER_HPP_ */
