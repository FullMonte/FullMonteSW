/*
 * VTKMeshReader.hpp
 *
 *  Created on: Mar 12, 2018
 *      Author: jcassidy
 */

#ifndef STORAGE_VTK_VTKMESHREADER_HPP_
#define STORAGE_VTK_VTKMESHREADER_HPP_

#include <string>

class TetraMesh;
class Partition;

class vtkFullMonteMeshFromUnstructuredGrid;

class vtkUnstructuredGridReader;

/** Class for reading meshes from .vtk files (text & binary) using VTK's own classes.
 * Most of the work is done elsewhere; this is just a wrapper for convenience.
 */

class VTKMeshReader
{
public:
	VTKMeshReader();
	~VTKMeshReader();

	void			filename(std::string fn);
	void			read();

	TetraMesh*	mesh() const;
	Partition*	regions() const;

private:
	std::string								m_filename;
	vtkFullMonteMeshFromUnstructuredGrid*	m_converter=nullptr;
	vtkUnstructuredGridReader*				m_reader=nullptr;
};

#endif /* STORAGE_VTK_VTKMESHREADER_HPP_ */
