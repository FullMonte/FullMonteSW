/*
 * VTKMeshWriter.hpp
 *
 *  Created on: Mar 12, 2018
 *      Author: jcassidy
 */

#ifndef STORAGE_VTK_VTKMESHWRITER_HPP_
#define STORAGE_VTK_VTKMESHWRITER_HPP_

#include <string>
#include <map>

class TetraMesh;
class OutputData;

class vtkFullMonteTetraMeshWrapper;

class vtkUnstructuredGridWriter;
class vtkFullMonteArrayAdaptor;

class VTKMeshWriter
{
public:
	VTKMeshWriter();
	~VTKMeshWriter();

	void filename(std::string fn);
	void mesh(TetraMesh* m);

	void addData(const char* name,OutputData* d);
	void removeData(OutputData* d);
	void removeData(const char* name);

	void addHeaderComment(const char* comment);

	void write();

private:
	std::string							m_filename;
	TetraMesh*							m_mesh=nullptr;
	vtkFullMonteTetraMeshWrapper*		m_wrapper=nullptr;
	vtkUnstructuredGridWriter*			m_writer=nullptr;

	bool								m_writeRegions=true;		///< If true, include a field for the mesh's regions
	bool								m_manualHeader=false;

	std::map<std::string,OutputData*>				m_arrays;
	std::map<std::string,vtkFullMonteArrayAdaptor*>	m_arrayAdaptors;
};

#endif /* STORAGE_VTK_VTKMESHWRITER_HPP_ */
