/*
 * VTKMeshWriter.cpp
 *
 *  Created on: Mar 12, 2018
 *      Author: jcassidy
 */

#include "VTKMeshWriter.hpp"

#include <FullMonteSW/Logging/FullMonteLogger.hpp>

#include <FullMonteSW/VTK/vtkFullMonteTetraMeshWrapper.h>
#include <FullMonteSW/VTK/vtkFullMonteArrayAdaptor.h>

#include <FullMonteSW/Warnings/Push.hpp>
#include <FullMonteSW/Warnings/VTK.hpp>
#include <vtkUnstructuredGridWriter.h>
#include <vtkUnstructuredGrid.h>
#include <vtkCellData.h>
#include <FullMonteSW/Warnings/Pop.hpp>

using namespace std;

VTKMeshWriter::VTKMeshWriter()
{
	m_writer = vtkUnstructuredGridWriter::New();
	m_wrapper = vtkFullMonteTetraMeshWrapper::New();
}

VTKMeshWriter::~VTKMeshWriter()
{
	m_wrapper->Delete();
	m_writer->Delete();
}

void VTKMeshWriter::filename(string fn)
{
	m_filename=fn;
}

void VTKMeshWriter::mesh(TetraMesh* m)
{
	m_mesh=m;
}

void VTKMeshWriter::addData(const char* name,OutputData* d)
{
	auto res = m_arrays.insert(make_pair(name,d));

	if (!res.second) {
		LOG_ERROR << "Failed to add array named '" << name << "' because the name already exists" << endl;
    }
}

void VTKMeshWriter::removeData(const char* name)
{
	if (!m_arrays.count(name)) {
	    LOG_ERROR << "Failed to remove array named '" << name << "' because it doesn't exist" << endl;
    }
	m_arrays.erase(name);
}

void VTKMeshWriter::removeData(OutputData* d)
{
	auto it = m_arrays.begin();
	for(; it != m_arrays.end() && it->second != d; ++it){}
	if (it != m_arrays.end()) {
		m_arrays.erase(it);
    } else {
		LOG_ERROR << "Failed to remove array " << d << "' because it doesn't exist" << endl;
    }
}

void VTKMeshWriter::addHeaderComment(const char* comment)
{
	m_writer->SetHeader(comment);
	m_manualHeader = true;
}

void VTKMeshWriter::write()
{
	// update the mesh if needed
	m_wrapper->mesh(m_mesh);
	// This command has been commented out because m_wrapper->mesh() already calls update().
	// We do not need to call update() twice.
	//m_wrapper->update(); 

	vtkUnstructuredGrid* ug = nullptr;

	if (m_writeRegions)
		ug = m_wrapper->regionMesh();
	else
		ug = m_wrapper->blankMesh();

	// get pre-existing array adaptor, or create new one
	map<string,vtkFullMonteArrayAdaptor*>::iterator it;
	bool inserted = false;
	// add data arrays
	for(const auto a : m_arrays)
	{		
		tie(it,inserted) = m_arrayAdaptors.insert(make_pair(a.first,nullptr));
		if (inserted)
			it->second = vtkFullMonteArrayAdaptor::New();
		// update source to point to specified array
		it->second->source(a.second);
		// This command has been commented out because it->second->source() already calls update().
		// We do not need to call update twice.
		//it->second->update();
		// it->second->array()->SetName(a.first.c_str());
		string merge = "";
		switch(a.second->outputType())
		{
			case OutputData::Energy:
			switch(a.second->unitType())
			{
				case OutputData::J_m:
				case OutputData::J_cm:
				case OutputData::J_mm:
					merge = a.first + " - Energy (J)";
					it->second->array()->SetName(merge.c_str());
					break;
				case OutputData::W_m:
				case OutputData::W_cm:
				case OutputData::W_mm:
					merge = a.first + " - Power (W)";
					it->second->array()->SetName(merge.c_str());
					break;
				default:
					merge = a.first + " - Energy (?)";
					it->second->array()->SetName(merge.c_str());
					break;
			}	
				break;
			case OutputData::Fluence:
				switch(a.second->unitType())
				{
					case OutputData::J_m:
						merge = a.first + " - Fluence (J/m^2)";
						it->second->array()->SetName(merge.c_str());
						break;
					case OutputData::J_cm:
						merge = a.first + " - Fluence (J/cm^2)";
						it->second->array()->SetName(merge.c_str());
						break;
					case OutputData::J_mm:
						merge = a.first + " - Fluence (J/mm^2)";
						it->second->array()->SetName(merge.c_str());
						break;
					case OutputData::W_m:
						merge = a.first + " - Fluence rate (W/m^2)";
						it->second->array()->SetName(merge.c_str());
						break;
					case OutputData::W_cm:
						merge = a.first + " - Fluence rate (W/cm^2)";
						it->second->array()->SetName(merge.c_str());
						break;
					case OutputData::W_mm:
						merge = a.first + " - Fluence rate (W/mm^2)";
						it->second->array()->SetName(merge.c_str());
						break;
					default:
						merge = a.first + " - Fluence (?)";
						it->second->array()->SetName(merge.c_str());
						break;
				}	
				break;
			case OutputData::PhotonWeight:
				merge = a.first + " - PhotonWeight (?)";
				it->second->array()->SetName(merge.c_str());
				break;
			case OutputData::EnergyPerVolume:
				switch(a.second->unitType())
				{
					case OutputData::J_m:
						merge = a.first + " - Energy/Volume (J/m^3)";
						it->second->array()->SetName(merge.c_str());
						break;
					case OutputData::J_cm:
						merge = a.first + " - Energy/Volume (J/cm^3)";
						it->second->array()->SetName(merge.c_str());
						break;
					case OutputData::J_mm:
						merge = a.first + " - Energy/Volume (J/mm^3)";
						it->second->array()->SetName(merge.c_str());
						break;
					case OutputData::W_m:
						merge = a.first + " - Power/Volume (W/m^3)";
						it->second->array()->SetName(merge.c_str());
						break;
					case OutputData::W_cm:
						merge = a.first + " - Power/Volume (W/cm^3)";
						it->second->array()->SetName(merge.c_str());
						break;
					case OutputData::W_mm:
						merge = a.first + " - Power/Volume (W/mm^3)";
						it->second->array()->SetName(merge.c_str());
						break;
					default:
						merge = a.first + " - Energy/Volume (?)";
						it->second->array()->SetName(merge.c_str());
						break;
				}
				break;
			default:
				merge = a.first + " - Unknown (?)";
				it->second->array()->SetName(merge.c_str());
				break;
		}
		ug->GetCellData()->AddArray(it->second->array());
	}

	auto iter = m_arrays.begin();
	if(m_manualHeader == false)
	{
		switch(iter->second->unitType())
					{
						case OutputData::J_m:
							m_writer->SetHeader("Energy unit: J; Dimension unit: m");
							break;
						case OutputData::J_cm:
							m_writer->SetHeader("Energy unit: J; Dimension unit: cm");
							break;
						case OutputData::J_mm:
							m_writer->SetHeader("Energy unit: J; Dimension unit: mm");
							break;
						case OutputData::W_m:
							m_writer->SetHeader("Power unit: W; Dimension unit: m");
							break;
						case OutputData::W_cm:
							m_writer->SetHeader("Power unit: W; Dimension unit: cm");
							break;
						case OutputData::W_mm:
							m_writer->SetHeader("Power unit: W; Dimension unit: mm");
							break;
						default:
							break;
					}
	}


	m_writer->SetFileName(m_filename.c_str());
	m_writer->SetInputData(ug);
	m_writer->Update();

	// If we have allocated an vtkFullMonteArrayAdaptor object in the for loop,
	// we need to release the allocated memory as well.
	if(inserted)
		it->second->Delete();
}

