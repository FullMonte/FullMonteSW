#ifndef STORAGE_VTK_VTKPHOTONPATHWRITER_HPP_
#define STORAGE_VTK_VTKPHOTONPATHWRITER_HPP_

#include <string>

using namespace std;

class OutputData;
class PacketPositionTraceSet;

class vtkFullMontePacketPositionTraceSetToPolyData;

class VTKPhotonPathWriter
{
public:
	VTKPhotonPathWriter();
	~VTKPhotonPathWriter();

	void filename(string fn);
	void source(OutputData* d);

	void write();

private:
	string							    m_filename;
	PacketPositionTraceSet*				m_data=nullptr;
    vtkFullMontePacketPositionTraceSetToPolyData* m_writer=nullptr;
};


#endif // STORAGE_VTK_VTKPHOTONPATHWRITER_HPP_