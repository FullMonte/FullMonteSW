/*
 * VTKMeshReader.cpp
 *
 *  Created on: Mar 12, 2018
 *      Author: jcassidy
 */

#include "VTKMeshReader.hpp"

#include <FullMonteSW/Geometry/TetraMesh.hpp>
#include <FullMonteSW/Geometry/Partition.hpp>

#include <FullMonteSW/VTK/vtkFullMonteMeshFromUnstructuredGrid.h>

#include <FullMonteSW/Warnings/Push.hpp>
#include <FullMonteSW/Warnings/VTK.hpp>
#include <vtkUnstructuredGridReader.h>
#include <FullMonteSW/Warnings/Pop.hpp>

using namespace std;

VTKMeshReader::VTKMeshReader()
{
	m_converter = vtkFullMonteMeshFromUnstructuredGrid::New();
	m_reader = vtkUnstructuredGridReader::New();
}

VTKMeshReader::~VTKMeshReader()
{
	m_converter->Delete();
	m_reader->Delete();
}

void VTKMeshReader::filename(string fn)
{
	m_filename=fn;
}

void VTKMeshReader::read()
{
	m_reader->SetFileName(m_filename.c_str());
	m_reader->Update(); //This actually does nothing; in theory, it should give the data structures time to update.

	m_converter->unstructuredGrid(m_reader->GetOutput());
	m_converter->update();
}

TetraMesh* VTKMeshReader::mesh() const
{
	return m_converter->mesh();
}

Partition* VTKMeshReader::regions() const
{
	return m_converter->mesh()->regions();
}
