/*
 * VTKSurfaceWriter.cpp
 *
 *  Created on: May 31, 2018
 *      Author: jcassidy
 */

#include "VTKSurfaceWriter.hpp"

#include <FullMonteSW/Logging/FullMonteLogger.hpp>
#include <FullMonteSW/Geometry/TetraMesh.hpp>
#include <FullMonteSW/Geometry/Points.hpp>
#include <FullMonteSW/Geometry/FaceLinks.hpp>
#include <FullMonteSW/Geometry/Permutation.hpp>

#include <FullMonteSW/Geometry/Queries/DynamicIndexRelabel.hpp>

#include <FullMonteSW/OutputTypes/DirectedSurface.hpp>
#include <FullMonteSW/OutputTypes/SpatialMap.hpp>

#include <FullMonteSW/Warnings/Push.hpp>
#include <FullMonteSW/Warnings/VTK.hpp>
#include <vtkPoints.h>
#include <vtkFloatArray.h>
#include <vtkPolyData.h>
#include <vtkPointData.h>

#include <vtkCellArray.h>
#include <vtkIdTypeArray.h>
#include <vtkCellData.h>
#include <FullMonteSW/Warnings/Pop.hpp>


VTKSurfaceWriter::VTKSurfaceWriter()
{
	surfWriter = vtkPolyDataWriter::New();
}

VTKSurfaceWriter::~VTKSurfaceWriter()
{
	surfWriter->Delete();
}

void VTKSurfaceWriter::filename(const char* fn)
{
	m_filename=fn;
}

const char* VTKSurfaceWriter::filename() const
{
	return m_filename.c_str();
}

void VTKSurfaceWriter::addData(const char* name,OutputData* d)
{
	auto res = m_arrays.insert(make_pair(name,d));

	if (!res.second) {
		LOG_ERROR << "Failed to add array named '" << name << "' because the name already exists" << endl;
    }
}

void VTKSurfaceWriter::removeData(const char* name)
{
	if (!m_arrays.count(name)) {
	    LOG_ERROR << "Failed to remove array named '" << name << "' because it doesn't exist" << endl;
    }
	m_arrays.erase(name);
}

void VTKSurfaceWriter::removeData(OutputData* d)
{
	auto it = m_arrays.begin();
	for(; it != m_arrays.end() && it->second != d; ++it){}
	if (it != m_arrays.end()) {
		m_arrays.erase(it);
    } else {
		LOG_ERROR << "Failed to remove array " << d << "' because it doesn't exist" << endl;
    }
}

void VTKSurfaceWriter::mesh(const TetraMesh* M)
{
	m_mesh=M;
}

const TetraMesh* VTKSurfaceWriter::mesh() const
{
	return m_mesh;
}

void VTKSurfaceWriter::addHeaderComment(const char* comment)
{
	surfWriter->SetHeader(comment);
	m_manualHeader = true;
}

void VTKSurfaceWriter::write()
{
	if (!m_mesh)
	{
		LOG_ERROR << "VTKSurfaceWriter::write() failed for lack of mesh" << endl;
		return;
	}

	// DirectedSurface is only used by TetraInternalKernel for directed surfaces.
	const DirectedSurface* ds = nullptr;
	
	// SpatialMap is the output of TetraSurfaceKernel. If nothing odd happens, 
	// m_data should either be able to be cast to DirectedSurface or to SpatialMap
	const SpatialMap<float>* sd;

	bool allDirectedSurface = true;
	bool allSpatialMap = true;
	for(auto a: m_arrays)
	{
		ds = dynamic_cast<const DirectedSurface*>(a.second);
		if (!ds)
			allDirectedSurface = false;

		sd = dynamic_cast<const SpatialMap<float>*>(a.second);
		if(!sd)
			allSpatialMap = false;
	}

	if (!allDirectedSurface && !allSpatialMap)
	{
		LOG_ERROR << "VTKSurfaceWriter::write() failed because input data can't be cast exclusively to DirectedSurface or SpatialMap" << endl;
		return;
	}

	// Initialize the fluence values as vtkFloatArray for the vtk mesh
	vector<vtkFloatArray*> vtkOutArrays;
	// Initialize the cell information for the vtk mesh
	vtkCellArray* ca = vtkCellArray::New();
	// Permutation of three points that belong to the faces (which are triangles) 
	Permutation* facePointPerm;
	
	// Helper variables to filter only the external surface faces from the SurfaceExitEnergy output data
	// value stores the fluence values of the faces in ascending order
	std::vector<vector<float>> value;
	// idx stores the index of the face which belongs the external surface
	std::vector<int> idx;
	if(!allDirectedSurface)
	{
		value.resize(m_arrays.size());
		vtkOutArrays.resize(m_arrays.size());
		unsigned data_count_idx = 0;
		int hits = -1;
		for (auto a: m_arrays)
		{
			vtkOutArrays[data_count_idx] = vtkFloatArray::New();
			sd = dynamic_cast<const SpatialMap<float>*>(a.second);

			// for-loop: go through all faces of the mesh and filter out the faces that belong to the external surface of the mesh
			// We need to ignore the first entry of the results because it is associated with the zero tetra (in this case the zero face) 
			// and that should always be 0.
			if (idx.size() > 0)
			{
				for(unsigned i=0; i < idx.size(); i++)
					value[data_count_idx].push_back(sd->get(idx[i]));
			}
			else
			{
				// count the number of faces that belong to the external surface
				hits = -1;
				for(unsigned i=1;i<sd->dim();++i)
				{
					// TetraMesh::FaceDescriptor(i) gets important information (neighboring tetras) of the current face
					const auto FTL = get(face_link, *m_mesh,TetraMesh::FaceDescriptor(i));
					// If either the upper or lower tetra of the face has the value 0, this is an external surface face
					if (FTL.tets.at(0).T.value() == 0 || FTL.tets.at(1).T.value() == 0)
					{
						hits++;
						idx.push_back(i);
						value[data_count_idx].push_back(sd->get(i));
					}
				}
			}
			// Set up and populate data fields

			// vtkOutArrays[data_count_idx]->SetNumberOfComponents(1);
			vtkOutArrays[data_count_idx]->SetNumberOfTuples(hits); // number of elements that will be inserted into struct
			
			string merge = "";
			switch(a.second->outputType())
			{
				case OutputData::Energy:
					switch(a.second->unitType())
					{
						case OutputData::J_m:
							merge = a.first + " - Energy (J;m) exiting mesh";
							vtkOutArrays[data_count_idx]->SetName(merge.c_str());
							break;
						case OutputData::J_cm:
							merge = a.first + " - Energy (J;cm) exiting mesh";
							vtkOutArrays[data_count_idx]->SetName(merge.c_str());
							break;
						case OutputData::J_mm:
							merge = a.first + " - Energy (J;mm) exiting mesh";
							vtkOutArrays[data_count_idx]->SetName(merge.c_str());
							break;
						case OutputData::W_m:
							merge = a.first + " - Power (W;m) exiting mesh";
							vtkOutArrays[data_count_idx]->SetName(merge.c_str());
							break;
						case OutputData::W_cm:
							merge = a.first + " - Power (W;cm) exiting mesh";
							vtkOutArrays[data_count_idx]->SetName(merge.c_str());
							break;
						case OutputData::W_mm:
							merge = a.first + " - Power (W;mm) exiting mesh";
							vtkOutArrays[data_count_idx]->SetName(merge.c_str());
							break;
						default:
							merge = a.first + " - Energy (?) exiting mesh";
							vtkOutArrays[data_count_idx]->SetName(merge.c_str());
							break;
					}	
					break;
				case OutputData::Fluence:
					switch(a.second->unitType())
					{
						case OutputData::J_m:
							merge = a.first + " - Fluence (J/m^2) exiting mesh";
							vtkOutArrays[data_count_idx]->SetName(merge.c_str());
							break;
						case OutputData::J_cm:
							merge = a.first + " - Fluence (J/cm^2) exiting mesh";
							vtkOutArrays[data_count_idx]->SetName(merge.c_str());
							break;
						case OutputData::J_mm:
							merge = a.first + " - Fluence (J/mm^2) exiting mesh";
							vtkOutArrays[data_count_idx]->SetName(merge.c_str());
							break;
						case OutputData::W_m:
							merge = a.first + " - Fluence rate (W/m^2) exiting mesh";
							vtkOutArrays[data_count_idx]->SetName(merge.c_str());
							break;
						case OutputData::W_cm:
							merge = a.first + " - Fluence rate (W/cm^2) exiting mesh";
							vtkOutArrays[data_count_idx]->SetName(merge.c_str());
							break;
						case OutputData::W_mm:
							merge = a.first + " - Fluence rate (W/mm^2) exiting mesh";
							vtkOutArrays[data_count_idx]->SetName(merge.c_str());
							break;
						default:
							merge = a.first + " - Fluence (?) exiting mesh";
							vtkOutArrays[data_count_idx]->SetName(merge.c_str());
							break;
					}	
					break;
				case OutputData::PhotonWeight:
					merge = a.first + " -  - PhotonWeight (?) exiting mesh";
					vtkOutArrays[data_count_idx]->SetName(merge.c_str());
					break;
				case OutputData::EnergyPerVolume:
					switch(a.second->unitType())
					{
						case OutputData::J_m:
							merge = a.first + " - Energy/Volume (J/m^3) exiting mesh";
							vtkOutArrays[data_count_idx]->SetName(merge.c_str());
							break;
						case OutputData::J_cm:
							merge = a.first + " - Energy/Volume (J/cm^3) exiting mesh";
							vtkOutArrays[data_count_idx]->SetName(merge.c_str());
							break;
						case OutputData::J_mm:
							merge = a.first + " - Energy/Volume (J/mm^3) exiting mesh";
							vtkOutArrays[data_count_idx]->SetName(merge.c_str());
							break;
						case OutputData::W_m:
							merge = a.first + " - Power/Volume (W/m^3) exiting mesh";
							vtkOutArrays[data_count_idx]->SetName(merge.c_str());
							break;
						case OutputData::W_cm:
							merge = a.first + " - Power/Volume (W/cm^3) exiting mesh";
							vtkOutArrays[data_count_idx]->SetName(merge.c_str());
							break;
						case OutputData::W_mm:
							merge = a.first + " - Power/Volume (W/mm^3) exiting mesh";
							vtkOutArrays[data_count_idx]->SetName(merge.c_str());
							break;
						default:
							merge = a.first + " - Energy/Volume (?) exiting mesh";
							vtkOutArrays[data_count_idx]->SetName(merge.c_str());

							break;
					}
					break;
				default:
					merge = a.first + " - Unknown (?) exiting mesh";
					vtkOutArrays[data_count_idx]->SetName(merge.c_str());
					break;
			}
			
			// vtkOutArrays[data_count_idx]->SetComponentName(0,"total");

			for(int i=0; i<hits;++i)
			{
				// populate data fields with surface fluence data
				vtkOutArrays[data_count_idx]->SetTypedComponent(i,0,value[data_count_idx][i]);
			}
			data_count_idx++;
		}
		//Relabel face subset and write out
		DynamicIndexRelabel relabel;
		
		// Create cells, relabeling points as we go
		vtkIdTypeArray* ids = vtkIdTypeArray::New();

		// We have 4 times the amount of faces wich describe the face triangles
		// 1. vtkType --> Triangle or Tetra (more options possible)
		// 2. Point 1 of triangle
		// 3. Point 2 of triangle
		// 4. Point 3 of triangle
		ids->SetNumberOfTuples(4*hits);
		
		for(int i=0;i<hits;++i)
		{			
			// get point indices for the present face
			const auto IDps = get(points,*m_mesh,TetraMesh::FaceDescriptor(idx[i]));

			// write cell out, relabeling points as we go
			ids->SetValue((i<<2)+0, 3);
			for(unsigned j=0;j<3;++j)
				ids->SetValue((i<<2)+j+1, relabel(IDps[j].value()));	
		}

		// Create Cells based on the extracted triangle points and the number of cells of which the mesh consists
		ca->SetCells(hits, ids);
		// write out the permuted points
		facePointPerm = relabel.permutation();
	}
	else
	{
		vtkOutArrays.resize(m_arrays.size()*3);
		unsigned data_count_idx = 0;
		for (auto a: m_arrays)
		{
			vtkOutArrays[data_count_idx] = vtkFloatArray::New();
			vtkOutArrays[data_count_idx+1] = vtkFloatArray::New();
			vtkOutArrays[data_count_idx+2] = vtkFloatArray::New();
			ds = dynamic_cast<const DirectedSurface*>(a.second);
			////// Set up and populate data fields
			// vtkOutArrays[data_count_idx]->SetNumberOfComponents(3);
			
			vtkOutArrays[data_count_idx]->SetNumberOfTuples(ds->exiting()->dim());
			vtkOutArrays[data_count_idx+1]->SetNumberOfTuples(ds->entering()->dim());
			vtkOutArrays[data_count_idx+2]->SetNumberOfTuples(ds->entering()->dim());
			
			string mergeEntering = "";
			string mergeExiting = "";
			string mergeTotal = "";
			switch(a.second->outputType())
			{
				case OutputData::Energy:
					switch(a.second->unitType())
					{
						case OutputData::J_m:
						case OutputData::J_cm:
						case OutputData::J_mm:
							mergeEntering = a.first + " - Energy (J) entering region";
							mergeExiting = a.first + " - Energy (J) exiting region";
							mergeTotal = a.first + " - Energy (J) total";
							vtkOutArrays[data_count_idx]->SetName(mergeEntering.c_str());
							vtkOutArrays[data_count_idx+1]->SetName(mergeExiting.c_str());
							vtkOutArrays[data_count_idx+2]->SetName(mergeTotal.c_str());
							break;
						case OutputData::W_m:
						case OutputData::W_cm:
						case OutputData::W_mm:
							mergeEntering = a.first + " - Power (W) entering region";
							mergeExiting = a.first + " - Power (W) exiting region";
							mergeTotal = a.first + " - Power (W) total";
							vtkOutArrays[data_count_idx]->SetName(mergeEntering.c_str());
							vtkOutArrays[data_count_idx+1]->SetName(mergeExiting.c_str());
							vtkOutArrays[data_count_idx+2]->SetName(mergeTotal.c_str());
							break;
						default:
							mergeEntering = a.first + " - Energy (?) entering region";
							mergeExiting = a.first + " - Energy (?) exiting region";
							mergeTotal = a.first + " - Energy (?) total";
							vtkOutArrays[data_count_idx]->SetName(mergeEntering.c_str());
							vtkOutArrays[data_count_idx+1]->SetName(mergeExiting.c_str());
							vtkOutArrays[data_count_idx+2]->SetName(mergeTotal.c_str());
							break;
					}	
					break;
				case OutputData::Fluence:
					switch(a.second->unitType())
					{
						case OutputData::J_m:
							mergeEntering = a.first + " - Fluence (J/m^2) entering region";
							mergeExiting = a.first + " - Fluence (J/m^2) exiting region";
							mergeTotal = a.first + " - Fluence (J/m^2) total";
							vtkOutArrays[data_count_idx]->SetName(mergeEntering.c_str());
							vtkOutArrays[data_count_idx+1]->SetName(mergeExiting.c_str());
							vtkOutArrays[data_count_idx+2]->SetName(mergeTotal.c_str());
							break;
						case OutputData::J_cm:
							mergeEntering = a.first + " - Fluence (J/cm^2) entering region";
							mergeExiting = a.first + " - Fluence (J/cm^2) exiting region";
							mergeTotal = a.first + " - Fluence (J/cm^2) total";
							vtkOutArrays[data_count_idx]->SetName(mergeEntering.c_str());
							vtkOutArrays[data_count_idx+1]->SetName(mergeExiting.c_str());
							vtkOutArrays[data_count_idx+2]->SetName(mergeTotal.c_str());
							break;
						case OutputData::J_mm:
							mergeEntering = a.first + " - Fluence (J/mm^2) entering region";
							mergeExiting = a.first + " - Fluence (J/mm^2) exiting region";
							mergeTotal = a.first + " - Fluence (J/mm^2) total";
							vtkOutArrays[data_count_idx]->SetName(mergeEntering.c_str());
							vtkOutArrays[data_count_idx+1]->SetName(mergeExiting.c_str());
							vtkOutArrays[data_count_idx+2]->SetName(mergeTotal.c_str());
							break;
						case OutputData::W_m:
							mergeEntering = a.first + " - Fluence rate (W/m^2) entering region";
							mergeExiting = a.first + " - Fluence rate (W/m^2) exiting region";
							mergeTotal = a.first + " - Fluence rate (W/m^2) total";
							vtkOutArrays[data_count_idx]->SetName(mergeEntering.c_str());
							vtkOutArrays[data_count_idx+1]->SetName(mergeExiting.c_str());
							vtkOutArrays[data_count_idx+2]->SetName(mergeTotal.c_str());
							break;
						case OutputData::W_cm:
							mergeEntering = a.first + " - Fluence rate (W/cm^2) entering region";
							mergeExiting = a.first + " - Fluence rate (W/cm^2) exiting region";
							mergeTotal = a.first + " - Fluence rate (W/cm^2) total";
							vtkOutArrays[data_count_idx]->SetName(mergeEntering.c_str());
							vtkOutArrays[data_count_idx+1]->SetName(mergeExiting.c_str());
							vtkOutArrays[data_count_idx+2]->SetName(mergeTotal.c_str());
							break;
						case OutputData::W_mm:
							mergeEntering = a.first + " - Fluence rate (W/mm^2) entering region";
							mergeExiting = a.first + " - Fluence rate (W/mm^2) exiting region";
							mergeTotal = a.first + " - Fluence rate (W/mm^2) total";
							vtkOutArrays[data_count_idx]->SetName(mergeEntering.c_str());
							vtkOutArrays[data_count_idx+1]->SetName(mergeExiting.c_str());
							vtkOutArrays[data_count_idx+2]->SetName(mergeTotal.c_str());
							break;
						default:
							mergeEntering = a.first + " - Fluence (?) entering region";
							mergeExiting = a.first + " - Fluence (?) exiting region";
							mergeTotal = a.first + " - Fluence (?) total";
							vtkOutArrays[data_count_idx]->SetName(mergeEntering.c_str());
							vtkOutArrays[data_count_idx+1]->SetName(mergeExiting.c_str());
							vtkOutArrays[data_count_idx+2]->SetName(mergeTotal.c_str());
							break;
					}	
					break;
				case OutputData::PhotonWeight:
					mergeEntering = a.first + " - PhotonWeight (?) entering region";
					mergeExiting = a.first + " - PhotonWeight (?) exiting region";
					mergeTotal = a.first + " - PhotonWeight (?) total";
					vtkOutArrays[data_count_idx]->SetName(mergeEntering.c_str());
					vtkOutArrays[data_count_idx+1]->SetName(mergeExiting.c_str());
					vtkOutArrays[data_count_idx+2]->SetName(mergeTotal.c_str());
					break;
				case OutputData::EnergyPerVolume:
					switch(a.second->unitType())
					{
						case OutputData::J_m:
							mergeEntering = a.first + " - Energy/Volume (J/m^3) entering region";
							mergeExiting = a.first + " - Energy/Volume (J/m^3) exiting region";
							mergeTotal = a.first + " - Energy/Volume (J/m^3) total";
							vtkOutArrays[data_count_idx]->SetName(mergeEntering.c_str());
							vtkOutArrays[data_count_idx+1]->SetName(mergeExiting.c_str());
							vtkOutArrays[data_count_idx+2]->SetName(mergeTotal.c_str());
							break;
						case OutputData::J_cm:
							mergeEntering = a.first + " - Energy/Volume (J/cm^3) entering region";
							mergeExiting = a.first + " - Energy/Volume (J/cm^3) exiting region";
							mergeTotal = a.first + " - Energy/Volume (J/cm^3) total";
							vtkOutArrays[data_count_idx]->SetName(mergeEntering.c_str());
							vtkOutArrays[data_count_idx+1]->SetName(mergeExiting.c_str());
							vtkOutArrays[data_count_idx+2]->SetName(mergeTotal.c_str());
							break;
						case OutputData::J_mm:
							mergeEntering = a.first +
							mergeExiting = a.first + " - Energy/Volume (J/mm^3) exiting region";
							mergeTotal = a.first + " - Energy/Volume (J/mm^3) total";
							vtkOutArrays[data_count_idx]->SetName(" - Energy/Volume (J/mm^3) entering region");
							vtkOutArrays[data_count_idx+1]->SetName(mergeExiting.c_str());
							vtkOutArrays[data_count_idx+2]->SetName(mergeTotal.c_str());
							break;
						case OutputData::W_m:	
							mergeEntering = a.first + " - Power/Volume (W/m^3) entering region";
							mergeExiting = a.first + " - Power/Volume (W/m^3) exiting region";
							mergeTotal = a.first + " - Power/Volume (W/m^3) total";
							vtkOutArrays[data_count_idx]->SetName(mergeEntering.c_str());
							vtkOutArrays[data_count_idx+1]->SetName(mergeExiting.c_str());
							vtkOutArrays[data_count_idx+2]->SetName(mergeTotal.c_str());
							break;
						case OutputData::W_cm:
							mergeEntering = a.first + " - Power/Volume (W/cm^3) entering region";
							mergeExiting = a.first + " - Power/Volume (W/cm^3) exiting region";
							mergeTotal = a.first + " - Power/Volume (W/cm^3) total";
							vtkOutArrays[data_count_idx]->SetName(mergeEntering.c_str());
							vtkOutArrays[data_count_idx+1]->SetName(mergeExiting.c_str());
							vtkOutArrays[data_count_idx+2]->SetName(mergeTotal.c_str());
							break;
						case OutputData::W_mm:
							mergeEntering = a.first + " - Power/Volume (W/mm^3) entering region";
							mergeExiting = a.first + " - Power/Volume (W/mm^3) exiting region";
							mergeTotal = a.first + " - Power/Volume (W/mm^3) total";
							vtkOutArrays[data_count_idx]->SetName(mergeEntering.c_str());
							vtkOutArrays[data_count_idx+1]->SetName(mergeExiting.c_str());
							vtkOutArrays[data_count_idx+2]->SetName(mergeTotal.c_str());
							break;
						default:
							mergeEntering = a.first + " - Energy/Volume (?) entering region";
							mergeExiting = a.first + " - Energy/Volume (?) exiting region";
							mergeTotal = a.first + " - Energy/Volume (?) total";
							vtkOutArrays[data_count_idx]->SetName(mergeEntering.c_str());
							vtkOutArrays[data_count_idx+1]->SetName(mergeExiting.c_str());
							vtkOutArrays[data_count_idx+2]->SetName(mergeTotal.c_str());
							break;
					}
					break;
				default:
					mergeEntering = a.first + a.first +a.first + " - Unknown (?) entering region";
					mergeExiting = a.first + a.first + " - Unknown (?) exiting region";
					mergeTotal = a.first + a.first + " - Unknown (?) total";
					vtkOutArrays[data_count_idx]->SetName(mergeEntering.c_str());
					vtkOutArrays[data_count_idx+1]->SetName(mergeExiting.c_str());
					vtkOutArrays[data_count_idx+2]->SetName(mergeTotal.c_str());
					break;
			}
			// Below is the old code on how to name the arrays
			// vtkOutArrays[data_count_idx]->SetComponentName(0,"entering");
			// vtkOutArrays[data_count_idx]->SetComponentName(1,"exiting");
			// vtkOutArrays[data_count_idx]->SetComponentName(2,"total");
			
			for(unsigned i=0;i<ds->exiting()->dim();++i)
			{
				vtkOutArrays[data_count_idx]->SetTypedComponent(i,0,ds->entering()->get(i));
				vtkOutArrays[data_count_idx+1]->SetTypedComponent(i,0,ds->exiting()->get(i));
				vtkOutArrays[data_count_idx+2]->SetTypedComponent(i,0,ds->entering()->get(i)+ds->exiting()->get(i));
			}
			data_count_idx += 3;
		}

		////// Relabel face subset and write out

		DynamicIndexRelabel relabel;
		
		// Create cells, relabeling points as we go
		vtkIdTypeArray* ids = vtkIdTypeArray::New();
		ids->SetNumberOfTuples(4*ds->permutation()->dim());
		
		for(unsigned i=0;i<ds->permutation()->dim();++i)
		{
			// get point indices for the present face
			const auto IDps = get(points,*m_mesh,TetraMesh::DirectedFaceDescriptor(ds->permutation()->get(i)));
	
			// write cell out, relabeling points as we go
			ids->SetValue((i<<2)+0, 3);
			for(unsigned j=0;j<3;++j)
				ids->SetValue((i<<2)+j+1, relabel(IDps[j].value()));
		}
		ca->SetCells(ds->permutation()->dim(), ids);

		// write out the permuted points
		facePointPerm = relabel.permutation();
	}
	

	vtkPoints* facePoints = vtkPoints::New();
	facePoints->SetNumberOfPoints(facePointPerm->dim());
	for(unsigned i=0;i<facePointPerm->dim();++i)
		facePoints->SetPoint(i,m_mesh->points()->get(facePointPerm->get(i)).data());

	// create final vtkPolyData
	vtkPolyData* pd = vtkPolyData::New();
	pd->SetPolys(ca);
	pd->SetPoints(facePoints);
	for(unsigned i=0; i<vtkOutArrays.size(); i++)
		pd->GetCellData()->AddArray(vtkOutArrays[i]);

	

	auto iter = m_arrays.begin();
	if(m_manualHeader == false)
	{
		switch(iter->second->unitType())
					{
						case OutputData::J_m:
							surfWriter->SetHeader("Energy unit: J; Dimension unit: m");
							break;
						case OutputData::J_cm:
							surfWriter->SetHeader("Energy unit: J; Dimension unit: cm");
							break;
						case OutputData::J_mm:
							surfWriter->SetHeader("Energy unit: J; Dimension unit: mm");
							break;
						case OutputData::W_m:
							surfWriter->SetHeader("Power unit: W; Dimension unit: m");
							break;
						case OutputData::W_cm:
							surfWriter->SetHeader("Power unit: W; Dimension unit: cm");
							break;
						case OutputData::W_mm:
							surfWriter->SetHeader("Power unit: W; Dimension unit: mm");
							break;
						default:
							break;
					}
	}

	surfWriter->SetInputData(pd);
	surfWriter->SetFileName(m_filename.c_str());
	surfWriter->Update();

	for(unsigned i=0; i<vtkOutArrays.size(); i++)
		vtkOutArrays[i]->Delete();

}
