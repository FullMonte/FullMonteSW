#if defined(SWIGTCL)
%module FullMonteVTKFilesTCL
#elif defined(SWIGPYTHON)
%module VTKFiles
#else
	#warning Requested wrapping language not supported
#endif
%feature("autodoc", "3");

%begin %{
#include <FullMonteSW/Warnings/SWIG.hpp>
%}

%include "std_string.i"


%header %{
#include "VTKMeshReader.hpp"
#include "VTKMeshWriter.hpp"
#include "VTKSurfaceWriter.hpp"
#include "VTKPhotonPathWriter.hpp"
%}

%include "VTKMeshReader.hpp"
%include "VTKMeshWriter.hpp"
%include "VTKSurfaceWriter.hpp"
%include "VTKPhotonPathWriter.hpp"