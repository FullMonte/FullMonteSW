#include "VTKPhotonPathWriter.hpp"

#include <FullMonteSW/OutputTypes/PacketPositionTraceSet.hpp>

#include <FullMonteSW/VTK/vtkFullMontePacketPositionTraceSetToPolyData.h>


VTKPhotonPathWriter::VTKPhotonPathWriter()
{
    m_writer = vtkFullMontePacketPositionTraceSetToPolyData::New();
}

VTKPhotonPathWriter::~VTKPhotonPathWriter()
{
}

void VTKPhotonPathWriter::filename(string fn)
{
	m_filename=fn;
}

void VTKPhotonPathWriter::source(OutputData* d)
{
    m_data=dynamic_cast<PacketPositionTraceSet*>(d);
}

void VTKPhotonPathWriter::write()
{
    m_writer->source(m_data);
    m_writer->update();

    m_writer->writeTraces(m_filename.c_str());
    
}