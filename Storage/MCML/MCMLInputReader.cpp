/*
 * MCMLInputReader.cpp
 *
 *  Created on: Jun 1, 2017
 *      Author: jcassidy
 */

#include "MCMLInputReader.hpp"
#include <iostream>
#include <fstream>

#include "MCMLCase.hpp"

#include <FullMonteSW/Geometry/Layered.hpp>
#include <FullMonteSW/Geometry/Layer.hpp>
#include <FullMonteSW/Geometry/Material.hpp>
#include <FullMonteSW/Geometry/MaterialSet.hpp>

#include <FullMonteSW/Warnings/Push.hpp>
#include <FullMonteSW/Warnings/Boost.hpp>
#include <boost/iostreams/operations.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <FullMonteSW/Warnings/Pop.hpp>

#include <FullMonteSW/Storage/Common/FlattenWhitespace.hpp>
#include <FullMonteSW/Storage/Common/CommentFilter.hpp>

#include <stdexcept>

#include <cstdio>

using namespace std;

MCMLInputReader::MCMLInputReader()
{
}

MCMLInputReader::~MCMLInputReader()
{
}

void MCMLInputReader::read()
{
	ifstream is(m_filename.c_str());

	if (!is.good())
		throw std::logic_error("MCMLInputReader::read() failed to open file");

	boost::iostreams::filtering_stream<boost::iostreams::input,char> F;

	flatten_whitespace FW;
	single_delim_comment_filter CF;

	F.push(FW);
	F.push(CF);
	F.push(is);

	float ver;
	unsigned Nr;

	F >> ver;
	F >> Nr;

	for(unsigned r=0;r<Nr;++r)
	{
		string fn;
		char ctype;

		F >> fn >> ctype;

		unsigned long long Nph;
		float dz, dr;

		F >> Nph;
		F >> dz >> dr;

		unsigned Nz,Nr,Na;

		F >> Nz >> Nr >> Na;

		MCMLCase* c = new MCMLCase();

		Layered* L = new Layered();
		c->geometry(L);
		c->outputFilename(fn, ctype);
		c->packets(Nph);
		c->rouletteThreshold(1e-4f);			// MCML #define value

		L->resolution(dr,dz);
		L->dims(Nr,Nz);
		L->angularBins(Na);

		unsigned Nl;
		F >> Nl;

		MaterialSet* M = new MaterialSet();

		float n_above,n_below;
		F >> n_above;

		// layer 0 is top exterior
		M->append(new Material(0.0f,0.0f,0.0f,n_above));

		for(unsigned l=1;l<=Nl;++l)
		{
			Layer* lp = new Layer();


			float n, mua, mus, g, d;
			F >> n >> mua >> mus >> g >> d;

			lp->thickness(d);
			L->addLayer(lp);

			Material* m = new Material(mua, mus, g, n);
			M->append(m);
		}

		F >> n_below;

		// layer Nl+1 is bottom exterior
		M->append(new Material(0.0f,0.0f,0.0f,n_below));

		c->materials(M);

		m_cases.push_back(c);

		c->geometry()->buildRegions();
	}


	// print out

	if(m_verbose)
	{

		cout << "Version " << ver << endl;
		cout << "  Cases: " << Nr << endl;

		for(start(); !done(); next())
		{
			MCMLCase* c = current();
			Layered* L = c->geometry();

			CylCoord resolution = L->resolution();
			CylIndex dims = L->dims();

			cout << "Packets: " << c->packets() << endl;
			cout << "Resolution dr=" << resolution.r << " dz=" << resolution.z << " da=" << L->angularResolution() << endl;
			cout << "Bins:      Nr=" << dims.ri << " Nz=" << dims.zi << " Na=" << L->angularBins() << endl;
			cout << "Output file: " << c->outputFilename() << " format '" << c->outputFormat() << "'" << endl;
			cout << "Layers (" << c->geometry()->layerCount() << ")" << endl;
			for(unsigned l=1;l<=c->geometry()->layerCount();++l)
			{
				Material* m = c->materials()->get(l+1);
				cout << "  thickness " << c->geometry()->layer(l)->thickness() << " mua=" << m->absorptionCoeff() << " mus=" << m->scatteringCoeff() << " g=" << m->anisotropy() << " n=" << m->refractiveIndex() << endl;
			}
		}
	}
}

void MCMLInputReader::filename(std::string fn)
{
	m_filename=fn;
}

void MCMLInputReader::start()
{
	m_iterator = m_cases.begin();
}

void MCMLInputReader::next()
{
	if (m_iterator != m_cases.end())
		m_iterator++;
}

bool MCMLInputReader::done() const
{
	return m_iterator == m_cases.end();
}

MCMLCase* MCMLInputReader::current() const
{
	return m_iterator != m_cases.end() ? *m_iterator : nullptr;
}

MCMLCase* MCMLInputReader::get(unsigned i) const
{
	list<MCMLCase*>::const_iterator it=m_cases.begin();
	for(unsigned j=0;j<i && it != m_cases.end();++j)
		++it;

	return it == m_cases.end() ? nullptr : *it;
}

unsigned MCMLInputReader::cases() const
		{
	return m_cases.size();
		}

void MCMLInputReader::verbose(bool v)
{
	m_verbose=v;
}
