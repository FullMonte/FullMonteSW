/*
 * MCMLOutputReader.cpp
 *
 *  Created on: Jun 1, 2017
 *      Author: jcassidy
 */

#include <iostream>

#include <FullMonteSW/Logging/FullMonteLogger.hpp>
#include <FullMonteSW/OutputTypes/OutputDataCollection.hpp>
#include <FullMonteSW/OutputTypes/SpatialMap2D.hpp>

#include <FullMonteSW/Geometry/Layered.hpp>
#include <FullMonteSW/Geometry/Layer.hpp>
#include <FullMonteSW/Geometry/MaterialSet.hpp>
#include <FullMonteSW/Geometry/Material.hpp>
#include "MCMLCase.hpp"
#include "MCMLOutputReader.hpp"

#include <fstream>
#include <sstream>
#include <tuple>

#include <FullMonteSW/Storage/TextFile/Util.hpp>


#include <FullMonteSW/Warnings/Push.hpp>
#include <FullMonteSW/Warnings/Boost.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <FullMonteSW/Warnings/Pop.hpp>

#include <FullMonteSW/Storage/Common/CommentFilter.hpp>
#include <FullMonteSW/Storage/Common/FlattenWhitespace.hpp>

using namespace std;

MCMLOutputReader::MCMLOutputReader()
{
}

MCMLOutputReader::~MCMLOutputReader()
{
}

void MCMLOutputReader::filename(std::string fn)
{
	m_filename=fn;
}

vector<float> MCMLOutputReader::readVector(istream& is,unsigned N)
{
	vector<float> v(N,numeric_limits<float>::quiet_NaN());

	for(unsigned i=0;i<N && !is.eof() && !is.fail();++i)
		is >> v[i];
	return v;
}

void MCMLOutputReader::clear()
{
	// reset everything but filename
	string fn = m_filename;
	*this = MCMLOutputReader();
	m_filename = fn;
}

void MCMLOutputReader::read()
{
	clear();

	ifstream is(m_filename.c_str());

	boost::iostreams::filtering_stream<boost::iostreams::input,char> F;

	flatten_whitespace WS;
	single_delim_comment_filter CF;

	F.push(CF);
	F.push(WS);
	F.push(is);

	string ver;

	m_Npkt=0;

	F >> ver;

	string section;

	F >> section;

	if (section != "InParm")
	{
		LOG_ERROR << "MCMLOutputReader::read() expecting section 'InParm' but found '" << section << "'" << endl;
		return;
	}

	string fn;
	char fmt;

	F >> fn >> fmt;
	F >> m_Npkt;
	F >> m_dz >> m_dr;
	F >> m_nz >> m_nr >> m_na;
	F >> m_nl;

	float nAbove, nBelow;

	F >> nAbove;

	Layered* G = new Layered();
	G->resolution(m_dz,m_dr);
	G->dims(m_nr,m_nz);
	G->angularBins(m_na);

	MaterialSet* MS = new MaterialSet();
	MCMLCase* C = new MCMLCase();
	C->packets(m_Npkt);
	C->geometry(G);
	C->materials(MS);
	C->outputFilename(fn,fmt);

	Material* ext = new Material();
	ext->refractiveIndex(nAbove);

	MS->set(0,ext);

	for(unsigned i=0;i<m_nl;++i)
	{
		float n,g,muA,muS,t;
		F >> n >> g >> muA >> muS >> t;

		Material* M = new Material();
		M->scatteringCoeff(muS);
		M->absorptionCoeff(muA);
		M->anisotropy(g);
		M->refractiveIndex(n);
		MS->set(i+1,M);

		Layer* L = new Layer();
		L->thickness(t);
		G->addLayer(L);
	}

	G->buildRegions();

	F >> nBelow;

	if (nBelow != nAbove)
	{
		LOG_WARNING << "MCMLOutputReader::read() - top and bottom refractive indices are mismatched, which cannot be represented in FullMonte" << endl;
	}


	for (F >> section; !F.eof(); F>>section)
	{
		if (section == "RAT")
		{
			F >> m_specularReflectance;
			F >> m_diffuseReflectance;
			F >> m_absorption;
			F >> m_transmission;
		}
		else if (section == "A_l")
			m_absorptionByLayer = readVector(F,m_nl);
		else if (section == "A_z")
			m_absorptionByDepth = readVector(F,m_nz);
		else if (section == "Rd_r")
			m_diffuseReflectanceByRadius = readVector(F,m_nr);
		else if (section == "Rd_a")
			m_diffuseReflectanceByAngle = readVector(F,m_na);
		else if (section == "Tt_r")
			m_transmissionByRadius = readVector(F,m_nr);
		else if (section == "Tt_a")
			m_transmissionByAngle = readVector(F,m_na);
		else if (section == "A_rz")
			m_absorptionByRadiusAndDepth = readVector(F,m_nr*m_nz);
		else if (section == "Rd_ra")
			m_diffuseReflectanceByRadiusAndAngle = readVector(F,m_na*m_nr);
		else if (section == "Tt_ra")
			m_transmissionByRadiusAndAngle = readVector(F,m_na*m_nr);
	}

	m_case=C;
}


void MCMLOutputReader::createResultVector(OutputDataCollection* C,const vector<float>& v, size_t dim,const char* name)
{
	if (v.size() > 0)
	{
		if (v.size() != dim)
		{
			stringstream ss;
			ss << "MCMLOutputReader::result() dimension mismatch in 1D vector " << name;
			throw std::logic_error(ss.str());
		}
		SpatialMap<float> *d = new SpatialMap<float>(v,AbstractSpatialMap::Volume,AbstractSpatialMap::Scalar);
		d->name(name);
		C->add(d);
	}
}

void MCMLOutputReader::create2DResultVector(OutputDataCollection* C,const vector<float>& v, size_t r,size_t c,const char* name)
{
	vector<float> vPad(v.size()+1);

	for(unsigned i=0;i<v.size();++i)
		vPad[i+1]=v[i];
	vPad[0]=0;

	if (v.size() > 0)
	{
		if (v.size() != r*c)
		{
			stringstream ss;
			ss << "MCMLOutputReader::result() dimension mismatch in 2D vector " << name;
			throw std::logic_error(ss.str());
		}
		SpatialMap2D<float> *d = new SpatialMap2D<float>(r,c,move(vPad),AbstractSpatialMap::Volume,AbstractSpatialMap::Scalar);
		d->name(name);
		C->add(d);
	}
}

MCMLCase* MCMLOutputReader::runParams() const
{
	return m_case;
}

OutputDataCollection* MCMLOutputReader::result() const
{
	OutputDataCollection *C = new OutputDataCollection();

	create2DResultVector(C,m_absorptionByRadiusAndDepth,m_nr,m_nz,"Absorption_rz");
	create2DResultVector(C,m_diffuseReflectanceByRadiusAndAngle,m_nr,m_na,"DiffuseReflectance_ra");
	create2DResultVector(C,m_transmissionByRadiusAndAngle,m_nr,m_na,"Transmission_ra");

	createResultVector(C,m_transmissionByAngle,m_na,"Transmission_a");
	createResultVector(C,m_transmissionByRadius,m_nr,"Transmission_r");
	createResultVector(C,m_diffuseReflectanceByAngle,m_na,"Reflectance_a");
	createResultVector(C,m_diffuseReflectanceByRadius,m_nr,"Reflectance_r");
	createResultVector(C,m_absorptionByDepth,m_nz,"Absorption_z");

	return C;
}

MCMLOutputReader::EnergyDisposition MCMLOutputReader::energyDisposition() const
{
	EnergyDisposition E;
	E.specularReflectance = m_specularReflectance;
	E.absorption = m_absorption;
	E.diffuseReflectance = m_diffuseReflectance;
	E.transmission = m_transmission;

	return E;
}
