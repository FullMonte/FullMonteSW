ADD_LIBRARY(FullMonteMCMLFile SHARED 
    MCMLCase.cpp 
    MCMLInputReader.cpp 
    MCMLOutputReader.cpp
)
TARGET_LINK_LIBRARIES(FullMonteMCMLFile 
    FullMonteGeometry 
    FullMonteTextFile 
    FullMonteLogging
)

IF(WRAP_TCL)
    SET_SOURCE_FILES_PROPERTIES(FullMonteMCMLFile.i PROPERTIES CPLUSPLUS ON)
    SWIG_ADD_LIBRARY(FullMonteMCMLFileTCL 
        TYPE SHARED 
        LANGUAGE tcl 
        SOURCES FullMonteMCMLFile.i
    )
    SWIG_LINK_LIBRARIES(FullMonteMCMLFileTCL 
        FullMonteMCMLFile 
        FullMonteGeometry 
        ${TCL_LIBRARY}
    )
    INSTALL(TARGETS FullMonteMCMLFileTCL LIBRARY 
        DESTINATION lib
    )
ENDIF()

IF(WRAP_PYTHON)
    SET_SOURCE_FILES_PROPERTIES(FullMonteMCMLFile.i PROPERTIES CPLUSPLUS ON)
    SWIG_ADD_LIBRARY(MCMLFile
        TYPE SHARED 
        LANGUAGE python 
        SOURCES FullMonteMCMLFile.i
    )
    SWIG_LINK_LIBRARIES(MCMLFile 
        FullMonteMCMLFile
        FullMonteGeometry 
        ${Python3_LIBRARIES}
    )
    SET_TARGET_PROPERTIES(_MCMLFile PROPERTIES LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib/fmpy)
ENDIF()

INSTALL(TARGETS FullMonteMCMLFile LIBRARY
    DESTINATION lib
)