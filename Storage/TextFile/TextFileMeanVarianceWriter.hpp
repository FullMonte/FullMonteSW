/*
 * TextFileMeanVarianceWriter.hpp
 *
 *  Created on: Oct 19, 2017
 *      Author: jcassidy
 */

#ifndef STORAGE_TEXTFILE_TEXTFILEMEANVARIANCEWRITER_HPP_
#define STORAGE_TEXTFILE_TEXTFILEMEANVARIANCEWRITER_HPP_

class MeanVarianceSet;

#include <string>

class TextFileMeanVarianceWriter
{
public:
	TextFileMeanVarianceWriter();
	~TextFileMeanVarianceWriter();

	void 			input(MeanVarianceSet*);
	void			filename(std::string fn);

	void 			write() const;

private:

	std::string			m_filename;
	MeanVarianceSet*	m_data=nullptr;
};

#endif /* STORAGE_TEXTFILE_TEXTFILEMEANVARIANCEWRITER_HPP_ */
