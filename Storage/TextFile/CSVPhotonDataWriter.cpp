/*
 * CSVPhotonDataWriter.cpp
 *
 *  Created on: Sep 13, 2022
 *      Author: Shuran Wang
 */

#include "CSVPhotonDataWriter.hpp"
#include <FullMonteSW/Logging/FullMonteLogger.hpp>
#include <FullMonteSW/OutputTypes/OutputData.hpp>
#include <FullMonteSW/OutputTypes/PhotonData.hpp>

#include <fstream>
#include <iomanip>
#include <FullMonteSW/OutputTypes/SpatialMap.hpp>
#include <FullMonteSW/OutputTypes/SpatialMap2D.hpp>

using namespace std;

CSVPhotonDataWriter::CSVPhotonDataWriter()
{

}

CSVPhotonDataWriter::~CSVPhotonDataWriter()
{
}

void CSVPhotonDataWriter::source(OutputData* d)
{
	m_data=dynamic_cast<PhotonData*>(d);
}

OutputData* CSVPhotonDataWriter::source() const
{
	return m_data;
}

void CSVPhotonDataWriter::filename(string fn)
{
	m_filename=fn;
}

void CSVPhotonDataWriter::precision(unsigned p)
{
	m_precision=p;
}

void CSVPhotonDataWriter::width(unsigned w)
{
	m_width=w;
}

void CSVPhotonDataWriter::columns(unsigned c)
{
    m_columns=c;
}

void CSVPhotonDataWriter::write()
{
	ofstream os(m_filename.c_str());

	if (m_precision != 0)
		os << scientific << setprecision(m_precision);

	if(!m_data) {
		LOG_ERROR << "CSVPhotonDataWriter::write() provided null data" << endl;
    }
	else if (const SpatialMap<float>* vf = m_data->weight())
	{
		if (vf->outputType() != AbstractSpatialMap::PhotonWeight)
		{
			LOG_ERROR << "CSVPhotonDataWriter::write() only writes photon weight data\n";
		}

		os << "Weight,Position_x,Position_y,Position_z,Direction_x,Direction_y,Direction_z\n";

		typedef vector<pair<array<float,3>, array<float,3> > > rvec;
		rvec* vr = m_data->ray();
		for(unsigned i=0;i < vf->dim(); ++i) {
			os << setw(m_width) << vf->get(i) << "," << vr->at(i).first[0] << "," << vr->at(i).first[1] << "," << vr->at(i).first[2] << ",";
			os << setw(m_width) << vr->at(i).second[0] << "," << vr->at(i).second[1] << "," << vr->at(i).second[2] << "\n";
		}
	}
	else
    {
		LOG_ERROR << "CSVPhotonDataWriter::write() unrecognized data type" << endl;
    }
}

