/*
 * TextFileMatrixReader.cpp
 *
 *  Created on: Jul 14, 2017
 *      Author: jcassidy
 */

#include <FullMonteSW/Logging/FullMonteLogger.hpp>
#include "TextFileMatrixReader.hpp"
#include <FullMonteSW/OutputTypes/SpatialMap.hpp>
#include <FullMonteSW/OutputTypes/SpatialMap2D.hpp>

#include <iostream>
#include <fstream>

using namespace std;

TextFileMatrixReader::TextFileMatrixReader()
{

}

TextFileMatrixReader::~TextFileMatrixReader()
{
	delete[] m_data;
}

void TextFileMatrixReader::filename(std::string fn)
{
	m_filename=fn;
}

void TextFileMatrixReader::clear()
{
	if (m_dims)
		delete[] m_dims;
	m_dims=nullptr;

	if (m_data)
		delete[] m_data;
	m_data=nullptr;

	m_D=0;
}

void TextFileMatrixReader::read()
{
	ifstream is(m_filename.c_str());

	clear();
	// First read from stream is the output type (Unknown, PhotonWeight, Fluence, Energy or Energy/Volume)
	string output;
	is >> output;
    if (output.compare("Fluence") == 0)
    {
        m_dataType = OutputData::Fluence;
    }
    else if (output.compare("Energy") == 0)
    {
        m_dataType = OutputData::Energy;
    }
    else if (output.compare("PhotonWeight") == 0)
    {
        m_dataType = OutputData::PhotonWeight;
    }
	else if (output.compare("Energy/Volume") == 0)
	{
		m_dataType = OutputData::EnergyPerVolume;
	}
	else
	{
		m_dataType = OutputData::UnknownOutputType;
	}
	string units;
	is >> units;
	if(units.find("J") != string::npos)
	{
		if(units.find("mm") != string::npos)
		{
			m_unitType = OutputData::J_mm;
		}
		else if(units.find("cm") != string::npos)
		{
			m_unitType = OutputData::J_cm;
		}
		else
		{
			m_unitType = OutputData::J_m;
		}
		is >> m_D;
	}
	else if(units.find("W") != string::npos)
	{
		if(units.find("mm") != string::npos)
		{
			m_unitType = OutputData::W_mm;
		}
		else if(units.find("cm") != string::npos)
		{
			m_unitType = OutputData::W_cm;
		}
		else
		{
			m_unitType = OutputData::W_m;
		}
		is >> m_D;
	}
	else if (units.find("?") != string::npos)
	{
		m_unitType = OutputData::UnknownUnitType;
		is >> m_D;
	}
	else
	{
		m_D = stoi(units);
	}

	unsigned N=1;

	m_dims = new unsigned[m_D];

	for(unsigned i=0;i<m_D && !is.fail();++i)
	{
		is >> m_dims[i];
		N *= m_dims[i];
	}
	// Ignore the last entry of the first line because it just defines what type that data is (unsigned, float, etc.)
	is >> output;
	N += (m_D==2);

	LOG_DEBUG << "Reading " << m_D << "-D data with total " << N << " elements" << endl;

	if (!is.fail())
		m_data = new float[N];				/// SPECIAL CASE: if D=2, has extra 0 element (for MCML-style files)

	unsigned i;
	for(i=0;i<N && !is.eof() && !is.fail();++i)
		is >> m_data[i];							/// SPECIAL CASE: if D=2, has extra 0 element (for MCML-style files)

	if (i < N && is.fail())
	{
		clear();
		LOG_ERROR << "TextFileMatrixReader::read() failure reading input stream" << endl;
	}
}

unsigned TextFileMatrixReader::size() const
{
	if (m_D == 0)
		return 0;

	unsigned N=m_dims[0];
	for(unsigned i=1; i<m_D; ++i)
		N *= m_dims[i];
	return N;
}

OutputData* TextFileMatrixReader::output() const
{
	SpatialMap<float>* o = nullptr;
	unsigned N = size();

	if (N == 0) {
		LOG_WARNING << "TextFileMatrixReader::output() called but no output data present" << endl;
    } else if (m_D == 1) {
		o = new SpatialMap<float>(size(), AbstractSpatialMap::UnknownSpaceType, AbstractSpatialMap::Scalar,m_dataType);
		o->unitType(m_unitType);
    } else if (m_D == 2) {
		o = new SpatialMap2D<float>(m_dims[0], m_dims[1], AbstractSpatialMap::UnknownSpaceType, AbstractSpatialMap::Scalar,m_dataType);
		o->unitType(m_unitType);
    } else {
		LOG_ERROR << "TextFileMatrixReader::output() called but output data dimension is not recognized" << endl;
    }

	for(unsigned i=0;i<(N+(m_D==2));++i)		/// SPECIAL CASE: D==2 ==> extra zero leading element
		o->set(i,m_data[i]);
	return o;
}
