/*
 * CSVPhotonDataWriter.hpp
 *
 *  Created on: Sep 13, 2022
 *      Author: Shuran Wang
 */

#ifndef STORAGE_TEXTFILE_CSVPHOTONDATAWRITER_HPP_
#define STORAGE_TEXTFILE_CSVPHOTONDATAWRITER_HPP_

class OutputData;
class PhotonData;
#include <string>

class CSVPhotonDataWriter
{
public:
	CSVPhotonDataWriter();
	~CSVPhotonDataWriter();

	void 			source(OutputData* d);
	OutputData*		source() const;

	void 			filename(std::string fn);
	void			precision(unsigned p);
	void			width(unsigned w);
    void            columns(unsigned c);

	void 			write();

private:
	std::string		m_filename;
	unsigned		m_precision=0;
	unsigned		m_width=0;
	unsigned		m_columns=10;

	PhotonData*		m_data=nullptr;
};

#endif /* STORAGE_TEXTFILE_CSVPHOTONDATAWRITER_HPP_ */
