#include <FullMonteSW/Logging/FullMonteLogger.hpp>

#include <FullMonteSW/Geometry/Predicates/ArrayPredicateEvaluator.hpp>
#include <FullMonteSW/Geometry/Predicates/VolumeCellPredicate.hpp>
#include <FullMonteSW/Geometry/Predicates/SurfaceCellPredicate.hpp>
#include <FullMonteSW/Geometry/Predicates/SurfaceOfRegionPredicate.hpp>

#include <FullMonteSW/Kernels/CUDA/CUDADirectedSurfaceScorer.hpp>
#include <FullMonteSW/Kernels/CUDA/TetraMCCUDAKernel.hpp>

//
// NOTE: All of this code is identical to
// AbstractDirectedSurfaceScorer and DirectedSurfaceScorer.
// I tried (but failed) to reuse those classes.
//

void CUDADirectedSurfaceScorer::createOutputSurfaces(Kernel* K) {
	clearSurfaces();

	const TetraMesh* M = dynamic_cast<const TetraMesh*>(K->geometry());
	if (!M) {
		LOG_ERROR << "CUDADirectedSurfaceScorer::createOutputSurfaces(K) given kernel whose geometry isn't a TetraMesh" << endl;
		return;
	}

	TetraCUDAKernel* TK = dynamic_cast<TetraCUDAKernel*>(K);
	if (!TK) {
		LOG_ERROR << "CUDADirectedSurfaceScorer::createOutputSurfaces(K) given kernel that isn't convertible to TetraCUDAKernel" << endl;
        return;
	}

	m_outputSurfaces.resize(m_outputCellPredicates.size(),nullptr);

	// boolean vector over all undirected faces indicating if they need fluence scored
	vector<bool> undirectedFaceCapture(M->faceTetraLinks()->size(),false);

	//cout << "Output directed surface creation" << endl;

	for(unsigned p=0;p<m_outputCellPredicates.size();++p) {
        const GeometryPredicate* pred = m_outputCellPredicates[p];

        const SurfaceCellPredicate* surf=nullptr;
        const VolumeCellPredicate* vol=nullptr;

        // get a SurfaceCellPredicate for the surface to be scored and bind it to the geometry to create an ArrayPredicateEvaluator
        if ((vol = dynamic_cast<const VolumeCellPredicate*>(pred))) {
            // create predicate for surface of region
            auto s = new SurfaceOfRegionPredicate();
            s->setRegionPredicate(vol);
            surf = s;
        } else if ((surf = dynamic_cast<const SurfaceCellPredicate*>(pred))) {
		}
        
        ArrayPredicateEvaluator* eval = surf->bind(K->geometry());
        ArrayPredicateEvaluator* volEval = vol ? vol->bind(K->geometry()) : nullptr;

        // loop over all undirected faces, marking faces that match the predicate and creating a Permutation for them
        vector<unsigned> IDfds;

        for(unsigned i = 1; i < M->faceTetraLinks()->size(); i++) {
            if((*eval)(i)) {
                undirectedFaceCapture[i] = true;

                // output permutation provides the directed face index for the exit
                
                // if using a volume predicate _and_ the tetra below the face (opposite normal) is in the region interior, then
                // we need to flip the face so the normal points outward
                bool flipFace = volEval && (*volEval)(M->faceTetraLinks()->get(i).upTet().T.value());

                // insert a _directed_ surface face index
                IDfds.push_back( (i<<1) | flipFace);
            }
        }
        m_outputSurfaces[p] = new Permutation();
        m_outputSurfaces[p]->resize(IDfds.size());
        for(const auto el : IDfds | boost::adaptors::indexed(0U)) {
            m_outputSurfaces[p]->set(el.index(),el.value());
        }

        LOG_DEBUG << "  [" << p << "] from " << (vol ? "volume" : "surface" ) << " predicate with " << m_outputSurfaces[p]->dim() << " faces" << endl;

        // if provided a volume, we dynamically allocated a surface. delete it now.
        if(vol) {
            delete surf;
        }
    }
    
    // apply the list of undirected faces to capture to the actual kernel
    for(unsigned i = 1; i < M->tetraCells()->size(); i++) {
        for(unsigned j = 0; j < 4; j++) {
            TK->markTetraFaceForFluenceScoring(i,j,undirectedFaceCapture[M->tetraFaceLinks()->get(i)[j].faceID.value()>>1]);
        }
    }
}

void CUDADirectedSurfaceScorer::clearSurfaces() {
    for(auto p : m_outputSurfaces) {
        delete p;
    }
    m_outputSurfaces.clear();
}

unsigned CUDADirectedSurfaceScorer::getNumberOfOutputSurfaces() const {
    return m_outputSurfaces.size();
}

Permutation* CUDADirectedSurfaceScorer::getOutputSurface(unsigned i) const {
    if (i >= m_outputSurfaces.size()) {
        LOG_ERROR << "Requested output surface (" << i << ") exceeds available surface count (" << m_outputSurfaces.size() << ")" << endl;
    }
    
    return m_outputSurfaces.at(i);
}

void CUDADirectedSurfaceScorer::addScoringRegionBoundary(VolumeCellPredicate* pred) {
    addGeometryPredicate(pred);
}

void CUDADirectedSurfaceScorer::addScoringSurface(SurfaceCellPredicate* pred) {
    addGeometryPredicate(pred);
}

void CUDADirectedSurfaceScorer::removeScoringSurface(SurfaceCellPredicate* pred) {
    removeGeometryPredicate(pred);
}

void CUDADirectedSurfaceScorer::removeScoringRegionBoundary(VolumeCellPredicate* pred) {
    removeGeometryPredicate(pred);
}

void CUDADirectedSurfaceScorer::addGeometryPredicate(GeometryPredicate* pred) {
    const auto it = find(m_outputCellPredicates.begin(),m_outputCellPredicates.end(),pred);
    if (it == m_outputCellPredicates.end()) {
        m_outputCellPredicates.push_back(pred);
    } else {
        LOG_ERROR << "CUDADirectedSurfaceScorer::addGeometryPredicate(pred) failed because the predicate is already present" << endl;
    }
}

void CUDADirectedSurfaceScorer::removeGeometryPredicate(GeometryPredicate* pred) {
    const auto it = find(m_outputCellPredicates.begin(),m_outputCellPredicates.end(),pred);
    if (it == m_outputCellPredicates.end()) {
        LOG_ERROR << "CUDADirectedSurfaceScorer::removeGeometryPredicate(pred) failed because the predicate could not be found" << endl;
    } else {
        m_outputCellPredicates.erase(it);
    }
}

