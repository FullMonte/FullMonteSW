#if defined(SWIGTCL)
%module FullMonteCUDAKernelTCL
#elif defined(SWIGPYTHON)
%module CUDAKernel
#else
	#warning Requested wrapping language not supported
#endif
%feature("autodoc", "3");

%begin %{
#include <FullMonteSW/Warnings/SWIG.hpp>
%}

%include "std_vector.i"
%include "std_string.i"

%include <FullMonteSW/Geometry/FullMonteGeometry_types.i>

%{
#include <FullMonteSW/Kernels/Kernel.hpp>
#include <FullMonteSW/Kernels/KernelObserver.hpp>
#include <FullMonteSW/Kernels/OStreamObserver.hpp>
#include <FullMonteSW/Kernels/MCKernelBase.hpp>

#include <FullMonteSW/Kernels/CUDA/CUDAMCKernelBase.hpp>
#include <FullMonteSW/Kernels/CUDA/CUDADirectedSurfaceScorer.hpp>
#include <FullMonteSW/Kernels/CUDA/TetraMCCUDAKernel.hpp>

#include <FullMonteSW/Kernels/CUDA/TetraCUDAVolumeKernel.hpp>
#include <FullMonteSW/Kernels/CUDA/TetraCUDAInternalKernel.hpp>
#include <FullMonteSW/Kernels/CUDA/TetraCUDASurfaceKernel.hpp>
#include <FullMonteSW/Kernels/CUDA/TetraCUDASVKernel.hpp>
%}

%include "../Kernel.hpp"
%include "MCKernelBase.hpp" 
%include "CUDAMCKernelBase.hpp"
%include "CUDADirectedSurfaceScorer.hpp"
%include "TetraMCCUDAKernel.hpp"

%template (TetraMCCUDAKernelAVX_V) TetraMCCUDAKernel<RNG_SFMT_AVX>;

%include "TetraCUDAVolumeKernel.hpp"
%include "TetraCUDAInternalKernel.hpp"
%include "TetraCUDASurfaceKernel.hpp"
%include "TetraCUDASVKernel.hpp"

