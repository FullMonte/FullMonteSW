#ifndef KERNELS_SOFTWARE_TETRACUDAVOLUMEKERNEL_HPP_
#define KERNELS_SOFTWARE_TETRACUDAVOLUMEKERNEL_HPP_

#include "TetraMCCUDAKernel.hpp"
    
/**
 * CUDA version of TetraVolumeKernel.
 * Uses CUDAAccel to accelerate MC-simulator and track ONLY volume absorption events.
 */
class TetraCUDAVolumeKernel : public TetraMCCUDAKernel<RNG_SFMT_AVX> {
public:
    TetraCUDAVolumeKernel(){
        m_cudaAccelerator.scoreVolume = true;
        m_cudaAccelerator.scoreSurfaceExit = false;
        m_cudaAccelerator.scoreDirectedSurface = false;
    }
};

#endif

