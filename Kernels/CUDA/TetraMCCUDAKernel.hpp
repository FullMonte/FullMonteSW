#ifndef KERNELS_SOFTWARE_TETRAMCCUDAKERNEL_HPP_
#define KERNELS_SOFTWARE_TETRAMCCUDAKERNEL_HPP_

#include <FullMonteSW/Config.h>

#include <FullMonteSW/Geometry/Sources/Print.hpp>
#include <FullMonteSW/Geometry/MaterialSet.hpp>
#include <FullMonteSW/Geometry/Material.hpp>
#include <FullMonteSW/Geometry/Partition.hpp>
#include <FullMonteSW/Geometry/Layer.hpp>
#include <FullMonteSW/Geometry/Layered.hpp>
#include <FullMonteSW/Geometry/FaceLinks.hpp>
#include <FullMonteSW/Geometry/Permutation.hpp>

#include <FullMonteSW/OutputTypes/SpatialMap.hpp>
#include <FullMonteSW/OutputTypes/OutputData.hpp>
#include <FullMonteSW/OutputTypes/OutputDataCollection.hpp>
#include <FullMonteSW/OutputTypes/DirectedSurface.hpp>

// this is a bit weird, but we borrow some Emitter stuff from the Kernels/Software directory
// TODO: Should Emitters be taken out of the Software directory?
#include <FullMonteSW/Kernels/Software/Material.hpp>
#include <FullMonteSW/Kernels/Software/RNG_SFMT_AVX.hpp>

#include <FullMonteSW/Kernels/Software/Emitters/Base.hpp>
#include <FullMonteSW/Kernels/Software/Emitters/Point.hpp>
#include <FullMonteSW/Kernels/Software/Emitters/Directed.hpp>
#include <FullMonteSW/Kernels/Software/Emitters/TetraMeshEmitterFactory.hpp>

#include <FullMonteSW/Kernels/CUDA/CUDADirectedSurfaceScorer.hpp>
#include <FullMonteSW/Kernels/CUDA/CUDAMCKernelBase.hpp>
#include <FullMonteSW/Kernels/CUDA/CUDATetrasFromLayered.hpp>
#include <FullMonteSW/Kernels/CUDA/CUDATetrasFromTetraMesh.hpp>

// these are from the CUDAAccel library
#include <FullMonteSW/External/CUDAAccel/CUDAAccel.h>
#include <FullMonteSW/External/CUDAAccel/FullMonteCUDATypes.h>
#include <FullMonteSW/External/CUDAAccel/FullMonteCUDAConstants.h>

#include <boost/align/aligned_alloc.hpp>
#include <boost/random/additive_combine.hpp>

#include <string>
#include <algorithm>
#include <random>
#include <ctime>

class Permutation;
class GeometryPredicate;
class VolumeCellPredicate;
class SurfaceCellPredicate;

////////////////////////////////////////////////////////////////////////////
/** Derived by TetraCUDAMCKernel. Following format of TetraMCKernel. */
class TetraCUDAKernel {
public:
    void markTetraFaceForFluenceScoring(unsigned IDt, unsigned faceIdx, bool en) {
        // mark 'faceIdx' of tetra 'IDt' to either score or not score ('en') surface fluence events
        unsigned mask = 1 << (faceIdx<<3);
        if(en) {
            m_tetras[IDt].faceFlags |= mask;
        } else {
            m_tetras[IDt].faceFlags &= ~mask;
        }
    }
protected:
    // the tetras for the CUDA kernel
    std::vector<CUDA::Tetra>        m_tetras;
    
};
////////////////////////////////////////////////////////////////////////////

/**
 * Base class of all Tetra MC-based CUDA simulators.
 * NOTE: This does little to no acceleration. This is simply a wrapper to
 * call methods from the CUDA accelerator (External/CUDAAccel) which performs
 * the actual acceleration.
 */
template<class RNGT>
class TetraMCCUDAKernel : public CUDAMCKernelBase, public TetraCUDAKernel, public CUDADirectedSurfaceScorer {
public:
    // RNGT is now RNG - whatever... TetraMCKernel did it    
    typedef RNGT RNG;

    /**
     * Constructor
     */
    TetraMCCUDAKernel() {
        // allocate sizeof(RNG) bytes aligned to 32 bytes
        // NOTE: this does NOT initialize the memory, that is done below
        void* p = boost::alignment::aligned_alloc(32,sizeof(RNG));

        // ensure the allocation succeededmakeThread
        if (!p) {
	        cerr << "Allocation failure in TetraMCCUDAKernel for RNG." << endl;
		    throw std::bad_alloc();
	    }

        // memory allocation was good, now use this aligned memory to initialize the RNG
        // NOTE: this calls the constructor of RNG (and therefore all child constructors).
        // If you simply cast p (i.e. m_rng = (RNG*) p) you won't get compile errors but the
        // RNG (notably the position into the buffer 'm_pos') will not be initialized and segfaults occur.
        m_rng = new (p) RNG();
    }

    /**
     * Destructor
     */ 
    ~TetraMCCUDAKernel() {
        // destroy the accelerator
        m_cudaAccelerator.destroy();

        // free aligned dynamic memory
        boost::alignment::aligned_free(m_rng);
    }

    ////////////////////////////
    // wrappers for calls to CUDAAccel API see External/CUDAAccel for more details
    int gpuDevice() { return m_cudaAccelerator.currentDeviceID(); }
    void gpuDevice(const int devID) { m_cudaAccelerator.changeDevice(devID); }
    void listGPUDevices() { m_cudaAccelerator.listDevices(); }
    size_t maxTetraEstimate(unsigned int packetCountHint=0) { return m_cudaAccelerator.maxTetraEstimate(packetCountHint); }
    ////////////////////////////
    
protected:
    // overide the methods of CUDAMCKernelBase
    virtual void parentPrepare() override;
    virtual void parentSync() override;
    virtual void parentStart() override;
    virtual void parentGather() override;

    // gather helpers
    void gatherVolume();
    void gatherSurfaceExit();
    void gatherDirectedSurface();
    
    // the list of emitters - launching is done host side
    Emitter::EmitterBase<RNG>*              m_emitter;
    
    // the CUDA representation of the materials (list of materials)    
    std::vector<CUDA::Material>             m_mats;

    // the host side random number generator (MUST BE ALIGNED TO 32 BYTES)
    // since SWIG cannot handle alignas() we cannot guarentee their wrappers
    // will allocate the RNG to 32 bytes (causing runtime errors in the TCL
    // interfaces but not in the C++ version). So instead we dynamically
    // allocate it at runtime and force the 32 byte alignment
    // (see TetraMCCUDAKernel constructor) to avoid this problem.
    RNG*                                    m_rng;
    
    // the actual CUDA accelerator
    CUDAAccel                               m_cudaAccelerator;

    // whether the Tetras need to be copied to the GPU or not on this sim run
    bool                                    m_copyTetrasThisRun=true;
};

///////////////////////////////////////////////////////////////////////////////////////////////
// Implementation below (see .cpp file for an explanation

/**
 * Prepare the Kernel to be run. Called before the kernel is actually run
 * so do some prelim stuff to get the kernel ready.
 */
template<class RNGT>
void TetraMCCUDAKernel<RNGT>::parentPrepare() {
    ////////////////////////////////
    //// Geometry setup
    // only update the Tetras if the Geometry has been updated (dirty) or if there are none (empty)
    const Geometry* G = geometry();
    if(!G) {
        throw std::logic_error("TetraMCCUDAKernel<RNG>::parentPrepare() no geometry specified");
    }
    unsigned numFaces_ = 0;

	if(m_tetras.empty() || m_dirtyGeometry) {
        // Tetras are no longer dirty, but we have to copy the tetras to the GPU
        m_dirtyGeometry=false;
        m_copyTetrasThisRun=true;

        if(const TetraMesh* M = dynamic_cast<const TetraMesh*>(G)) {
            //// TETRA MESH ////
            LOG_INFO << "Building CUDA kernel tetras from TetraMesh" << endl;
            
            // grab number of faces
            numFaces_ = M->faceTetraLinks()->size();

            CUDATetrasFromTetraMesh TF;
            TF.mesh(M);
            TF.update();
            m_tetras = TF.tetras();
        } else if (const Layered* L = dynamic_cast<const Layered*>(G)) {
            //// LAYERED ////
            LOG_INFO << "Building CUDA kernel tetras from Layered geometry (" << L->layerCount() << " layers)" << endl;
            
            // no faces in Layered, cannot score faces
            numFaces_ = 0;

            CUDATetrasFromLayered TL;       
            TL.layers(L);
            TL.update();
            m_tetras = TL.tetras();
        } else {
            throw std::logic_error("TetraMCCUDAKernel<RNG>::parentPrepare() geometry is neither Layered nor TetraMesh");
        }
    } else {
        // don't need to copy tetras to GPU this run
        m_copyTetrasThisRun=false;

        // don't need to copy, but still need the number of faces
        if(const TetraMesh* M = dynamic_cast<const TetraMesh*>(G)) {
            // grab number of faces
            numFaces_ = M->faceTetraLinks()->size();
        } else if (const Layered* L = dynamic_cast<const Layered*>(G)) {
            numFaces_ = 0;
        } else {
            throw std::logic_error("TetraMCCUDAKernel<RNG>::parentPrepare() geometry is neither Layered nor TetraMesh");
        }
    }
    ////////////////////////////////

        ////////////////////////////////
    //// Material setup
    MaterialSet* MS = materials();
    if (!MS)
		throw std::logic_error("TetraMCCUDAKernel<RNGT>::parentPrepare() no materials specified");
    // convert SW material to CUDA representation
    float mu_a, mu_s, mu_t, n, g;
    m_mats.resize(MS->size());
   
    for(unsigned i = 0; i < MS->size(); ++i) {
        // get the material from the material set
        ::Material* m = MS->get(i);
        mu_a = m->absorptionCoeff();
        mu_s = m->scatteringCoeff();
        n = m->refractiveIndex();
        g = m->anisotropy();
        mu_t = mu_a + mu_s;

        // create the CUDA::Material
        m_mats[i].n = n;
        m_mats[i].mu_s = mu_s;
        m_mats[i].mu_a = mu_a;
        m_mats[i].g = g;
        m_mats[i].gg = g*g;
        m_mats[i].recip_2g = 1.0f/(2.0f*g);
        m_mats[i].one_minus_gg = 1.0f - g*g;
        m_mats[i].one_plus_gg = 1.0 + g*g;
        m_mats[i].mu_t = mu_t;
        m_mats[i].recip_mu_t = 1.0f / mu_t;
        m_mats[i].m_prop = {-1.0f, -mu_t, n/SPEED_OF_LIGHT_MM_PER_NS};
        m_mats[i].m_init = {1.0f/mu_t, 1.0f, 0.0f};
        m_mats[i].absfrac = mu_a / mu_t;
        m_mats[i].scatters = (mu_s != 0.0f);
        m_mats[i].isotropic = (g < 0.001f);
    }
    ////////////////////////////////
	
    ////////////////////////////////
    // Emitter setup
    if (const TetraMesh* M = dynamic_cast<const TetraMesh*>(G))
	{
        if(!m_src) {
            throw std::logic_error("TetraMCCUDAKernel<RNGT>::parentPrepare() no sources specified");
        }
		
        // TODO: Move this out of here as it doesn't quite belong
		Emitter::TetraEmitterFactory<RNGT> factory(M,MS);
		((Source::Abstract*)m_src)->acceptVisitor(&factory);
        m_emitter = factory.emitter();
	}
	else if (const Layered* L = dynamic_cast<const Layered*>(G))
	{
		if (m_src) {
			LOG_ERROR << "TetraMCCUDAKernel::parentPrepare() ignoring provided source with Layered geometry (only PencilBeam allowed)" << endl;
        }

//#ifdef __ALTIVEC__
		// optimized vector code for P8 goes here
#ifdef USE_SSE
		SSE::UnitVector3 d{{{ 0.0f, 0.0f, 1.0f }}};
		SSE::UnitVector3 a{{{ 1.0f, 0.0f, 0.0f }}};
		SSE::UnitVector3 b{{{ 0.0f, 1.0f, 0.0f }}};

		array<float,3> origin{{0.0f,0.0f,0.0f}};
#else
		SSE::UnitVector3 d({0.0f, 0.0f, 1.0f, 0.0f });
		SSE::UnitVector3 a({1.0f, 0.0f, 0.0f, 0.0f });
		SSE::UnitVector3 b({0.0f, 1.0f, 0.0f, 0.0f });

		array<float,3> origin{{0.0f,0.0f,0.0f}};
#endif
		Emitter::Point P{1U,SSE::Vector3(origin.data())};
		Emitter::Directed D(PacketDirection(d,a,b));

        m_emitter = new Emitter::PositionDirectionEmitter<RNGT,Emitter::Point,Emitter::Directed>(P,D);
	}
	else
    {
		throw std::logic_error("TetraMCCUDAKernel<RNG,Scorer>::parentPrepare() geometry is neither Layered nor TetraMesh");
    }
    ////////////////////////////////

    // seed the RNG
    m_rng->seed(rngSeed_);

    //////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////
    //// NOTE: order important in this block. m_tetras are marked for scoring
    //// in the call to createOutputSurfaces(). Once we call cudaAccelerator.init
    //// the m_tetras are copied to the device, so any changes afterwards are lost
    
    //// Prepare scoring of DirectedSurface events
    // make sure this is done BEFORE calling cudaAccelerator.init
    if(m_cudaAccelerator.scoreDirectedSurface) {
        createOutputSurfaces(this);
    }
    
    //// Initialize the CUDA accelerator.
    // Must be done AFTER materials and tetras are setup (above)
    m_cudaAccelerator.init(
        rngSeed_,
        Nstep_max_,
        Nhit_max_,
        numFaces_,
        wmin_,
        prwin_,
        m_mats,
        m_tetras,
        m_copyTetrasThisRun
    );
    //////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////
}

/**
 * Start the simulator
 * Launches the actual CUDA accelerator
 */
template<class RNGT>
void TetraMCCUDAKernel<RNGT>::parentStart() {
    // check if we have emitters
    if(!m_emitter) {
        throw std::logic_error("Missing emitter");
    }

    // first we will pre-launch (in the host) a certain number of packets
    std::vector<CUDA::LaunchPacket> launched_packets(Npkt_);

    // Only a certain number of packets can run in one instance of the CUDA kernel
    // since each packet gets its own thread. After being initialized with the number
    // of tetras and materials (memory usage) CUDAAccel can determine how many packets 
    // it can simulate in one kernel instance. First we grab that number
    const unsigned int packets_per_instance = m_cudaAccelerator.maxPacketsPossible();
    unsigned int packets_remaining = Npkt_;
    unsigned int packets_to_simulate_this_iteration;
    
    // comparator for LaunchPacket - sorts by tetraID which could bunch 
    // packets launched by same emitter together in warps/blocks
    struct LaunchPacketTetraIDComp {
        inline bool operator() (const CUDA::LaunchPacket& lhs, const CUDA::LaunchPacket& rhs) {
            return lhs.tetraID < rhs.tetraID;
        }
    };

    // launch packets iteratively until no packets remain
    do {
        // determine number of packets to simulate in this iteration and reserve that many LaunchPackets
        packets_to_simulate_this_iteration = min(packets_remaining, packets_per_instance);
        launched_packets.resize(packets_to_simulate_this_iteration);

        // TODO: can this for loop be sped up?
        // TODO: thread this?
        for(unsigned long long i = 0; i < packets_to_simulate_this_iteration; i++) {
            // generator a launch packet in the host format
            LaunchPacket lpkt = m_emitter->emit(*m_rng,packets_to_simulate_this_iteration,i);

            // convert the host style launch packet to the CUDA version
            // position
            launched_packets[i].p = {
                lpkt.pos[0],
                lpkt.pos[1],
                lpkt.pos[2]
            };

            // direction
            launched_packets[i].dir = {
                {lpkt.dir.d[0], lpkt.dir.d[1], lpkt.dir.d[2]},
                {lpkt.dir.a[0], lpkt.dir.a[1], lpkt.dir.a[2]},
                {lpkt.dir.b[0], lpkt.dir.b[1], lpkt.dir.b[2]}
            };

            // tetra ID
            launched_packets[i].tetraID = lpkt.element;
        }

        // sort the packets based on their tetra ID
        //sort(launched_packets.begin(), launched_packets.end(), LaunchPacketTetraIDComp());

        // actually launch the accelerator (async)
        m_cudaAccelerator.launch(launched_packets);

        // NOTE: no need to sync (i.e. cudaDeviceSynchronize) here since all the device functions are in the same queue.
        
        // update the number of packets remaining (remember, unsigned int)
        if(packets_remaining <= packets_per_instance) {
            // simulated all the packets, done
            packets_remaining = 0;
        } else {
            // subtract the number of packets we justed simulated
            packets_remaining -= packets_to_simulate_this_iteration; 
        }
    } while(packets_remaining > 0);
}

/**
 * Wait for the simulation to finish.
 */
template<class RNGT>
void TetraMCCUDAKernel<RNGT>::parentSync() {
    // wait for the CUDA kernel to finish
    m_cudaAccelerator.sync();
}

/**
 * Gather the results from this simulation.
 */
template<class RNGT>
void TetraMCCUDAKernel<RNGT>::parentGather() {
    // clear the results
    OutputDataCollection* C = results();
    C->clear();
    
    //// gather the different data from the device
    // gather volume energy
    if(m_cudaAccelerator.scoreVolume) {
        gatherVolume();
    }

    // gather surface exits
    if(m_cudaAccelerator.scoreSurfaceExit) {
        gatherSurfaceExit();
    }

    // gather directed surface
    if(m_cudaAccelerator.scoreDirectedSurface) {
        gatherDirectedSurface();
    }
}

/**
 * Gather the volume absorption data from the device.
 */
template<class RNGT>
void TetraMCCUDAKernel<RNGT>::gatherVolume() {
    // gather the results to the OutputDataCollection
    OutputDataCollection* C = results();

    // get data from device and convert to float
    // NOTE: This gathers all the tetras including tetra zero
    std::vector<float> vf(m_tetras.size());
    m_cudaAccelerator.copyTetraEnergy(vf);

    // NOTE: Since we ALWAYS insert a zero tetra into the mesh, we need to account for this
    // and remove the first entry (the zero tetra) from the output array
    vf.erase(vf.begin());
    
    // create map
    SpatialMap<float> *vmap = new SpatialMap<float>(std::move(vf), AbstractSpatialMap::Volume, AbstractSpatialMap::Scalar,AbstractSpatialMap::PhotonWeight);
    vmap->name("VolumeEnergy");

    // add the volume absorptions to the output data
    C->add(vmap);
}

/**
 * Gather the mesh surface exit energy from the device.
 */
template<class RNGT>
void TetraMCCUDAKernel<RNGT>::gatherSurfaceExit() {
    // gather the results to the OutputDataCollection
    OutputDataCollection* C = results();

    // get data from device and convert to float
    // NOTE: the vf vector is sized correctly by the copySurfaceExitEnergy function of CUDAAccel
    std::vector<float> vf;
    m_cudaAccelerator.copySurfaceExitEnergy(vf);

    // create map
    SpatialMap<float> *smap = new SpatialMap<float>(std::move(vf), AbstractSpatialMap::Surface, AbstractSpatialMap::Scalar,AbstractSpatialMap::PhotonWeight);
    smap->name("SurfaceExitEnergy");

    // add surface exit energy to the output data
    C->add(smap);
}

/**
 * Gather the internal surface enter/exit events from the device.
 */
template<class RNGT>
void TetraMCCUDAKernel<RNGT>::gatherDirectedSurface() {
    // gather the results to the OutputDataCollection
    OutputDataCollection* C = results();
    
    // get the DirectedSurface data from the device - this is the amount of energy
    // that passed through each face of the mesh if the face was marked for scoring
    // CONVENTION: CUDAAccel use these conventions for the array args to copyDirectedSurfaceEnergy()
    // array1 (i.e. vf1):   energy passing through face 'i' with a direction the SAME as the normal.
    // array2 (i.e. vf2):   energy passing through face 'i' with a direction OPPOSITE the normal.
    std::vector<float> vf1, vf2;
    m_cudaAccelerator.copyDirectedSurfaceEnergy(vf1, vf2);

    // iterating over all the output surfaces we wished to score
    for(unsigned i = 0; i < getNumberOfOutputSurfaces(); i++) {
        // permutation mapping from all directed faces to output exit undirected faces
        Permutation* perm = getOutputSurface(i);

        // setup outputs for entering and exiting energies
        SpatialMap<float>* exiting  = new SpatialMap<float>(perm->dim(),AbstractSpatialMap::Surface,AbstractSpatialMap::Scalar,AbstractSpatialMap::PhotonWeight);
        SpatialMap<float>* entering = exiting->clone();

        // Misleading name - this is the DirectedSurface output data
        DirectedSurface* ds = new DirectedSurface();
        ds->entering(entering);
        ds->exiting(exiting);
        ds->permutation(perm);

        // looping over all directed faces of the current output surface
        for(unsigned i = 0; i < perm->dim(); i++) {
            // get the current directed face index of the surface
            unsigned IDfd = perm->get(i);

            // extract the undirected face idx
            unsigned IDfu = IDfd >> 1;

            // determine whether this face was flipped
            bool faceFlip = IDfd & 0x1;

            if(faceFlip) {
                // If the face NEEDS to be flipped, then the energy passing in the SAME direction as the normal is EXITING the surface (otherwise ENTERING)
                exiting->set(i, vf1[IDfu]);
                entering->set(i, vf2[IDfu]);
            } else {
                // If the face DOES NOT NEED to be flipped, then the energy passing in the SAME direction as the normal is ENTERING the surface (otherwise EXITING)
                exiting->set(i, vf2[IDfu]);
                entering->set(i, vf1[IDfu]);
            }
        }

        // add this mapping to the output data
        C->add(ds);
    }
}

#endif

