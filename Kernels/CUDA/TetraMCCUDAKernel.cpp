#include <FullMonteSW/Kernels/CUDA/TetraMCCUDAKernel.hpp>

#include <FullMonteSW/OutputTypes/SpatialMap.hpp>
#include <FullMonteSW/OutputTypes/OutputData.hpp>
#include <FullMonteSW/OutputTypes/OutputDataCollection.hpp>

// TYS: All relevant code in HPP file.
// Due to difficulty with having templates in CPP+HPP in a shared directory

