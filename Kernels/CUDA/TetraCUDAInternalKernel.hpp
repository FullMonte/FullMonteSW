#ifndef KERNELS_SOFTWARE_TETRACUDAINTERNALKERNEL_HPP_
#define KERNELS_SOFTWARE_TETRACUDAINTERNALKERNEL_HPP_

#include "TetraMCCUDAKernel.hpp"

/**
 * CUDA version of TetraInternalKernel.
 * Tracks volume absorption, energy exiting mesh and energy exiting internal surfaces
 */
class TetraCUDAInternalKernel : public TetraMCCUDAKernel<RNG_SFMT_AVX> {
public:
    TetraCUDAInternalKernel(){
        m_cudaAccelerator.scoreVolume = true;
        m_cudaAccelerator.scoreSurfaceExit = true;
        m_cudaAccelerator.scoreDirectedSurface = true;
    }
};

#endif

