/*
 * TetrasFromLayers.hpp
 *
 *  Created on: May 17, 2017
 *      Author: jcassidy
 */

#ifndef KERNELS_SOFTWARE_TETRASFROMLAYERED_HPP_
#define KERNELS_SOFTWARE_TETRASFROMLAYERED_HPP_

#include <vector>
#include "Tetra.hpp"

/** Create kernel tetras from a layered geometry description
 *
 * Uses degenerate tetras (with 2 faces at infinity) that will never see a photon interaction
 *
 *
 * For i=1..N, tetra i spans faces i-1,i and has thickness t_i
 * Face i is at z_i
 *
 * z_0 = 0
 * z_i = z_(i-1)+t_i
 *
 * Initial launch direction should be +z
 */

class Layered;

class TetrasFromLayered
{
public:
	TetrasFromLayered();
	~TetrasFromLayered();

	void						layers(const Layered* L);
	void						update();

	const std::vector<Tetra>&	tetras() const;

private:
	const Layered*				m_layers=nullptr;
	std::vector<Tetra>			m_tetras;

};



#endif /* KERNELS_SOFTWARE_TETRASFROMLAYERED_HPP_ */
