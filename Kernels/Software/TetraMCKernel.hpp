/*
 * TetraMCKernel.hpp
 *
 *  Created on: Feb 1, 2016
 *      Author: jcassidy
 */

#ifndef KERNELS_SOFTWARE_TETRAMCKERNEL_HPP_
#define KERNELS_SOFTWARE_TETRAMCKERNEL_HPP_

#include <FullMonteSW/Logging/FullMonteLogger.hpp>

#include <FullMonteSW/Geometry/Sources/Print.hpp>
#include <FullMonteSW/Geometry/MaterialSet.hpp>
#include <FullMonteSW/Geometry/Material.hpp>

#include "Emitters/Base.hpp"			//	TODO: Only here until a Layered factory exists
#include "Emitters/Point.hpp"
#include "Emitters/Directed.hpp"

#include <FullMonteSW/Geometry/Partition.hpp>

#include "Logger/AbstractScorer.hpp"

#include "Tetra.hpp"
#include "Material.hpp"

#include "ThreadedMCKernelBase.hpp"
#include "Emitters/TetraMeshEmitterFactory.hpp"

#include "TetrasFromLayered.hpp"
#include "TetrasFromTetraMesh.hpp"

#include <FullMonteSW/Geometry/Layer.hpp>
#include <FullMonteSW/Geometry/Layered.hpp>

#include <FullMonteSW/Geometry/Sources/Abstract.hpp>
#include <FullMonteSW/Geometry/Sources/Composite.hpp>


class Partition;
class TetraMesh;

#include <boost/align/aligned_alloc.hpp>
#include "Logger/tuple_for_each.hpp"

#include <boost/functional/hash.hpp>
#include <unordered_map>

#include "RNG_SFMT_AVX.hpp"

class TetraKernel
{
public:
	virtual ~TetraKernel();
	/**
	 * @brief Utility function for directed surface scoring. 
	 * Flags a certain face of a tetraID to be scored for entering/exiting photon energy
	 * 
	 * @param IDt ID of tetrahedron
	 * @param faceIdx Id of the tetrahedrons face which should be flagged
	 * @param en Activate or deactivate the flag
	 */
	void markTetraFaceForFluenceScoring(unsigned IDt,unsigned faceIdx,bool en);

	/**
	 * @brief Specify a pair of regions in the mesh whose interface boundary should behave in a special way
	 *  in terms of reflection, transmission and absorption. The transmission probability is inferred based on the
	 *  probability values for reflection and absorption. t = 1 - r - a 
	 * 
	 * @param IDm0 First region ID. Order between IDm0 and IDm1 does not matter
	 * @param IDm1 Second region ID. Order between IDm0 and IDm1 does not matter
	 * @param r Probability of a reflection event on the specified interface boundary
	 * @param a Probability of an absorption event on the specified interface boundary
	 * @param lambertian Specifies if lmabertian reflection (true) or specular reflection should take place
	 */
	void markReflectiveSurface(unsigned IDm0,unsigned IDm1,float r,float a=0.0,bool lambertian=false);

	/**
	 * @brief Un-specify a pair of regions in the mesh that have previously been specified for special behavior on their interface boundary
	 * 
	 * @param IDm0 First region ID. Order between IDm0 and IDm1 does not matter
	 * @param IDm1 Second region ID. Order between IDm0 and IDm1 does not matter
	 */
	void unmarkReflectiveSurface(unsigned IDm0,unsigned IDm1);
	
	/**
	 * @brief Delete the previously specified special behaviors for all interface boundaries
	 * 
	 */
	void unmarkAllReflectiveSurfaces();

protected:

	void applyReflectiveSurfaces(unsigned nMaterials);
	unsigned applyReflectiveSurface(unsigned IDm0,unsigned IDm1,float r,float a=0.0,bool lambertian=false);
	void clearReflectiveSurfaces();

	struct ReflectiveFaceDef {
		float prob_reflect=1.0;
		float weight_multiplier=1.0;
		bool lambertianReflection = false;
	};

	ReflectiveFaceDef** m_reflectiveFaceTable=nullptr;

	std::vector<Tetra> m_tetras;

	/// map of (IDm0,IDm1) -> (r,a,lambertian)
	/// Requires IDm0 < IDm1 to get unique lookup
	std::unordered_map<std::pair<unsigned,unsigned>,std::tuple<float,float,bool>,boost::hash<std::pair<unsigned,unsigned>>> m_reflectiveFaces;

private:
	void allocateReflectiveFaceTable(unsigned nMaterials);
	void deleteReflectiveFaceTable();
};

template<class RNGT,class ScorerT>class TetraMCKernel : public ThreadedMCKernelBase, public TetraKernel
{
public:
	TetraMCKernel()
	{
		tuple_for_each(m_scorer, [this](AbstractScorer& s){ addScorer(&s); });
	}

	typedef RNGT RNG;
	class 	Thread;
	typedef ScorerT	Scorer;


#ifndef SWIG
	typedef decltype(createLogger(*static_cast<ScorerT*>(nullptr))) 	Logger;
#endif

protected:
	virtual ThreadedMCKernelBase::Thread* makeThread() final override;

	virtual void parentPrepare() 						override;

    void updateDetectedWeight(); 

	std::vector<x86Kernel::Material> 		    m_mats;

	Emitter::EmitterBase<RNG>* 				    m_emitter;
    
    std::vector<Emitter::DetectorBase<RNG>*>    m_detectors;

	Scorer									    m_scorer;

};

/** 
 * This method updates the detected weight for each Source::CylDetector object to output it to the user
 */
template<class RNGT, class Scorer> void TetraMCKernel<RNGT,Scorer>::updateDetectedWeight() 
{
    auto srcType = dynamic_cast<const Source::Composite*>(m_src); 
    
    if (srcType == nullptr)
        return;

    for (auto detBase : m_detectors) 
    {
        Emitter::Detector<RNGT, Emitter::CylDetector<RNGT>>* detTemp = dynamic_cast<Emitter::Detector<RNGT, Emitter::CylDetector<RNGT>>*>(detBase);

        unsigned detectorID = detTemp->detector().detectorID(); 

        for (Source::Abstract* src : srcType->elements())
        {
            Source::CylDetector* tempDet = dynamic_cast<Source::CylDetector*>(src); 
            if (tempDet && tempDet->detectorID() == detectorID)
            {
                tempDet->detectedWeight(detTemp->detector().detectedWeight());
                break;
            }
        }
    }
}

template<class RNGT,class Scorer>void TetraMCKernel<RNGT,Scorer>::parentPrepare()
{
	
	////// Geometry setup
    const Geometry* G = geometry();
    if (!G)
	    throw std::logic_error("TetraMCKernel<RNG,Scorer>::parentPrepare() no geometry specified");
	
    // only update the Tetras if the Geometry has been updated (dirty) or if there are none (empty)
	if(m_tetras.empty() || m_dirtyGeometry) {
		m_dirtyGeometry=false;

		if (const TetraMesh* M = dynamic_cast<const TetraMesh*>(G))
		{
			LOG_INFO << "Building kernel tetras from TetraMesh" << endl;
			TetrasFromTetraMesh TF;
			TF.mesh(M);
			TF.update();

			m_tetras = TF.tetras();
		}
		else if (const Layered* L = dynamic_cast<const Layered*>(G))
		{
			LOG_INFO << "Building kernel tetras from Layered geometry (" << L->layerCount() << " layers)" << endl;
			TetrasFromLayered TL;

			TL.layers(L);
			TL.update();

			m_tetras = TL.tetras();
		}
		else
        {
			throw std::logic_error("TetraMCKernel<RNG,Scorer>::parentPrepare() geometry is neither Layered nor TetraMesh");
        }
		//cout << "Done with " << m_tetras.size() << " tetras" << endl;
	}

	////// Material setup
	const MaterialSet* MS = materials();
	if (!MS)
		throw std::logic_error("TetraMCKernel<RNGT,Scorer>::parentPrepare() no materials specified");

	m_mats.resize(MS->size());

	// copy materials
	for(unsigned i=0;i<MS->size();++i)
	{
		::Material* m = MS->get(i);
		m_mats[i] = x86Kernel::Material(m->absorptionCoeff(), m->scatteringCoeff(),m->refractiveIndex());
	}

    ////// Emitter setup
	if (const TetraMesh* M = dynamic_cast<const TetraMesh*>(G))
	{
        if(!m_src) {
            throw std::logic_error("TetraMCKernel<RNGT>::parentPrepare() no sources specified");
        }
		Emitter::TetraEmitterFactory<RNGT> factory(M, MS);
        factory.setKernelTetra(m_tetras.data()); 
		((Source::Abstract*)m_src)->acceptVisitor(&factory);
        m_emitter = factory.emitter();
        m_detectors = factory.detectors(); 
        
        if (!m_detectors.empty()) {
            m_dirtyGeometry = true; // This is because the m_tetras list is edited when a 
                                    // detector is created. So have to reset everything. A better way to do this is to 
                                    // just loop over the dirty tetra and clean them after the kernel finished running. TODO
        }
		
	}
	else if (const Layered* L = dynamic_cast<const Layered*>(G))
	{
		if (m_src) {
			LOG_WARNING << "TetraMCKernel::parentPrepare() ignoring provided source with Layered geometry (only PencilBeam allowed)" << endl;
        }
        LOG_WARNING << "TetraMCKernel::parentPrepare() No support for detectors in Layerd mesh" << endl;

//#ifdef __ALTIVEC__
		// optimized vector code for P8 goes here
#ifdef USE_SSE
		SSE::UnitVector3 d{{{ 0.0f, 0.0f, 1.0f }}};
		SSE::UnitVector3 a{{{ 1.0f, 0.0f, 0.0f }}};
		SSE::UnitVector3 b{{{ 0.0f, 1.0f, 0.0f }}};

		array<float,3> origin{{0.0f,0.0f,0.0f}};
#else
		SSE::UnitVector3 d({0.0f, 0.0f, 1.0f, 0.0f });
		SSE::UnitVector3 a({1.0f, 0.0f, 0.0f, 0.0f });
		SSE::UnitVector3 b({0.0f, 1.0f, 0.0f, 0.0f });

		array<float,3> origin{{0.0f,0.0f,0.0f}};
#endif
		Emitter::Point P{1U,SSE::Vector3(origin.data())};
		Emitter::Directed D(PacketDirection(d,a,b));

        m_emitter = new Emitter::PositionDirectionEmitter<RNGT,Emitter::Point,Emitter::Directed>(P,D);
	}
	else
    {
		throw std::logic_error("TetraMCKernel<RNG,Scorer>::parentPrepare() geometry is neither Layered nor TetraMesh");
    }




	////// Material setup
	if (!materials())
		throw std::logic_error("TetraMCKernel<RNGT,Scorer>::parentPrepare() no materials specified");

	m_mats.resize(materials()->size());

	applyReflectiveSurfaces(materials()->size());

	bool m_debugReflectiveSurfaces = false;

	if (m_debugReflectiveSurfaces)
	{
		for(unsigned i=0;i<materials()->size();++i)
			cout << "   " << setw(12) << i << ' ';
		cout << endl;

		cout << "Reflection probability" << endl;
		for(unsigned i=0;i<materials()->size();++i)
		{
			cout << setw(2) << i << ' ' << fixed;
			for(unsigned j=0;j<materials()->size();++j)
				cout << m_reflectiveFaceTable[i][j].prob_reflect << ' ';
			cout << endl;
		}

		cout << "Weight multiplier" << endl;
		for(unsigned i=0;i<materials()->size();++i)
		{
			cout << setw(2) << i << ' ' << fixed;
			for(unsigned j=0;j<materials()->size();++j)
				cout << m_reflectiveFaceTable[i][j].weight_multiplier << ' ';
			cout << endl;
		}

		for(unsigned tetID=0;tetID<m_tetras.size();++tetID)
		{
			if (m_tetras[tetID].faceFlags)
			{
				cout << "Tetra " << setw(6) << tetID << " in region " << m_tetras[tetID].matID << " has flags set" << endl;
				for(unsigned f=0;f<4;++f)
				{
					unsigned IDt_adj = m_tetras[tetID].adjTetras[f];

					cout << "  Face " << m_tetras[tetID].IDfds[f] << " tetra " << IDt_adj << " material " << m_tetras[IDt_adj].matID << endl;
				}

			}
		}
	}

	// copy materials
	for(unsigned i=0;i<materials()->size();++i)
	{
		::Material* m = materials()->get(i);
		m_mats[i] = x86Kernel::Material(m->absorptionCoeff(), m->scatteringCoeff(),m->refractiveIndex());
	}

	resetSeedRng();
}


template<class RNGT,class Scorer>ThreadedMCKernelBase::Thread* TetraMCKernel<RNGT,Scorer>::makeThread()
{
	void *p = boost::alignment::aligned_alloc(32,sizeof(typename TetraMCKernel<RNG_SFMT_AVX,Scorer>::Thread));

	if (!p)
	{
		cerr << "Allocation failure in TetraMCKernel<RNGT,Scorer>::makeThread()" << endl;
		throw std::bad_alloc();
	}

	// create the thread-local state

	typename TetraMCKernel<RNG_SFMT_AVX,Scorer>::Thread* t = new (p) typename TetraMCKernel<RNG_SFMT_AVX,Scorer>::Thread(*this,createLogger(m_scorer));

	// seed its RNG
	t->seed(TetraMCKernel<RNG_SFMT_AVX,Scorer>::getUnsignedRNGSeed());

	return t;
}

inline void TetraKernel::markTetraFaceForFluenceScoring(unsigned IDt,unsigned faceIdx,bool en)
{
	m_tetras[IDt].setFaceFlag(faceIdx,Tetra::FluenceScoring,en);
}


#endif /* KERNELS_SOFTWARE_TETRAMCKERNEL_HPP_ */


