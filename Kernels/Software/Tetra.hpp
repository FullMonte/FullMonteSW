#ifndef KERNEL_TETRA_HPP_INCLUDED_
#define KERNEL_TETRA_HPP_INCLUDED_

#include "sse.hpp"

#include <array>
#include <iostream>
#include <limits>
#include <vector>

#include <fstream>

#include "FullMonteSW/Geometry/NonSSE.hpp"

using namespace std;

typedef struct {
        /**
         * @brief Position [x (32 Bit float), y (32 Bit float), z (32 Bit float), 0] 
         * of the next position of the photon 
         * Pe = position + direction * step_length
         * __m128 is 128 Bit = 16 Byte large
         * 
         */
//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
        __m128 Pe; 
#else
        m128 Pe;
#endif

        /**
         * @brief Distance to the nearest face of the tetra where the photon is in or
         * the random step length for the photon.
         * all 4 values (4 floats = 16 Byte) have the same value
         * 16 Byte size
         * 
         */
//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
        __m128 distance;     
#else
        m128 distance;
#endif
        /**
         * @brief Face ID of the tetras face which has the smallest distance to 
         * the photons current position
         * 
         * 4 Byte size
         * 
         */
        int IDfe;           
        
        /**
         * @brief  Tetra ID of the tetra which is adjacent to the face which is 
         * nearest to the photons current position 
         * 4 Byte size
         * 
         */
        unsigned IDte;      
        
        /**
         * @brief stores the index of the nearest face to the photon
         * 4 Byte size
         * 
         */
        int idx;            
        
        /**
         * @brief specifies if the photon intersects with the face of the current tetra within 
         * the random step length. This means the photons leaves the current tetra
         * 1 Byte size
         * 
         */
        bool hit;          
    } StepResult;



/** High-performance tetra representation for the software kernel using SSE instructions.
 *
 * The x,y,z, and C (constant) components of the normal for the four faces are stored together to facilitate matrix multiplication.
 * 
 * C is the constant which is needed to check wether a point is inside the tetrahedron or not. it is calculated for each face
 * 
 * by \f$ C =1/||P_2-P_1 * P_3-P_1||(P_2-P_1 * P_3-P_1))^T x P_1 \f$
 * 
 * Normals (nx, ny, nz) are oriented so that points inside the tetra have a positive altitude h= dot(p,n)-C
 */
class Tetra {
public:

   enum FaceFlags { FluenceScoring=0, SpecialInterface=1 };

    /// x component of normal vector for all 4 tetra faces 4x32 Bit = 128 Bit = 16 Bytes
//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE    
    __m128 nx;
#else
    m128 nx;
#endif

    /// y component of normal vector for all 4 tetra faces 4x32 Bit = 128 Bit = 16 Bytes
//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE     
    __m128 ny;
#else
    m128 ny;
#endif

    /// z component of normal vector for all 4 tetra faces 4x32 Bit = 128 Bit = 16 Bytes
//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE     
    __m128 nz;
#else
    m128 nz;
#endif

    /// Constants of all 4 tetra faces. These constants are used to check if a given point is inside the tetrahedron or not.
//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE     
    __m128 C;
#else
    m128 C;
#endif
    /// The four directed face IDs of the current tetra extracted from the array of faces (4 x 4B = 16 B)
    std::array<unsigned,4> 	IDfds;

    /// Adjacent tetras ID for each face of the current tetra (4 x 4B = 16 B)		
    std::array<unsigned,4>  adjTetras;  
    
    /**< Flags for each face (4Byte = 32Bit -> 8Bit each)
    * For face i=0..3, content of (faceFlags >> (i<<3)) is:
    * Bit 	Meaning
    * === 	=======
    * 7
    * 6
    * 5
    * 4
    * 3
    * 2
    * 1		special interface (reflection/absorption probabilities at face)
    * 0		whether to score fluence through the surface (DirectedSurfaceScorer)
    */
    unsigned faceFlags=0;				

    /// Material ID for this tetra (4 B)
    unsigned matID;         				

    /// IDs of detectors enclosed in this tetra. size = 0 if none
    std::vector<unsigned> detectorEnclosedIDs;

//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE  
    /// Computes the dot product of the provided vector with each of the four face normals
    inline __m128 dots(__m128) const;

    /// Computes the height of the provided point over each of the four faces (h > 0 -> inside tetra)
    inline __m128 heights(__m128) const;

    /// Checks if a point is within the tetra, with optional tolerance
    inline bool pointWithin(__m128,float eps=0.0f) const;

    StepResult getIntersection(__m128,__m128,__m128 s) const;
#else
        /// Computes the dot product of the provided vector with each of the four face normals
    inline m128 dots(m128) const;

    /// Computes the height of the provided point over each of the four faces (h > 0 -> inside tetra)
    inline m128 heights(m128) const;

    /// Checks if a point is within the tetra, with optional tolerance
    inline bool pointWithin(m128,float eps=0.0f) const;

    StepResult getIntersection(m128,m128,m128 s) const;
#endif
    std::array<float,3> face_normal(unsigned i) const;
    
    float face_constant(unsigned i) const;

    bool getFaceFlag(unsigned faceIdx,unsigned flagIdx) const
    {
    		return (faceFlags >> ((faceIdx<<3) + flagIdx)) & 0x1;
    }
    
    void setFaceFlag(unsigned faceIdx,Tetra::FaceFlags flagIdx,bool flag)
    {
    		unsigned mask = 1 << ((faceIdx<<3)+flagIdx);
    		if (flag)
    			faceFlags |= mask;
    		else
    			faceFlags &= ~mask;
    }

private:
    friend void printTetra(const Tetra& T,std::ostream& os);

} __attribute__ ((aligned(64)));

typedef Tetra KernelTetra;

/**
 * @brief Prints all information of the tetra.
 * (For debug)
 * 
 * @param T Tetra which should be printed
 * @param os outputstream to write to
 */
void printTetra(const Tetra& T,std::ostream& os);

/**
 * @brief get the constant C for the tetras face with index i
 * 
 * @param i index of a tetras face
 * @return float constant C
 */
inline float Tetra::face_constant(unsigned i) const
{
//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
	float f[4];
	_mm_store_ps(f,C);
	return f[i];
#else
    return C[i];
#endif
}

/**
 * @brief Return the normal of a tetra face of index i
 * 
 * @param i index of a tetras face
 * @return std::array<float,3> normal vector (x, y, z) of face with index i
 */
inline std::array<float,3> Tetra::face_normal(unsigned i) const
{
//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
	float x[4],y[4],z[4];
	_mm_store_ps(x,nx);
	_mm_store_ps(y,ny);
	_mm_store_ps(z,nz);
	return std::array<float,3>{{ x[i], y[i], z[i] }};
#else
    return std::array<float,3>{{ nx[i], ny[i], nz[i] }};
#endif
}

//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE 
/**
 * @brief Calculate the dot product of the normal vectors of all 4 faces of the tetras
 * and the direction of the photon
 * 
 * @param d direction/position of the photon, both dot products are needed 
 * vector representation (x, y, z, 0)
 * @return __m128 normals of all faces in the tetra = (nx[i] * d[0] + ny[i] * d[1] + nz[i] * d[2])
 */
inline __m128 Tetra::dots(const __m128 d) const
{
	return _mm_add_ps(
			_mm_add_ps(
					_mm_mul_ps(ny,_mm_shuffle_ps(d,d,_MM_SHUFFLE(1,1,1,1))),
					_mm_mul_ps(nz,_mm_shuffle_ps(d,d,_MM_SHUFFLE(2,2,2,2)))
			),
			_mm_mul_ps(nx,_mm_shuffle_ps(d,d,_MM_SHUFFLE(0,0,0,0))));
}

/**
 * @brief Calculate the heights of the photons current position for all faces
 * 
 * @param p position of the photon
 * @return __m128 height of the photon to each face of the current tetra
 */
inline __m128 Tetra::heights(const __m128 p) const
{
	return _mm_sub_ps(dots(p),C);
}


/**
 * @brief Calculates if a given photon with its position p, direction d intersects with a 
 * face of the current tetra within a random step length. 
 * 
 * @param p current photon position
 * @param d photon direction
 * @param s random step length of photon
 * @return StepResult Returns the struct StepResult with updated positon (Pe), distance to the 
 * nearest face (distance), faceID of that face (IDfe), the adjacent tetra of that face (IDte), 
 * the index of the nearest face within the __m128 representation (idx), and if an intersection 
 * acutally occurs (hit)
 */
inline StepResult Tetra::getIntersection(const __m128 p,const __m128 d,__m128 s) const
{
    /*
    //Write Variable output to file for diff SSE
    std::ofstream outfile;
    outfile.open("SSE.txt", std::ios_base::app);
    float f[4];
    _mm_store_ps (f, p);
    outfile << "input p: "<< (double) f[0]<< " " << (double) f[1] << " "<< (double) f[2] <<" "<< (double) f[3] <<endl;
    _mm_store_ps (f, d);
    outfile << "input d: "<< (double) f[0]<< " " << (double) f[1] << " "<< (double) f[2] <<" "<< (double) f[3] <<endl;
    _mm_store_ps (f, s);
    outfile << "input s: "<< (double) f[0]<< " " << (double) f[1] << " "<< (double) f[2] <<" "<< (double) f[3] <<endl;
    */
    
    StepResult result;

    result.idx=-1;
    
    /**
     * @brief use the first entry as random step length
     * s[i] = s[0]
     * 
     */
    s = _mm_shuffle_ps(s,s,_MM_SHUFFLE(0,0,0,0));

    /**
     * @brief calculate dot = n (dot) d
     * calculate the entries of the dot product between n and d for all faces of a tetra
     * 
     * dot[i] = nx[i] * d[0]
     */
    __m128 dot = _mm_mul_ps(nx,_mm_shuffle_ps(d,d,_MM_SHUFFLE(0,0,0,0)));

    /// dot[i] = dot[i] + ny[i] * d[1]
    dot = _mm_add_ps(dot,_mm_mul_ps(ny,_mm_shuffle_ps(d,d,_MM_SHUFFLE(1,1,1,1))));

    /// dot[i] = dot[i] + nz[i] * d[2]
    dot = _mm_add_ps(dot,_mm_mul_ps(nz,_mm_shuffle_ps(d,d,_MM_SHUFFLE(2,2,2,2))));
    
    /**
     * @brief calculate constraint if a point lies within the tetra. This also
     * represents the height which the photon position is over the face of the respective tetra
     * height = C - n (dot) p
     * 
     * h1[i] = nx[i] * p[0]
     */
    __m128 h1 = _mm_mul_ps(nx,_mm_shuffle_ps(p,p,_MM_SHUFFLE(0,0,0,0)));

    /// h1[i] = h1[i] + ny[i] * p[1]
    h1 = _mm_add_ps(h1,_mm_mul_ps(ny,_mm_shuffle_ps(p,p,_MM_SHUFFLE(1,1,1,1))));

    /// h1[i] = h1[i] + nz[i] * p[2]
    h1 = _mm_add_ps(h1,_mm_mul_ps(nz,_mm_shuffle_ps(p,p,_MM_SHUFFLE(2,2,2,2))));

    /**
     * @brief -height (= C - n dot p) should be negative if inside tetra, 
     * may occasionally be (small) positive due to numerical error dot 
     * negative means facing outwards. Subtract packed single-precision (32-bit) 
     * floating-point elements in h1 from packed single-precision (32-bit) floating-point 
     * elements in C, and store the results in h1.
     * 
     * h1[i] = C[i] - h1[i]
     */
    h1 = _mm_sub_ps(C,h1);

    // 
    /**
     * @brief Divide packed single-precision (32-bit) floating-point elements 
     * in h1 by packed elements in dot, and store the results in dist. dist is the distance 
     * of the photon to the 4 faces of the tetra.
     * 
     * 
     * dist[i] = h1[i]/dot[i]
     */
    __m128 dist = _mm_div_ps(h1,dot);

    
    /**
     * @brief Checks if dot is positive or negative (_mm_blendv_ps). If it is negative, use
     * the value of dist otherwise use infinity (which is apparently s --> )
     * 
     * selects dist where dist>0 and dot<0 (facing outwards), s otherwise
     * 
     * (if dot[i] < 0) --> dist[i] = dist[i]
     * (else dot[i] >= 0) --> dist[i] = inf
     */
    dist = _mm_blendv_ps(
    			_mm_set1_ps(std::numeric_limits<float>::infinity()),
				dist,
				dot);

    
    // at most three of the dot products should be negative

    //     -height  dot     h/dot   meaning
    //      -       +       -       OK: inside, facing away (no possible intersection)
    //      -       -       +       OK: inside, facing towards (intersection possible)
    //      +       +       +       OK: outside, facing in (this is the entry face with roundoff error, no possible intersection)
    //      +       -       -       ERROR: outside, facing out (problem!! this must be the entry face, but exiting!)

    // require C - n dot p < 0 (above face) and d dot n < 0

    /// Determines the minimum value of dist and also returns its index (0, 1, 2, 3)
    pair<unsigned,__m128> min_idx_val = getMinIndex4p(dist);

    /**
     * @brief _mm_ucomilt_ss --> Compare the lower single-precision (32-bit) floating-point element  
     * in min_idx_val.second and s for less-than, and return the boolean result (0 or 1). This instruction will not  
     * signal an exception for QNaNs.
     * Is the smallest distance of the photon smaller than the random step length s?
     * If yes, return true. This means that the photon intersects with one of the tetras
     * faces within the random step length 
     * 
     * if min_idx_val[0] < s[0] 
     * 
     */
    result.hit = _mm_ucomilt_ss(min_idx_val.second,s);

    
    /**
     * @brief Choose the face with the smallest distance to the photons position and store it in
     * IDfe. Bitwise AND of the index in min_idx_val because the index can also have the 
     * value 4 which would result in bad memory access (size of IDfds is 4 [0-3])
     * 
     */
    result.IDfe = IDfds[min_idx_val.first&3];
    
    /**
     * @brief Choose the adjacent tetra of the nearest face to the photons position and store it in
     * IDte. Bitwise AND of the index in min_idx_val because the index can also have the 
     * value 4 which would result in bad memory access (size of IDte is 4 [0-3])
     * 
     */
    result.IDte = adjTetras[min_idx_val.first&3];
    
    /// will be 4 if no min found... how can we not find a minimum in 4 numbers? Are all inf?
    result.idx = min_idx_val.first;

    /// compare the smallest distance min_idx_val.second with the random step length
    /// and store the smaller value in result.distance
    result.distance=_mm_min_ps(min_idx_val.second,s);
    
    /// compare the result of the last command with 0 and store the larger value in result.distance
    result.distance=_mm_max_ps(_mm_setzero_ps(),result.distance);
    
    /**
     * @brief Determine the new position of the photon after the step length. result.distance
     * has the same value in each float element
     * 
     * Pe[i] = p[i] + d[i] * result.distance[i]
     * 
     */
    result.Pe = _mm_add_ps(p,_mm_mul_ps(d,result.distance));

    return result;
}


/**
 * @brief Verifies that a point is within the specified tetrahedron, 
 * using tolerance epsilon /f$ < \epsilon/f$.
 * 
 * @param p position of the photon [x, y, z, 0]
 * @param eps tolerance value 
 * @return true if p lies in the current tetra
 * @return false if p lies outside the current tetra
 */
 
inline bool Tetra::pointWithin(__m128 p,float eps) const
{
	__m128 h = heights(p);
    // cmpWithTolerance = h[i] + eps
    __m128 cmpWithTolerance = _mm_add_ps(h,_mm_set1_ps(eps));
    
    // movemask shows if top (sign) bit is set. 0 means all positive.
    return _mm_movemask_ps(cmpWithTolerance) == 0;			
}
#else
/**
 * @brief Calculate the dot product of the normal vectors of all 4 faces of the tetras
 * and the direction of the photon
 * 
 * @param d direction/position of the photon, both dot products are needed 
 * vector representation (x, y, z, 0)
 * @return __m128 normals of all faces in the tetra = (nx[i] * d[0] + ny[i] * d[1] + nz[i] * d[2])
 */
inline m128 Tetra::dots(const m128 d) const
{
	return add128(
			add128(
					mult128(ny,shuffle128(d,d,1,1,1,1)),
					mult128(nz,shuffle128(d,d,2,2,2,2))
			),
			mult128(nx,shuffle128(d,d,0,0,0,0)));
}

/**
 * @brief Calculate the heights of the photons current position for all faces
 * 
 * @param p position of the photon
 * @return __m128 height of the photon to each face of the current tetra
 */
inline m128 Tetra::heights(const m128 p) const
{
	return sub128(dots(p),C);
}

/**
 * @brief Calculates if a given photon with its position p, direction d intersects with a 
 * face of the current tetra within a random step length. 
 * 
 * @param p current photon position
 * @param d photon direction
 * @param s random step length of photon
 * @return StepResult Returns the struct StepResult with updated positon (Pe), distance to the 
 * nearest face (distance), faceID of that face (IDfe), the adjacent tetra of that face (IDte), 
 * the index of the nearest face within the __m128 representation (idx), and if an intersection 
 * acutally occurs (hit)
 */
inline StepResult Tetra::getIntersection(const m128 p,const m128 d,m128 s) const
{
    /*
    //Write variable output to file NonSSE
    std::ofstream outfile;
    outfile.open("NonSSE.txt", std::ios_base::app);
    float f1, f2, f3, f4;
    f1 = p[0];
    f2 = p[1];
    f3 = p[2];
    f4 = p[3];
    outfile << "input p: "<< (double)f1 << " " << (double)f2 << " " << (double)f3 << " " << (double)f4 <<endl;
    f1 = d[0];
    f2 = d[1];
    f3 = d[2];
    f4 = d[3];
    outfile << "input d: "<< (double)f1 << " " << (double)f2 << " " << (double)f3 << " " << (double)f4 <<endl;
    f1 = s[0];
    f2 = s[1];
    f3 = s[2];
    f4 = s[3];
    outfile << "input s: "<< (double)f1 << " " << (double)f2 << " " << (double)f3 << " " << (double)f4 <<endl;
    */
    StepResult result;

    result.idx=-1;
    
    /**
     * @brief use the first entry as random step length
     * s[i] = s[0]
     * 
     */
    s = shuffle128(s,s,0,0,0,0);

    /**
     * @brief calculate dot = n (dot) d
     * calculate the entries of the dot product between n and d for all faces of a tetra
     * 
     * dot[i] = nx[i] * d[0]
     */
    m128 dot = mult128(nx,shuffle128(d,d,0,0,0,0));

    /// dot[i] = dot[i] + ny[i] * d[1]
    dot = add128(dot,mult128(ny,shuffle128(d,d,1,1,1,1)));

    /// dot[i] = dot[i] + nz[i] * d[2]
    dot = add128(dot,mult128(nz,shuffle128(d,d,2,2,2,2)));
    
    /**
     * @brief calculate constraint if a point lies within the tetra. This also
     * represents the height which the photon position is over the face of the respective tetra
     * height = C - n (dot) p
     * 
     * h1[i] = nx[i] * p[0]
     */
    m128 h1 = mult128(nx,shuffle128(p,p,0,0,0,0));

    /// h1[i] = h1[i] + ny[i] * p[1]
    h1 = add128(h1,mult128(ny,shuffle128(p,p,1,1,1,1)));

    /// h1[i] = h1[i] + nz[i] * p[2]
    h1 = add128(h1,mult128(nz,shuffle128(p,p,2,2,2,2)));

    /**
     * @brief height (= C - n dot p) should be negative if inside tetra, 
     * may occasionally be (small) positive due to numerical error dot 
     * negative means facing outwards. Subtract packed single-precision (32-bit) 
     * floating-point elements in h1 from packed single-precision (32-bit) floating-point 
     * elements in C, and store the results in h1.
     * 
     * h1[i] = C[i] - h1[i]
     */
    h1 = sub128(C,h1);


    // 
    /**
     * @brief Divide packed single-precision (32-bit) floating-point elements 
     * in h1 by packed elements in dot, and store the results in dist. dist is the distance 
     * of the photon to the 4 faces of the tetra.
     * 
     * 
     * dist[i] = h1[i]/dot[i]
     */
    m128 dist = div128(h1,dot);

    
    /**
     * @brief Checks if dot is positive or negative (_mm_blendv_ps). If it is negative, use
     * the value of dist otherwise use infinity (which is apparently s --> )
     * 
     * selects dist where dist>0 and dot<0 (facing outwards), s otherwise
     * 
     * (if dot[i] < 0) --> dist[i] = dist[i]
     * (else dot[i] > 0) --> dist[i] = inf
     */
    m128 inf = set1_ps128(std::numeric_limits<float>::infinity());
    dist = blend128(
    			inf,
				dist,
				dot);

    // at most three of the dot products should be negative

    //     -height  dot     h/dot   meaning
    //      -       +       -       OK: inside, facing away (no possible intersection)
    //      -       -       +       OK: inside, facing towards (intersection possible)
    //      +       +       +       OK: outside, facing in (this is the entry face with roundoff error, no possible intersection)
    //      +       -       -       ERROR: outside, facing out (problem!! this must be the entry face, but exiting!)

    // require C - n dot p < 0 (above face) and d dot n < 0

    /// Determines the minimum value of dist and also returns its index (0, 1, 2, 3)
    pair<unsigned,m128> min_idx_val = getMinIndex128(dist);

    /**
     * @brief _mm_ucomilt_ss --> Compare the lower single-precision (32-bit) floating-point element  
     * in min_idx_val.second and s for less-than, and return the boolean result (0 or 1). This instruction will not  
     * signal an exception for QNaNs.
     * Is the smallest distance of the photon smaller than the random step length s?
     * If yes, return true. This means that the photon intersects with one of the tetras
     * faces within the random step length 
     * 
     * if min_idx_val[0] < s[0] 
     * 
     */
    result.hit = complo128(min_idx_val.second,s);

    
    /**
     * @brief Choose the face with the smallest distance to the photons position and store it in
     * IDfe. Bitwise AND of the index in min_idx_val because the index can also have the 
     * value 4 which would result in bad memory access (size of IDfds is 4 [0-3])
     * 
     */
    result.IDfe = IDfds[min_idx_val.first&3];
    
    /**
     * @brief Choose the adjacent tetra of the nearest face to the photons position and store it in
     * IDte. Bitwise AND of the index in min_idx_val because the index can also have the 
     * value 4 which would result in bad memory access (size of IDte is 4 [0-3])
     * 
     */
    result.IDte = adjTetras[min_idx_val.first&3];
    
    /// will be 4 if no min found
    result.idx = min_idx_val.first;

    /// compare the smallest distance min_idx_val.second with the random step length
    /// and store the smaller value in result.distance
    result.distance=min128(min_idx_val.second,s);
    
    /// compare the result of the last command with 0 and store the larger value in result.distance
    result.distance=max128(set_zero128(),result.distance);
    
    /**
     * @brief Determine the new position of the photon after the step length. result.distance
     * has the same value in each float element
     * 
     * Pe[i] = p[i] + d[i] * result.distance[i]
     * 
     */
    result.Pe = add128(p,mult128(d,result.distance));
        
    return result;
}


/**
 * @brief Verifies that a point is within the specified tetrahedron, 
 * using tolerance epsilon /f$ < \epsilon/f$.
 * 
 * @param p position of the photon [x, y, z, 0]
 * @param eps tolerance value 
 * @return true if p lies in the current tetra
 * @return false if p lies outside the current tetra
 */
 
inline bool Tetra::pointWithin(std::array<float,4> p,float eps) const
{
	std::array<float,4> h = heights(p);
    // cmpWithTolerance = h[i] + eps
    std::array<float,4> cmpWithTolerance = add128(h,set1_ps128(eps));
    
    // movemask shows if top (sign) bit is set. 0 means all positive.
    return movemask128(cmpWithTolerance) == 0;			
}
#endif

#endif
