/*
 * TetrasFromTetraMesh.cpp
 *
 *  Created on: May 17, 2017
 *      Author: jcassidy
 */

#include "TetrasFromTetraMesh.hpp"

#include <FullMonteSW/Logging/FullMonteLogger.hpp>
#include <FullMonteSW/Geometry/TetraMesh.hpp>
#include <FullMonteSW/Geometry/Partition.hpp>
#include <FullMonteSW/Geometry/FaceLinks.hpp>

#include <functional>

TetrasFromTetraMesh::TetrasFromTetraMesh()
{
}

TetrasFromTetraMesh::~TetrasFromTetraMesh()
{
}

void TetrasFromTetraMesh::mesh(const TetraMesh* M)
{
	m_mesh=M;
}

void TetrasFromTetraMesh::update()
{
	// build the kernel tetras (halfspace representation) from the provided mesh
	makeKernelTetras();

	checkKernelFaces();
}


/** Validates the orientation of faces within the kernel tetra structure.
 *
 */

bool TetrasFromTetraMesh::checkKernelFaces() const
{
	bool status_ok=true;

	LOG_DEBUG << "Checking faces on " << m_tetras.size() << " tetras" << endl;

	// iterate over all tetras
	for(unsigned t=1; t < m_tetras.size(); ++t)
	{
		Tetra T = m_tetras[t];

		// face reciprocity: each face points to either zero tetra or a tetra that points back to this one
		for(unsigned f=0;f<4;++f)
		{
			Tetra adjTetra;

			if (T.adjTetras[f] >= m_tetras.size())
			{
				status_ok=false;
				LOG_ERROR << "  Tetra " << t << " face " << f << " connects to a tetra out of array range" << endl;
			}
			else
				adjTetra = m_tetras[T.adjTetras[f]];

			if (T.adjTetras[f] != 0 && boost::count_if(adjTetra.adjTetras, [t](unsigned i){ return i==t; }) != 1)
			{
				LOG_ERROR << "  Tetra " << t << " face " << f << " connects to tetra " << T.adjTetras[f] << " but that tetra connects to " <<
                    m_tetras[t].adjTetras[0] << " " << m_tetras[t].adjTetras[1] << " " << m_tetras[t].adjTetras[2] << " " << m_tetras[t].adjTetras[3] << endl;
				status_ok = false;
			}
		}

//		// face orientation:
//		for(unsigned f=0;f<4;++f)
//		{
//
//		}

//		bool tet_ok=true;
//		for(int f=0;f<4;++f)
//		{
//			std::array<float,4> h = to_array<float,4>(m_tetras[IDt].heights(to_m128(tetraPointCoords[f])));
//
//			for(unsigned i=0;i<4;++i)
//				if (h[i] < -4e-5)
//				{
//					tet_ok=status_ok=false;
//					cout << "Error: incorrect height of " << f << "'th tetra point (ID " << IDps[f] << ") over " << i << "'th face (ID" << m_tetras[IDt].IDfs[i] << "): " << h[i] << endl;
//				}
//
//			if (!tet_ok)
//			{
//				cout << "tet " << IDt << " face " << f << " opposite-corner heights: ";
//				for(unsigned i=0;i<4;++i)
//					cout << h[i] << ' ';
//				cout << endl;
//			}
//		}
	}
//
//	printTetra(m_tetras[0],std::cout);
//	printTetra(m_tetras[120569],std::cout);

	return status_ok;
}



/** Converts the TetraMesh representation to the packed data structures used by the kernel.
 *
 * 
 * Produces: m_tetras
 *
 */
//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
/// union allows you to store different data types in the same memory location. i.e. f[0] = first 32 bit of __m128 v
union __m128_union {
	std::array<float,4>		f; // size 4x32 Bit = 128 Bit
	__m128					v; // size 128 Bit packed as 4 32 Bit floats
	__m128i					vi; // size 128 Bit packed as 2 64 Bit longs
}; // complete size of union: 128 Bit 

Tetra TetrasFromTetraMesh::convert(const TetraMesh& M,TetraMesh::TetraDescriptor t)
{
	Tetra T;

	T.faceFlags = 0;
	//Get all faces F of the tetra which is described by the TetraDescriptor t from the tetra mesh M
	const auto F = get(directed_faces,M,t);

	__m128_union mtx[4];
	// Loop through all 4 faces of the tetra
	for(unsigned i=0;i<4;++i)
	{
		T.IDfds[i] 		= get(id,M,F[i]);
		T.adjTetras[i] 	= get(id,
				M,
				get(tetra_below_face,M,F[i]));

		array<double,3> n;
		float c;

		// This calculates the normal and the constant c for the given tetra face
		tie(n,c) = get(face_plane,M,F[i]);

		// save the matrix transposed so all x/y/z/c values live in a single __m128
		for(unsigned j=0;j<3;++j)
			mtx[j].f[i] = n[j];
		mtx[3].f[i] = c;
	}
	// This actually is undefined behavior for the union, since mtx[i].f[j] is the active member
	T.nx = mtx[0].v;
	T.ny = mtx[1].v;
	T.nz = mtx[2].v;
	T.C  = mtx[3].v;

	T.matID = get(region,M,t);

	return T;
}
#else
Tetra TetrasFromTetraMesh::convert(const TetraMesh& M,TetraMesh::TetraDescriptor t)
{
	Tetra T;

	T.faceFlags = 0;
	//Get all faces F of the tetra which is described by the TetraDescriptor t from the tetra mesh M
	const auto F = get(directed_faces,M,t);

	m128 mtx[4];
	// Loop through all 4 faces of the tetra
	for(unsigned i=0;i<4;++i)
	{
		T.IDfds[i] 		= get(id,M,F[i]);
		T.adjTetras[i] 	= get(id,
				M,
				get(tetra_below_face,M,F[i]));

		array<double,3> n;
		float c;

		// This calculates the normal and the constant c for the given tetra face (quite non descriptive get method...)
		tie(n,c) = get(face_plane,M,F[i]);

		// save the matrix transposed so all x/y/z/c values live in a single __m128
		for(unsigned j=0;j<3;++j)
			mtx[j][i] = n[j];
		mtx[3][i] = c;
	}
	// This actually is undefined behavior for the union, since mtx[i].f[j] is the active member
	T.nx = mtx[0];
	T.ny = mtx[1];
	T.nz = mtx[2];
	T.C  = mtx[3];

	T.matID = get(region,M,t);

	return T;
}
#endif
void TetrasFromTetraMesh::makeKernelTetras()
{
	m_tetras.resize(get(num_tetras,*m_mesh));

	boost::transform(
		m_mesh->tetras(),
		m_tetras.begin(),
		std::bind(convert,*m_mesh,std::placeholders::_1));
}

const std::vector<Tetra>& TetrasFromTetraMesh::tetras() const
{
	return m_tetras;
}
