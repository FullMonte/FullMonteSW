#ifndef SSE_INCLUDED
#define SSE_INCLUDED

#include <FullMonteSW/Config.h>

//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE

#include <mmintrin.h>
#include <emmintrin.h>
#include <smmintrin.h>
#include "avx_mathfun.h"

#else

#include <functional>

#include <algorithm>
#include <FullMonteSW/Geometry/NonSSE.hpp>
#include <FullMonteSW/Geometry/Vector.hpp>
#include <FullMonteSW/Geometry/UnitVector.hpp>
#include "Emitters/SpinMatrix.hpp"
//#include <math.h>

#endif
#include <math.h>
#include <utility>
#include <fstream>
#include <iostream>
#include <cassert>

#ifdef USE_SSE

/**
 * @brief check if any value in __m128 is NAN and returns true if a NAN is present
 * 
 * @param x vector of float values
 * @return true if one value of x is NAN
 * @return false if no values of x are NAN
 */
inline bool sse_ps_isnan(__m128 x)
{
    // _mm_movemask_ps - returns an int
    // Set each bit of mask dst based on the most significant bit of the
    // corresponding packed single-precision (32-bit) floating-point element in a.
    // Q: How does the compiler handle x != x for __m128 types?
	//return _mm_movemask_ps(x != x);
    //#include <xmmintrin.h> // For SSE intrinsics

    // Use _mm_cmpunord_ps to create a mask where NaNs are marked
    __m128 nan_mask = _mm_cmpunord_ps(x, x);
                
    // Extract the mask as an integer and check if any bit is set
    return _mm_movemask_ps(nan_mask) != 0;
}
#else
inline bool sse_ps_isnan(m128 x)
{
    for (unsigned i=0; i<4; i++)
    {
        if (std::isnan(x[i]))
            return true;
    }
	return false;
}
#endif

/**
 * @brief calculate photon reflection 
 * d' = d + 2*(d dot n)*n = d + 2*costheta*n
 * 
 * @param d direction of photon
 * @param n normal vector of face which reflects the photon
 * @param sincos dot prodcut of d and n (d dot n)
 * @return __m128 reflection direction of photon
 */
//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE

inline __m128 reflect(__m128 d,__m128 n,__m128 sincos)
{
    // reflect d' = d + 2*(d dot n)*n = d + 2*costheta*n
    /**
     * @brief Explanation of
     * _mm_shuffle_ps(sincos,sincos,_MM_SHUFFLE(1,1,1,1))
     * ndot[0] = sincos[1] = costheta
     * ndot[1] = sincos[1]
     * ndot[2] = sincos[1]
     * ndot[3] = sincos[1]
     * 
     */
    __m128 ndot = _mm_shuffle_ps(sincos,sincos,_MM_SHUFFLE(1,1,1,1));   

    /**
     * @brief Add packed single-precision (32-bit) floating-point 
     * elements in ndot and ndot, and store the results in ndot.
     * 2*costheta
     *    |
     *    V
     * ndot[i] = ndot[i] + ndot[i]
     * 
     */
    ndot = _mm_add_ps(ndot,ndot);
    
    /**
     * @brief Multiply packed single-precision (32-bit) floating-point 
     * elements in ndot and n, and store the results in dst. Then add packed 
     * single-precision (32-bit) floating-point 
     * elements in d and dst, and return the results.
     * _mm_add_ps          _mm_mul_ps
     *         |            2*costheta*n
     *         |                |
     *         V                V
     *    d[i] +         (ndot[i] * n[i])
     * 
     * @return return the new direction d'
     */
    return _mm_add_ps(d,_mm_mul_ps(ndot,n));
}
#else
inline m128 reflect(m128 d,m128 n,m128 sincos)
{
    // reflect d' = d + 2*(d dot n)*n = d + 2*costheta*n
    m128 ndot = shuffle128(sincos,sincos,1,1,1,1);   
    ndot = add128(ndot,ndot);
    /*m128 ndot, result;
    for (int i=0; i<4; ++i)
    {
        ndot[i] = sincos[1];
        ndot[i] += ndot[i];
        result[i] = d[i] + (ndot[i] * n[i]);
    }*/
    return add128(d, mult128(ndot, n));
}
#endif

#ifdef USE_SSE
inline void calculateSpinmatrix(__m128 n, __m128 v, __m128* Q)
{
    __m128 one = _mm_set_ss(1.0f);
    // Spinmatrix calculation 
    const int mask = (1<<3)-1;// Mask indicating which elements are occupied
    // normalize the input vectors
    // For a general vector, we would have to normalize it first, but the normal of the 
    // tetras are already normalized, so we can leave out that step
    // __m128 n_dot = _mm_dp_ps(n,n,(mask<<4) | 0x7);
    // __m128 k = _mm_sqrt_ss(n_dot);
    // __m128 n_norm = _mm_div_ps(n,_mm_shuffle_ps(k,k,_MM_SHUFFLE(0,0,0,0)));
    // __m128 v_dot = _mm_dp_ps(v,v,(mask<<4) | 0x7);
    // __m128 j = _mm_sqrt_ss(v_dot);
    // __m128 v_norm = _mm_div_ps(v,_mm_shuffle_ps(j,j,_MM_SHUFFLE(0,0,0,0)));

    // Check if the vectors are parallel (or anti-parallel)
    // float parallelCheck = dot(v_norm, n_norm) = cos_theta;
    __m128 cos_theta = _mm_dp_ps(n,v,(mask<<4) | 0x7);
    float parallelCheck;
    _mm_store_ss(&parallelCheck, cos_theta);
    if(parallelCheck == 1.0f) {                               
        // identity matrix does no rotation
        Q[0] = _mm_setr_ps(1.0f, 0.0f, 0.0f, 0.0f);
        Q[1] = _mm_setr_ps(0.0f, 1.0f, 0.0f, 0.0f);
        Q[2] = _mm_setr_ps(0.0f, 0.0f, 1.0f, 0.0f);
    }
    else if (parallelCheck == -1.0f) {
        // identity matrix inverts direction
        Q[0] = _mm_setr_ps(-1.0f, 0.0f, 0.0f, 0.0f);
        Q[1] = _mm_setr_ps(0.0f, -1.0f, 0.0f, 0.0f);
        Q[2] = _mm_setr_ps(0.0f, 0.0f, -1.0f, 0.0f);
    }
    else
    {
        // https://socratic.org/questions/what-is-the-half-angle-identities
        // cos(theta) = dot(v_norm, n_norm) = parallelCheck                                
        // const float theta = acos(dot(n_norm, v_norm));
        __m128 div2 = _mm_set_ss(2.0f);
        __m128 cos_halftheta;
        if (parallelCheck >= 0)
        {
            // cos(theta/2) = + sqrt( (1+cos(theta)/2) ) if parallelCheck >=0 i.e. cos(theta) >= 0
            cos_halftheta = _mm_sqrt_ss(_mm_max_ss(_mm_setzero_ps(),_mm_div_ss(_mm_add_ss(one,cos_theta),div2)));
        }
        else
        {
            // cos(theta/2) = - sqrt( (1+cos(theta)/2) ) if parallelCheck < 0 i.e. cos(theta) < 0
            cos_halftheta = _mm_sub_ss(_mm_setzero_ps(),_mm_sqrt_ss(_mm_max_ss(_mm_setzero_ps(),_mm_div_ss(_mm_add_ss(one,cos_theta),div2))));
        }
        // sin(theta/2) = sqrt( (1-cos^2(theta/2)) )
        __m128 sin_halftheta = _mm_sqrt_ss(_mm_max_ss(_mm_setzero_ps(),_mm_sub_ss(one,_mm_mul_ss(cos_halftheta,cos_halftheta))));

        __m128 sincos_halftheta = _mm_shuffle_ps(sin_halftheta,cos_halftheta,_MM_SHUFFLE(0,0,0,0));
        sincos_halftheta = _mm_shuffle_ps(sincos_halftheta,sincos_halftheta,_MM_SHUFFLE(0,0,0,2));
        
        // calculate cross product of n and v -> normal x z_axis
        // b = cross(n,v)
        __m128 bca = _mm_shuffle_ps(n,n,_MM_SHUFFLE(3,1,0,2));
        __m128 fde = _mm_shuffle_ps(v,v,_MM_SHUFFLE(3,0,2,1));

        __m128 bf_cd_ae = _mm_mul_ps(bca,fde);

        __m128 cab = _mm_shuffle_ps(n,n,_MM_SHUFFLE(3,0,2,1));
        __m128 efd = _mm_shuffle_ps(v,v,_MM_SHUFFLE(3,1,0,2));

        __m128 ec_af_bd = _mm_mul_ps(efd,cab);

        __m128 b =_mm_sub_ps(bf_cd_ae,ec_af_bd);
        
        // determine components of Spinmatrix 
        __m128 q = _mm_shuffle_ps(b,one,_MM_SHUFFLE(0,0,0,0));
        q = _mm_shuffle_ps(q,b,_MM_SHUFFLE(2,1,1,2));

        // q0 = q[0] = cos(theta/2.0f) * 1;
        // q1 = q[1] = sin(theta/2.0f) * b[0];
        // q2 = q[2] = sin(theta/2.0f) * b[1];
        // q3 = q[3] = sin(theta/2.0f) * b[2];
        q = _mm_mul_ps(q,sincos_halftheta);

        // Need to calculate all components of the Spinmatrix
        //Q[0] = {q0*q0 + q1*q1 - q2*q2 - q3*q3,  2.0f*(q1*q2 - q0*q3),           2.0f*(q1*q3 + q0*q2)};
        //Q[1] = {2.0f*(q2*q1 + q0*q3),           q0*q0 - q1*q1 + q2*q2 - q3*q3,  2.0f*(q2*q3 - q0*q1)};
        //Q[2] = {2.0f*(q3*q1 - q0*q2),           2.0f*(q3*q2 + q0*q1),           q0*q0 - q1*q1 - q2*q2 + q3*q3};

        // 1. Calculate the diagonal components
        __m128 q2 = _mm_mul_ps(q,q);
        
        // Perform dot product between q components and the sign values to get the correct diagonal value
        // Need new mask because we are now using all 4 entries of the __m128 type instead of only 3
        const int mask_Q = (1<<4)-1;// Mask indicating which elements are occupied
        // q00 = q0*q0 + q1*q1 - q2*q2 - q3*q3 -> only first value in __m128 is of interest
        __m128 sign00 = _mm_setr_ps(1.0f, 1.0f, -1.0f, -1.0f);
        __m128 q00 = _mm_dp_ps(q2,sign00,(mask_Q<<4) | 0x7);
        // q11 = q0*q0 - q1*q1 + q2*q2 - q3*q3 -> only first value in __m128 is of interest
        __m128 sign11 = _mm_setr_ps(1.0f, -1.0f, 1.0f, -1.0f);
        __m128 q11 = _mm_dp_ps(q2,sign11,(mask_Q<<4) | 0x7);
        // q22 = q0*q0 - q1*q1 - q2*q2 + q3*q3 -> only first value in __m128 is of interest
        __m128 sign22 = _mm_setr_ps(1.0f, -1.0f, -1.0f, 1.0f);
        __m128 q22 = _mm_dp_ps(q2,sign22,(mask_Q<<4) | 0x7);

        // rearrange the q components to get all 6 multiplication combinations  for the spinmatrix
        // Q_values0[0] = q0*q3
        // Q_values0[1] = q1*q2
        // Q_values0[2] = q3*q1
        // Q_values0[3] = q2*q0
        __m128 Q_values0 = _mm_mul_ps(_mm_shuffle_ps(q,q,_MM_SHUFFLE(2,3,1,0)), _mm_shuffle_ps(q,q,_MM_SHUFFLE(0,1,2,3)));
        // Q_values1[0] = q2*q3
        // Q_values1[1] = q0*q1
        // Q_values1[2] = don't care
        // Q_values1[3] = don't care
        __m128 Q_values1 = _mm_mul_ps(_mm_shuffle_ps(q,q,_MM_SHUFFLE(0,0,0,2)), _mm_shuffle_ps(q,q,_MM_SHUFFLE(0,0,1,3)));

        // all q componets have to be multiplied by 2
        __m128 mul2 = _mm_setr_ps(2.0f, 2.0f, 2.0f, 0.0f);
        // There are 3 subtraction components in the spinmatrix q01, q12 and q20 -> need to arrange the q values accordingly
        // Q_values_neg[0] = 2 * Q_values0[0] = 2 * q0*q3
        // Q_values_neg[1] = 2 * Q_values0[3] = 2 * q2*q0
        // Q_values_neg[2] = 2 * Q_values1[1] = 2 * q0*q1
        // Q_values_neg[3] = don't care
        __m128 Q_values_neg = _mm_mul_ps(mul2, _mm_shuffle_ps(Q_values0,Q_values1,_MM_SHUFFLE(1,1,3,0)));
        // There are 3 addition components in the spinmatrix q02, q10 and q21 -> need to arrange the q values accordingly
        // Q_values_pos[0] = 2 * Q_values0[1] = 2 * q1*q2
        // Q_values_pos[1] = 2 * Q_values0[2] = 2 * q3*q1
        // Q_values_pos[2] = 2 * Q_values1[0] = 2 * q2*q3
        // Q_values_pos[3] = don't care
        __m128 Q_values_pos = _mm_mul_ps(mul2, _mm_shuffle_ps(Q_values0,Q_values1,_MM_SHUFFLE(1,0,2,1)));
        
        // Q_sub[0] = 2*(q1*q2 - q0*q3)
        // Q_sub[1] = 2*(q3*q1 - q2*q0)
        // Q_sub[2] = 2*(q2*q3 - q0*q1)
        // Q_sub[3] = don't care
        __m128 Q_sub = _mm_sub_ps(Q_values_pos,Q_values_neg);
        // Q_add[0] = 2*(q1*q2 + q0*q3)
        // Q_add[1] = 2*(q3*q1 + q2*q0)
        // Q_add[2] = 2*(q2*q3 + q0*q1)
        // Q_add[3] = don't care
        __m128 Q_add = _mm_add_ps(Q_values_pos,Q_values_neg);

        // set the rotation matrix
        //Q[0] = {q0*q0 + q1*q1 - q2*q2 - q3*q3,  2.0f*(q1*q2 - q0*q3),           2.0f*(q1*q3 + q0*q2)};
        Q[0] = _mm_shuffle_ps(q00,Q_sub,_MM_SHUFFLE(3,0,0,0));
        Q[0] = _mm_shuffle_ps(Q[0],Q_add,_MM_SHUFFLE(3,1,2,0));

        //Q[1] = {2.0f*(q2*q1 + q0*q3),           q0*q0 - q1*q1 + q2*q2 - q3*q3,  2.0f*(q2*q3 - q0*q1)};
        Q[1] = _mm_shuffle_ps(q11,Q_add,_MM_SHUFFLE(3,0,0,0));
        Q[1] = _mm_shuffle_ps(Q[1],Q_sub,_MM_SHUFFLE(3,2,0,2));

        //Q[2] = {2.0f*(q3*q1 - q0*q2),           2.0f*(q3*q2 + q0*q1),           q0*q0 - q1*q1 - q2*q2 + q3*q3};
        Q[2] = _mm_shuffle_ps(Q_add,Q_sub,_MM_SHUFFLE(3,1,2,2));
        Q[2] = _mm_shuffle_ps(Q[2],q22,_MM_SHUFFLE(3,0,0,2));
    }
}

inline __m128 lambertian_reflect(__m128 n, __m256 rng0, __m128 rng1)
{
    // phi = rng0*2*PI
    __m256 phi = _mm256_mul_ps(rng0,_mm256_set1_ps(2.0f*M_PI));
    __m256 sin_phi_256;
    __m256 cos_phi_256;
    std::tie(sin_phi_256,cos_phi_256) = sincos_psp(phi);
    __m128 sin_phi = _mm256_castps256_ps128(sin_phi_256);
    __m128 cos_phi = _mm256_castps256_ps128(cos_phi_256);
    __m128 one = _mm_set_ss(1.0f);

    cos_phi = _mm_shuffle_ps(cos_phi,one,_MM_SHUFFLE(0,0,0,0));

    __m128 sin_theta = rng1;
    // cos(theta) = sqrt(1 - sin(theta)^2)
    __m128 cos_theta = _mm_sqrt_ss(_mm_max_ss(_mm_setzero_ps(),_mm_sub_ss(one,_mm_mul_ss(sin_theta,sin_theta))));

    // sincos_phi[0] = sin_phi[0]
    // sincos_phi[1] = sin_phi[0]
    // sincos_phi[2] = cos_phi[0]
    // sincos_phi[3] = cos_phi[2] = 1
    __m128 sincos_phi = _mm_shuffle_ps(sin_phi,cos_phi,_MM_SHUFFLE(2,0,0,0));
    // sincos_phi[0] = sincos_phi[0] = sin_phi
    // sincos_phi[1] = sincos_phi[2] = cos_phi
    // sincos_phi[2] = sincos_phi[3] = 1
    // sincos_phi[3] = sincos_phi[3] = 1
    sincos_phi = _mm_shuffle_ps(sincos_phi,sincos_phi,_MM_SHUFFLE(3,3,2,0));

    // sincos_theta[0] = sin_theta[0]
    // sincos_theta[1] = sin_theta[0]
    // sincos_theta[2] = abs_cos_theta[0]
    // sincos_theta[3] = abs_cos_theta[1] = 0
    __m128 sincos_theta = _mm_shuffle_ps(sin_theta,cos_theta,_MM_SHUFFLE(1,0,0,0));

    // p_ijk[0] = sincos_phi[0] * sincos_theta[0] = sin_phi * sin_theta
    // p_ijk[1] = sincos_phi[1] * sincos_theta[1] = cos_phi * sin_theta
    // p_ijk[2] = sincos_phi[2] * sincos_theta[2] = 1 * abs_cos_theta
    // p_ijk[3] = sincos_phi[3] * sincos_theta[3] = 1 * 0 = 0
    __m128 p_ijk = _mm_mul_ps(sincos_phi, sincos_theta);

    // Spinmatrix calculation
    // rotate around the z_axis -> This will also always be a normalized unit vector
    __m128 z = _mm_setr_ps(0.0f, 0.0f, 1.0f, 0.0f);
    __m128 Q[3];
    calculateSpinmatrix(n, z, Q);
    
    const int mask = (1<<3)-1; // Mask indicating which elements are occupied  
    // rotate to orient around the tetra face normal using the rotation matrix
    // dot-product between p_ijk and every line of Q
    __m128 p_uvw0 = _mm_dp_ps(p_ijk,Q[0],(mask<<4) | 0x7);
    __m128 p_uvw1 = _mm_dp_ps(p_ijk,Q[1],(mask<<4) | 0x7);
    __m128 p_uvw2 = _mm_dp_ps(p_ijk,Q[2],(mask<<4) | 0x7);

    __m128 p_uvw = _mm_shuffle_ps(p_uvw0,p_uvw1,_MM_SHUFFLE(0,0,0,0));
    p_uvw = _mm_shuffle_ps(p_uvw,p_uvw2,_MM_SHUFFLE(0,0,2,0));
    // normalize the direction vector to get newdir
    __m128 norm_p = _mm_dp_ps(p_uvw,p_uvw,(mask<<4) | 0x7);
    __m128 k = _mm_sqrt_ss(norm_p);

    __m128 newdir = _mm_div_ps(p_uvw,_mm_shuffle_ps(k,k,_MM_SHUFFLE(0,0,0,0)));

    return newdir;
}
#else
inline m128 lambertian_reflect(m128 n, float rng0, float rng1)
{
    float phi = 2.0f*M_PI*rng0;
    // compute the theta angle (angle of elevation towards z-axis) for lambertian emission pattern
    float theta = asin(rng1);

    float p_ijk0 = sin(theta) * sin(phi);
    float p_ijk1 = sin(theta) * cos(phi);
    float p_ijk2 = std::abs(cos(theta));  // abs() makes it a hemisphere, rather than a sphere
                                
    // generate unit vector on hemi-sphere oriented about the z-axis
    std::array<float,3> p_ijk { p_ijk0, p_ijk1, p_ijk2 };
    std::array<float,3> normal = {n[0], n[1], n[2]};
    std::array<std::array<float,3>,3> Q = SpinMatrix::k(normal);
    // rotate to orient around the tetra face normal using the rotation matrix
    std::array<float,3> p_uvw {
        p_ijk[0]*Q[0][0] + p_ijk[1]*Q[0][1] + p_ijk[2]*Q[0][2],
        p_ijk[0]*Q[1][0] + p_ijk[1]*Q[1][1] + p_ijk[2]*Q[1][2],
        p_ijk[0]*Q[2][0] + p_ijk[1]*Q[2][1] + p_ijk[2]*Q[2][2]
    };

    // create a PacketDirection from the vector we just created
    Vector<3,float> d = normalize(Vector<3,float>(p_uvw));
    m128 newdir = {d[0], d[1], d[2], 0.0f};
    return newdir;
}
#endif

/**
 * @brief Determines the minimum value of vector v and also returns its
 * index
 * 
 * @param v Vector with 4 float values
 * @return std::pair<unsigned,__m128> 
 */
//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
inline std::pair<unsigned,__m128> getMinIndex4p(__m128 v)
{
	int mask;
    /**
     * @brief Explanation of
     * _mm_shuffle_ps(v,v,_MM_SHUFFLE(2,3,0,1))
     * Output:
     * v[1]
     * v[0]
     * v[3]
     * v[2]
     * 
     * Explanation of complete command:
     * halfmin[0] = min(v[0], v[1])   --> halfmin[0]=halfmin[1]
     * halfmin[1] = min(v[1], v[0])
     * halfmin[2] = min(v[2], v[3])   --> halfmin[2]=halfmin[3]
     * halfmin[3] = min(v[3], v[2])
     * 
     * Get the minimum value of the first and last two elements
     * 
     */
    __m128 halfmin = _mm_min_ps(v,_mm_shuffle_ps(v,v,_MM_SHUFFLE(2,3,0,1)));

    /**
     * @brief Explanation of
     * _mm_shuffle_ps(v,v,_MM_SHUFFLE(0,0,2,2))
     * Output:
     * halfmin[2]
     * halfmin[2]
     * halfmin[0]
     * halfmin[0]
     * 
     * Explanation of complete command:
     * allmin[0] = min(halfmin[0], halfmin[2])
     * allmin[1] = min(halfmin[1], halfmin[2])
     * allmin[2] = min(halfmin[2], halfmin[0])
     * allmin[3] = min(halfmin[3], halfmin[0])
     * 
     * Since halfmin[0]=halfmin[1] halfmin[2]=halfmin[3], we get the minimum value 
     * the input vector v
     * 
     */
    __m128 allmin  = _mm_min_ps(halfmin,_mm_shuffle_ps(halfmin,halfmin,_MM_SHUFFLE(0,0,2,2)));

    /**
     * @brief Compares v element wise with allmin.
     * If v[i]==allmin[i] set eqmask[i] to all 1s (0xffffffff)
     * else set eqmask[i] to all 0s
     * 
     */
    __m128 eqmask =  _mm_cmpeq_ps(v,allmin);

    /**
	 * @brief Creates a 4-bit mask from the most significant bits of the four SP FP values in eqmask
     * Example:
     * if v[0] is the minimum value --> mask = ...0001 => 1 (int)
     * if v[1] is the minimum value --> mask = ...0010 => 2 (int)
     * if v[2] is the minimum value --> mask = ...0100 => 4 (int)
     * if v[3] is the minimum value --> mask = ...1000 => 8 (int)
	 * 
	 */
    mask = _mm_movemask_ps(eqmask);

    /**
     * @brief if mask == 0, then no minimum could be found and this function returns 4 which 
     * is interpreted as an error.
     * else returns the number of trailing 0-bits in mask (in this case 0,1,2,3), starting 
     * at the least significant bit position. If mask is 0, the result is undefined.
     * ctz = count trailing zeros
     * 
     * @return returns a pair consisting of (index of min value of v, vector with 4 times the min value)
     */
    return std::make_pair(mask == 0 ? 4 : __builtin_ctz(mask),allmin);
}
#else
inline std::pair<unsigned,m128> getMinIndex4p(m128 v)
{
    m128 halfmin, allmin;
    unsigned min_upper_lower_half[2], min_index;
    
    for (int i=0; i < 2; i++)
    {
        if (v[2*i] < v[2*i+1])
        {
            halfmin[2*i] = v[2*i];
            halfmin[2*i+1] = v[2*i];
            min_upper_lower_half[i] = 2*i;
        }
        else
        {
            halfmin[2*i] = v[2*i+1];
            halfmin[2*i+1] = v[2*i+1];
            min_upper_lower_half[i] = 2*i+1;
        }
    }
    
    if (halfmin[0] < halfmin[3])
    {
        min_index = min_upper_lower_half[0];
        for(int i=0; i<4; i++)
        {
            allmin[i] = halfmin[0];
        }
    }
    else
    {
        min_index = min_upper_lower_half[1];
        for(int i=0; i<4; i++)
        {
            allmin[i] = halfmin[3];
        }
    }
    
    return std::make_pair(min_index, allmin);
}
#endif


/**
 * @brief Calculates the Fresnel reflection probability R
 * If a photon encounters a boundary and that boundary is an interface (a change in refractive index
 * from \f$n_i\f$ to \f$n_t\f$), then it may either refelct or have its angle to the normal refract 
 * from incidence angle \f$\theta_i\f$ to transmitted angle \f$\theta_t\f$
 * 
 * \f[ R = \frac{R_s+R_p}{2} 
 * = \frac{1}{2}\left[\left|\frac{n_i\cos\theta_i-n_t\cos\theta_t}{n_i\cos\theta_i+n_t\cos\theta_t}\right|^2
 * + \left|\frac{n_i\cos\theta_t-n_t\cos\theta_i}{n_i\cos\theta_t+n_t\cos\theta_i}\right|^2\right] \f]
 * 
 * @param n1n2 Represents two refractive indices \f$n_i\f$ and \f$n_t\f$, when a photon encounters 
 * a boundary which is an interface. In this case the refractive index changes from \f$n_1\f$ 
 * to \f$n_2\f$
 * n1n2[0] = n1
 * n1n2[1] = n2
 * n1n2[2] = don't care
 * n1n2[3] = don't care
 * 
 * @param sincos sin and cos calculations of either \f$\theta_i\f$ or \f$\theta_t\f$ respectively
 * sincos[0] = sin(theta_i)
 * sincos[1] = cos(theta_i)
 * sincos[2] = sin(theta_t)
 * sincos[3] = cos(theta_t)
 * @return __m128 The first element [0] is the Fresnel reflection probability R
 */
//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
inline __m128 FresnelSSE(__m128 n1n2,__m128 sincos)
{
    // Rs = (n1*costheta_i - n2*costheta_t) / (n1*costheta_i + n2*costheta_t)
    // Rp = (n1*costheta_t - n2*costheta_i) / (n1*costheta_t + n2*costheta_i)

    // n = [n1 n2 n1 n2]
    /**
     * @brief Move the lower 2 single-precision (32-bit) floating-point elements 
     * from n1n2 (2nd argument) to the upper 2 elements of n, and copy the lower 2 
     * elements from n1n2 (1st argument) to the lower 2 elements of n.
     * 
     * n[0] = n1n2[0] (n1) n_i
     * n[1] = n1n2[1] (n2) n_t
     * n[2] = n1n2[0] (n1) n_i
     * n[3] = n1n2[1] (n2) n_t
     * 
     */
    __m128 n = _mm_movelh_ps(n1n2,n1n2);    
    /**
     * @brief 
     * 
     * terms[0] = sincos[1] * n[0] --> (r0=costheta_i*n1)
     * terms[1] = sincos[3] * n[1] --> (r1=costheta_t*n2)
     * terms[2] = sincos[3] * n[2] --> (r2=costheta_t*n1)
     * terms[3] = sincos[1] * n[3] --> (r3=costheta_i*n2)
     */
    __m128 trig = _mm_shuffle_ps(sincos,sincos,_MM_SHUFFLE(1,3,3,1));
    __m128 terms = _mm_mul_ps(trig,n);
    
    /**
     * @brief Alternatively add and subtract packed single-precision (32-bit) 
     * floating-point elements in terms to/from packed elements in shuffle(terms), 
     * and store the results in numden.
     * 
     * numden[0] = terms[0] - terms[1] --> (n1*cos_i - n2*cos_t)
     * numden[1] = terms[1] + terms[0] --> (n2*cos_t + n1*cos_i)
     * numden[2] = terms[2] - terms[3] --> (n1*cos_t - n2*cos_i)
     * numden[3] = terms[3] + terms[2] --> (n2*cos_i + n1*cos_t)
     */
    __m128 numden = _mm_addsub_ps(terms,_mm_shuffle_ps(terms,terms,_MM_SHUFFLE(2,3,0,1)));

    /**
     * rs_rp[0] = numden[0] / numden[1] <-- rs
     * rs_rp[1] = numden[2] / numden[3] <-- rp
     * rs_rp[2] = numden[0] / numden[0] <-- can be ignored
     * rs_rp[3] = numden[0] / numden[0] <-- can be ignored
     * 
     */
    __m128 rs_rp = _mm_div_ps(_mm_shuffle_ps(numden,numden,_MM_SHUFFLE(0,0,2,0)),_mm_shuffle_ps(numden,numden,_MM_SHUFFLE(0,0,3,1)));

    // calculate rs^2, rp^2
    __m128 rs2_rp2 = _mm_mul_ps(rs_rp,rs_rp);
 
    /**
     * @brief calculate (rs^2+rp^2) / 2
     * 
     *
     * _mm_hadd_ps
     * Horizontally add adjacent pairs of single-precision (32-bit) floating-point 
     * elements in a and b, and pack the results in dst.
     * tmp[0] = a[1] + a[0]
     * tmp[1] = a[3] + a[2]
     * tmp[2] = b[1] + b[0]
     * tmp[3] = b[3] + b[2]
     * 
     * _mm_mul_ss
     * Multiply the lower single-precision (32-bit) floating-point element in a (tmp) and b (0.5), 
     * store the result in the lower element of dst, and copy the upper 3 packed elements 
     * from a to the upper elements of dst.
     * 
     * result[0] = tmp[0] * b[0]
     * result[1] = tmp[1]
     * result[2] = tmp[2]
     * result[3] = tmp[3]
     */
    return _mm_mul_ss(_mm_hadd_ps(rs2_rp2,rs2_rp2),_mm_set1_ps(0.5));
}
#else
inline m128 FresnelSSE(m128 n1n2,m128 sincos)
{
    m128 n;
    n[0] = n1n2[0];
    n[1] = n1n2[1];
    n[2] = n1n2[0];
    n[3] = n1n2[1];
    
    m128 trig, terms, numden, rs_rp, rs2_rp2, result;
    
    trig[0] = sincos[1];
    trig[1] = sincos[3];
    trig[2] = sincos[3];
    trig[3] = sincos[1];
    for(int i=0; i<4; ++i)
    {
        terms[i] = trig[i] * n[i];
    }

    numden[0] = terms[0] - terms[1];
    numden[1] = terms[1] + terms[0];
    numden[2] = terms[2] - terms[3];
    numden[3] = terms[3] + terms[2];

    rs_rp[0] = numden[0] / numden[1];
    rs_rp[1] = numden[2] / numden[3];
    rs_rp[2] = 1;
    rs_rp[3] = 1;

    rs2_rp2[0] = rs_rp[0] * rs_rp[0];
    rs2_rp2[1] = rs_rp[1] * rs_rp[1];
    rs2_rp2[2] = 1;
    rs2_rp2[3] = 1;
    
    result[0] = (rs2_rp2[0] + rs2_rp2[1]) * 0.5;
    result[1] = 2;
    result[2] = (rs2_rp2[0] + rs2_rp2[1]);
    result[3] = 2;

    return result;
}
#endif 



/**
 * @brief Calculates the sine and cosine of theta_i and theta_t
 * 
 * @param n1_n2_ratio The ratio between the refractive indices of two boudaries which create
 * an interface
 * @param cosi cos(theta_i)
 * @return __m128 returns r0=sin(theta_i) r1=cos(theta_i) r2=sin(theta_t) r3=cos(theta_t)
 */
//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
inline __m128 RefractSSE(__m128 n1_n2_ratio,__m128 cosi)
{
    /**
     * @brief Copy single-precision (32-bit) floating-point element a 
     * to the lower element of one, and zero the upper 3 elements.
     * one[0] = 1.0
     * one[1] = 0
     * one[2] = 0
     * one[3] = 0
     * 
     */
    __m128 one = _mm_set_ss(1.0);
    
    /**
     * _mm_sqrt_ss
     * Compute the square root of the lower single-precision (32-bit) floating-point element in a,
     * store the result in the lower element of sini, and copy the upper 3 packed elements from a 
     * to the upper elements of sini.
     * sini[0] = sqrt(one[0] - cosi[0] * cosi[0])
     * sini[1] = 0
     * sini[2] = 0
     * sini[3] = 0
     * 
     * _mm_max_ss
     * Compare the lower single-precision (32-bit) floating-point elements in a and b, store the 
     * maximum value in the lower element of dst, and copy the upper 3 packed elements from a to 
     * the upper element of dst.
     * = max(0, one[0] - cosi[0] * cosi[0])
     * = 0
     * = 0
     * = 0
     * 
     * _mm_sub_ss
     * Subtract the lower single-precision (32-bit) floating-point element in b from the lower 
     * single-precision (32-bit) floating-point element in a, store the result in the lower 
     * element of dst, and copy the upper 3 packed elements from a to the upper elements of dst.
     *  = one[0] - cosi[0] * cosi[0]
     *  = one[1]
     *  = one[2]
     *  = one[3]
     * 
     * _mm_mul_ss
     * Multiply the lower single-precision (32-bit) floating-point element in a and b, store the 
     * result in the lower element of dst, and copy the upper 3 packed elements from a to the 
     * upper elements of dst.
     * = cosi[0] * cosi[0]
     * = cosi[1]
     * = cosi[2]
     * = cosi[3]
     * 
     */
    __m128 sini = _mm_sqrt_ss(_mm_max_ss(_mm_setzero_ps(),_mm_sub_ss(one,_mm_mul_ss(cosi,cosi))));
    
    /**
     * @brief Unpack and interleave single-precision (32-bit) floating-point elements from the 
     * low half of sini and cosi, and store the results in sini_cosi.
     * sini_cosi represents  r0=sin(theta_i) r1=cos(theta_i)
     * 
     * sini_cosi[0] = sini[0]
     * sini_cosi[1] = cosi[0]
     * sini_cosi[2] = sini[1]
     * sini_cosi[3] = cosi[1]
     * 
     */
    __m128 sini_cosi = _mm_unpacklo_ps(sini,cosi);

    /**
     * _mm_movehl_ps
     * Move the upper 2 single-precision (32-bit) floating-point elements from b to the lower 2 
     * elements of dst, and copy the upper 2 elements from a to the upper 2 elements of dst. 
     * = n1_n2_ratio[2]
     * = n1_n2_ratio[3]
     * = n1_n2_ratio[2]
     * = n1_n2_ratio[3]
     * 
     * sint[0] = n1_n2_ratio[2] * sini_cosi[0]
     * sint[1] = n1_n2_ratio[3]
     * sint[2] = n1_n2_ratio[2]
     * sint[3] = n1_n2_ratio[3]
     */
    __m128 upper = _mm_movehl_ps(n1_n2_ratio,n1_n2_ratio);
	__m128 sint = _mm_mul_ss(upper, sini_cosi);
    
	assert(!sse_ps_isnan(sint));

    /**
     * cost[0] = sqrt(max(0, one[0] - sint[0] * sint[0])
     * cost[1] = one[1]
     * cost[2] = one[2]
     * cost[3] = one[3]
     */ 
    __m128 cost = _mm_sqrt_ss(_mm_max_ss(_mm_setzero_ps(),_mm_sub_ss(one,_mm_mul_ss(sint,sint))));

    assert(!sse_ps_isnan(cost));
    /**
     * return[0] = sini_cosi[0] -> (sini)
     * return[1] = sini_cosi[1] -> (cosi)
     * return[2] = sint[0]
     * return[3] = cost[0]
     */
	return _mm_movelh_ps(sini_cosi,_mm_unpacklo_ps(sint,cost));
}
#else
inline m128 RefractSSE(m128 n1_n2_ratio,m128 cosi)
{
    m128 result;
    float sini, sint, cost;
    
    sini = sqrtf(std::max(0.0, 1.0-(cosi[0]*cosi[0])));
    
    sint = n1_n2_ratio[2] * sini;
    assert(!std::isnan(sint));
    
    cost = sqrtf(std::max(0.0, 1.0-(sint*sint)));
    assert(!std::isnan(cost));
    
    result[0] = sini;
    result[1] = cosi[0];
    result[2] = sint;
    result[3] = cost;

    return result;
}
#endif




#endif //SSE_INCLUDED


