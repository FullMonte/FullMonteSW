/*
 * MCMLKernel.cpp
 *
 *  Created on: Jul 12, 2017
 *      Author: jcassidy
 */

#include "MCMLKernel.hpp"
#include <FullMonteSW/Storage/MCML/MCMLCase.hpp>

float surfaceTransmission(const Kernel* K)
{
	return 1.0f-specularReflectance(K);
}

float specularReflectance(const Kernel* K)
{
	if (!K)
		throw std::logic_error("specularReflectance(K) missing kernel");
	if (!K->materials())
		throw std::logic_error("Missing materials");
	if (!(K->materials()->exterior()))
		throw std::logic_error("Missing material 0");
	if (!(K->materials()->get(1)))
		throw std::logic_error("Missing material 1");
	if (!(K->geometry()))
		throw std::logic_error("Missing geometry");

	float n0 = K->materials()->exterior()->refractiveIndex();
	float n1 = K->materials()->get(1)->refractiveIndex();

	if (!dynamic_cast<const Layered*>(K->geometry()))
		throw std::logic_error("specularReflectance(K) called but geometry is not convertible to layered");

	return specularReflectance(n0,n1);
}

float specularReflectance(float n0,float n1)
{
	float Rf = (n0-n1)/(n0+n1);
		Rf *= Rf;
	return Rf;
}

float specularReflectance(const MCMLCase* C)
{
	if (!C)
		throw std::logic_error("specularReflectance(MCMLCase* C) missing case");
	if (!C->materials())
		throw std::logic_error("Missing materials");
	if (!(C->materials()->exterior()))
		throw std::logic_error("Missing material 0");
	if (!(C->materials()->get(1)))
		throw std::logic_error("Missing material 1");

	float n0 = C->materials()->exterior()->refractiveIndex();
	float n1 = C->materials()->get(1)->refractiveIndex();

	float Rf = (n0-n1)/(n0+n1);
	Rf *= Rf;

	return Rf;
}
