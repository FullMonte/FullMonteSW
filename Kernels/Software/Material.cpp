/*
 * TetraKernelBase.cpp
 *
 *  Created on: Feb 17, 2016
 *      Author: jcassidy
 */
#include <FullMonteSW/Config.h>

#include <iomanip>
#include <math.h>
#include "Material.hpp"

using namespace std;

namespace x86Kernel
{

Material::Material()
{}

Material::Material(float muA_,float muS_,float n_)
{
	muT = muA_+muS_;
	n = n_;
//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
	// propagation vector along unit physical step: ignore first entry (0.0f), time elapsed increases, convert distance to dimensionless step, and physical step decreases by 1)
	m_prop = _mm_set_ps(0.0f,	n/c0,	-muT,	-1.0f);

	// initial propagation vector (ignore first entry, 0 time, 1 dimensionless step remaining, 1/muT physical step remaining)
	m_init = _mm_set_ps(0.0f,	0.0f,		1.0f,		1.0f/muT);
#else
	// propagation vector along unit physical step: physical step decreases by 1, convert to dimensionless step, time elapsed increases, and ignore last entry )
	m_prop = {-1.0f, -muT, n/c0, 0.0f};

	// initial propagation vector (1/muT physical step remaining, 1 dimensionless step remaining, 0 time, 0 X)
	m_init = {1.0f/muT, 1.0f,	0.0f,	0.0f};
#endif
	absfrac = muA_/muT;
	if(isnan(absfrac))
		absfrac = 0.0f;
	scatters = muS_ != 0.0f;
}

};
