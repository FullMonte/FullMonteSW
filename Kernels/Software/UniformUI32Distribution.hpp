/*
 * UniformUI32Distribution.hpp
 *
 *  Created on: Feb 17, 2016
 *      Author: jcassidy
 */

#ifndef KERNELS_SOFTWARE_UNIFORMUI32DISTRIBUTION_HPP_
#define KERNELS_SOFTWARE_UNIFORMUI32DISTRIBUTION_HPP_

#include "FullMonteSW/Config.h"

//#include <iostream>
//#include <fstream>

/**
 * @brief This RNG code is used for choosing which source  to take next in the Composite class
 * Returns 8 uint32_t's
 */

class UniformUI32Distribution
{
public:
	typedef uint32_t				result_type;
	static constexpr std::size_t	OutputsPerInputBlock=8;
	static constexpr std::size_t	OutputElementSize=1;

	typedef uint32_t				input_type;
	static constexpr std::size_t	InputBlockSize=8;

	template<class RNG>void calculate(RNG& rng,uint32_t* dst)
	{
		const uint32_t* ip = rng.getBlock();
//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
		const __m256i* i = (const __m256i*) ip;
		_mm256_store_si256((__m256i*)dst,_mm256_load_si256(i));
		
		//std::ofstream outfile;
    	//outfile.open("UI32SSE.txt", std::ios_base::app);
		//outfile << "Result dst: "<< dst[0]<< " " << dst[1] << " "<< dst[2] <<" "<< dst[3]<< " "<< dst[4]<< " " << dst[5] << " "<< dst[6] <<" "<< dst[7]<<std::endl;
#else
		std::array<long long, 4> i;
		std::copy((long long*) ip, (long long*) ip+4, i.data());
		std::copy((uint32_t *) i.begin(), (uint32_t *) i.end(), dst);

		//std::ofstream outfile;
    	//outfile.open("UI32NonSSE.txt", std::ios_base::app);
		//outfile << "Result dst: "<< dst[0]<< " " << dst[1] << " "<< dst[2] <<" "<< dst[3]<< " "<< dst[4]<< " " << dst[5] << " "<< dst[6] <<" "<< dst[7]<<std::endl;
#endif
	}
};

#endif /* KERNELS_SOFTWARE_UNIFORMUI32DISTRIBUTION_HPP_ */
