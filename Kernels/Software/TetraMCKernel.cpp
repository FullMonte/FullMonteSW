/*
 * TetraMCKernel.cpp
 *
 *  Created on: Jun 9, 2016
 *      Author: jcassidy
 */

#include "Emitters/TetraMeshEmitterFactory.hpp"
#include "RNG_SFMT_AVX.hpp"
#include "TetraMCKernel.hpp"

#include <iostream>
using namespace std;

void TetraKernel::allocateReflectiveFaceTable(unsigned nMaterials)
{
	if (m_reflectiveFaceTable)
		deleteReflectiveFaceTable();
	m_reflectiveFaceTable = new ReflectiveFaceDef*[nMaterials];
	m_reflectiveFaceTable[0] = new ReflectiveFaceDef[nMaterials*nMaterials];

	ReflectiveFaceDef defaultValue;
	defaultValue.weight_multiplier = 1.0;
	defaultValue.prob_reflect = 1.0;

	for(unsigned i=0;i<nMaterials;++i)
		m_reflectiveFaceTable[i] = m_reflectiveFaceTable[0]+i*nMaterials;

	for(unsigned i=0;i<nMaterials;++i)
		m_reflectiveFaceTable[0][i] = defaultValue;

}

void TetraKernel::markReflectiveSurface(unsigned IDm0,unsigned IDm1,float r,float a,bool lambertian)
{
	float t = 1 - a - r;
	if (t < 0 )
	{
		if (IDm0 > IDm1)
			std::swap(IDm0,IDm1);
		LOG_ERROR << "Special interface definition for RegionID " << IDm0 << " and RegionID " << IDm1 << endl
				  << "Probabilities for absorption (a), reflection (r) and transmission (t) must sum up to 1" << endl
				  << "  a = " << a << endl
				  << "+ r = " << r << endl
				  << "+ t = " << t << endl
				  << "------------" << endl
				  << "    = " << a+r+t << endl
				  << "==> Ignoring this input" << endl;
	}
	else
	{
		if (IDm0 > IDm1)
			std::swap(IDm0,IDm1);
		LOG_INFO << "Special interface definition for RegionID " << IDm0 << " and RegionID " << IDm1 << endl
				 << "Probabilities for absorption (a), reflection (r) and transmission (t) are:" << endl
				 << "  a = " << a << endl
				 << "+ r = " << r << endl
				 << "+ t = " << t << endl
				 << "------------" << endl
				 << "    = 1" << endl;
		m_reflectiveFaces.insert(std::make_pair(std::make_pair(IDm0,IDm1),std::make_tuple(r,a,lambertian)));
	}
}

void TetraKernel::unmarkReflectiveSurface(unsigned IDm0,unsigned IDm1)
{
	if (IDm0 > IDm1)
		std::swap(IDm0,IDm1);

	auto it = m_reflectiveFaces.find(std::make_pair(IDm0,IDm1));
	if (it != m_reflectiveFaces.end())
		m_reflectiveFaces.erase(it);
}

void TetraKernel::unmarkAllReflectiveSurfaces()
{
	m_reflectiveFaces.clear();
}

void TetraKernel::deleteReflectiveFaceTable()
{
	delete [] m_reflectiveFaceTable[0];
	delete [] m_reflectiveFaceTable;
	m_reflectiveFaceTable = nullptr;
}

TetraKernel::~TetraKernel()
{
}

// TYS: All relevant code in HPP file.
// Due to difficulty with having templates in CPP+HPP in a shared directory

void TetraKernel::clearReflectiveSurfaces()
{
	for(auto& tet : m_tetras)
		for(unsigned i=0;i<4;++i)
			tet.setFaceFlag(i,Tetra::SpecialInterface,false);
}

void TetraKernel::applyReflectiveSurfaces(unsigned nMaterials)
{
	allocateReflectiveFaceTable(nMaterials);
	for (const auto& def : m_reflectiveFaces)
	{
		float r,a;
		bool lambertian;
		unsigned IDm0,IDm1;
		std::tie(IDm0,IDm1) = def.first;
		std::tie(r,a, lambertian) = def.second;
		applyReflectiveSurface(IDm0,IDm1,r,a,lambertian);
	}
}

unsigned TetraKernel::applyReflectiveSurface(unsigned IDm0,unsigned IDm1,float r,float a,bool lambertian)
{
	// set up the mapping matrix
	if (!m_reflectiveFaceTable)
		throw std::bad_alloc();

	ReflectiveFaceDef s;
	s.lambertianReflection = lambertian;
	if (a == 1.0){
		s.weight_multiplier = 0.0;
		s.prob_reflect = 0.0;
	}
	else {
		s.weight_multiplier = 1.0-a;
		s.prob_reflect = a > 0 ? r/(1.0-a) : r;
	}

	m_reflectiveFaceTable[IDm0][IDm1] = s;
	m_reflectiveFaceTable[IDm1][IDm0] = s;


	// iterate through tetra definitions and apply flag on each face
	unsigned nReflectiveTetras=0;
	unsigned nReflectiveFaces=0;

	if (IDm0 > IDm1)
		std::swap(IDm0,IDm1);

	LOG_INFO << "Applying reflective surface for (" << IDm0 << "," << IDm1 << ")" << endl;

	for(unsigned IDt=0;IDt<m_tetras.size();++IDt)
	{
		bool reflectiveTetra = false;
		for(unsigned i=0;i<4;++i)
		{
			unsigned IDma = m_tetras[IDt].matID;
			unsigned IDmb = m_tetras[m_tetras[IDt].adjTetras[i]].matID;
			if (IDma > IDmb)
				std::swap(IDma,IDmb);

			bool reflectiveFace = (IDm0 == IDma) & (IDm1 == IDmb);


			// NOTE: this function may be called multiple times with different args
			//
			// Screwed up the code below before where I set the flag *equal* to reflectiveFace
			//
			// However if it had previously been set for materials (A,B) but doesn't match for (C,D) it was
			// incorrectly cleared. Duh.
			//
			// The right thing to do is to *set* it if it's a reflective face, else *leave it alone* not clear

			if (reflectiveFace)
				m_tetras[IDt].setFaceFlag(i,Tetra::SpecialInterface,true);

			reflectiveTetra |= reflectiveFace;

			nReflectiveFaces += reflectiveFace;
		}
		nReflectiveTetras += reflectiveTetra;
	}

	LOG_INFO << "Marked " << nReflectiveFaces << " faces for the reflective surface of regions (" << IDm0 << "," << IDm1 << ")" << endl;

	return nReflectiveFaces;
}
