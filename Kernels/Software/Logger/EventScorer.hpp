/*
 * EventScorer.hpp
 *
 *  Created on: Sep 28, 2016
 *      Author: jcassidy
 */

#ifndef KERNELS_SOFTWARE_LOGGER_EVENTSCORER_HPP_
#define KERNELS_SOFTWARE_LOGGER_EVENTSCORER_HPP_

#include "MultiThreadWithIndividualCopy.hpp"

#include <FullMonteSW/OutputTypes/MCEventCounts.hpp>

#include "LoggerWithState.hpp"

class OutputDataCollection;


/**
 *
 * eventCommit to be provided by a derived class
 */


class EventLogger : public LoggerWithState<MCEventCounts>
{
public:
    /** Event-specific overrides to implement AbstractLogger functions */
    inline void eventLaunch(AbstractScorer&,const Ray3,unsigned,double)				{ ++m_state.Nlaunch; 	}

    inline void eventAbsorb(AbstractScorer&,const Point3,unsigned,double,double)	{ ++m_state.Nabsorb; 	}
    inline void eventScatter(AbstractScorer&,const UVect3,const UVect3,double)		{ ++m_state.Nscatter; 	}

    inline void eventBoundary(AbstractScorer&,const Point3,int,int,int)				{ ++m_state.Nbound; 	}

    inline void eventInterface(AbstractScorer&,const Ray3,int,unsigned)				{ ++m_state.Ninterface;	}
    inline void eventRefract(AbstractScorer&,const Point3,UVect3)					{ ++m_state.Nrefr; 		}
    inline void eventReflectInternal(AbstractScorer&,const Point3,const UVect3)		{ ++m_state.Ntir; 		}
    inline void eventReflectFresnel(AbstractScorer&,const Point3,UVect3)			{ ++m_state.Nfresnel; 	}

    inline void eventExit(AbstractScorer&,const Ray3,int,double)					{ ++m_state.Nexit; 		}
    inline void eventDie(AbstractScorer&,double)									{ ++m_state.Ndie; 		}
    inline void eventRouletteWin(AbstractScorer&,double,double)						{ ++m_state.Nwin; 		}

    inline void eventAbnormal(AbstractScorer&,const Packet&,unsigned,unsigned)		{ ++m_state.Nabnormal; 	}

    inline void eventSpecialReflect(AbstractScorer&,const Point3,UVect3){ ++m_state.NspecialReflect; }
    inline void eventSpecialTransmit(AbstractScorer&,const Point3,int,unsigned,unsigned){ ++m_state.NspecialTransmit; }
    inline void eventSpecialAbsorb(AbstractScorer&,const Point3,int,double){ ++m_state.NspecialAbsorb; }
    inline void eventSpecialTerminate(AbstractScorer&,const Packet&,const Point3,int){ ++m_state.NspecialTerm; }

    static void postResults(OutputDataCollection* C,const MCEventCounts& st);

    // eventClear provided by LoggerWithState
    void prepare(const Kernel* K);

private:
    static OutputData* d;
    static MCEventCountsOutput events;
};

typedef MultiThreadWithIndividualCopy<EventLogger> EventScorer;

#endif /* KERNELS_SOFTWARE_LOGGER_EVENTSCORER_HPP_ */
