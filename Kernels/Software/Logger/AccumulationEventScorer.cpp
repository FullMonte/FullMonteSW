#include <FullMonteSW/Kernels/Software/Logger/AccumulationEventScorer.hpp>
#include <FullMonteSW/OutputTypes/OutputDataCollection.hpp>

#include <FullMonteSW/OutputTypes/AccumulationEventSet.hpp>
#include <FullMonteSW/OutputTypes/AccumulationEvent.hpp>

using namespace std;


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////// AccumulationEventScorer() base class

AccumulationEventScorer::AccumulationEventScorer()
{
}

AccumulationEventScorer::~AccumulationEventScorer()
{
}

void AccumulationEventScorer::clear()
{
	m_traceList.clear();
}

void AccumulationEventScorer::prepare(Kernel*)
{
}

void AccumulationEventScorer::postResults(OutputDataCollection* C) const
{
	AccumulationEventSet* S = new AccumulationEventSet;

	// copy traces into a MemTraceSet result set
	for(const auto& tr : m_traceList)
	{
		AccumulationEvent* T = new AccumulationEvent;

		T->resize(tr.size());
		for(unsigned i=0;i<tr.size();++i)
			T->set(i,tr[i].Tet_or_FaceID,tr[i].weight_increment);

		S->append(T);
	}

	// query derived class for the appropriate name, then add to the result set
	S->name(traceName());
	C->add(S);
}

void AccumulationEventScorer::merge(const vector<EventRecord>& v)
{
	// copy the vector before locking
	vector<EventRecord> a = v;

	// lock and move the vector to the end of the list
	unique_lock<mutex> L(m_traceListMutex);
	m_traceList.emplace_back(move(a));
}





//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////// AccumulationEventScorer::LoggerBase base class

AccumulationEventScorer::LoggerBase::LoggerBase()
{
}

AccumulationEventScorer::LoggerBase::~LoggerBase()
{
}

void AccumulationEventScorer::LoggerBase::merge(AccumulationEventScorer& S)
{
	S.merge(m_trace);
	m_trace.clear();
}

void AccumulationEventScorer::LoggerBase::record(int address,double w)
{
	EventRecord tmp;
	tmp.weight_increment=w;
	tmp.Tet_or_FaceID=address;
	m_trace.push_back(tmp);
}




//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////// TetraAccumulationEventScorer members

TetraAccumulationEventScorer::TetraAccumulationEventScorer()
{

}

TetraAccumulationEventScorer::~TetraAccumulationEventScorer()
{

}

TetraAccumulationEventScorer::Logger TetraAccumulationEventScorer::createLogger()
{
	return TetraAccumulationEventScorer::Logger();
}

string TetraAccumulationEventScorer::s_traceName="TetraAccumulationEvent";
