/*
 * PacketPostmortem.cpp
 *
 *  Created on: Sep 26, 2017
 *      Author: jcassidy
 */

#include "PacketPostmortem.hpp"

#include <FullMonteSW/Logging/FullMonteLogger.hpp>
#include <FullMonteSW/Kernels/Software/Packet.hpp>
#include <FullMonteSW/OutputTypes/OutputDataCollection.hpp>

#include <sstream>
#include <iostream>
#include <iomanip>
using namespace std;


PacketPostmortem::PacketPostmortem()
{
}

PacketPostmortem::~PacketPostmortem()
{
}

void PacketPostmortem::prepare(Kernel*)
{
}

// lock to prevent conflicting access from commit() methods - shouldn't happen but just in case...
void PacketPostmortem::clear()
{
}

void PacketPostmortem::postResults(OutputDataCollection*) const
{
}






PacketPostmortem::Logger::Logger()
{
}

PacketPostmortem::Logger::~Logger()
{
}

void PacketPostmortem::Logger::eventDie(AbstractScorer&,double)
{
	stringstream ss;

	ss << "Packet died with ||d||= " << setw(10) << sqrtf(float(dot(m_direction.d,m_direction.d))) <<
			setw(10) << " dot(d,a)=" << float(dot(m_direction.d,m_direction.a)) <<
			setw(10) << " dot(d,b)=" << float(dot(m_direction.d,m_direction.b)) <<
			setw(10) << " dot(a,b)=" << float(dot(m_direction.a,m_direction.b));

	LOG_DEBUG << ss.str() << endl;
}

void PacketPostmortem::Logger::eventNewTetra(AbstractScorer&,const Packet& pkt,const Tetra&,const unsigned)
{
	m_direction = pkt.dir;
}

void PacketPostmortem::Logger::eventClear(AbstractScorer&)
{
}

PacketPostmortem::Logger PacketPostmortem::createLogger()
{
	return Logger();
}


