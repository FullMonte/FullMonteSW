#include <FullMonteSW/Kernels/Software/Logger/MemTraceScorer.hpp>
#include <FullMonteSW/OutputTypes/OutputDataCollection.hpp>

#include <FullMonteSW/OutputTypes/MemTraceSet.hpp>
#include <FullMonteSW/OutputTypes/MemTrace.hpp>

using namespace std;


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////// MemTraceScorer() base class

MemTraceScorer::MemTraceScorer()
{
}

MemTraceScorer::~MemTraceScorer()
{
}

void MemTraceScorer::clear()
{
	m_traceList.clear();
}

void MemTraceScorer::prepare(Kernel*)
{
}

void MemTraceScorer::postResults(OutputDataCollection* C) const
{
	MemTraceSet* S = new MemTraceSet;

	// copy traces into a MemTraceSet result set
	for(const auto& tr : m_traceList)
	{
		MemTrace* T = new MemTrace;

		T->resize(tr.size());
		for(unsigned i=0;i<tr.size();++i)
			T->set(i,tr[i].address,tr[i].count);

		S->append(T);
	}

	// query derived class for the appropriate name, then add to the result set
	S->name(traceName());
	C->add(S);
}

void MemTraceScorer::merge(const vector<RLERecord>& v)
{
	// copy the vector before locking
	vector<RLERecord> a = v;

	// lock and move the vector to the end of the list
	unique_lock<mutex> L(m_traceListMutex);
	m_traceList.emplace_back(move(a));
}





//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////// MemTraceScorer::LoggerBase base class

MemTraceScorer::LoggerBase::LoggerBase()
{
}

MemTraceScorer::LoggerBase::~LoggerBase()
{
}

void MemTraceScorer::LoggerBase::merge(MemTraceScorer& S)
{
	S.merge(m_trace);
	m_trace.clear();
}

void MemTraceScorer::LoggerBase::record(unsigned address,unsigned N)
{
	if (m_trace.empty() || m_trace.back().address != address)	// empty trace, or address differs from previous
	{
		RLERecord tmp;
		tmp.count=N;
		tmp.address=address;
		m_trace.push_back(tmp);
	}
	else								// another hit to same address
		m_trace.back().count += N;
}




//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////// TetraMemTraceScorer members

TetraMemTraceScorer::TetraMemTraceScorer()
{

}

TetraMemTraceScorer::~TetraMemTraceScorer()
{

}

TetraMemTraceScorer::Logger TetraMemTraceScorer::createLogger()
{
	return TetraMemTraceScorer::Logger();
}

string TetraMemTraceScorer::s_traceName="TetraMemTrace";
