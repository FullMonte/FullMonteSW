#ifndef PATHSCORER_HPP_INCLUDED_
#define PATHSCORER_HPP_INCLUDED_

#include <FullMonteSW/Config.h>

#include <list>
#include <array>

#include <FullMonteSW/Kernels/Event.hpp>
#include <FullMonteSW/OutputTypes/PacketPositionTrace.hpp>
#include <FullMonteSW/OutputTypes/PacketPositionTraceSet.hpp>

#include <mutex>
#include <vector>

#include "BaseLogger.hpp"

#include "AbstractScorer.hpp"

using namespace std;

class PathScorer : public AbstractScorer
{
public:
	PathScorer();
	virtual ~PathScorer();

	virtual void prepare(Kernel* K) override;
	virtual void clear() override;
	virtual void postResults(OutputDataCollection* C) const override;

	typedef PacketPositionTrace::Step Step;

	class Logger;

	Logger createLogger();

	// deactivate if normally not needed to save memory
	void deactivate () {
		m_active = false;
	}

	////// Surface to monitor
	void monitor (std::array<float,3> n, std::array<float,3> c, float r) {
#ifdef USE_SSE
		__m128 _n = _mm_setr_ps(n[0],n[1],n[2],0.0f);
		__m128 _c = _mm_setr_ps(c[0],c[1],c[2],0.0f);
#else
		m128 _n = {n[0],n[1],n[2],0.0f};
		m128 _c = {c[0],c[1],c[2],0.0f};
#endif
		m_plane = std::make_pair(_c,_n);
		m_radius = r;
		m_image_score = true;
		m_active = true;
	}

	// accept a commit from a logger
	void commit(Logger& L);

	bool is_active() { return m_active; }

private:
	std::mutex 							m_mutex;
	std::list<PacketPositionTrace*>		m_traces;
	std::size_t							m_reserveSize=16384;
	static PacketPositionTraceSet*		tracemap;

	// if using image scorer
	bool    m_image_score = false;
	// describes surface of interest (circle)
	BaseLogger::Ray3     m_plane;
	float    m_radius;

	bool    m_active;
};


/** Traces the photon's path into a list of vectors in memory.
 *
 * Registers a chance in tetra when it is absorbed.
 */

class PathScorer::Logger : public BaseLogger
{
public:
	Logger(std::size_t reserveSize);
	Logger(Logger&& lm_) = default;
	Logger(const Logger& lm_)=delete;

	~Logger();

	typedef std::list<PacketPositionTrace> State;

    inline void eventLaunch(AbstractScorer& S,const Ray3 r,unsigned IDt,double /*w*/)
    	{ 
			PathScorer& SS = static_cast<PathScorer&>(S);
			if (SS.is_active()) m_IDt=IDt; launch(r.first); 
		}

    inline void eventAbsorb(AbstractScorer& S,const Point3 p,unsigned IDt,double w0,double /*dw*/)
    	{ 
			PathScorer& SS = static_cast<PathScorer&>(S);
			if (SS.is_active()) m_wLast=w0; m_IDt=IDt; propagate(p,m_wLast,0.0f); 
		}

    inline void eventRefract(AbstractScorer& S,const Point3 p,UVect3& /*dir*/)
    	{ 
			PathScorer& SS = static_cast<PathScorer&>(S);
			if (SS.is_active()) propagate(p,m_wLast,0.0f); 
		}                	// refracted

    inline void eventReflectInternal(AbstractScorer& S,const Point3 p,const UVect3 /*dir*/)
    	{ 
			PathScorer& SS = static_cast<PathScorer&>(S);
			if (SS.is_active()) propagate(p,m_wLast,0.0f); 
		}			// internal reflection

    inline void eventReflectFresnel(AbstractScorer& S,const Point3 p,UVect3 /*dir*/)
    	{ 
			PathScorer& SS = static_cast<PathScorer&>(S);
			if (SS.is_active()) propagate(p,m_wLast,0.0f); 
		}         	// fresnel reflection
	
	inline void eventSpecialReflect(AbstractScorer& S,const Point3 p,UVect3 /*dir*/)
		{ 
			PathScorer& SS = static_cast<PathScorer&>(S);
			if (SS.is_active()) propagate(p,m_wLast,0.0f); 
		}			// reflective surface reflection

    inline void eventSpecialTransmit(AbstractScorer& S,const Point3 p,int /*FaceID*/,unsigned /*Current Tetra connected to FaceID*/,unsigned /*Other Tetra connected to FaceID*/)
		{ 
			PathScorer& SS = static_cast<PathScorer&>(S);
			if (SS.is_active()) propagate(p,m_wLast,0.0f); 
		}

    inline void eventSpecialAbsorb(AbstractScorer& S,const Point3 p,int /*FaceID*/,double w0)
		{ 
			PathScorer& SS = static_cast<PathScorer&>(S);
			if (!SS.is_active()) return; m_wLast=w0; propagate(p,m_wLast,0.0f); 
		}

    inline void eventSpecialTerminate(AbstractScorer& S,const Packet&,const Point3,int)
		{ 
			PathScorer& SS = static_cast<PathScorer&>(S);
			if (!SS.is_active()) return;
			if (!SS.m_image_score) terminate(static_cast<PathScorer&>(S)); 
			else eventClear(S);
		}

    // termination events
    inline void eventExit(AbstractScorer& S,const Ray3 r,int /*IDf*/,double w)
    	{ 
			PathScorer& SS = static_cast<PathScorer&>(S);
			if (!SS.is_active()) {
				eventClear(S);
				return;
			}
			propagate(r.first,w,0.0f); 
			if (SS.m_image_score) {
				Point3 pos = r.first;
				UVect3 dir = r.second;
				Point3 c = SS.m_plane.first;
				UVect3 n = SS.m_plane.second;
				Point3 i;
				const int mask = (1<<3)-1; // Mask indicating which elements are occupied  

				// get intersection with required plane
				float l_dot_n;
			#ifdef USE_SSE
				__m128 dot_temp = _mm_dp_ps(n, dir, (mask<<4) | 0x7);
				_mm_store_ss(&l_dot_n, dot_temp);
			#else
				l_dot_n = n[0]*dir[0] + n[1]*dir[1] + n[2]*dir[2];
			#endif

				if (l_dot_n > 1e-4 || l_dot_n < -1e-4) {	// intersection exists, possible to hit surface
					float dot_p;
			#ifdef USE_SSE
					__m128 dot_temp = _mm_sub_ps(c,pos);
					dot_temp = _mm_dp_ps(dot_temp, n, (mask<<4) | 0x7);
					_mm_store_ss(&dot_p, dot_temp);
			#else
					m128 temp = sub128(c, pos);
					dot_p = temp[0]*n[0] + temp[1]*n[1] + temp[2]*n[2];
			#endif
					float d = dot_p / l_dot_n;
					if (d < 0)  {
						eventClear(S);
						return;
					} 	// opposite direction
			#ifdef USE_SSE
					__m128 scale = _mm_set1_ps(d);
					i = _mm_add_ps(pos, _mm_mul_ps(scale, dir));
			#else
					i = add128(pos, {d*dir[0], d*dir[1], d*dir[2], 0.0f});
			#endif
				} else  {
					eventClear(S);
					return;
				}

				// get distance between intersection and center of circle
				float d;
			#ifdef USE_SSE
				__m128 diff = _mm_sub_ps(i, c);
				//__m128 sqr = _mm_mul_ps(diff, diff);
				__m128 norm_p = _mm_dp_ps(diff,diff,(mask<<4) | 0x7);
				__m128 k = _mm_sqrt_ss(norm_p);
				_mm_store_ss(&d, k);
			#else
				m128 diff = sub128(i, c);
				m128 norm_p = mult128(diff, diff);
				d = sqrt(norm_p[0] + norm_p[1] + norm_p[2]);
			#endif

				if (d > SS.m_radius)  {
					eventClear(S);
					return;
				}	// will not hit surface, abort
			}

			// record if still alive
			terminate(static_cast<PathScorer&>(S)); 
		};	// exited geometry

    inline void eventDie(AbstractScorer& S,double /*w*/)
    	{ 
			PathScorer& SS = static_cast<PathScorer&>(S);
			if (SS.is_active() && !SS.m_image_score) terminate(static_cast<PathScorer&>(S)); 
			else eventClear(S);
		}

    /// Commit is a no-op since each packet's trace will be committed when it ends
    inline void eventCommit(AbstractScorer&){}

    void eventClear(AbstractScorer&);

private:
    void propagate(Point3 p,float w,float t);
    void terminate(PathScorer& S);

//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
	void launch(__m128 p);
	__m128	m_lastPos;
#else
	void launch(m128 p);
	m128	m_lastPos;
#endif
    float 	m_length=0.0f;
    float	m_wLast=0.0f;
    unsigned m_IDt=0;	///< Current tetra
    unsigned m_IDm=0;	///< Current material (not used yet)
    
    std::size_t	m_reserveSize=16384;

    std::vector<Step>	m_trace;

    friend class PathScorer;
};


#endif
