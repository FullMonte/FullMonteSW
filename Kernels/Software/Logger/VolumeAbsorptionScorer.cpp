/*
 * VolumeAbsorptionScorer.cpp
 *
 *  Created on: Sep 29, 2016
 *      Author: jcassidy
 */

#include "VolumeAbsorptionScorer.hpp"



#include <FullMonteSW/OutputTypes/OutputData.hpp>
#include <FullMonteSW/OutputTypes/OutputDataCollection.hpp>

#include <FullMonteSW/Geometry/TetraMesh.hpp>

#include <FullMonteSW/Kernels/Kernel.hpp>

VolumeAbsorptionScorer::VolumeAbsorptionScorer()
{
}

VolumeAbsorptionScorer::~VolumeAbsorptionScorer()
{
	num_instances--;
	if(num_instances == 0 && vmap) delete vmap;
}

void VolumeAbsorptionScorer::clear()
{
	m_acc.clear();
	num_instances--;
	if(num_instances == 0 && vmap) delete vmap;
}

void VolumeAbsorptionScorer::prepare(Kernel* K)
{
	const TetraMesh* M = dynamic_cast<const TetraMesh*>(K->geometry());

	if (!M)
		throw std::logic_error("VolumeAbsorptionScorer::prepare(K) can't cast to TetraMesh");

	m_acc.resize(get(num_tetras,*M));
	setDimension(K->geometry()->getUnit());
	setUnit(K->getUnit());	
}

SpatialMap<float>* VolumeAbsorptionScorer::vmap = nullptr;
unsigned VolumeAbsorptionScorer::num_instances = 0;

void VolumeAbsorptionScorer::postResults(OutputDataCollection* C) const
{
	// convert to float
	// m_acc.size has the same size as the number of tetras in the mesh.
	// Since we inserted a zero tetra into the mesh, we need to account for that and reduce the size
	// of the output vector by one to get the sam tetra number as the input mesh
	std::vector<float> vf(m_acc.size()-1);
	// The first entry of m_acc will be the absorption score of the zero tetra. This score should (and will) always be 0.
	// This lets us discard this entry without changing the simulation results in any way
	for(unsigned i=1;i<m_acc.size();++i)
		vf[i-1] = m_acc[i];
	// create vector
	if (num_instances > 0) {
        LOG_WARNING << "VolumeAbsorptionScorer: multiple vmap being created or previous one not deleted." << endl;
		if (vmap) delete vmap;	// Delete vmap just in case.
	}
	
	vmap = new SpatialMap<float>(vf,AbstractSpatialMap::Volume,AbstractSpatialMap::Scalar,AbstractSpatialMap::PhotonWeight);
	num_instances++;

	switch (getUnit())
	{
	case Kernel::Joule:
		switch (getDimension())
		{
			case DimensionUnit::m:
				vmap->unitType(AbstractSpatialMap::J_m);
				break;
			case DimensionUnit::cm:
				vmap->unitType(AbstractSpatialMap::J_cm);
				break;
			case DimensionUnit::mm:
				vmap->unitType(AbstractSpatialMap::J_mm);
				break;
			default:
				throw std::logic_error("VolumeAbsorptionScorer::postResults(C) Impossible unit combination - Photon weight should be interpreted as 'Joule' but dimension unit is set to 'NONE'");
				break;
		}
		break;
	case Kernel::Watt:
		switch (getDimension())
		{
			case DimensionUnit::m:
				vmap->unitType(AbstractSpatialMap::W_m);
				break;
			case DimensionUnit::cm:
				vmap->unitType(AbstractSpatialMap::W_cm);
				break;
			case DimensionUnit::mm:
				vmap->unitType(AbstractSpatialMap::W_mm);
				break;
			default:
				throw std::logic_error("VolumeAbsorptionScorer::postResults(C) Impossible unit combination - Photon weight should be interpreted as 'Watt' but dimension unit is set to 'NONE'");
				break;
		}
		break;
	
	default:
		switch (getDimension())
		{
			case DimensionUnit::m:
				throw std::logic_error("VolumeAbsorptionScorer::postResults(C) Impossible unit combination - Raw photon weight should be used (unit is set to 'NONE') but dimension unit is set to 'm'");
				break;
			case DimensionUnit::cm:
				throw std::logic_error("VolumeAbsorptionScorer::postResults(C) Impossible unit combination - Raw photon weight should be used (unit is set to 'NONE') but dimension unit is set to 'cm'");
				break;
			case DimensionUnit::mm:
				throw std::logic_error("VolumeAbsorptionScorer::postResults(C) Impossible unit combination - Raw photon weight should be used (unit is set to 'NONE') but dimension unit is set to 'mm'");
				break;
			default:
				break;
		}
		break;
	}
	
	vmap->name("VolumeEnergy");
	C->add(vmap);
}

void VolumeAbsorptionScorer::queueSize(unsigned q)
{
	m_queueSize=q;
}

VolumeAbsorptionScorer::Logger VolumeAbsorptionScorer::createLogger()
{
	return VolumeAbsorptionScorer::Logger(m_queueSize);
}




/**********************************************************************************************************************************
 * Logger for volume absorption scoring
 */

VolumeAbsorptionScorer::Logger::Logger(std::size_t qSize) :
	m_handle(qSize)
{
}

void VolumeAbsorptionScorer::Logger::eventClear()
{
	m_handle.clear();
}

void VolumeAbsorptionScorer::Logger::eventCommit(AbstractScorer& S)
{
 	m_handle.commit(static_cast<VolumeAbsorptionScorer&>(S).m_acc);
}
