/* BaseLogger.hpp
 *
 *  Created on: Sep 28, 2016
 *      Author: jcassidy
 */

#ifndef KERNELS_SOFTWARE_LOGGER_BASELOGGER_HPP_
#define KERNELS_SOFTWARE_LOGGER_BASELOGGER_HPP_

#include <FullMonteSW/Config.h>

//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
#include "immintrin.h"
#else
#include "FullMonteSW/Geometry/NonSSE.hpp"
#endif

#include <utility>


class Packet;
class Tetra;

namespace x86Kernel
{
    class Material;
}
/** Logger Concept
 *
 * The Logger is a type that is passed into a simulation Kernel to record events via its eventXXX(...) methods.
 * An instance of a Logger will be called by a single execution thread of the kernel, so its methods need not be thread-safe.
 * Any accesses to other objects by a contained pointer or by reference, though, will need to be done in a thread-safe manner if
 * multiple Loggers could hold the same reference.
 *
 * The Logger should be created by a matching Scorer, which is responsible for creating Logger instances, and for packaging up the
 * resulting output.
 *
 * No virtual methods are used. To preserve performance, it is intended that all polymorphism occur at compile time (via templates)
 * instead of run time (virtual methods).
 *
 * Logger concept requirements
 *
 *		default constructible
 *		move- or copy-constructible
 *
 *		eventXXX(...)								Methods as defined below (default to no-op), overridden as necessary
 *
 * 		void eventClear()							Clears the state
 *		void eventCommit(AbstractScorer*)			Commit its results so the logger may be deleted without loss of info
 *
 *
 * StandaloneLogger concept requirements (in addition to Logger)
 * 		void prepare(Kernel* k)
 * 		static void postResults(OutputDataCollection*,State&)
 *
 *
 * NOTE: clear and commit are not provided by default; derived classes should provide them
 */

class AbstractScorer;

class BaseLogger
{
public:
	typedef std::true_type is_logger;

	BaseLogger(const BaseLogger&)=default;
	BaseLogger(BaseLogger&&)=default;							///< Movable


    /** All eventXXX(...) methods below are invoked within the simulation kernel. They should be optimized for speed,
     * and so should not use virtual calls if it can be avoided.
     *
     * Access to the Logger need not be thread-safe, since each thread gets its own private logger object.
     */
//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
	typedef std::pair<__m128,__m128> 	Ray3;
	typedef __m128 						Point3;
	typedef __m128 						UVect3;
#else
    typedef std::pair<m128,m128> 	    Ray3;
	typedef m128 						Point3;
	typedef m128 						UVect3;
#endif
    inline void eventLaunch(AbstractScorer&,const Ray3 /*dir*/,unsigned /*IDt*/,double /*w*/){};

    inline void eventBoundary(AbstractScorer&,const Point3 /*p*/,int,int,int){};        // boundary (same material)

    inline void eventAbsorb(AbstractScorer&,const Point3 /*p*/,unsigned /*IDt*/,double /*w0*/,double /*dw*/){};


    inline void eventScatter(AbstractScorer&,const UVect3 /*d0*/,const UVect3 /*d*/,double /*g*/){};

    inline void eventInterface(AbstractScorer&,const Ray3,int,unsigned){};          // found a material interface; possible results are:
    inline void eventRefract(AbstractScorer&,const Point3,UVect3){};                //      refracted
    inline void eventReflectInternal(AbstractScorer&,const Point3,const UVect3){};  //      internal reflection

    inline void eventReflectFresnel(AbstractScorer&,const Point3,UVect3){};         //      fresnel reflection

    inline void eventNewTetra(AbstractScorer&,const Packet& /*pkt*/,const Tetra& /*T0*/,const unsigned /*tetraFaceIndex*/,unsigned /*IDt_new*/, const x86Kernel::Material& /*currMat*/){};
    	// packet pkt propagating from tetra T0 from face (tetraFaceIndex) 0..3

    // termination events
    inline void eventExit(AbstractScorer&,const Ray3 /*ray*/,int /*IDfe*/,double /*w*/){};            			// exited geometry
    inline void eventDie(AbstractScorer&,double){};                                     // lost Russian roulette
    inline void eventRouletteWin(AbstractScorer&,double,double){};

    inline void eventAbnormal(AbstractScorer&,const Packet&,unsigned,unsigned){};

    inline void eventTimeGate(AbstractScorer&,const Packet& /*pkt*/){};						// Exceeded time gate
    inline void eventNoHit(AbstractScorer&,const Packet& /*pkt*/,const Tetra& /*tet*/){};			// No hit in intersection

    // master termination event called when packet is finished tracing (after reason-specific events above)
    inline void eventTerminate(AbstractScorer&,const Packet& /*pkt*/){};

    inline void eventSpecialAbsorb(AbstractScorer&,const Point3 /*p*/,int /*IDf*/,double /*dw*/){}
    inline void eventSpecialReflect(AbstractScorer&,const Point3 /*p*/,const UVect3 /*newdir*/){};  //      internal reflection
    inline void eventSpecialTransmit(AbstractScorer&,const Point3 /*p*/,const int /*IDfe*/,const unsigned /*IDt*/,const unsigned /*IDte*/){};
    inline void eventSpecialTerminate(AbstractScorer&,const Packet&,Point3 /*p*/,int /*IDfe*/){};

    inline void eventCommit(AbstractScorer&){};

protected:
	BaseLogger(){}
	~BaseLogger(){}
};




#include <type_traits>

namespace Events {

/** For each invocation of DEFINE_EVENT(E), creates a tag type E_tag and an instance of that tag type E, as well as a
 * function that dispatches
 *
 * 		E					An event name
 * 		E_tag		(auto) 	Tag type generated by DEFINE_EVENT(E)
 * 		Events::E	(auto)	Instance of tag type generated by DEFINE_EVENT(E)
 *
 * 		Logger				Class fulfilling Logger concept
 * 		l					Instance of class L
 *
 * 		s					AbstractScorer* pointing to the corresponding Scorer
 *
 * log_event(l,E,...) will call l.eventE(s,...)
 */

#define DEFINE_EVENT(ename) typedef struct {} ename##_tag; \
	extern ename##_tag ename; \
	template<class Logger,class Scorer,typename... Args>inline typename std::enable_if< std::is_same<typename Logger::is_logger,std::true_type>::value,void>::type \
		log_event(Logger& l,Scorer& s,Events::ename##_tag,Args... args) \
		{ l.event##ename(s,args...); }

// Example instance
//DEFINE_EVENT(Launch) typedef struct {} Launch_tag;
//extern Launch_tag Launch;
//log_event(Logger& l, Events::E_tag, ...)
//{
//	l.eventLaunch(...);
//}

DEFINE_EVENT(Launch)
DEFINE_EVENT(Absorb)
DEFINE_EVENT(Scatter)
DEFINE_EVENT(Boundary)
DEFINE_EVENT(Interface)
DEFINE_EVENT(ReflectInternal)
DEFINE_EVENT(Refract)
DEFINE_EVENT(ReflectFresnel)
DEFINE_EVENT(Exit)
DEFINE_EVENT(NewTetra)
DEFINE_EVENT(RouletteWin)
DEFINE_EVENT(Die)
DEFINE_EVENT(Abnormal)
DEFINE_EVENT(TimeGate)
DEFINE_EVENT(NoHit)
DEFINE_EVENT(Commit)
DEFINE_EVENT(Clear)
DEFINE_EVENT(Terminate)
DEFINE_EVENT(SpecialAbsorb)
DEFINE_EVENT(SpecialTransmit)
DEFINE_EVENT(SpecialReflect)
DEFINE_EVENT(SpecialTerminate)
}

#endif /* KERNELS_SOFTWARE_LOGGER_BASELOGGER_HPP_ */
