/*
 * SurfaceExitScorer.cpp
 *
 *  Created on: Sep 29, 2016
 *      Author: jcassidy
 */

#include "SurfaceExitScorer.hpp"

#include <FullMonteSW/OutputTypes/OutputData.hpp>
#include <FullMonteSW/OutputTypes/OutputDataCollection.hpp>


#include <FullMonteSW/Geometry/FaceLinks.hpp>

#include <FullMonteSW/Kernels/Kernel.hpp>

#include <cassert>

#include <FullMonteSW/Warnings/Push.hpp>
#include <FullMonteSW/Warnings/Boost.hpp>
#include <boost/range/algorithm.hpp>
#include <FullMonteSW/Warnings/Pop.hpp>


SurfaceExitScorer::SurfaceExitScorer()
{
}

SurfaceExitScorer::~SurfaceExitScorer()
{
	delete smap;
}

void SurfaceExitScorer::prepare(Kernel* K)
{
	const TetraMesh* M = dynamic_cast<const TetraMesh*>(K->geometry());

	if (!M)
		throw std::logic_error("SurfaceExitScorer::prepare(K) invalid cast to TetraMesh");

	m_acc.resize(get(num_faces,*M));
	setDimension(K->geometry()->getUnit());
	setUnit(K->getUnit());
}

SpatialMap<float>*  SurfaceExitScorer::smap = nullptr;

void SurfaceExitScorer::postResults(OutputDataCollection* C) const
{
	// convert to float
	// m_acc.size has the same size as the number of tetra faces in the mesh.
	// Since we inserted a zero tetra (and zero tetra face) into the mesh, 
	// we need to account for that in the output routine VTKSurfaceWriter.cpp
	std::vector<float> vf(m_acc.size());

	for(unsigned i=0;i<m_acc.size();++i)
		vf[i] = m_acc[i];
	
	// create vector
	smap = new SpatialMap<float>(vf,AbstractSpatialMap::Surface,AbstractSpatialMap::Scalar,AbstractSpatialMap::PhotonWeight);

	switch (getUnit())
	{
	case Kernel::Joule:
		switch (getDimension())
		{
			case DimensionUnit::m:
				smap->unitType(AbstractSpatialMap::J_m);
				break;
			case DimensionUnit::cm:
				smap->unitType(AbstractSpatialMap::J_cm);
				break;
			case DimensionUnit::mm:
				smap->unitType(AbstractSpatialMap::J_mm);
				break;
			default:
				throw std::logic_error("SurfaceExitScorer::postResults(C) Impossible unit combination - Photon weight should be interpreted as 'Joule' but dimension unit is set to 'NONE'");
				break;
		}
		break;
	case Kernel::Watt:
		switch (getDimension())
		{
			case DimensionUnit::m:
				smap->unitType(AbstractSpatialMap::W_m);
				break;
			case DimensionUnit::cm:
				smap->unitType(AbstractSpatialMap::W_cm);
				break;
			case DimensionUnit::mm:
				smap->unitType(AbstractSpatialMap::W_mm);
				break;
			default:
				throw std::logic_error("SurfaceExitScorer::postResults(C) Impossible unit combination - Photon weight should be interpreted as 'Watt' but dimension unit is set to 'NONE'");
				break;
		}
		break;
	
	default:
		switch (getDimension())
		{
			case DimensionUnit::m:
				throw std::logic_error("SurfaceExitScorer::postResults(C) Impossible unit combination - Raw photon weight should be used (unit is set to 'NONE') but dimension unit is set to 'm'");
				break;
			case DimensionUnit::cm:
				throw std::logic_error("SurfaceExitScorer::postResults(C) Impossible unit combination - Raw photon weight should be used (unit is set to 'NONE') but dimension unit is set to 'cm'");
				break;
			case DimensionUnit::mm:
				throw std::logic_error("SurfaceExitScorer::postResults(C) Impossible unit combination - Raw photon weight should be used (unit is set to 'NONE') but dimension unit is set to 'mm'");
				break;
			default:
				break;
		}
		break;
	}

	smap->name("SurfaceExitEnergy");

	C->add(smap);
}

void SurfaceExitScorer::clear()
{
	m_acc.clear();
	delete smap;
}

SurfaceExitScorer::Logger SurfaceExitScorer::createLogger()
{
	return Logger();
}

SurfaceExitScorer::Logger::Logger()
{
}

SurfaceExitScorer::Logger::~Logger()
{
}
