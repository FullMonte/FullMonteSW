/*
 * EventScorer.cpp
 *
 *  Created on: May 31, 2017
 *      Author: jcassidy
 */

#include "EventScorer.hpp"
#include <FullMonteSW/OutputTypes/OutputDataCollection.hpp>

OutputData* EventLogger::d = nullptr;
MCEventCountsOutput EventLogger::events;


void EventLogger::postResults(OutputDataCollection* C,const MCEventCounts& st)
{
	events = MCEventCountsOutput(st);
	events.name("EventCounts");
	
	d = &events;
	C->add(d);
}

void EventLogger::prepare(const Kernel*)
{
}
