/*
 * ConservationScorer.cpp
 *
 *  Created on: May 31, 2017
 *      Author: jcassidy
 */

#include <FullMonteSW/OutputTypes/MCConservationCounts.hpp>
#include <FullMonteSW/OutputTypes/OutputDataCollection.hpp>

#include "ConservationScorer.hpp"

OutputData* ConservationLogger::d = nullptr;
MCConservationCountsOutput ConservationLogger::conservation;

void ConservationLogger::postResults(OutputDataCollection* C,const MCConservationCounts& st)
{
	conservation = MCConservationCountsOutput(st);
	conservation.name("ConservationCounts");
	
	d = &conservation;

	C->add(d);
}


void ConservationLogger::prepare(const Kernel*)
{
}
