/*
 * SurfaceExitImageScorer.cpp
 *
 *  Created on: Sep 8, 2022
 *      Author: Shuran Wang
 */

#include "SurfaceExitImageScorer.hpp"

#include <FullMonteSW/OutputTypes/OutputData.hpp>
#include <FullMonteSW/OutputTypes/OutputDataCollection.hpp>

#include <FullMonteSW/Geometry/Ray.hpp>
#include <FullMonteSW/Geometry/FaceLinks.hpp>
#include <FullMonteSW/OutputTypes/PhotonData.hpp>

#include <FullMonteSW/Kernels/Kernel.hpp>

#include <cassert>

#include <FullMonteSW/Warnings/Push.hpp>
#include <FullMonteSW/Warnings/Boost.hpp>
#include <boost/range/algorithm.hpp>
#include <FullMonteSW/Warnings/Pop.hpp>


SurfaceExitImageScorer::SurfaceExitImageScorer()
{
}

SurfaceExitImageScorer::~SurfaceExitImageScorer()
{
}

void SurfaceExitImageScorer::prepare(Kernel* K)
{
	const TetraMesh* M = dynamic_cast<const TetraMesh*>(K->geometry());

	if (!M)
		throw std::logic_error("SurfaceExitImageScorer::prepare(K) invalid cast to TetraMesh");

	setDimension(K->geometry()->getUnit());
	setUnit(K->getUnit());
}

PhotonData* SurfaceExitImageScorer::pdata = nullptr;
SpatialMap<float>* SurfaceExitImageScorer::smap = nullptr;
vector<pair<array<float,3>, array<float,3>> >* SurfaceExitImageScorer::vray = nullptr;

void SurfaceExitImageScorer::postResults(OutputDataCollection* C) const
{
	// create vector
	smap = new SpatialMap<float>(m_weight,AbstractSpatialMap::Point,AbstractSpatialMap::Scalar,AbstractSpatialMap::PhotonWeight);

	switch (getUnit())
	{
	case Kernel::Joule:
		switch (getDimension())
		{
			case DimensionUnit::m:
				smap->unitType(AbstractSpatialMap::J_m);
				break;
			case DimensionUnit::cm:
				smap->unitType(AbstractSpatialMap::J_cm);
				break;
			case DimensionUnit::mm:
				smap->unitType(AbstractSpatialMap::J_mm);
				break;
			default:
				throw std::logic_error("SurfaceExitImageScorer::postResults(C) Impossible unit combination - Photon weight should be interpreted as 'Joule' but dimension unit is set to 'NONE'");
				break;
		}
		break;
	case Kernel::Watt:
		switch (getDimension())
		{
			case DimensionUnit::m:
				smap->unitType(AbstractSpatialMap::W_m);
				break;
			case DimensionUnit::cm:
				smap->unitType(AbstractSpatialMap::W_cm);
				break;
			case DimensionUnit::mm:
				smap->unitType(AbstractSpatialMap::W_mm);
				break;
			default:
				throw std::logic_error("SurfaceExitImageScorer::postResults(C) Impossible unit combination - Photon weight should be interpreted as 'Watt' but dimension unit is set to 'NONE'");
				break;
		}
		break;
	
	default:
		switch (getDimension())
		{
			case DimensionUnit::m:
				throw std::logic_error("SurfaceExitImageScorer::postResults(C) Impossible unit combination - Raw photon weight should be used (unit is set to 'NONE') but dimension unit is set to 'm'");
				break;
			case DimensionUnit::cm:
				throw std::logic_error("SurfaceExitImageScorer::postResults(C) Impossible unit combination - Raw photon weight should be used (unit is set to 'NONE') but dimension unit is set to 'cm'");
				break;
			case DimensionUnit::mm:
				throw std::logic_error("SurfaceExitImageScorer::postResults(C) Impossible unit combination - Raw photon weight should be used (unit is set to 'NONE') but dimension unit is set to 'mm'");
				break;
			default:
				break;
		}
		break;
	}

	smap->name("SurfaceExitImageEnergy");

	if (pdata) delete pdata;
	pdata = new PhotonData();
	pdata->weight(smap);
	pdata->unitType(smap->unitType());

	// create vector of rays
	vray = new vector<pair<array<float,3>, array<float,3>> > ();
	for (unsigned i = 0; i < m_pinfo.size(); i++) {
		BaseLogger::Point3 pos = m_pinfo[i].first;
		BaseLogger::UVect3 dir = m_pinfo[i].second;
		pair<array<float,3>, array<float,3>> tempr = make_pair(array<float,3>{ pos[0],pos[1],pos[2]}, array<float,3>{ {dir[0], dir[1], dir[2]} });
		vray->push_back(tempr);
	}

	pdata->ray(vray);
	pdata->name("SurfaceExitPhotonData");

	C->add(pdata);
}

void SurfaceExitImageScorer::clear()
{
}

void SurfaceExitImageScorer::commit(std::pair<float,BaseLogger::Ray3> photon)
{
	std::unique_lock<std::mutex> L(m_mutex);
	m_weight.emplace_back(photon.first);
	m_pinfo.emplace_back(photon.second);
}

SurfaceExitImageScorer::Logger SurfaceExitImageScorer::createLogger()
{
	return Logger();
}

SurfaceExitImageScorer::Logger::Logger()
{
}

SurfaceExitImageScorer::Logger::~Logger()
{
}
