/*
 * SurfaceExitImageScorer.hpp
 *
 *  Created on: Sep 7, 2022
 *      Author: Shuran Wang
 */

#ifndef KERNELS_SOFTWARE_LOGGER_SurfaceExitImageScorer_HPP_
#define KERNELS_SOFTWARE_LOGGER_SurfaceExitImageScorer_HPP_

#include "AbstractScorer.hpp"
#include "AtomicMultiThreadAccumulator.hpp"
#include <vector>
#include "BaseLogger.hpp"

#include <array>

#include <FullMonteSW/OutputTypes/SpatialMap.hpp>

class PhotonData;

/**
 * Records photon weight, position and direction for rays exiting mesh and hitting circular surface outside of mesh.
 * 
 * Since surface exit is a relatively rare phenomenon, use an atomic accumulator to track.
 */

class SurfaceExitImageScorer : public AbstractScorer
{
public:
	SurfaceExitImageScorer();
	~SurfaceExitImageScorer();
	

	////// Concrete Scorer override requirements
	virtual void prepare(Kernel* K) override;
	virtual void clear() override;
	virtual void postResults(OutputDataCollection* C) const override;


	////// Scorer concept requirements
	class Logger;
	Logger createLogger();
	void commit(std::pair<float, BaseLogger::Ray3> photon);

	////// Surface to monitor
	void monitor (std::array<float,3> n, std::array<float,3> c, float r) {
#ifdef USE_SSE
		__m128 _n = _mm_setr_ps(n[0],n[1],n[2],0.0f);
		__m128 _c = _mm_setr_ps(c[0],c[1],c[2],0.0f);
#else
		m128 _n = {n[0],n[1],n[2],0.0f};
		m128 _c = {c[0],c[1],c[2],0.0f};
#endif
		m_plane = std::make_pair(_c,_n);
		m_radius = r;
	}

private:
	std::mutex 			m_mutex;
	std::vector <float> m_weight;
	std::vector <BaseLogger::Ray3> m_pinfo;

	// describes surface of interest (circle)
	BaseLogger::Ray3     m_plane;
	float    m_radius;

	static PhotonData* pdata;
	static SpatialMap<float>* smap;
	static std::vector<std::pair<std::array<float,3>, std::array<float,3>> >* vray;
};

class SurfaceExitImageScorer::Logger : public BaseLogger
{
public:
	typedef SurfaceExitImageScorer Scorer;

	Logger();
	~Logger();

	Logger(Logger&& lv_) = default;
	Logger(const Logger& lv_) = delete;
	
	inline void eventExit(AbstractScorer& S,const Ray3 R,int IDf,double w);

	// commit/clear are no-op because we accumulate atomically
	void eventCommit(AbstractScorer&){}
	void eventClear(AbstractScorer&){}

};

inline void SurfaceExitImageScorer::Logger::eventExit(AbstractScorer& S,const BaseLogger::Ray3 R,int,double w)
{
	SurfaceExitImageScorer& SS = static_cast<SurfaceExitImageScorer&>(S);

	Point3 pos = R.first;
	UVect3 dir = R.second;
	Point3 c = SS.m_plane.first;
	UVect3 n = SS.m_plane.second;
	Point3 i;
    const int mask = (1<<3)-1; // Mask indicating which elements are occupied  

	// get intersection with required plane
	float l_dot_n;
#ifdef USE_SSE
    __m128 dot_temp = _mm_dp_ps(n, dir, (mask<<4) | 0x7);
    _mm_store_ss(&l_dot_n, dot_temp);
#else
	l_dot_n = n[0]*dir[0] + n[1]*dir[1] + n[2]*dir[2];
#endif

	if (l_dot_n > 1e-4 || l_dot_n < -1e-4) {	// intersection exists, possible to hit surface
		float dot_p;
#ifdef USE_SSE
		__m128 dot_temp = _mm_sub_ps(c,pos);
		dot_temp = _mm_dp_ps(dot_temp, n, (mask<<4) | 0x7);
		_mm_store_ss(&dot_p, dot_temp);
#else
		m128 temp = sub128(c, pos);
		dot_p = temp[0]*n[0] + temp[1]*n[1] + temp[2]*n[2];
#endif
		float d = dot_p / l_dot_n;
		if (d < 0) return; 	// opposite direction
#ifdef USE_SSE
		__m128 scale = _mm_set1_ps(d);
		i = _mm_add_ps(pos, _mm_mul_ps(scale, dir));
#else
		i = add128(pos, {d*dir[0], d*dir[1], d*dir[2], 0.0f});
#endif
	} else return;

	// get distance between intersection and center of circle
	float d;
#ifdef USE_SSE
	__m128 diff = _mm_sub_ps(i, c);
	//__m128 sqr = _mm_mul_ps(diff, diff);
    __m128 norm_p = _mm_dp_ps(diff,diff,(mask<<4) | 0x7);
    __m128 k = _mm_sqrt_ss(norm_p);
	_mm_store_ss(&d, k);
#else
	m128 diff = sub128(i, c);
	m128 norm_p = mult128(diff, diff);
	d = sqrt(norm_p[0] + norm_p[1] + norm_p[2]);
#endif

	if (d > SS.m_radius) return; 	// will not hit surface, abort

	SS.commit(std::make_pair(w,R));
}

#endif /* KERNELS_SOFTWARE_LOGGER_SurfaceExitImageScorer_HPP_ */
