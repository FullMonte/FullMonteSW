/*
 * DirectedSurfaceScorer.cpp
 *
 *  Created on: May 22, 2018
 *      Author: jcassidy
 */

#include "DirectedSurfaceScorer.hpp"

#include <FullMonteSW/Kernels/Software/TetraMCKernel.hpp>

#include <FullMonteSW/Logging/FullMonteLogger.hpp>

#include <FullMonteSW/Geometry/Predicates/ArrayPredicateEvaluator.hpp>
#include <FullMonteSW/Geometry/Predicates/VolumeCellPredicate.hpp>
#include <FullMonteSW/Geometry/Predicates/SurfaceCellPredicate.hpp>
#include <FullMonteSW/Geometry/Predicates/SurfaceOfRegionPredicate.hpp>

#include <boost/range.hpp>
#include <boost/range/adaptor/indexed.hpp>


AbstractDirectedSurfaceScorer::AbstractDirectedSurfaceScorer()
{

}

AbstractDirectedSurfaceScorer::~AbstractDirectedSurfaceScorer()
{

}

void AbstractDirectedSurfaceScorer::createOutputSurfaces(Kernel* K)
{
	clearSurfaces();

	const TetraMesh* M = dynamic_cast<const TetraMesh*>(K->geometry());
	if (!M)
	{
		LOG_ERROR << "AbstractDirectedSurfaceScorer::createOutputSurfaces(K) given kernel whose geometry isn't a TetraMesh" << endl;
		return;
	}

	TetraKernel* TK = dynamic_cast<TetraKernel*>(K);
	if (!TK)
	{
		LOG_ERROR << "AbstractDirectedSurfaceScorer::createOutputSurfaces(K) given kernel that isn't convertible to TetraMCKernel" << endl;
        return;
	}

	m_outputSurfaces.resize(m_outputCellPredicates.size(),nullptr);

	// boolean vector over all undirected faces indicating if they need fluence scored
	vector<bool> undirectedFaceCapture(M->faceTetraLinks()->size(),false);


	/** Iterate over all provided predicates:
	 * 		- marking undirected faces for counting
	 * 		- create Permutation for output data
	 */

	LOG_DEBUG << "Output directed surface creation" << endl;

	for(unsigned p=0;p<m_outputCellPredicates.size();++p)
	{
		const GeometryPredicate* pred = m_outputCellPredicates[p];

		const SurfaceCellPredicate* surf=nullptr;
		const VolumeCellPredicate* vol=nullptr;

		// get a SurfaceCellPredicate for the surface to be scored and bind it to the geometry to create an ArrayPredicateEvaluator
		if ((vol = dynamic_cast<const VolumeCellPredicate*>(pred)))
		{
			// create predicate for surface of region
			auto s = new SurfaceOfRegionPredicate();
			s->setRegionPredicate(vol);
			surf = s;
		}
		else if ((surf = dynamic_cast<const SurfaceCellPredicate*>(pred)))
		{
		}

		ArrayPredicateEvaluator* eval = surf->bind(K->geometry());

		ArrayPredicateEvaluator* volEval = vol ? vol->bind(K->geometry()) : nullptr;


		// loop over all undirected faces, marking faces that match the predicate and creating a Permutation for them
		vector<unsigned> IDfds;

		for(unsigned i=1;i<M->faceTetraLinks()->size();++i)
		{
			if ((*eval)(i))
			{
				undirectedFaceCapture[i] = true;


				// output permutation provides the directed face index for the exit

				// if using a volume predicate _and_ the tetra below the face (opposite normal) is in the region interior, then
				// we need to flip the face so the normal points outward
				bool flipFace = volEval && (*volEval)(M->faceTetraLinks()->get(i).upTet().T.value());

				// insert a _directed_ surface face index
				IDfds.push_back( (i<<1) | flipFace);
			}
		}
		m_outputSurfaces[p] = new Permutation();
		m_outputSurfaces[p]->resize(IDfds.size());
		for(const auto el : IDfds | boost::adaptors::indexed(0U))
			m_outputSurfaces[p]->set(el.index(),el.value());

		LOG_DEBUG << "  [" << p << "] from " << (vol ? "volume" : "surface" ) << " predicate with " << m_outputSurfaces[p]->dim() << " faces" << endl;

		// if provided a volume, we dynamically allocated a surface. delete it now.
		if (vol)
			delete surf;
	}

	// apply the list of undirected faces to capture to the actual kernel
	for(unsigned i=1;i<M->tetraCells()->size();++i)
		for(unsigned j=0;j<4;++j)
			TK->markTetraFaceForFluenceScoring(i,j,undirectedFaceCapture[M->tetraFaceLinks()->get(i)[j].faceID.value()>>1]);
}

void AbstractDirectedSurfaceScorer::clearSurfaces()
{
	for(auto p : m_outputSurfaces)
		delete p;
	m_outputSurfaces.clear();
}

unsigned AbstractDirectedSurfaceScorer::getNumberOfOutputSurfaces() const
{
	return m_outputSurfaces.size();
}

Permutation* AbstractDirectedSurfaceScorer::getOutputSurface(unsigned i) const
{
	if (i >= m_outputSurfaces.size()) {
		LOG_ERROR << "Requested output surface (" << i << ") exceeds available surface count (" << m_outputSurfaces.size() << ")" << endl;
    }
	return m_outputSurfaces.at(i);
}

void AbstractDirectedSurfaceScorer::addScoringRegionBoundary(VolumeCellPredicate* pred)
{
	addGeometryPredicate(pred);
}

void AbstractDirectedSurfaceScorer::addScoringSurface(SurfaceCellPredicate* pred)
{
	addGeometryPredicate(pred);
}

void AbstractDirectedSurfaceScorer::removeScoringSurface(SurfaceCellPredicate* pred)
{
	removeGeometryPredicate(pred);
}

void AbstractDirectedSurfaceScorer::removeScoringRegionBoundary(VolumeCellPredicate* pred)
{
	removeGeometryPredicate(pred);
}

void AbstractDirectedSurfaceScorer::numericalAperture(float NA)
{
	m_NA = abs(NA);
}

float AbstractDirectedSurfaceScorer::numericalAperture() const
{
	return m_NA;
}

void AbstractDirectedSurfaceScorer::addGeometryPredicate(GeometryPredicate* pred)
{
	const auto it = find(m_outputCellPredicates.begin(),m_outputCellPredicates.end(),pred);
	if (it == m_outputCellPredicates.end())
		m_outputCellPredicates.push_back(pred);
	else
		LOG_WARNING << "AbstractDirectedSurfaceScorer::addGeometryPredicate(pred) failed because the predicate is already present" << endl;
}


void AbstractDirectedSurfaceScorer::removeGeometryPredicate(GeometryPredicate* pred)
{
	const auto it = find(m_outputCellPredicates.begin(),m_outputCellPredicates.end(),pred);
	if (it == m_outputCellPredicates.end())
		LOG_WARNING << "AbstractDirectedSurfaceScorer::removeGeometryPredicate(pred) failed because the predicate could not be found" << endl;
	else
		m_outputCellPredicates.erase(it);
}
