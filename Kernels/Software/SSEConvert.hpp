/*
 * SSEConvert.hpp
 *
 *  Created on: Feb 17, 2016
 *      Author: jcassidy
 */

#ifndef KERNELS_SOFTWARE_SSECONVERT_HPP_
#define KERNELS_SOFTWARE_SSECONVERT_HPP_

#include "FullMonteSW/Config.h"

//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
#include <mmintrin.h>
#else
#include "FullMonteSW/Geometry/NonSSE.hpp"
#endif
#include <array>

//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
inline std::array<float,4> as_array(__m128 x)
{
	std::array<float,4> o;
	_mm_storeu_ps(o.data(),x);
	return o;
}
#else
inline std::array<float,4> as_array(m128 x)
{
	return x;
}
#endif


#endif /* KERNELS_SOFTWARE_SSECONVERT_HPP_ */
