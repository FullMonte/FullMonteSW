/*
 * HenyeyGreenstein.hpp
 *
 *  Created on: Feb 17, 2016
 *      Author: jcassidy
 */

#ifndef KERNELS_SOFTWARE_HENYEYGREENSTEIN_HPP_
#define KERNELS_SOFTWARE_HENYEYGREENSTEIN_HPP_

#include "FullMonteSW/Config.h"

//#include <iostream>
//#include <fstream>
//#include <stdalign.h>

//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE

#include "avx_mathfun.h"
#include "FloatVectorBase.hpp"

#else
#include "FloatVectorBase_NonSSE.hpp"
#include <cmath>

#endif


/** Scalar Henyey-Greenstein function for cos(theta)
 *
 * @param	x		u [0,1) variable for which to calculate inverse
 * @param	g		[-1,1) anisotropy factor
 *
 * @return	cos(theta)
 */
float henyeyGreensteinDeflection(float g,float x);




/** Vector Henyey-Greenstein function, returning 8 4-element float vectors per invocation.
 * Each block of 4 output floats gives cos(theta) sin(theta) cos(phi) sin(phi) where theta is deflection and phi is azimuth.
 */
 
//#ifdef __ALTIVEC__ 
//optimized vector code for P8 goes here
#ifdef USE_SSE

/** Broadcasts one element of an __m128 to all elements of an __m256 */

template<unsigned I>inline __m256 broadcastElement8f(__m128 x)
{
	__m128 tmp = _mm_shuffle_ps(x,x,_MM_SHUFFLE(I,I,I,I));
	return _mm256_insertf128_ps(_mm256_castps128_ps256(tmp),tmp,1);
}

class HenyeyGreenstein8f : public FloatVectorBase
{
	public:
	typedef float					result_type;
	static constexpr std::size_t	OutputsPerInputBlock=8;		///< Outputs generated per invocation
	static constexpr std::size_t	OutputElementSize=4;		///< Number of output elements per input

	typedef uint32_t				input_type;
	static constexpr std::size_t	InputBlockSize=16;			///< Inputs consumed per invocation (8 azimuth, 8 deflection)

	template<class RNG>void calculate(RNG& rng,float* o) const;

	/// Get/set g parameter
	void gParam(float g);
	float gParam() const;

private:
	/// Convenience functions for accessing elements of the parameter vector by name
	inline __m256 g() const					{ return broadcastElement8f<0>(m_params);	}
	inline __m256 one_minus_gg() const		{ return broadcastElement8f<1>(m_params); 	}
	inline __m256 one_plus_gg() const		{ return broadcastElement8f<2>(m_params);	}
	inline __m256 recip_2g() const			{ return broadcastElement8f<3>(m_params);	}

	__m128		m_params;			// 0: g, 1: 1-g^2 , 2: 1+g^2, 3: 1/2g
	bool 		m_isotropic=false;	// true if g=0
};




/** Evaluate the Henyey-Greenstein function for random numbers, storing output in 32 (8x4) floats at o.
 */

template<class RNG>void HenyeyGreenstein8f::calculate(RNG& rng,float* o) const
{
	/// Calculate uniform-random azimuth angle
	__m256 az 	= _mm256_mul_ps(
			ui32ToU01(*(const __m256i*) rng.getBlock()),
			twopi());

	__m256 sinaz, cosaz;
	std::tie(sinaz,cosaz) = sincos_psp(az);

	__m256 pm1defl = ui32ToPM1(*(const __m256i*)rng.getBlock());
	__m256 cosdefl, sindefl;  

	if (!m_isotropic)
	{
		// t = 1-g^2 / (1+g*P)			where P is random [-1,1)
		__m256 t = _mm256_mul_ps(
						one_minus_gg(),
						_mm256_rcp_ps(										// use approximate reciprocal (error ~1e-11 from Intel)
								_mm256_add_ps(
										one(),
										_mm256_mul_ps(
												g(),
												pm1defl))));
		/*************************************************************************
		 * @brief The approximate reciprocal from Intel does not yield similar results
		 * compared to the NonSSE recipocal calcualtion (1/x). This should not pose 
		 * a problem and both results are equally valid. If you are expecting the 
		 * same results from the SSE and NonSSE versions of the code, these functions 
		 * are the reason for the diverging results. You can use the SSE reciprocal 
		 * calculation below to get similar results to the NonSSE code.
		 * 
		 * You must make the library mathfun available for the NonSSE part
		 * if you want to get the same results. 
		 ************************************************************************* 
		 
		__m256 t = _mm256_mul_ps(
						one_minus_gg(),
						_mm256_div_ps(
								one(),										
								_mm256_add_ps(
										one(),
										_mm256_mul_ps(
												g(),
												pm1defl))));
		 *************************************************************************
		 */

		cosdefl = _mm256_mul_ps(
						recip_2g(),
						_mm256_sub_ps(
								one_plus_gg(),
								_mm256_mul_ps(t,t)));

		// clip to [-1,1]
		cosdefl = _mm256_max_ps(
					_mm256_min_ps(cosdefl,one()),
					_mm256_sub_ps(zero(),one()));
	}
	else
		cosdefl = pm1defl;

	// compute sin x = sqrt(1-cos2 x)
	sindefl = _mm256_sqrt_ps(
				_mm256_sub_ps(
						one(),
						_mm256_mul_ps(cosdefl,cosdefl)));
	
	// interleave sine & cosine values independently
	__m256 sin_lo = _mm256_unpacklo_ps(sindefl,sinaz);
	__m256 sin_hi = _mm256_unpackhi_ps(sindefl,sinaz);

	__m256 cos_lo = _mm256_unpacklo_ps(cosdefl,cosaz);
	__m256 cos_hi = _mm256_unpackhi_ps(cosdefl,cosaz);

	// interleave sines & cosines and store to get cos(theta) sin(theta) cos(phi) sin(phi)
	_mm256_store_ps(o,   _mm256_unpacklo_ps(cos_lo,sin_lo));
	_mm256_store_ps(o+8, _mm256_unpackhi_ps(cos_lo,sin_lo));
	_mm256_store_ps(o+16,_mm256_unpacklo_ps(cos_hi,sin_hi));
	_mm256_store_ps(o+24,_mm256_unpackhi_ps(cos_hi,sin_hi));

	//std::ofstream outfile;
    //outfile.open("GreensteinSSE.txt", std::ios_base::app);
	//outfile << "Result sinaz: "<< sinaz[0]<< " " << sinaz[1] << " "<< sinaz[2] <<" "<< sinaz[3]<< " "<< sinaz[4]<< " " << sinaz[5] << " "<< sinaz[6] <<" "<< sinaz[7]<<std::endl;
	//outfile << "Result cosaz: "<< cosaz[0]<< " " << cosaz[1] << " "<< cosaz[2] <<" "<< cosaz[3]<< " "<< cosaz[4]<< " " << cosaz[5] << " "<< cosaz[6] <<" "<< cosaz[7]<<std::endl;
	//outfile << "Result cosdefl: "<< cosdefl[0]<< " " <<    cosdefl[1] << " "<<    cosdefl[2] <<" "<<   cosdefl[3]<< " "<<   cosdefl[4]<< " " <<    cosdefl[5] << " "<<   cosdefl[6] <<" "<<   cosdefl[7]<<std::endl;
	//outfile << "Result sindefl: "<< sindefl[0]<< " " <<    sindefl[1] << " "<<    sindefl[2] <<" "<<   sindefl[3]<< " "<<   sindefl[4]<< " " <<    sindefl[5] << " "<<   sindefl[6] <<" "<<   sindefl[7]<<std::endl;					
	//outfile << "Result cos_lo_sin_lo_interleave_lo: "<< o[0]<< " " <<    o[1] << " "<<    o[2] <<" "<<   o[3]<< " "<<   o[4]<< " " <<    o[5] << " "<<   o[6] <<" "<<   o[7]<<std::endl;
	//outfile << "Result cos_lo_sin_lo_interleave_hi: "<< o[8+0]<< " " <<  o[8+1] << " "<<  o[8+2] <<" "<< o[8+3]<< " "<< o[8+4]<< " " <<  o[8+5] << " "<< o[8+6] <<" "<< o[8+7]<<std::endl;
	//outfile << "Result cos_hi_sin_hi_interleave_lo: "<< o[16+0]<< " " << o[16+1] << " "<< o[16+2] <<" "<<o[16+3]<< " "<<o[16+4]<< " " << o[16+5] << " "<<o[16+6] <<" "<<o[16+7]<<std::endl;
	//outfile << "Result cos_hi_sin_hi_interleave_hi: "<< o[24+0]<< " " << o[24+1] << " "<< o[24+2] <<" "<<o[24+3]<< " "<<o[24+4]<< " " << o[24+5] << " "<<o[24+6] <<" "<<o[24+7]<<std::endl;
}

#else

class HenyeyGreenstein8f : public FloatVectorBase_NonSSE
{
	public:
	typedef float					result_type;
	static constexpr std::size_t	OutputsPerInputBlock=8;		///< Outputs generated per invocation
	static constexpr std::size_t	OutputElementSize=4;		///< Number of output elements per input

	typedef uint32_t				input_type;
	static constexpr std::size_t	InputBlockSize=16;			///< Inputs consumed per invocation (8 azimuth, 8 deflection)

	template<class RNG>void calculate(RNG& rng,float* o) const;

	/// Get/set g parameter
	void gParam(float g);
	float gParam() const;

private:

	inline std::array<float, 8> g() const				{ return {m_params[0], m_params[0], m_params[0], m_params[0], m_params[0], m_params[0], m_params[0], m_params[0]};	}
	inline std::array<float, 8> one_minus_gg() const	{ return {m_params[1], m_params[1], m_params[1], m_params[1], m_params[1], m_params[1], m_params[1], m_params[1]}; 	}
	inline std::array<float, 8> one_plus_gg() const		{ return {m_params[2], m_params[2], m_params[2], m_params[2], m_params[2], m_params[2], m_params[2], m_params[2]};	}
	inline std::array<float, 8> recip_2g() const		{ return {m_params[3], m_params[3], m_params[3], m_params[3], m_params[3], m_params[3], m_params[3], m_params[3]};	}

	std::array<float,4>		m_params;			// 0: g, 1: 1-g^2 , 2: 1+g^2, 3: 1/2g
	bool 		m_isotropic=false;	// true if g=0
};




/** Evaluate the Henyey-Greenstein function for random numbers, storing output in 32 (8x4) floats at o.
 */

template<class RNG>void HenyeyGreenstein8f::calculate(RNG& rng,float* o) const
{
	std::array<float, 8> az = zero();
	std::array<float, 8> U01 = ui32ToU01(*(const std::array<long long, 4>*) rng.getBlock());
	std::array<float, 8> _twopi = twopi();
	std::array<float, 8> sinaz = zero();
	std::array<float, 8> cosaz = zero();
	
	for(int i=0; i<8; i++)
	{
		az[i] = U01[i] * _twopi[i];
	}

	for(int i=0; i<8; i++)
	{
		sinaz[i] = sinf(az[i]);
		cosaz[i] = cosf(az[i]); // Comment out this loop if you want to use the SSE calucations for similar results.
	}

	/*******************************************************************************************
	 * @brief The SSE function sincos_psp does not provide the same results as sincosf (NonSSE)
	 * Occasionally, there is a small round-off error at one of the results which then lead to
	 * divergence in the end-result.
	 * This small change has a large impact on the end-result of the simulation, resulting in
	 * non-identical results between SSE and Non-SSE. Both results should still be valid, since this 
	 * is only a very small divergence in the RNG
	 * 
	 ********************************************************************************************
	 */
	//__m256 _sinaz, _cosaz;
	//std::tie(_sinaz,_cosaz) = sincos_psp(_mm256_setr_ps(az[0], az[1], az[2], az[3], az[4], az[5], az[6], az[7]));
	//_mm256_storeu_ps(sinaz.data(), _sinaz);
	//_mm256_storeu_ps(cosaz.data(), _cosaz);
	

	std::array<float, 8> pm1defl = ui32ToPM1(*(const std::array<long long, 4>*)rng.getBlock());
	std::array<float, 8> cosdefl, sindefl;
	
	float zero_sub_one = 0.0f - 1.0f;
	if (!m_isotropic)
	{

		// t = 1-g^2 / (1+g*P)			where P is random [-1,1)
		std::array<float, 8> t = zero();
		std::array<float, 8> recip = zero();
		for(int i =0; i<8; i++)
			recip[i] = one()[i] + (g()[i] * pm1defl[i]);
		
		/*******************************************************************************************
	 	 * @brief The SSE function _mm256_rcp_ps does not provide the same results as the reciprocal (1/x) in
	 	 * NonSSE Occasionally, there is a significant round-off error at the results which then lead to
	 	 * divergence in the end-result.
	 	 * This change has a large impact on the end-result of the simulation, resulting in
	 	 * non-identical results between SSE and Non-SSE. Both results should still be valid, since this 
		 * is only a very small divergence in the RNG
		 * 
		 * There us currently an option to not use the SSE reciprocal function but just calculate
		 * the reciprocal by hand through an SSE intrinsic for division (1/x). Then, the divergence
		 * is again very small
	 	 ********************************************************************************************
		 */
		//__m256 rec = _mm256_rcp_ps(_mm256_setr_ps(recip[0], recip[1], recip[2], recip[3], recip[4], recip[5], recip[6], recip[7]));
		//_mm256_storeu_ps(recip.data(), rec);
		
		
		for(int i=0; i<8; i++)
		{
			// If you want NonSSE and SSE to  produce the exact same results, uncomment the two above lines
			// and delete '1.0f/' below
			t[i] = one_minus_gg()[i] * 1.0f/recip[i]; 
			cosdefl[i] = recip_2g()[i] * (one_plus_gg()[i] - (t[i] * t[i]));
			
			// clip to [-1,1]
			float min;
			if(cosdefl[i] < 1.0f)
				min = cosdefl[i];
			else
				min = 1.0f;

			if (min < zero_sub_one)
				cosdefl[i] = zero_sub_one;
			else
				cosdefl[i] = min;
		}
	}
	else
	{
		cosdefl = pm1defl;
	}

	// compute sin x = sqrt(1-cos2 x)
	for(int i=0; i<8; i++)
	{
		sindefl[i] = sqrt(1.0f - (cosdefl[i]*cosdefl[i]));
	}

	// interleave sine & cosine values independently
	std::array<float, 8> sin_lo = {sindefl[0], sinaz[0], sindefl[1], sinaz[1], sindefl[4], sinaz[4], sindefl[5], sinaz[5]};
	std::array<float, 8> sin_hi = {sindefl[2], sinaz[2], sindefl[3], sinaz[3], sindefl[6], sinaz[6], sindefl[7], sinaz[7]};
	
	std::array<float, 8> cos_lo = {cosdefl[0], cosaz[0], cosdefl[1], cosaz[1], cosdefl[4], cosaz[4], cosdefl[5], cosaz[5]};
	std::array<float, 8> cos_hi = {cosdefl[2], cosaz[2], cosdefl[3], cosaz[3], cosdefl[6], cosaz[6], cosdefl[7], cosaz[7]};
	
	// interleave sines & cosines and store to get cos(theta) sin(theta) cos(phi) sin(phi)
	std::array<float, 8> cos_lo_sin_lo_interleave_lo = {cos_lo[0], sin_lo[0], cos_lo[1], sin_lo[1], cos_lo[4], sin_lo[4], cos_lo[5], sin_lo[5]};
	std::array<float, 8> cos_lo_sin_lo_interleave_hi = {cos_lo[2], sin_lo[2], cos_lo[3], sin_lo[3], cos_lo[6], sin_lo[6], cos_lo[7], sin_lo[7]};
	std::array<float, 8> cos_hi_sin_hi_interleave_lo = {cos_hi[0], sin_hi[0], cos_hi[1], sin_hi[1], cos_hi[4], sin_hi[4], cos_hi[5], sin_hi[5]};
	std::array<float, 8> cos_hi_sin_hi_interleave_hi = {cos_hi[2], sin_hi[2], cos_hi[3], sin_hi[3], cos_hi[6], sin_hi[6], cos_hi[7], sin_hi[7]};
	
	std::copy(cos_lo_sin_lo_interleave_lo.begin(), cos_lo_sin_lo_interleave_lo.end(), o);
	std::copy(cos_lo_sin_lo_interleave_hi.begin(), cos_lo_sin_lo_interleave_hi.end(), o+8);
	std::copy(cos_hi_sin_hi_interleave_lo.begin(), cos_hi_sin_hi_interleave_lo.end(), o+16);
	std::copy(cos_hi_sin_hi_interleave_hi.begin(), cos_hi_sin_hi_interleave_hi.end(), o+24);

	//std::ofstream outfile;
	//outfile.open("GreensteinNonSSE.txt", std::ios_base::app);
	//outfile << "Result sinaz: "<< sinaz[0]<< " " << sinaz[1] << " "<< sinaz[2] <<" "<< sinaz[3]<< " "<< sinaz[4]<< " " << sinaz[5] << " "<< sinaz[6] <<" "<< sinaz[7]<<std::endl;
	//outfile << "Result cosaz: "<< cosaz[0]<< " " << cosaz[1] << " "<< cosaz[2] <<" "<< cosaz[3]<< " "<< cosaz[4]<< " " << cosaz[5] << " "<< cosaz[6] <<" "<< cosaz[7]<<std::endl;					
	//outfile << "Result cosdefl: "<< cosdefl[0]<< " " << cosdefl[1] << " "<< cosdefl[2] <<" "<< cosdefl[3]<< " "<< cosdefl[4]<< " " << cosdefl[5] << " "<< cosdefl[6] <<" "<< cosdefl[7]<<std::endl;
	//outfile << "Result sindefl: "<< sindefl[0]<< " " << sindefl[1] << " "<< sindefl[2] <<" "<< sindefl[3]<< " "<< sindefl[4]<< " " << sindefl[5] << " "<< sindefl[6] <<" "<< sindefl[7]<<std::endl;					
	//outfile << "Result cos_lo_sin_lo_interleave_lo: "<< o[0]<< " " <<    o[1] << " "<<    o[2] <<" "<<   o[3]<< " "<<   o[4]<< " " <<    o[5] << " "<<   o[6] <<" "<<   o[7]<<std::endl;
	//outfile << "Result cos_lo_sin_lo_interleave_hi: "<< o[8+0]<< " " <<  o[8+1] << " "<<  o[8+2] <<" "<< o[8+3]<< " "<< o[8+4]<< " " <<  o[8+5] << " "<< o[8+6] <<" "<< o[8+7]<<std::endl;
	//outfile << "Result cos_hi_sin_hi_interleave_lo: "<< o[16+0]<< " " << o[16+1] << " "<< o[16+2] <<" "<<o[16+3]<< " "<<o[16+4]<< " " << o[16+5] << " "<<o[16+6] <<" "<<o[16+7]<<std::endl;
	//outfile << "Result cos_hi_sin_hi_interleave_hi: "<< o[24+0]<< " " << o[24+1] << " "<< o[24+2] <<" "<<o[24+3]<< " "<<o[24+4]<< " " << o[24+5] << " "<<o[24+6] <<" "<<o[24+7]<<std::endl;
}
#endif


#endif /* KERNELS_SOFTWARE_HENYEYGREENSTEIN_HPP_ */
