/*
 * FloatUVect2Distribution.hpp
 *
 *  Created on: Feb 17, 2016
 *      Author: jcassidy
 */

#ifndef KERNELS_SOFTWARE_FLOATUVECT2DISTRIBUTION_HPP_
#define KERNELS_SOFTWARE_FLOATUVECT2DISTRIBUTION_HPP_

#include "FullMonteSW/Config.h"
//#include <iostream>
//#include <fstream>
//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
#include "FloatVectorBase.hpp"
#include "avx_mathfun.h"
#else
#include "FloatVectorBase_NonSSE.hpp"
#endif

/** 2D unit vector distribution returning 2 floats per draw.
 *
 */
//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
class FloatUVect2Distribution : public FloatVectorBase
{
public:
	typedef float					result_type;
	static constexpr std::size_t	OutputsPerInputBlock=8;		///< Outputs generated per invocation
	static constexpr std::size_t	OutputElementSize=2;		///< Number of output elements per input

	typedef uint32_t				input_type;
	static constexpr std::size_t	InputBlockSize=8;			///< Inputs consumed per invocation

	template<class RNG>void calculate(RNG& rng,float *dst)
	{
		const uint32_t* rp = rng.getBlock();

		__m256i r = _mm256_load_si256((const __m256i*)rp);

		__m256 theta = _mm256_mul_ps(						// theta = 2*pi*u01
				twopi(),
				_mm256_sub_ps(									// [1,2) - 1.0 -> [0,1)
						_mm256_or_ps(
								_mm256_castsi256_ps(
#ifdef HAVE_AVX2
										_mm256_srli_epi32(r,9)
#else
										_EMU_mm256_srli_epi32(r,9)
#endif
								),
								ldexp(0)),
						one()
				)
				);

		__m256 costheta, sintheta;
		std::tie(sintheta,costheta) = sincos_psp(theta);

		

		// swizzle (sin_0,cos_0,sin_1,cos_1,...)
		_mm256_store_ps(dst,	_mm256_unpacklo_ps(sintheta,costheta));
		_mm256_store_ps(dst+8,	_mm256_unpackhi_ps(sintheta,costheta));

		//std::ofstream outfile;
    	//outfile.open("FloatUVectSSE.txt", std::ios_base::app);
		//outfile << "Result mantissa: "<< rand_mantissa[0]<< " " << rand_mantissa[1] << " "<< rand_mantissa[2] <<" "<< rand_mantissa[3]<<std::endl;
		//outfile << "Result lo: "<< dst[0]<< " " << dst[1] << " "<< dst[2] <<" "<< dst[3]<< " "<< dst[4]<< " " << dst[5] << " "<< dst[6] <<" "<< dst[7]<<std::endl;
		//outfile << "Result hi: "<< dst[8+0]<< " " << dst[8+1] << " "<< dst[8+2] <<" "<< dst[8+3]<< " "<< dst[8+4]<< " " << dst[8+5] << " "<< dst[8+6] <<" "<< dst[8+7]<<std::endl;

	}
};
#else
class FloatUVect2Distribution : public FloatVectorBase_NonSSE
{
public:
	typedef float					result_type;
	static constexpr std::size_t	OutputsPerInputBlock=8;		///< Outputs generated per invocation
	static constexpr std::size_t	OutputElementSize=2;		///< Number of output elements per input

	typedef uint32_t				input_type;
	static constexpr std::size_t	InputBlockSize=8;			///< Inputs consumed per invocation

	template<class RNG>void calculate(RNG& rng,float *dst)
	{
		const uint32_t* rp = rng.getBlock();

		std::array<long long, 4> r;
		std::copy((long long*) rp, (long long*) rp+4, r.data());

		std::array<long long, 4> rand_mantissa = _NonSSE_mm256_srli_epi32(r,9); // random mantissa [1,2) due to implicit leading 1
        float *float_cast = (float*) rand_mantissa.data();
        std::array<float,8> _ldexp = zero();
		_ldexp = ldexp(0); // set exp s.t. [1,2)
        std::array<float,8> _one = one();
		std::array<float,8> _twopi = twopi();
        std::array<float,8> result_or = zero();
		std::array<float,8> theta = zero();

        unsigned char *c1 = reinterpret_cast<unsigned char *>(float_cast);
        unsigned char *c2 = reinterpret_cast<unsigned char *>(_ldexp.data());
        std::array<unsigned char,4*8> c = {'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0'};
        for(int j=0; j<4*8; j++)
        {
            c[j] = c1[j] | c2[j];
        }
        
		float f = 0;
		for(int i=0; i<8; i++)
		{
		memcpy(&f, c.data()+4*i, sizeof(f));
		result_or[i] = f;
		//result_or = (float*) c.data();
		}

		std::array<float,8> costheta = zero();
		std::array<float,8> sintheta = zero();
        for(int i=0; i<8; i++)
        {
            theta[i] = _twopi[i] * (result_or[i] - _one[i]);
			//sincosf(theta, &sintheta, &costheta);
			//sintheta[i] = sinf(theta);
			//costheta[i] = cosf(theta);
        }
	for(int i=0; i<8; i++)
	{
		sintheta[i] = sinf(theta[i]);
		costheta[i] = cosf(theta[i]);
	}

		/*******************************************************************************************
	 	 * @brief The SSE function sincos_psp does not provide the same results as sincosf (NonSSE)
		 * Occasionally, there is a small round-off error at one of the results which then lead to
		 * divergence in the end-result.
		 * This small change has a large impact on the end-result of the simulation, resulting in
		 * non-identical results between SSE and Non-SSE. Both results should still be valid, since this 
		 * is only a very small divergence in the RNG
		 * 
		 ********************************************************************************************
		 */
		//__m256 _sintheta, _costheta;
		//std::tie(_sintheta,_costheta) = sincos_psp(_mm256_setr_ps(theta[0], theta[1], theta[2], theta[3], theta[4], theta[5], theta[6], theta[7]));
		//_mm256_storeu_ps(sintheta.data(), _sintheta);
		//_mm256_storeu_ps(costheta.data(), _costheta);

		// swizzle (sin_0,cos_0,sin_1,cos_1,...)
		std::array<float, 8> cos_lo_sin_lo_interleave_lo = {sintheta[0], costheta[0], sintheta[1], costheta[1], sintheta[4], costheta[4], sintheta[5], costheta[5]};
		std::array<float, 8> cos_lo_sin_lo_interleave_hi = {sintheta[2], costheta[2], sintheta[3], costheta[3], sintheta[6], costheta[6], sintheta[7], costheta[7]};
		
		std::copy(cos_lo_sin_lo_interleave_lo.begin(), cos_lo_sin_lo_interleave_lo.end(), dst);
		std::copy(cos_lo_sin_lo_interleave_hi.begin(), cos_lo_sin_lo_interleave_hi.end(), dst+8);

		//std::ofstream outfile;
    	//outfile.open("FloatUVectNonSSE.txt", std::ios_base::app);
		//outfile << "Result mantissa: "<< rand_mantissa[0]<< " " << rand_mantissa[1] << " "<< rand_mantissa[2] <<" "<< rand_mantissa[3]<<std::endl;
		//outfile << "Result lo: "<< dst[0]<< " " << dst[1] << " "<< dst[2] <<" "<< dst[3]<< " "<< dst[4]<< " " << dst[5] << " "<< dst[6] <<" "<< dst[7]<<std::endl;
		//outfile << "Result hi: "<< dst[8+0]<< " " << dst[8+1] << " "<< dst[8+2] <<" "<< dst[8+3]<< " "<< dst[8+4]<< " " << dst[8+5] << " "<< dst[8+6] <<" "<< dst[8+7]<<std::endl;
	}
};
#endif




#endif /* KERNELS_SOFTWARE_FLOATUVECT2DISTRIBUTION_HPP_ */
