#ifndef PACKET_INCLUDED
#define PACKET_INCLUDED

#include <FullMonteSW/Config.h>

//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
#include <immintrin.h>
#else

#endif


#include "PacketDirection.hpp"


/** Represents a packet at the moment of launch, without a step length (not yet drawn) or weight (implicitly 1.0) */

struct LaunchPacket
{
	LaunchPacket(){}

	PacketDirection		dir;

    SSE::Point3			pos;

	unsigned			element;
};



class Packet {
public:
	PacketDirection dir;
//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
    __m128 p;     		///< Position [x, y, z, 0]
    __m128 oldP;        // Position pre-hop. Used in checking for packet intersection with cylindrical detectors.
    __m128 s;           ///< Dimensionless step length remaining (?)
#else
    m128 p;     		///< Position [x, y, z, 0]
    m128 oldP;
    m128 s;           ///< Dimensionless step length remaining (?)
#endif
    float w=1.0f;     	///< Packet weight

public:

    Packet(const LaunchPacket& lp) :
    	dir(lp.dir),
		p(lp.pos),
        oldP(lp.pos)
    {}

    float weight() const { return w; }

    SSE::UnitVector3 direction() const { return dir.d; }

    Packet& operator=(const Packet& p_) = default;
};

#endif
