#ifndef SSEMATH_INCLUDED
#define SSEMATH_INCLUDED

#include <FullMonteSW/Config.h>

//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
#include <smmintrin.h>
#else
#include "FullMonteSW/Geometry/NonSSE.hpp"
#endif

#include <boost/range/algorithm.hpp>
#include <array>
#include <limits>

#include <cassert>
#include <boost/static_assert.hpp>

#include <FullMonteSW/Warnings/Push.hpp>
#include <FullMonteSW/Warnings/Boost.hpp>

#include <FullMonteSW/Warnings/Pop.hpp>

#include <cmath>

namespace SSE {

class SSEBase
{
protected:
	SSEBase(){}
//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
	SSEBase(__m128 v) : m_v(v){}
#else
	SSEBase(m128 v) : m_v(v){}
#endif

public:

	SSEBase abs() const
	{
//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
		__m128i sign = _mm_set1_epi32(0x7fffffff);
		return _mm_and_ps(__m128(sign),m_v);
#else
        m128 result;
        for(int i=0; i<4; ++i)
        {
            result[i] = fabs(m_v[i]);
        }
		return result;
#endif
	}

//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
	explicit operator __m128() const { return m_v; }
#else
	explicit operator m128() const { return m_v; }
#endif

	float operator[](unsigned i) const
	{
//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
		float f[4];
		_mm_store_ps(f,m_v);
		return f[i];
#else
		return m_v[i];
#endif	
	}

	static SSEBase undef()
	{
//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
		return SSEBase(_mm_set1_ps(std::numeric_limits<float>::quiet_NaN()));
#else
		return SSEBase(set1_ps128(std::numeric_limits<float>::quiet_NaN()));
#endif
	}

	static SSEBase zero()
	{
//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
		return SSEBase(_mm_setzero_ps());
#else
		return SSEBase(set_zero128());
#endif
	}

	static SSEBase one()
	{
//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
		return SSEBase(_mm_set_ss(1.0f));
#else
		m128 one = {1.0f, 0.0f, 0.0f, 0.0f};
        return SSEBase(one);
#endif
	}

	static SSEBase ones()
	{
//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
		return SSEBase(_mm_set1_ps(1.0f));
#else
		return SSEBase(set1_ps128(1.0f));
#endif
	}

	bool isnan() const
	{
#ifdef USE_SSE
		return _mm_movemask_ps(m_v != m_v);
#else
		for (unsigned i=0; i>4; i++)
		{
			if (std::isnan(m_v[i]))
				return true;
		}
		return false;
#endif
	}


protected:
//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
	__m128 m_v;
#else
	m128 m_v;
#endif
};

class Scalar : public SSEBase
{
public:
	

//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
	explicit Scalar(__m128 s) 	: SSEBase(s){}
	Scalar() : SSEBase(_mm_setzero_ps()){}
#else
	explicit Scalar(m128 s) 	: SSEBase(s){}
	Scalar() : SSEBase(set_zero128()){}
#endif

//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
/// Fill a Scalar using the bottom element (useful when extending the output of _ss functions)
template<unsigned I=0>static Scalar fill(__m128 s)
	{
		return Scalar(_mm_shuffle_ps(s,s,_MM_SHUFFLE(I,I,I,I)));
	}		
#else
template<unsigned I=0>static Scalar fill(m128 s)
	{
		return Scalar(shuffle128(s,s,I,I,I,I));

	}
#endif

//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
	explicit Scalar(float s) 	: SSEBase(_mm_set1_ps(s)){}
#else
	explicit Scalar(float s) 	: SSEBase(set1_ps128(s)){}
#endif

	explicit operator float() const
	{
//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
		return _mm_cvtss_f32(m_v);
#else
		return m_v[0];
#endif
	}

	static Scalar sqrt(Scalar s)
	{
//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
		return Scalar(_mm_sqrt_ps(s.m_v));
#else
		m128 result;
		for(int i=0; i<4; ++i)
        {
            result[i] = std::sqrt(s.m_v[i]); 
        }
		return Scalar(result);
#endif
	}

protected:
	Scalar(SSEBase s) : SSEBase(s){}
};

template<std::size_t D>class Vector : public SSEBase
{

public:
	Vector(){}
	
//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
	explicit Vector(__m128 v) : SSEBase(v){}
	explicit Vector(const float* p) : SSEBase(_mm_loadu_ps(p)){}		///< Construct from unaligned pointer
#else
	explicit Vector(m128 v) : SSEBase(v){}
	explicit Vector(const float* p) : SSEBase(loadu_ps128(p)){}
#endif

	template<typename T>explicit Vector(std::array<T,D> a,
			typename std::enable_if< std::is_floating_point<T>::value, int>::type=0)
	{
		float f[4];
		unsigned i;
		for(i=0;i<D;++i)
			f[i]=a[i];
		for(;i<4;++i)
			f[i]=0.0f;
//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
		m_v = _mm_load_ps(f);
#else
		m_v = load_ps128(f);
#endif
	}

	/** Basic arithmetic */

	Vector operator-(const Vector rhs) const
	{
//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
		return Vector(_mm_sub_ps(m_v,rhs.m_v));
#else
		return Vector(sub128(m_v,rhs.m_v));
#endif
	}

	Vector operator+(const Vector rhs) const
	{
//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
		return Vector(_mm_add_ps(m_v,rhs.m_v));
#else
		return Vector(add128(m_v,rhs.m_v));
#endif
	}

	Vector operator*(const Scalar rhs) const
	{
//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
		return Vector(_mm_mul_ps(m_v,__m128(rhs)));
#else
		return Vector(mult128(m_v,m128(rhs)));
#endif
	}

	Vector operator/(const Scalar rhs) const
	{
//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
		return Vector(_mm_div_ps(m_v,__m128(rhs)));
#else
		return Vector(div128(m_v,m128(rhs)));
#endif
	}

	static Scalar dot(Vector lhs,Vector rhs)
	{
		// for i [0,4)
		// 	mask[i+4] == 1 -> include lhs[i]*rhs[i]
		// 	mask[i]   == 1 -> o[i] = dot(lhs,rhs)
//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
		return Scalar(_mm_dp_ps(lhs.m_v,rhs.m_v,(mask<<4) | 0x7));
#else
		return Scalar(dp_ps128(lhs.m_v,rhs.m_v,(mask<<4) | 0x7));
#endif
	}

	// Check for unit vector
	template<std::size_t N>bool vec_isunit(float tol2) const
	{
#ifdef USE_SSE
		// upper 4 bits: inclusion mask
		// lower 4 bits: result-destination mask
		const int mask = (((1<<N)-1)<<4) | 0x1;
		__m128 res = _mm_sub_ps(_mm_dp_ps(m_v,m_v,mask),_mm_set1_ps(1.0));
		__m128 tol2v = _mm_set1_ps(tol2);

		return _mm_movemask_ps(res < tol2v) & _mm_movemask_ps(-res < tol2v) & 0x1;
#else
		Scalar d = dot(*this,*this);
		return std::abs(d[0]-1.0) < tol2;
#endif
	}

	std::array<float,D> array() const
	{
		float f[4];
//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
		_mm_store_ps(f,m_v);
#else
		for(int i=0; i<4; ++i)
        {
            f[i] = m_v[i];
        }
#endif
		return *reinterpret_cast<std::array<float,3>*>(f);
	}

	/// Returns the index of the smallest element in terms of absolute value
	unsigned indexOfSmallestElement() const
	{
		const std::array<float,D> d = array();
		std::array<float,D> du;

		boost::transform(d, du.begin(), [](float i){ return std::abs(i); });
		return boost::min_element(du)-du.begin();

	}

	/// Return i'th component
	template<unsigned i>Scalar component()
	{
		BOOST_STATIC_ASSERT(i < D);
//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
		return Scalar(_mm_shuffle_ps(m_v,m_v,_MM_SHUFFLE(i,i,i,i)));
#else
		return Scalar(shuffle128(m_v,m_v,i,i,i,i));
#endif
	}


	static Vector undef(){ return SSEBase::undef(); }
	static Vector zero() { return SSEBase::zero(); 	}

	using SSEBase::isnan;

protected:
	using SSEBase::m_v;
	explicit Vector(SSEBase v) : SSEBase(v){}
	static constexpr int 	mask = (1<<D)-1;			// Mask indicating which elements are occupied

	friend Vector<3> cross(Vector<3>,Vector<3>);
};



using Vector3 = Vector<3>;
using Vector2 = Vector<2>;

// u = (a,b,c)   v = (d,e,f)
// cross product is (bf-ce, cd-af, ae-bd)

inline Vector3 cross(Vector3 u,Vector3 v)
{
//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
    __m128 bca = _mm_shuffle_ps(u.m_v,u.m_v,_MM_SHUFFLE(3,0,2,1));
    __m128 fde = _mm_shuffle_ps(v.m_v,v.m_v,_MM_SHUFFLE(3,1,0,2));

    __m128 bf_cd_ae = _mm_mul_ps(bca,fde);

    __m128 cab = _mm_shuffle_ps(u.m_v,u.m_v,_MM_SHUFFLE(3,1,0,2));
    __m128 efd = _mm_shuffle_ps(v.m_v,v.m_v,_MM_SHUFFLE(3,0,2,1));

    __m128 ec_af_bd = _mm_mul_ps(efd,cab);

    return Vector3(_mm_sub_ps(bf_cd_ae,ec_af_bd));
#else
	m128 bca = shuffle128(u.m_v,u.m_v,3,0,2,1);
    m128 fde = shuffle128(v.m_v,v.m_v,3,1,0,2);

    m128 bf_cd_ae = mult128(bca,fde);

    m128 cab = shuffle128(u.m_v,u.m_v,3,1,0,2);
    m128 efd = shuffle128(v.m_v,v.m_v,3,0,2,1);

    m128 ec_af_bd = mult128(efd,cab);

    return Vector3(sub128(bf_cd_ae,ec_af_bd));
#endif
}

template<std::size_t D>inline Scalar norm2(Vector<D> v)
{
	return dot(v,v);
}

template<std::size_t D>inline Scalar norm(Vector<D> v)
{
	return Scalar(sqrt(float(dot(v,v))));
}

template<std::size_t D>inline Scalar dot(Vector<D> lhs,Vector<D> rhs)
{
	return Vector<D>::dot(lhs,rhs);
}


enum Checking { NoCheck, Assert, Except, Normalize, Silent };

template<std::size_t D>class UnitVector : public Vector<D>
{
public:
	UnitVector(const UnitVector&) = default;

	template<typename FT>UnitVector(std::array<FT,D> a,Checking c) :
		Vector<D>(a)
		{
			check(c);
		}

	explicit UnitVector() : Vector<D>(SSEBase::undef()){}

	UnitVector(Vector<D> v,Checking c) : Vector<D>(v)
	{
		check(c);
	}

	using Vector<D>::isnan;

	using Vector<D>::operator+;
	using Vector<D>::operator-;
	using Vector<D>::operator*;
	using Vector<D>::operator/;

	using Vector<D>::dot;

	static UnitVector normalize(Vector<D> v)
	{
		Scalar norm2 = dot(v,v);
//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
		__m128 k = _mm_sqrt_ss(__m128(norm2));

		return SSE::UnitVector<D>(_mm_div_ps(__m128(v),_mm_shuffle_ps(k,k,_MM_SHUFFLE(0,0,0,0))));
#else
		m128 k = sqrt_ss128(m128(norm2));

		return SSE::UnitVector<D>(div128(m128(v),shuffle128(k,k,0,0,0,0)));
#endif
	}

	static UnitVector normalize_approx(Vector<D> v)
	{
		Scalar norm2 = dot(v,v);
//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
		__m128 k = _mm_rsqrt_ss(norm2.m_v);

		return _mm_mul_ps(_mm_shuffle_ps(k,k,_MM_SHUFFLE(0,0,0,0)),v.m_v);
#else
		m128 k = rsqrt_ss(norm2.m_v);

		return mult128(shuffle128(k,k,0,0,0,0),v.m_v);
#endif
	}

	bool check(Checking c,float eps=1e-4)
	{
		if (c == NoCheck)
			return true;

		float e = std::abs(float(Scalar::sqrt(dot(*this,*this)))-1.0f);

		switch(c)
		{
		case Assert:
			assert(e < eps);
			break;
		case Except:
			if (e > eps)
				throw std::logic_error("Non-unit vector in UnitVector::check()");
			break;
		case Normalize:
			*this = normalize(*this);
		default:
			break;
		}

		return e < eps;
	}


	/** Return the i'th basis vector (v[i]=1.0, v[j != i]=0.0) */
	template<unsigned i>static UnitVector basis()
	{
//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
		__m128 one = _mm_set_ss(1.0f);

		BOOST_STATIC_ASSERT(i < D);

		return UnitVector(_mm_shuffle_ps(one,one,_MM_SHUFFLE(i!=3, i!=2, i!=1, i!=0)));
#else
		m128 one = {1.0f, 0.0f, 0.0f, 0.0f};

		BOOST_STATIC_ASSERT(i < D);

		return UnitVector(shuffle128(one,one,i!=3, i!=2, i!=1, i!=0));
#endif
	}

	static UnitVector basis(unsigned i)
	{
		float f[4]{0.0,0.0,0.0,0.0};
		f[i]=1.0f;
		assert(i < D);
//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
		return _mm_load_ps(f);
#else
		m128 result = {f[0], f[1], f[2], f[3]};
		return result;
#endif
	}

public:
//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
	explicit UnitVector(__m128 v) : Vector<D>(v){}
	
#else
	explicit UnitVector(m128 v) : Vector<D>(v){}
#endif
	explicit UnitVector(std::array<float,D> v) : Vector<D>(v){}
	explicit UnitVector(Vector<D> v) : Vector<D>(v){}		// protect because this waives checking or normalization
	
};

using UnitVector3 = UnitVector<3>;
using UnitVector2 = UnitVector<2>;

using Point3 = Vector<3>;
using Point2 = Vector<2>;



/** Returns a pair of vectors orthonormal to eachother and to v */

std::pair<UnitVector3,UnitVector3> normalsTo(UnitVector3 v);

template<std::size_t D>UnitVector<D> normalize(Vector<D> v)
{
	return UnitVector<D>::normalize(v);
}

template<std::size_t D>float normSquared(Vector<D> v)
{
	return float(dot(v,v));
}

};

#endif // SSEMATH_INCLUDED
