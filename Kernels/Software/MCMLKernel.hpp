/*
 * MCMLKernel.hpp
 *
 *  Created on: Jun 1, 2017
 *      Author: jcassidy
 */

#ifndef KERNELS_SOFTWARE_MCMLKERNEL_HPP_
#define KERNELS_SOFTWARE_MCMLKERNEL_HPP_

#include "TetraMCKernel.hpp"
#include "RNG_SFMT_AVX.hpp"

#include "TetraMCKernelThread.hpp"

#include "Logger/LoggerTuple.hpp"
#include "Logger/CylAbsorption.hpp"
#include "Logger/ConservationScorer.hpp"
#include "Logger/EventScorer.hpp"
#include "Logger/PathScorer.hpp"

#include "Logger/AtomicMultiThreadAccumulator.hpp"
#include "Logger/QueuedMultiThreadAccumulator.hpp"

typedef std::tuple<
		EventScorer,
		ConservationScorer,
		CylAbsorptionScorer<AtomicMultiThreadAccumulator<double,double>>
		>
		MCMLScorerPack;

typedef std::tuple<
		EventScorer,
		ConservationScorer,
		CylAbsorptionScorer<QueuedMultiThreadAccumulator<double,double>>
		>
		MCMLScorerPackQ;

float surfaceTransmission(const Kernel* K);
float specularReflectance(const Kernel* K);
float specularReflectance(float n0,float n1);

class MCMLCase;
float specularReflectance(const MCMLCase* K);

class MCMLKernel : public TetraMCKernel<RNG_SFMT_AVX,MCMLScorerPack>
{
public:
	MCMLKernel()
	{
		rouletteWMin(1e-4);
	}

	CylAbsorptionScorer<AtomicMultiThreadAccumulator<double,double>>& cylAbsorptionScorer(){ return get<2>(m_scorer); }
};

class MCMLKernelQ : public TetraMCKernel<RNG_SFMT_AVX,MCMLScorerPackQ>
{
public:
	MCMLKernelQ()
	{
		rouletteWMin(1e-4);
	}

	CylAbsorptionScorer<QueuedMultiThreadAccumulator<double,double>>& cylAbsorptionScorer(){ return get<2>(m_scorer); }
};


typedef std::tuple<
		EventScorer,
		ConservationScorer,
		CylAbsorptionScorer<AtomicMultiThreadAccumulator<double,double>>,
		PathScorer
		>
		MCMLScorerPackWithTraces;

class MCMLKernelWithTraces : public TetraMCKernel<RNG_SFMT_AVX,MCMLScorerPackWithTraces>
{
public:
	MCMLKernelWithTraces()
	{
		rouletteWMin(1e-4);
	}

};



#endif /* KERNELS_SOFTWARE_MCMLKERNEL_HPP_ */
