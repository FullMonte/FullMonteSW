/*
 ** FiberEmitter.hpp
 **
 **  Created on: April 15th , 2018
 **      Author: Yasmin Afsharnejad
 */

/*
   FiberEmitter builds a cut-end fiber and generates the position and direction of photons emitted from the fiber.
   The fiber end has a non-zero radius. 
   The fiber direction (x, y, z components of direction vector) should be specified. This code will normalize any arbitrary direction vector.
   Position of center of fiber end (x,y,z) should also be specified.
   Numerical aperture (NA) of the fiber should be specified. The light source will emit uniformly through a cone within an angle alpha of the direction vector of the fiber end, where alpha = Sin-1 (NA)

   The FiberCone class has two methods called "direction" and "position". 
   The direction method genrates the direction of launch packets emitting from the fiber. 
   The position method generates the position of launch packets emitting from the fiber. 

 */


#ifndef FIBEREMITTER_HPP_
#define FIBEREMITTER_HPP_

#include <FullMonteSW/Logging/FullMonteLogger.hpp>
#include <FullMonteSW/Geometry/Vector.hpp>
#include <FullMonteSW/Kernels/Software/SSEMath.hpp>
#include <iomanip>
#include <FullMonteSW/Geometry/UnitVector.hpp>

#include <FullMonteSW/Geometry/Queries/TetraEnclosingPointByLinearSearch.hpp>
#include <FullMonteSW/Kernels/Software/Emitters/SpinMatrix.hpp>
#include <FullMonteSW/Geometry/TetraMesh.hpp>
#include <FullMonteSW/Geometry/MaterialSet.hpp>
#include <FullMonteSW/Geometry/Material.hpp>
#include "TetraLookupCache.hpp"


#include <iostream>
#include <array>
#include <math.h>
#include <cmath>
#include <stdio.h>

//BOOST libraries
#include <boost/random/mersenne_twister.hpp>
#include <boost/align/aligned_allocator.hpp>
#include <boost/range.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/adaptor/indexed.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/random/variate_generator.hpp>
#include <random>

#include <mutex>
#include <thread>

using namespace std;
#define PI 3.14159265

namespace Emitter
{

	template<class RNG>class FiberCone 
	{
		private:


		public:
			//FiberCone class gets numericalAperture, fiber direction, radius of fiber-end and position of center of fiber end as input. 	

			FiberCone(
                float numAperture,
                std::array<float,3>fiberDirection,
                float fiberEndRadius,
                std::array<float,3>fiberEndPosition,
                TetraMesh::TetraDescriptor fiberEndElementId,
                const MaterialSet* materialset,
				const TetraMesh* mesh,
				TetraMesh::TetraMeshRTree * rtree) :
			        m_NA{numAperture},
                    fiberDir{fiberDirection},
                    fiberRadius{fiberEndRadius},
                    fiberPos{fiberEndPosition},
					m_materialset{materialset},
                    m_mesh{mesh},
                    endpoint_Id{fiberEndElementId}
			{
                // assign the RTree object of the TetraMesh to the local RTree object
				m_rtree = rtree;

				// get the refractive index of the material where the point is located in
				unsigned region_index = get(region,*m_mesh, endpoint_Id);
				m_refractiveIndex = new float(m_materialset->get(region_index)->refractiveIndex());

				//normalize the fiber direction array given from input
				float normDir ;
				double accum = 0.;

				for (unsigned i = 0; i < fiberDir.size(); ++i) {
					accum += fiberDir[i] * fiberDir[i];
				}   
				normDir =  sqrt(accum);

				for (unsigned i=0; i< fiberDir.size(); i++) {
					fiberDir[i] = fiberDir[i]/normDir;
                }
				
				// set the spin matrix that aligns the FiberCone emitter normal to the z-axis
				Q = SpinMatrix::k(fiberDir);
			} 

			// rotate vector using the spin matrix
			array<float,3> rotate(array<float,3> P) const
			{
                // rotate the point (actually rotates a vector, not a point) by the spin matrix
                // See SpinMatrix.hpp
				array<float,3> posWithinRadius;
				posWithinRadius[0] = P[0]*Q[0][0] + P[1]*Q[0][1] + P[2]*Q[0][2] ;
				posWithinRadius[1] = P[0]*Q[1][0] + P[1]*Q[1][1] + P[2]*Q[1][2] ;
				posWithinRadius[2] = P[0]*Q[2][0] + P[1]*Q[2][1] + P[2]*Q[2][2] ;

				return posWithinRadius;
			}


			//generates the direction of packets.
			//This method gets a random number as an input and generates the direction vector of packets (PacketDirection) as output. 
			//This is how the direction vectors are generated: 
			//1. Initially I assume that the fiber is parallel to the z direction and I generate the direction vector (Px, Py, Pz). Afterwards, I rotate the direction vector along with the     direction of fiber axis. 
			//2. alphaWithin is uniformly random selected within the alpha of aperture
			//3. Assuming that the direction vector is a normalized vector, one can deduce geometrically that Pz of direction vector would be Pz = cos(alphaWithin);
			//4. Once Pz is determined, Px and Py should be selected randomly on a circle of radius tan(alphaWithin)= abs(sqrt(1-Pz*Pz));
			// To do so, a uniform random Phi between [-pi,pi] is picked. Hence,  Px = cosPhi * radius;   Py = sinPhi * radius;
			//5. Now compute the Q rotation matrix to rotate the z axis to the fiber axix direction
			// refer to: https://math.stackexchange.com/questions/542801/rotate-3d-coordinate-system-such-that-z-axis-is-parallel-to-a-given-vector 

			PacketDirection direction(RNG& rng, SSE::Point3 /*p*/) const 
			{
				//alphaWithin is uniformly random selected within the alpha of aperture
				float randNum1=*rng.floatPM1(); //random number [-1,1]
				
				float NA_div_n = m_NA/m_refractiveIndex[0];
				float alpha;
				if(NA_div_n <= 1.0f)
					alpha= asin(NA_div_n); //alpha of aperture ;alpha= asin(NA/n)
				else
					alpha= asin(1.0f); //max alpha of aperture ;alpha= asin(NA/n)
				float alphaWithin= alpha* randNum1; //emit within a cone with alpha angel


				///Initially I assume that the fiber is parallel to the z direction and I generate the direction vector (Px, Py, Pz). Afterwards, I rotate the direction vector along with the direction of fiber axis.
				//pick a point (Px,Py,Pz)on an imaginary 2D circlular plane perpendicular to z-axis
				float Px, Py, Pz; // direction vector (Px, Py, Pz)	

				//Assuming that the direction vector is a normalized vector, one can deduce geometrically that Pz of direction vector would be Pz = cos(alphaWithin);
				Pz = cos(alphaWithin); //assuming normalized vector

				
				float randNum2= *rng.floatPM1(); //random number [-1,1]
				float phi =  PI * randNum2;//phi within a circle around the central axis of fiber [-pi,pi]
				float cosPhi = cos(phi);
				float sinPhi = sin(phi);
				float radius = abs(sqrt(1-Pz*Pz));//  //tan(alphaWithin); // tan^2a = 1-cos^2a; //radius of the disk		
				Px = cosPhi * radius;//on a planar disk of radius 
				Py = sinPhi * radius;//on a planar disk of radius	

				//// rotate Pxyz to center the z-axis on the direction of the fiber cone
				std::array<float,3> d_rotate = rotate({Px, Py, Pz});

				// make a direction normal out of the point we just created
                SSE::UnitVector3 d = SSE::UnitVector3::normalize(SSE::Vector3(d_rotate));

				// compute normals a,b for d
                SSE::UnitVector3 a;
                if (std::abs(d[0]) < std::abs(d[1]))
                {
                    if (std::abs(d[0]) < std::abs(d[2]))
                        a = SSE::UnitVector3::normalize(cross(d,SSE::UnitVector3::basis<0>()));
                    else
                        a = SSE::UnitVector3::normalize(cross(d,SSE::UnitVector3::basis<2>()));
                }
                else if (std::abs(d[1]) < std::abs(d[2]))
                {
                    a = SSE::UnitVector3::normalize(cross(d,SSE::UnitVector3::basis<1>()));
                }
                else
                {
                    a = SSE::UnitVector3::normalize(cross(d,SSE::UnitVector3::basis<2>()));
                }

                SSE::UnitVector3 b(cross(d,a),SSE::Assert);

				//Convert direction vectors to SSE vectors used in PacketDirection.   
				return PacketDirection(d, a, b);
			}


			//This method generates the position of packets.
			//This method gets a random number as an input and generates the position vector of packets as output. 
			//This is how the position vectors are generated: 
			//1. Initially I assume that the fiber is parallel to the z direction and I generate the position vector (Px, Py, Pz) within the radius of fiber end point. Afterwards, I rotate the position vector along with the direction of fiber axis.
			//2. First I select a random radius called "withinRadius" within the radius of fiber. The probability of radius selection forms uniformity within the surface.
			//3. pick a point (Px,Py) on an imaginary 2D circlular plane of radius "withinRadius" perpendicular to z-axis. (Pz =0)
			//4. Afterwards, the (Px,Py,Pz) which was computed relative to the origin (0,0,0) is moved to the position of cut end fiber. 
			//5. Finally , I rotate the position vector along with the direction of fiber axis. I compute the Q rotation matrix to rotate the z axis to the fiber axis direction
			// refer to: https://math.stackexchange.com/questions/542801/rotate-3d-coordinate-system-such-that-z-axis-is-parallel-to-a-given-vector

			std::pair<unsigned,SSE::Point3> position(RNG& rng) const

			{
				//select a number with radius of fiber
				float rand0= *rng.floatU01();//number within [0,1]
				float withinRadius = fiberRadius * abs(sqrt(rand0)); //radius within the radius of fiber

				//pick a point (Px,Py,Pz)on an imaginary 2D circlular plane perpendicular to z-axis
				float Px, Py, Pz; 
				float rand1= *rng.floatU01();
				float phi = rand1* 2 * PI;// [0,2pi]


				float cosPhi = cos(phi);
				float sinPhi = sin(phi);
				Pz = 0; //position of fiber endpoint on z-axis

				Px = cosPhi * withinRadius;//on a planar disk of withinRadius at the position of fiber end-cut
				Py = sinPhi * withinRadius;//on a planar disk of withinRadius at the position of fiber end-cut
				//cout <<"Point on the disk " << Px <<"," << Py << "," << Pz <<endl;             

				// rotate Pxyz to align z-axis to fiber direction
				array<float,3> posWithinRadius = rotate({Px,Py,Pz});
				posWithinRadius[0] = posWithinRadius[0] + fiberPos[0]; 
				posWithinRadius[1] = posWithinRadius[1] + fiberPos[1];
				posWithinRadius[2] = posWithinRadius[2] + fiberPos[2];

				unsigned int el_value; //element id of the tetra enclosing the position of the photon 

				//if the radius of the fiber is zero, the position of the packets is always the same and equal to the fiber end-point 
				if (fiberRadius == 0)
				{
					el_value = endpoint_Id.value(); 
				}
				else 
				{
                    //perform search using RTree (which falls back to linear search) 
			        TetraMesh::TetraDescriptor el = m_rtree->search(posWithinRadius);
					el_value = el.value();
					//determine the refractive index of the material from which the photon is launched
					unsigned region_index = get(region,*m_mesh, el);
					*m_refractiveIndex = m_materialset->get(region_index)->refractiveIndex();
				}

				
				LOG_DEBUG << "position vector " << posWithinRadius[0] << "," << posWithinRadius[1] << "," << posWithinRadius[2] << endl ;
				LOG_DEBUG << "element id " << el_value << endl;  
				
				//convert position vector of fiber end point to SSE vector
                return std::make_pair(el_value, SSE::Point3(posWithinRadius));

			}


		private:

			float m_NA; //Numerical aperture
			std::array<float,3> fiberDir; //direction of fiber axis
			float fiberRadius;//radius of fiber end-point
			std::array<float,3> fiberPos;//position of fiber end-point
			float* m_refractiveIndex=nullptr; // refractive index of the material the photon is launched in
			const MaterialSet* m_materialset;//pointer to the material set
			const TetraMesh* m_mesh;//pointer to the mesh
			TetraMesh::TetraDescriptor endpoint_Id; //element id of the tetra enclosing the position of the fiber end-point
	        std::array<std::array<float,3>,3> Q; // the rotation matrix
            TetraMesh::TetraMeshRTree* m_rtree=nullptr;  // rtree for searching mesh
	};	



};

#endif
