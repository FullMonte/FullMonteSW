/*
 * EmitterFactory.hpp
 *
 *  Created on: Jan 27, 2016
 *      Author: jcassidy
 */

#ifndef KERNELS_SOFTWARE_EMITTERS_TETRAMESHEMITTERFACTORY_HPP_
#define KERNELS_SOFTWARE_EMITTERS_TETRAMESHEMITTERFACTORY_HPP_

#include <FullMonteSW/Geometry/Sources/Abstract.hpp>

#include <FullMonteSW/Warnings/Push.hpp>
#include <FullMonteSW/Warnings/Boost.hpp>
#include <boost/range.hpp>
#include <boost/range/any_range.hpp>
#include <boost/range/adaptor/map.hpp>
#include <FullMonteSW/Warnings/Pop.hpp>



// Emitter/Detector classes
#include "Base.hpp"
#include "Point.hpp"
#include "Directed.hpp"
#include "Isotropic.hpp"
#include "Triangle.hpp"
#include "Line.hpp"
#include "Tetra.hpp"
#include "Composite.hpp"
#include "FiberConeEmitter.hpp"
#include "HemiSphere.hpp"
#include "Cylinder.hpp"
#include "CylDetector.hpp"

#include <FullMonteSW/Logging/FullMonteLogger.hpp>

// Source descriptions
#include <FullMonteSW/Geometry/Sources/Composite.hpp>
#include <FullMonteSW/Geometry/Sources/Point.hpp>
#include <FullMonteSW/Geometry/Sources/Line.hpp>
#include <FullMonteSW/Geometry/Sources/Ball.hpp>
#include <FullMonteSW/Geometry/Sources/Volume.hpp>
#include <FullMonteSW/Geometry/Sources/SurfaceTri.hpp>
#include <FullMonteSW/Geometry/Sources/PencilBeam.hpp>
#include <FullMonteSW/Geometry/Sources/Surface.hpp>
#include <FullMonteSW/Geometry/Sources/Fiber.hpp>
#include <FullMonteSW/Geometry/Sources/TetraFace.hpp>
#include <FullMonteSW/Geometry/Sources/Cylinder.hpp>
#include <FullMonteSW/Geometry/Sources/CylDetector.hpp>
#include <FullMonteSW/Geometry/RayWalk.hpp>

#include <FullMonteSW/Geometry/TetraMesh.hpp>
#include <FullMonteSW/Geometry/FaceLinks.hpp>
//#include <FullMonteSW/Geometry/Points.hpp>
#include <FullMonteSW/Kernels/Software/Tetra.hpp>

#include "../SSEMath.hpp"


#include <FullMonteSW/Kernels/Software/RNG_SFMT_AVX.hpp>
#include <FullMonteSW/Geometry/Queries/TetrasNearPoint.hpp>

#include "RandomInPlane.hpp"
#include "TetraLookupCache.hpp"

#include <iomanip>
#include <vector>
#include <mutex>
#include <sstream>

class TetraMesh;

namespace Emitter
{

	template<typename RNG>class EmitterBase;

	/** TetraEmitterFactory takes generic source descriptions (descendents of Source::Base) and makes them into
	 * emitters that can be used with the tetrahedral MC kernel.
	 *
	 * It inherits from Source::Visitor to generalize over source types.
	 */

	template<class RNG>class TetraEmitterFactory : public Source::Abstract::Visitor
	{

		public:
			TetraEmitterFactory(){}
			TetraEmitterFactory(const TetraMesh* M, const MaterialSet*MS) : m_mesh(M), m_materialset(MS){}

			virtual void doVisit(Source::Point* p) 				override;
			virtual void doVisit(Source::Ball* b)				override;
			virtual void doVisit(Source::Line* l)				override;
			virtual void doVisit(Source::Volume* v)				override;
			virtual void doVisit(Source::Surface* s)			override;
			virtual void doVisit(Source::SurfaceTri* st)		override;
			virtual void doVisit(Source::PencilBeam* b)			override;
			virtual void doVisit(Source::Fiber* f) 				override;
			virtual void doVisit(Source::TetraFace* tf)			override;
			virtual void doVisit(Source::Cylinder* tf)			override;
            virtual void doVisit(Source::CylDetector* cd)       override;

			virtual void undefinedVisitMethod(Source::Abstract*) override;

			Emitter::EmitterBase<RNG>* emitter() const;

			boost::iterator_range<std::vector<Source::Abstract*>::const_iterator> csources() const
			{
				return boost::iterator_range<std::vector<Source::Abstract*>::const_iterator>(m_sources.cbegin(), m_sources.cend());
			}

			boost::any_range<
				Emitter::EmitterBase<RNG>*,
				boost::random_access_traversal_tag> cemitters() const
				{
					return m_emitters | boost::adaptors::map_values;
				}

            void setKernelTetra(KernelTetra* tetras) { m_tetras = tetras; }

            std::vector<Emitter::DetectorBase<RNG>*>    detectors() { return m_detectors; }
		private:
			std::vector<Source::Abstract*> 					m_sources;
			std::vector<std::pair<float,Emitter::EmitterBase<RNG>*>>	m_emitters;
            std::vector<Emitter::DetectorBase<RNG>*>    m_detectors; // Stores the detectors. Detectors IDs should index this
			const TetraMesh*						m_mesh=nullptr;
			const MaterialSet*						m_materialset=nullptr;
            KernelTetra*                                  m_tetras=nullptr; // a vector that stores the kernel tetra 

			bool								m_debug=false;

			float								m_tetraInteriorEpsilon=1e-5f;

            unsigned                            m_numDetectors=0; // used to ID detectors
	};

};

namespace Emitter
{

	/** Set up an isotropic point source by locating the tetra enclosing it */

	template<class RNG>void TetraEmitterFactory<RNG>::doVisit(Source::Point* ps)
	{
		unsigned elHint = ps->elementHint();
		array<float,3> pos = ps->position();

		TetraMesh::TetraDescriptor el = m_mesh->rtree()->search(pos);

		// The source should never be in tetra 0 (tetra 0 is a 'fake' tetra for internal use).
		// If the enclosing tetra was set to 0 then the source is (likely) outside of the mesh.
		if(el.value() == 0) {
			LOG_ERROR << "Point source at (" <<  pos[0] << ", " << pos[1] << ", " << pos[2] << ") does not seem to be inside the mesh. We could not find a tetrahedron containing this point." << std::endl;
			exit(-1);
		}

		if(elHint != -1u && el.value() != elHint) {
			LOG_WARNING << "Element hint provided (" << elHint << ") does not match enclosing tetra found (" << el.value() << ")" << endl;
        }

		LOG_INFO << "Point source at " << pos[0] << ',' << pos[1] << ',' << pos[2] << " in tetra " << el.value() << endl;

		Point P(el.value(),SSE::Point3(ps->position()));

		auto ips = new PositionDirectionEmitter<RNG,Point,Isotropic<RNG>>(P,Isotropic<RNG>());

		m_emitters.push_back(make_pair(ps->power(),ips));
	}



	///** Set up a pencil beam by locating the face on which it impinges */
	//
	template<class RNG>void TetraEmitterFactory<RNG>::doVisit(Source::PencilBeam* pb)
	{
		array<float,3> pos = pb->position();

		//	std::array<float,3> pos;				// the position after advancing to the face
		//	unsigned tetHint = pb->elementHint();
		//
		////	RayWalkIterator it=RayWalkIterator::init(*m_mesh,pb->position(),pb->direction()),it_end;
		////
		////	if (m_debug)
		////	{
		////		pos = pb->position();
		////		array<float,3> dir = pb->direction();
		////
		////		cout << "TetraMeshEmitterFactory: starting at " << pos[0] << ' ' << pos[1] << ' ' << pos[2] << " direction " << dir[0] << ' ' << dir[1] << ' ' << dir[2] << endl;
		////	}
		////
		////	for(; it != it_end && it->matID == 0; ++it)
		////	{
		////		if (m_debug)
		////			cout << "  Stepping through tet " << setw(9) << it->IDt << " (" << it->dToOrigin << " from origin) " << endl;
		////		tet=it->IDt;
		////	}
		////
		////	if (it == it_end)
		////		throw std::logic_error("ERROR: TetraEmitterFactory<RNG>::visit(Source::PencilBeam) shows no intersection of ray with mesh face");
		////
		////	// assign position to the exit point of the last tet with matID=0
		////	pos = it->f0.p;
		//
		////	TriFilterRegionBounds TF(m_mesh);
		////	TF.includeRegion(0,true);			// TODO: remove hard-coded value here
		////	TF.includeRegion(18,false);			// TODO: remove hard-coded value here
		////	TF.bidirectional(false);			// include face only if going from mat 0 into mat !0
		//
		////#warning "Hard-coded region bounds for face-intersection calculation in TetraMeshEmitterFactory for PencilBeam"
		//
		//	RTIntersection res = m_mesh->findSurfaceFace(pb->position(), pb->direction(), &TF);
		//
		//	tet = res.IDt_to;
		//	unsigned IDt_opposite = res.IDt_from;
		//
		//	if (res.IDt_to == 0)
		//		std::swap(tet,IDt_opposite);
		//
		//	pos = res.q;
		//	m_debug=true;
		//
		//	if (tetHint != -1U && tetHint != tet)
		//		cout << "WARNING: Tetra hint provided (" << tetHint << ") does not match the tetra located by search (" << tet << ")" << endl;
		//	else if (tetHint == -1U && m_debug)
		//		cout << "INFO: No tetra hint provided, found " << tet << " (material " << m_mesh->getMaterial(tet) << ")" << endl;
		//
		//	if (m_debug)
		//	{
		//		cout << "INFO: Created pencil beam emitter impinging on face " << res.IDf << " into tetra " << tet << " with material " <<
		//			m_mesh->getMaterial(tet) << " at position " << res.q[0] << ',' << res.q[1] << ',' << res.q[2] << endl;
		//
		//		//unsigned IDt_opposite = m_mesh->getTetraFromFace(-res.IDf);
		//
		//		cout << "INFO:   Opposite face " << -res.IDf << " impinges tet " << IDt_opposite << " material " << m_mesh->getMaterial(IDt_opposite) << endl;
		//
		//	}
		//
		//
		//	::Tetra T = m_mesh->getTetra(tet);
		//	bool showTetStats=m_debug;
		//
		//	if (!T.pointWithin(to_m128(pos),m_tetraInteriorEpsilon))
		//	{
		//		showTetStats=true;
		//		cout << "ERROR! Point is not inside the tetra!" << endl;
		//	}
		////
		////	if (showTetStats)
		////		{
		////			cout << "Tetra normals (ID " << tet << ", material " << m_mesh->getMaterial(tet) << ": ";
		////
		////			std::array<float,4> h = to_array<float,4>(T.heights(to_m128(pos)));
		////
		////			cout << "Heights: ";
		////			for(unsigned i=0;i<4;++i)
		////				cout << h[i] << ' ';
		////			cout << endl;
		////		}
		//
		//
		//
		//	// compute normals a,b for d
		SSE::UnitVector3 d = SSE::UnitVector3::normalize(SSE::Vector3((pb->direction())));

		// compute a for different cases
		SSE::UnitVector3 a;
		if (std::abs(d[0]) < std::abs(d[1]))
		{
			if (std::abs(d[0]) < std::abs(d[2]))
				a = SSE::UnitVector3::normalize(cross(d,SSE::UnitVector3::basis<0>()));
			else
				a = SSE::UnitVector3::normalize(cross(d,SSE::UnitVector3::basis<2>()));
		}
		else if (std::abs(d[1]) < std::abs(d[2]))
			a = SSE::UnitVector3::normalize(cross(d,SSE::UnitVector3::basis<1>()));
		else
			a = SSE::UnitVector3::normalize(cross(d,SSE::UnitVector3::basis<2>()));

		// calculate b
		SSE::UnitVector3 b(cross(d,a),SSE::Assert);


		// This is just a first attempt at fixing this issue; In an earlier version, the elementID
		// was simply hardcoded to 1 which caused problems in DEBUG mode.
		// Is there any reason why we shouldn't use the tetra of the given position of the pencilbeam?

		// Locate the tetra enclosing the point
		TetraMesh::TetraDescriptor el = m_mesh->rtree()->search(pos);
		// The source should never be in tetra 0 (tetra 0 is a 'fake' tetra for internal use).
		// If the enclosing tetra was set to 0 then the source is (likely) outside of the mesh.
		if(el.value() == 0) {
			LOG_ERROR << "ERROR: Pencil Beam source at (" <<  pos[0] << ", " << pos[1] << ", " << pos[2] << ") does not seem to be inside the mesh. We could not find a tetrahedron containing this point." << std::endl;
			exit(-1);
		}

		Point P{el.value(),SSE::Vector3(pos)};
		Directed D(PacketDirection(d,a,b));

		LOG_DEBUG << "w=" << pb->power() << " Position: " << pos[0] << ' ' << pos[1] << ' ' << pos[2] << "  direction: " << d.array()[0] << ' ' << d.array()[1] << ' ' << d.array()[2] << endl;
		LOG_DEBUG << "a=" << a.array()[0] << ' ' << a.array()[1] << ' ' << a.array()[2] << "  b: " << b.array()[0] << ' ' << b.array()[1] << ' ' << b.array()[2] << endl;

		auto pbs = new PositionDirectionEmitter<RNG,Point,Directed>(P,D);
		m_emitters.push_back(make_pair(pb->power(),pbs));
	}

	template<class RNG>void TetraEmitterFactory<RNG>::doVisit(Source::SurfaceTri*)
	{
		//	// find face in TetraMesh
		//	IDf = m.getFaceID(f);
		//
		//	if(force_boundary)
		//	{
		//		unsigned tetraDown = m.getTetraFromFace(-IDf), tetraUp = m.getTetraFromFace(IDf);
		//		if (tetraUp){
		//			IDt=tetraUp;
		//			if (tetraDown)
		//				cerr << "Surprise: source is not on a boundary" << endl;
		//			else
		//				IDf = -IDf;
		//		}
		//		else if (tetraDown)
		//			IDt=tetraDown;
		//	}
		//	else
		//		IDt = m.getTetraFromFace(IDf);


		//	Triangle<RNG> T(A,B,C);
		//	Directed D(dir);
		//
		//	auto sts = new PositionDirectionEmitter<RNG,Triangle<RNG>,Directed>(T,D);
		//
		//	m_emitters.push_back(make_pair(st->power(),sts));

		throw std::logic_error("TetraEmitterFactory<RNG>::doVisit - unsupported (SurfaceTri)");
	}

	template<class RNG>void TetraEmitterFactory<RNG>::doVisit(Source::Volume* vs)
	{
		unsigned IDt=vs->elementID();

		// checking the tetra ID input is valid
		unsigned Nt = get(num_tetras, *m_mesh);
		if(IDt == 0 || IDt > Nt) {
			cerr << "ERROR: Volume source was given tetra ID " << IDt << " however, based on the provided mesh, the tetra ID must be in range (0, " << Nt << "]" << endl;
			exit(-1);
		}

		array<::Point<3,double>,4> P = get(point_coords,*m_mesh,TetraMesh::TetraDescriptor(IDt));

		Tetra<RNG> 		T(
				IDt,
				SSE::Vector3(P[0]),
				SSE::Vector3(P[1]),
				SSE::Vector3(P[2]),
				SSE::Vector3(P[3]));

		Isotropic<RNG> 	I;
		auto vss = new PositionDirectionEmitter<RNG,Tetra<RNG>,Isotropic<RNG>>(T,I);

		m_emitters.push_back(make_pair(vs->power(),vss));
	}

	using namespace std;

	template<class RNG>void TetraEmitterFactory<RNG>::doVisit(Source::Line* l)
	{
		Emitter::EmitterBase<RNG>* ls=nullptr;

		// each segment is a (length, IDt) pair specifying the tetras that make up the line source
		std::vector<std::pair<float,unsigned>> segments;

		// 	RayFaceWalk RW;

		// getting difference of the source coordinates for normalizing and getting the direction
		std::array<float, 3> source_diff;
		source_diff[0] = l->endpoint(1)[0] - l->endpoint(0)[0];
		source_diff[1] = l->endpoint(1)[1] - l->endpoint(0)[1];
		source_diff[2] = l->endpoint(1)[2] - l->endpoint(0)[2];


		RayWalkIterator it=RayWalkIterator::init(
				*m_mesh,
				l->endpoint(0),
				normalize(source_diff));

		RayWalkIterator itEnd = RayWalkIterator::endAt(l->length());

		// TODO: error check

		for(; it != itEnd; ++it)
			segments.emplace_back(it->dToOrigin+it->lSeg,it->IDt);

		LOG_DEBUG << "Created a line source with " << segments.size() << " segments" << endl;

		for(const auto& s : segments) {
			LOG_DEBUG << "\t\ttet " << s.second << " l up to " << s.first << endl;
        }

		Emitter::Line<RNG> P(
				SSE::Point3(l->endpoint(0)),
				SSE::Point3(l->endpoint(1)),
				segments);

		switch(l->pattern())
		{
			case Source::Line::Isotropic:
				ls = new PositionDirectionEmitter<RNG,Line<RNG>,Isotropic<RNG>>(P,Isotropic<RNG>());
				break;
			case Source::Line::Normal:
				ls = new PositionDirectionEmitter<RNG,Line<RNG>,RandomInPlane<RNG>>(P,RandomInPlane<RNG>(P.displacement()));
				break;
			default:
				throw std::logic_error("TetraEmitterFactory<RNG>::doVisit(Source::Line*) unsupported emission pattern");
		}


		m_emitters.push_back(make_pair(l->power(),ls));
	}

	template<class RNG>void TetraEmitterFactory<RNG>::doVisit(Source::Surface*)
	{
		throw std::logic_error("TetraEmitterFactory<RNG>::doVisit - unsupported (Surface)");
	}

	template<class RNG>void TetraEmitterFactory<RNG>::undefinedVisitMethod(Source::Abstract*)
	{
		throw std::logic_error("TetraEmitterFactory<RNG>::doVisit - unsupported source type");
	}


	/** Approximate a ball source by a set of Tetra sources.
	 *
	 */

	template<class RNG>void TetraEmitterFactory<RNG>::doVisit(Source::Ball* bs)
	{
		TetrasNearPoint TP;

		TP.mesh(m_mesh);
		TP.centre(bs->centre());
		TP.radius(bs->radius());

		vector<std::pair<unsigned,float>> w;
		float wsum=0.0f;

		// check
		TP.start();
		for(TP.start(); !TP.done(); TP.next())
		{
			const auto T = TP.currentTetraID();
			TetraMesh::TetraDescriptor TD(T);
			w.emplace_back(
					get(id, *m_mesh, TD),
					get(volume, *m_mesh, TD));
		}

		// radius is too small to fully contain any tetras
		if (w.size() == 0) {
			LOG_ERROR << "Ball Source has a radius which is too small to fully contain any tetras." << endl;
			exit(-1);
		}

		for(const auto p : w)
			wsum += p.second;

		for(const auto p : w)
		{
			Source::Volume vs(p.second/wsum,p.first);
			doVisit(&vs);
		}
	}

	//Emitter factory that creates an emitter from a cut-end fiber light source. The resulting emitter provides the position and direction of launch packets.  
	template<class RNG>void TetraEmitterFactory<RNG>::doVisit(Source::Fiber* f)
	{
		Emitter::EmitterBase<RNG>* fs=nullptr;

		array<float,3> pos = f->fiberPos();
		
		// Locate the tetra enclosing the point
		TetraMesh::TetraDescriptor el = m_mesh->rtree()->search(pos);

		// error checking 
		if(el.value() == 0) {
			LOG_ERROR << "Fiber Cone source at (" <<  pos[0] << ", " << pos[1] << ", " << pos[2] << ") does not seem to be inside the mesh. We could not find a tetrahedron containing this point." << std::endl;
			exit(-1);
		}
		
		//FiberCone Emitter gets the features of fiber as input and provides both position and direction of packets.
		Emitter::FiberCone<RNG> PD(
            f->numericalAperture(),
            f->fiberDir(),
            f->radius(),
            f->fiberPos(),
            el,
			m_materialset,
            m_mesh,
			m_mesh->rtree()
        );  

		fs= new PositionDirectionEmitter<RNG,FiberCone<RNG>,FiberCone<RNG>>(PD,PD);

		m_emitters.push_back(make_pair(f->power(),fs));
	}

	template<class RNG>void TetraEmitterFactory<RNG>::doVisit(Source::TetraFace* tf)
	{
		// get the points of the tetra face
		auto p = get(point_coords, *m_mesh, TetraMesh::FaceDescriptor(tf->faceID()));
		
        // for the tetra ID, always take the one in the direction we are emitting.
        // if we are emitting normal to the (original) face, we are emitting towards the upwards tetra
        // if we are emitting anti-normal to the (original) face, we are emitting towards the downward tetra
        unsigned int IDt;
        if(tf->emitNormal()) {
            IDt = get(tetra_above_face, *m_mesh, TetraMesh::FaceDescriptor(tf->faceID())).value();
        } else {
            IDt = get(tetra_below_face, *m_mesh, TetraMesh::FaceDescriptor(tf->faceID())).value();
        }
		
        assert(IDt != 0);
        //Triangle Emitter gets the features of TetraFace as input and provides a position randomly in the triangular plane
		Emitter::Triangle<RNG> T(IDt, SSE::Vector3(p[0]), SSE::Vector3(p[1]), SSE::Vector3(p[2]));

        // build direction based on emitting in a hemi-sphere or isotropic
        if(tf->emitHemiSphere()) {
			// create emission pattern for the triangle source
            HemiSphere<RNG> HS(
				IDt,
				tf->emitNormal(),
				tf->faceID(),
				tf->numericalAperture(),
				tf->hemiSphereEmitDistribution(),
				m_materialset,
            	m_mesh);

		    // create the source and add it to the list
		    auto fs = new PositionDirectionEmitter<RNG,Triangle<RNG>,HemiSphere<RNG>>(T, HS);
		    m_emitters.push_back(make_pair(tf->power(), fs));
		}
		else {
		    // create the source and add it to the list
		    auto fs = new PositionDirectionEmitter<RNG,Triangle<RNG>,Isotropic<RNG>>(T, Isotropic<RNG>());
		    m_emitters.push_back(make_pair(tf->power(), fs));
        }
	}


    template<class RNG>void TetraEmitterFactory<RNG>::doVisit(Source::Cylinder* c)
    {
        assert(c->radius() > 0.0f);

        const std::array<float,3> e0 = c->endpoint(0);
        const std::array<float,3> e1 = c->endpoint(1);
        const float r = c->radius();
		const float na = c->numericalAperture();

        Emitter::Cylinder<RNG> cyl(
            m_mesh,
			m_mesh->rtree(),
			m_materialset,
            e0,
            e1,
            r,
			na,
            c->emitHemiSphere(),
            ThetaDistributionFunc::fromString(c->hemiSphereEmitDistribution()),
            c->emitVolume()
        );

		// Locate the tetra enclosing the point
		TetraMesh::TetraDescriptor el = m_mesh->rtree()->search(e0);

        if(el.value() == 0) {
            LOG_ERROR << "Cylinder source endpoint 0 does not seem to be in the mesh - could not find a containing tetrahedron" << std::endl;
        }

		el = m_mesh->rtree()->search(e1);

        if(el.value() == 0) {
            LOG_ERROR << "Cylinder source endpoint 1 does not seem to be in the mesh - could not find a containing tetrahedron" << std::endl;
        }

        auto cs = new PositionDirectionEmitter<RNG,Cylinder<RNG>,Cylinder<RNG>>(cyl,cyl);
        
        m_emitters.push_back(make_pair(c->power(),cs));
    }

    template<class RNG> void TetraEmitterFactory<RNG>::doVisit(Source::CylDetector* cd)
    {
        assert(cd->radius() >=  0.0f);

        const std::array<float,3> e0 = cd->endpoint(0);
        const std::array<float,3> e1 = cd->endpoint(1);
        const float r = cd->radius();
        const float na = cd->numericalAperture();
        const unsigned detectorID = ++m_numDetectors; 
        cd->detectorID(detectorID); 

        Emitter::CylDetector<RNG> cylD(
            m_mesh,
			m_mesh->rtree(),
            m_tetras, 
            e0,
            e1,
            r,
            na, 
            detectorID
        );
        cylD.setDetectionType(Source::CylDetector::DetectionTypeFromString(cd->detectionType())); 

		// Ensure endpoints are in the mesh - doesn't cover all cases, but does some error checking

        TetraMesh::TetraDescriptor el = m_mesh->rtree()->search(e0);

        if(el.value() == 0) {
            LOG_ERROR << "Cylinder detector endpoint 0 does not seem to be in the mesh - could not find a containing tetrahedron" << std::endl;
        }

        el = m_mesh->rtree()->search(e1);

        if(el.value() == 0) {
            LOG_ERROR << "Cylinder detector endpoint 1 does not seem to be in the mesh - could not find a containing tetrahedron" << std::endl;
        }


        auto db = new Detector<RNG, Emitter::CylDetector<RNG>>(cylD); 
        m_detectors.push_back(db);
    }

	template<class RNG>Emitter::EmitterBase<RNG>* TetraEmitterFactory<RNG>::emitter() const
	{
		EmitterBase<RNG>* e=nullptr;

		if (m_emitters.size() == 0)
			throw std::logic_error("TetraEmitterFactory<RNG>::source() - no sources!");
		else if (m_emitters.size() == 1)
			e = m_emitters.front().second;
		else {
			e = new Emitter::Composite<RNG>(m_emitters, m_detectors);
        }
        

		return e;
	}

};

#endif /* KERNELS_SOFTWARE_EMITTERS_TETRAMESHEMITTERFACTORY_HPP_ */
