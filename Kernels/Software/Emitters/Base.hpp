/*
 * Base.hpp
 *
 *  Created on: Jan 27, 2016
 *      Author: jcassidy
 */

#ifndef KERNELS_SOFTWARE_EMITTERS_BASE_HPP_
#define KERNELS_SOFTWARE_EMITTERS_BASE_HPP_

#include "../Packet.hpp"
#include <tuple>

namespace Emitter
{

/** Base class for all emitters. All it needs to do is emit a LaunchPacket (dir, pos, element ID) using the virtual emit(RNG&, unsigned long long, unsigned long long) method.
 *
 * @tparam 	RNG		The random-number generator to be used with this class
 */

template<class RNG>class EmitterBase
{
public:
	EmitterBase(){}
	virtual ~EmitterBase(){}

	/// Emit a packet (direction, position, element ID) - constness ensures it is thread-safe to share
	// both unsigned long long variables are used for the Composite source which inherits this emit fuction
	virtual LaunchPacket emit(RNG&, unsigned long long, unsigned long long) const=0;



private:
};


/** 
 * Base class for all detectors. All it needs to do is to detect a packet (dir, pos, elementID) using virtual detect(RNG&, Packet)
 *
 * @tparam RNG          The random-number generator to be used with this class
 */
template<class RNG> class DetectorBase
{
public:
    DetectorBase() {}
    virtual ~DetectorBase() {}

    // Detects a packet if its angle is within an NA, and then calculates the portion of the packet being detected.
    virtual void detect(RNG&, Packet&, float) = 0;
private:
};


/** Convenience class that permits composing a position distribution with a direction distribution. By design, the source emits
 * only within a single geometry-model element maintained by this base class.
 *
 * @tparam		RNG			Random number generator class
 * @tparam		Position	Class with a position(RNG&) function to provide the origin point
 * @tparam		Direction	Class with a direction(RNG&, SSE::Point3) function provided the PacketDirection
 */

template<class RNG,class Position,class Direction>class PositionDirectionEmitter : public EmitterBase<RNG>
{
public:
	/// Create from a Position&, Direction&, and element
	PositionDirectionEmitter(const Position& P,const Direction& D) :
		m_pos(P),
		m_dir(D){}

	/// Dispatch the position and direction requests to the component elements (note these can be inlined due to template)
	// both unsigned long longs are not needed for the base emit function. They are needed for the Composite source
	virtual LaunchPacket emit(RNG& rng, unsigned long long, unsigned long long) const override
	{
		LaunchPacket lpkt;
		std::tie(lpkt.element,lpkt.pos) = m_pos.position(rng);
		lpkt.dir = m_dir.direction(rng, lpkt.pos);

		return lpkt;
	}

	/// Access the position distribution
	Position& 	positionDef()	{ return m_pos; }

	/// Access the direction distribution
	Direction& 	directionDef()	{ return m_dir; }

private:
	Position		m_pos;				///< Position distribution
	Direction		m_dir;				///< Direction distribution
};

/** Convenience class that permits composing a detection distribution. By design, the source detects
 * only within a single geometry-model element maintained by this base class.
 *
 * @tparam		RNG			    Random number generator class
 * @tparam		DetectorType	Class with a detect function to modify the packet wieght
 */
template<class RNG, class DetectorType> class Detector : public DetectorBase<RNG>
{
public:
    // Create an element of DetectorType
    Detector(const DetectorType& D) :
        m_detector(D) {};

    // Call DetectorType's detect function
    virtual void detect(RNG& rng, Packet& packet, float n) 
    {
        m_detector.detect(rng, packet, n); 
    }

    // Access the detection distribution
    DetectorType& detector() { return m_detector; }

private:
    DetectorType    m_detector; 
};
};

#endif /* KERNELS_SOFTWARE_EMITTERS_BASE_HPP_ */
