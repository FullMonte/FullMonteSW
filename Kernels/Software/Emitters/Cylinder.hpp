#ifndef KERNELS_SOFTWARE_EMITTERS_CYLINDER_HPP_
#define KERNELS_SOFTWARE_EMITTERS_CYLINDER_HPP_

#include <FullMonteSW/Logging/FullMonteLogger.hpp>

#include <FullMonteSW/Kernels/Software/Emitters/SpinMatrix.hpp>

#include <FullMonteSW/Geometry/Queries/TetraEnclosingPointByLinearSearch.hpp>

#include <FullMonteSW/Geometry/Points.hpp>
#include <FullMonteSW/Geometry/TetraMesh.hpp>
#include <FullMonteSW/Geometry/FaceLinks.hpp>
#include <FullMonteSW/Geometry/MaterialSet.hpp>
#include <FullMonteSW/Geometry/Material.hpp>

#include <iostream>
#include <vector>

namespace Emitter
{

/**
 * Cylinder source emitter by picking a random point inside or on surface
 */
template<class RNG> class Cylinder {
public:
    /**
     * Constructor
     */
	Cylinder(
        const TetraMesh* mesh,
        TetraMesh::TetraMeshRTree* rtree,
        const MaterialSet* materialset,
        std::array<float,3> p0,
        std::array<float,3> p1,
        float radius,
        float numericalAperture,
        bool emitHemiSphere=true,
        ThetaDistribution thetaDist=LAMBERT,
        bool inVolume=false) :
            m_mesh(mesh),
            m_materialset{materialset},
		    m_endpoint({p0,p1}),
		    m_radius(radius),
            m_NA(numericalAperture),
            m_emitHemiSphere(emitHemiSphere),
            m_thetaDistribution(thetaDist),
            m_inVolume(inVolume)
    {
        assert(m_mesh != nullptr);

        // compute direction vector and normal
        std::array<float,3> dir = {
            p1[0] - p0[0],
            p1[1] - p0[1],
            p1[2] - p0[2]
        };

        // compute the midpoint of the cylinder
        m_midpoint = {
            (p1[0] + p0[0]) / 2.0f,
            (p1[1] + p0[1]) / 2.0f,
            (p1[2] + p0[2]) / 2.0f
        };
        
        // compute normal for cylinder direction and height of cylinder
        m_dirNormal = normalize(dir);
        m_height = norm(dir);

        // allocate memory for the refractive index; the value will be changed when the position the photon is emitted from is known
        m_refractiveIndex = new float(1.0f);

        LOG_DEBUG << "Emitter::Cylinder direction normal=(" << m_dirNormal[0] << "," << m_dirNormal[1] << "," << m_dirNormal[2] << ")" << 
                                       " midpoint=(" << m_midpoint[0] << "," << m_midpoint[1] << "," << m_midpoint[2] << ")" <<
                                       " height="<< m_height << endl;

        // spin matrix for z-axis and cylinder direction (must be set after setting m_dirNormal)
        Q = SpinMatrix::k(m_dirNormal);

        // assign the RTree object of the TetraMesh to the local RTree object
		m_rtree = rtree;
    }

    ~Cylinder()
    {
        // TYS: Doing this here causes crashes. Is the Cylinder object being 'moved' or 'copied' or something...? All of this is also true for fiber cone
        // For now, I guess we leak the memory???
        //delete m_refractiveIndex;
    }

    /**
     * Determines if a point is within this cylinder
     */
    bool isPointInside(const std::array<float,3> q) {
        // first check if q lies between the circular planes of the cylinder ends
        std::array<float,3> q_e0 = {
            q[0] - m_endpoint[0][0],
            q[1] - m_endpoint[0][1],
            q[2] - m_endpoint[0][2]
        };
        
        std::array<float,3> q_e1 = {
            q[0] - m_endpoint[1][0],
            q[1] - m_endpoint[1][1],
            q[2] - m_endpoint[1][2]
        };

        bool betweenEnds = (dot(q_e0, m_dirNormal) >= 0) && (dot(q_e1, m_dirNormal) <= 0);

        if(betweenEnds) {
            // point lines between the ends of the cylinder
            // now ensure that the (shortest) distance from mid-line of the cylinder to the point is less than the radius
            return norm(cross(q_e0, m_dirNormal)) < m_radius;
        } else {
            return false;
        }
    }

    /**
     * Picks a random point in or on the sphere (depending on the settings)
     *
     * @param rng the random number generator to use
     * @return the tetra ID and random position pair
     */
	std::pair<unsigned,SSE::Point3> position(RNG& rng) const {
        //// STEP 1: pick a random point assuming the cylinder is centered on z-axis with the midpoint at the origin
	    // random z position in range [-h/2,h/2) --- centered on origin
        float z = (*rng.floatPM1()) * (m_height/2.0f);
        // random angle in range [0,2PI) --- polar coordinates in 2D
        float phi = 2.0f*M_PI*(*rng.floatU01());
        // choose radius depending on choice of generating in volume or on surface
        // why use root(rand) where rand is in range [0,1)? See this: http://www.anderswallin.net/2009/05/uniform-random-points-in-a-circle-using-polar-coordinates/
        float r = m_inVolume ? (std::sqrt(*rng.floatU01()) * m_radius) : m_radius;

        // get cartesian positions from polar coordinates (phi and r)
        float x = r*cos(phi);
        float y = r*sin(phi);

        // the random point in the xyz system
        std::array<float,3> p_ijk = {x,y,z};

        //// STEP 2: rotate the point using the new system normals (from rotation matrix)
        const array<float,3> p_uvw {
            p_ijk[0]*Q[0][0] + p_ijk[1]*Q[0][1] + p_ijk[2]*Q[0][2],
            p_ijk[0]*Q[1][0] + p_ijk[1]*Q[1][1] + p_ijk[2]*Q[1][2],
            p_ijk[0]*Q[2][0] + p_ijk[1]*Q[2][1] + p_ijk[2]*Q[2][2]
        };

        // p_ijk is also a vector from the origin point. we spun the direction 'vector' (p_uvw), now move to the midpoint of the cylinder
        // remember, we assumed that the cylinder was centered along the z-axis with the midpoint at the origin
        const array<float,3> P {
            p_uvw[0] + m_midpoint[0],
            p_uvw[1] + m_midpoint[1],
            p_uvw[2] + m_midpoint[2]
        };
        
        //// STEP 3: find tetra containing point P first using the RTree
        // perform search using RTree (which falls back to linear search) 
        TetraMesh::TetraDescriptor el = m_rtree->search(P);		
        unsigned IDt = el.value();
        //determine the refractive index of the material from which the photon is launched
        unsigned region_index = get(region,*m_mesh, el);
        *m_refractiveIndex = m_materialset->get(region_index)->refractiveIndex();

        LOG_DEBUG << "Cylinder::position emitting from tetra " << IDt << " from position " << P[0] << "," << P[1] << "," << P[2] << endl ;

        return std::make_pair(IDt, SSE::Point3(P));
    }

    /**
     * Generate the direction vector based on the point on the surface of the cylinder
     * 
     * @param rng the random number generator
     * @param p the random point in the cylinder
     */
    PacketDirection direction(RNG& rng, SSE::Point3 p) const {
        //// STEP 1: find the normal of the cylinder at random point 'p'
        // the vector formed by the first endpoint of the cylinder and the random point
        std::array<float,3> vec = {
            p[0] - m_endpoint[0][0],
            p[1] - m_endpoint[0][1],
            p[2] - m_endpoint[0][2],
        };

        // the projected distance from from the random point p onto the line formed by the direction normal of the cylinder
        float dist = dot(vec, m_dirNormal);

        // ensure the distance makes sense
        assert(dist>= 0 && dist<=m_height);

        // create a vector out of the closest point on the center line of the cylinder and the random point we are emitting from
        // this is the normal of the cylinder surface at the point 'p'
        std::array<float,3> closestPointOnLine = {
            m_endpoint[0][0] + m_dirNormal[0]*dist,
            m_endpoint[0][1] + m_dirNormal[1]*dist,
            m_endpoint[0][2] + m_dirNormal[2]*dist
        };

        // the vector from the closest point on the cylinder center line to the random point
        std::array<float,3> pointVec = {
            p[0] - closestPointOnLine[0],
            p[1] - closestPointOnLine[1],
            p[2] - closestPointOnLine[2]
        };

        // the normal vector going from center line of cylinder to random point (surface normal at random point)
        std::array<float,3> normalAtP = normalize(pointVec);

        //// STEP 2: generate the direction based on the the normal at this point on the cylinder
        SSE::UnitVector3 d;
        if(m_emitHemiSphere) {
            // first assume cone aligned to z-axis
            // generate random angles
            const float rnd0 = *rng.floatU01();
            const float rnd1 = *rng.floatU01();

            // compute phi angle (in x-y plane)
            const float phi = 2.0f*M_PI*rnd0;

            // compute theta angle
            float theta;
            if(m_thetaDistribution == LAMBERT) {
                // lambert
                theta = asin(rnd1);
            } else if (m_thetaDistribution == CUSTOM) 
            {
                // custom: the numerical aperture defines the emission angle of the source
                float NA_div_n = m_NA/m_refractiveIndex[0];
                if(NA_div_n <= 1)
                    theta = rnd1 * asin(m_NA/m_refractiveIndex[0]);
                else // uniform
                    theta = rnd1 * asin(1.0f);
            } else {
                // uniform
                theta = acos(2.0f*rnd1 - 1.0f);
            }

            // generate unit vector on hemi-sphere oriented about the z-axis
            const std::array<float,3> p_ijk {
                sin(theta)*sin(phi),
                sin(theta)*cos(phi),
                abs(cos(theta))  // abs() makes it a hemisphere, rather than a sphere
            };

            // the rotation matrix to rotate the normal at this point (p) to the z-axis
            std::array<std::array<float,3>,3> Qp = SpinMatrix::k(normalAtP);

            // rotate to orient around the tetra face normal using the rotation matrix
            const array<float,3> p_uvw {
                p_ijk[0]*Qp[0][0] + p_ijk[1]*Qp[0][1] + p_ijk[2]*Qp[0][2],
                p_ijk[0]*Qp[1][0] + p_ijk[1]*Qp[1][1] + p_ijk[2]*Qp[1][2],
                p_ijk[0]*Qp[2][0] + p_ijk[1]*Qp[2][1] + p_ijk[2]*Qp[2][2]
            };
            
            // set direction vector
            d = SSE::UnitVector3::normalize(SSE::Vector3(p_uvw));
        } else {
            // direction vector is simply the normal
            d = normalize(SSE::Vector3(normalAtP));
        }

        //// STEP 3: create the normal vectors for the direction vector d and create packet direction
        SSE::UnitVector3 a,b;
        tie(a,b) = SSE::normalsTo(d);

        return PacketDirection(d,a,b);
    }

    /**
     * Set the theta distribution
     * @param the distribution enum to use
     */
    void setThetaDistribution(ThetaDistribution d) {
        m_thetaDistribution = d;
    }

    /**
     * Set the theta distribution by a string
     * @param the distribution in string form (currently only: "UNIFORM", "LAMBERT" or "CUSTOM")
     */
    void setThetaDistribution(std::string dStr) {
        m_thetaDistribution = ThetaDistributionFunc::fromString(dStr);
    }

    // query some of the cylinder properties
    std::array<float,3> endpoint(int i) const { return m_endpoint[i]; }
	float radius() const { return m_radius; }
	float height() const { return m_height; }
    std::array<float,3> dirNormal() const { return m_dirNormal; }

private:
    // the tetra mesh being emitted from
    const TetraMesh* m_mesh;
    
    // refractive index of the material the photon is launched in
    float* m_refractiveIndex=nullptr; 
	
    //pointer to the material set
    const MaterialSet* m_materialset;

    // the RTree for queries
    TetraMesh::TetraMeshRTree* m_rtree;

    // the two endpoints of the cylinder
    std::array<float,3> m_endpoint[2];

    // the midpoint of the cylinder
    std::array<float,3> m_midpoint;

    // the normal vector of the cylinder
    std::array<float,3> m_dirNormal;

    // the radius of the cylinder
    float m_radius=1.0;

    // the height of the cylinder
    float m_height=0.0;

    // defining the numerical aperture of a tetrahedral face (default value emits in a hemisphere)
    float m_NA=1.0;

    // whether to emit on the hemisphere centered on the surface normal (false = emit normal)
    bool m_emitHemiSphere=true;

    // the theta angle distribution for hemisphere emittion (uniform or lambert)
    ThetaDistribution m_thetaDistribution=UNIFORM;

    // whether to emit within the volume of the cylinder (false = emit only on the surface)
    bool m_inVolume=false;

    // the spin matrix for the direction vector of the cylinder (aligns cylinder direction vector to z-axis)
    std::array<std::array<float,3>,3> Q;
};
}

#endif

