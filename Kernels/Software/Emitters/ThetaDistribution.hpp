#ifndef KERNELS_SOFTWARE_EMITTERS_THETADISTRIBUTION_HPP_
#define KERNELS_SOFTWARE_EMITTERS_THETADISTRIBUTION_HPP_

#include <FullMonteSW/Logging/FullMonteLogger.hpp>

#include <string>
#include <algorithm>

enum ThetaDistribution { UNIFORM, LAMBERT, CUSTOM };

class ThetaDistributionFunc {
public:
    static ThetaDistribution fromString(std::string str) {
        // case insensitive
        transform(str.begin(), str.end(), str.begin(), ::tolower);

        // convert from string to distribution type
        if (str == "uniform") {
            return ThetaDistribution::UNIFORM;
        } else if (str == "lambert") {
            return ThetaDistribution::LAMBERT;
        } else if (str == "custom") {
            return ThetaDistribution::CUSTOM;
        }else {
            LOG_ERROR << "thetaDistributionFromString string " << str << " not recognized - defaulting to UNIFORM\n";
            return ThetaDistribution::UNIFORM;
        }
    }

    static std::string toString(ThetaDistribution d) {
        if(d == ThetaDistribution::LAMBERT) {
            return "lambert";
        } else if (d == ThetaDistribution::CUSTOM) {
            return "custom";
        } else {
             return "uniform";
        }
    }

private:
    // static class
    ThetaDistributionFunc();
};

#endif
