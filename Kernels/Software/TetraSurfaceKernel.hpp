/*
 * TetraSurfaceKernel.hpp
 *
 *  Created on: Feb 1, 2016
 *      Author: jcassidy
 */

#ifndef KERNELS_SOFTWARE_TETRASURFACEKERNEL_HPP_
#define KERNELS_SOFTWARE_TETRASURFACEKERNEL_HPP_

#include "TetraMCKernel.hpp"
#include "RNG_SFMT_AVX.hpp"

#include "TetraMCKernelThread.hpp"

#include "Logger/LoggerTuple.hpp"
#include "Logger/SurfaceExitScorer.hpp"
#include "Logger/ConservationScorer.hpp"
#include "Logger/EventScorer.hpp"
#include "Logger/SurfaceExitImageScorer.hpp"
#include "Logger/PathScorer.hpp"

typedef std::tuple<
		EventScorer,
		ConservationScorer,
		SurfaceExitScorer,
		SurfaceExitImageScorer,
		PathScorer>
		TetraSurfaceScorer;

class TetraSurfaceKernel : public TetraMCKernel<RNG_SFMT_AVX,TetraSurfaceScorer>
{
public:
	TetraSurfaceKernel(){
		// deactivate in default
		get<4>(m_scorer).deactivate();
	}

	const EventScorer& 				eventScorer() 			const { return get<0>(m_scorer); }
	const ConservationScorer& 		conservationScorer() 	const { return get<1>(m_scorer); }
	const SurfaceExitScorer& 		surfaceScorer() 		const { return get<2>(m_scorer); }
	const SurfaceExitImageScorer& 	cameraScorer()			const { return get<3>(m_scorer); }
	const PathScorer&               pathScorer()            const { return get<4>(m_scorer); }

	void monitor(std::array<float,3> n, std::array<float,3> c, float r) {
		get<3>(m_scorer).monitor(n, c, r);
		get<4>(m_scorer).monitor(n, c, r);
	}

private:
	EventScorer& 				eventScorer()			{ return get<0>(m_scorer); }
	ConservationScorer& 		conservationScorer() 	{ return get<1>(m_scorer); }
	SurfaceExitScorer& 			surfaceScorer() 		{ return get<2>(m_scorer); }
	SurfaceExitImageScorer& 	cameraScorer()			{ return get<3>(m_scorer); }
	PathScorer& 	            pathScorer()			{ return get<4>(m_scorer); }
};



#endif /* KERNELS_SOFTWARE_TETRASURFACEKERNEL_HPP_ */

