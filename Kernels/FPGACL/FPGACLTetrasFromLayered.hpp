#ifndef KERNELS_SOFTWARE_FPGACLTETRASFROMLAYERED_HPP_
#define KERNELS_SOFTWARE_FPGACLTETRASFROMLAYERED_HPP_

#include <FullMonteSW/External/FPGACLAccel/FullMonteFPGACLTypes.h>

#include <vector>

class Layered;

/** Create FPGACL kernel tetras from a layered geometry description
 *
 * Uses degenerate tetras (with 2 faces at infinity) that will never see a photon interaction
 *
 *
 * For i=1..N, tetra i spans faces i-1,i and has thickness t_i
 * Face i is at z_i
 *
 * z_0 = 0
 * z_i = z_(i-1)+t_i
 *
 * Initial launch direction should be +z
 */
class FPGACLTetrasFromLayered {
public:
    void                                layers(const Layered* L);
    void                                update();

    const std::vector<FPGACL::Tetra>&   tetras() const;

private:
    const Layered*                      m_layers=nullptr;
    std::vector<FPGACL::Tetra>          m_tetras;
};

#endif
