#ifndef KERNELS_SOFTWARE_TETRAMCFPGACLKERNEL_HPP_
#define KERNELS_SOFTWARE_TETRAMCFPGACLKERNEL_HPP_

#include <FullMonteSW/Config.h>

#include <FullMonteSW/Geometry/Sources/Print.hpp>
#include <FullMonteSW/Geometry/MaterialSet.hpp>
#include <FullMonteSW/Geometry/Material.hpp>
#include <FullMonteSW/Geometry/Partition.hpp>
#include <FullMonteSW/Geometry/Layer.hpp>
#include <FullMonteSW/Geometry/Layered.hpp>
#include <FullMonteSW/Geometry/FaceLinks.hpp>
#include <FullMonteSW/Geometry/Permutation.hpp>

#include <FullMonteSW/OutputTypes/SpatialMap.hpp>
#include <FullMonteSW/OutputTypes/OutputData.hpp>
#include <FullMonteSW/OutputTypes/OutputDataCollection.hpp>
#include <FullMonteSW/OutputTypes/DirectedSurface.hpp>

#include <FullMonteSW/Kernels/Software/Material.hpp>
#include <FullMonteSW/Kernels/Software/RNG_SFMT_AVX.hpp>

// this is a bit weird, but we borrow some Emitter stuff from the Kernels/Software directory
// TODO: Should Emitters be taken out of the Software directory?
#include <FullMonteSW/Kernels/Software/Emitters/Base.hpp>
#include <FullMonteSW/Kernels/Software/Emitters/Point.hpp>
#include <FullMonteSW/Kernels/Software/Emitters/Directed.hpp>
#include <FullMonteSW/Kernels/Software/Emitters/TetraMeshEmitterFactory.hpp>

#include <FullMonteSW/Kernels/FPGACL/FPGACLMCKernelBase.hpp>
#include <FullMonteSW/Kernels/FPGACL/FPGACLTetrasFromLayered.hpp>
#include <FullMonteSW/Kernels/FPGACL/FPGACLTetrasFromTetraMesh.hpp>

// these are from the FPGACLAccel library
#include <FullMonteSW/External/FPGACLAccel/FPGACLAccel.h>
#include <FullMonteSW/External/FPGACLAccel/FullMonteFPGACLTypes.h>
#include <FullMonteSW/External/FPGACLAccel/FullMonteFPGACLConstants.h>

#include <boost/align/aligned_alloc.hpp>
#include <boost/random/additive_combine.hpp>

#include <string>
#include <algorithm>
#include <random>
#include <ctime>
#include <chrono>

class Permutation;
class GeometryPredicate;
class VolumeCellPredicate;
class SurfaceCellPredicate;

////////////////////////////////////////////////////////////////////////////
/** Derived by TetraFPGACLMCKernel. Following format of TetraMCKernel. */
class TetraFPGACLKernel {
protected:
    // the tetras for the FPGACL kernel
    std::vector<FPGACL::Tetra>        m_tetras;
    
};
////////////////////////////////////////////////////////////////////////////

/**
 * Base class of all Tetra MC-based FPGACL simulators.
 * NOTE: This does little to no acceleration. This is simply a wrapper to
 * call methods from the FPGACL accelerator (External/FPGACLAccel) which performs
 * the actual acceleration.
 */
template<class RNGT>
class TetraMCFPGACLKernel : public FPGACLMCKernelBase, public TetraFPGACLKernel {
public:
    // RNGT is now RNG - whatever... TetraMCKernel did it    
    typedef RNGT RNG;

    /**
     * Constructor
     */
    TetraMCFPGACLKernel() {
        // allocate sizeof(RNG) bytes aligned to 32 bytes
        // NOTE: this does NOT initialize the memory, that is done below
        void* p = boost::alignment::aligned_alloc(32,sizeof(RNG));

        // ensure the allocation succeeded
        if (!p) {
	        cerr << "Allocation failure in TetraMCKernel<RNGT,Scorer>::makeThread()" << endl;
		    throw std::bad_alloc();
	    }

        // memory allocation was good, now use this aligned memory to initialize the RNG
        // NOTE: this calls the constructor of RNG (and therefore all child constructors).
        // If you simply cast p (i.e. m_rng = (RNG*) p) you won't get compile errors but the
        // RNG (notably the position into the buffer 'm_pos') will not be initialized and segfaults occur.
        m_rng = new (p) RNG();
    }

    /**
     * Destructor
     */ 
    ~TetraMCFPGACLKernel() {
        // destroy the accelerator
        FPGACL::destroy();

        // free aligned dynamic memory
        boost::alignment::aligned_free(m_rng);
    }

    ////////////////////////////
    // wrappers for calls to FPGACLAccel API see External/FPGACLAccel for more details
    // TODO: this is work in progress on the FPGACLAccel end
    //int gpuDevice() { return cudaAccelerator.currentDeviceID(); }
    //void gpuDevice(const int devID) { cudaAccelerator.changeDevice(devID); }
    //void listGPUDevices() { cudaAccelerator.listDevices(); }
    //size_t maxTetraEstimate(unsigned int packetCountHint=0) { return cudaAccelerator.maxTetraEstimate(packetCountHint); }
    ////////////////////////////

    // getter/setter for binary file
    std::string binaryFile() { return m_aocxBinFile; }
    void binaryFile(const std::string file) { m_aocxBinFile = file; }
    
protected:
    // overide the methods of FPGACLMCKernelBase
    virtual void parentPrepare() override;
    virtual void parentSync() override;
    virtual void parentStart() override;
    virtual void parentGather() override;

    // gather helpers
    void gatherVolume();
    void gatherSurfaceExit();
    void gatherDirectedSurface();
    
    // the list of emitters - launching is done host side
    Emitter::EmitterBase<RNG>*              m_emitter;
    
    // the FPGACL representation of the materials (list of materials)    
    std::vector<FPGACL::Material>           m_mats;

    // the host side random number generator (MUST BE ALIGNED TO 32 BYTES)
    // since SWIG cannot handle alignas() we cannot guarentee their wrappers
    // will allocate the RNG to 32 bytes (causing runtime errors in the TCL
    // interfaces but not in the C++ version). So instead we dynamically
    // allocate it at runtime and force the 32 byte alignment
    // (see TetraMCFPGACLKernel constructor) to avoid this problem.
    RNG*                                    m_rng;

    // the AOCX file to use to program the FPGA. Default to full_monte_device.aocx in working directory
    std::string                             m_aocxBinFile="full_monte_device.aocx";
};

///////////////////////////////////////////////////////////////////////////////////////////////
// Implementation below
std::chrono::steady_clock::time_point start;

/**
 * Prepare the Kernel to be run. Called before the kernel is actually run
 * so do some prelim stuff to get the kernel ready.
 */
template<class RNGT>
void TetraMCFPGACLKernel<RNGT>::parentPrepare() {
    ////////////////////////////////
    //// Material setup
    MaterialSet* MS = materials();
    if (!MS)
		throw std::logic_error("TetraMCCUDAKernel<RNGT>::parentPrepare() no materials specified");
    // convert SW material to FPGACL representation
    float mu_a, mu_s, mu_t, n, g;
    m_mats.resize(materials()->size());

    for(unsigned i = 0; i < materials()->size(); ++i) {
        // get the material from the material set
        ::Material* m = materials()->get(i);
        mu_a = m->absorptionCoeff();
        mu_s = m->scatteringCoeff();
        n = m->refractiveIndex();
        g = m->anisotropy();
        mu_t = mu_a + mu_s;

        // create the FPGACL::Material
        m_mats[i].n = n;
        m_mats[i].mu_s = mu_s;
        m_mats[i].mu_a = mu_a;
        m_mats[i].g = g;
        m_mats[i].gg = g*g;
        m_mats[i].recip_2g = 1.0f/(2.0f*g);
        m_mats[i].one_minus_gg = 1.0f - g*g;
        m_mats[i].one_plus_gg = 1.0 + g*g;
        m_mats[i].mu_t = mu_t;
        m_mats[i].recip_mu_t = 1.0f / mu_t;
        m_mats[i].m_prop = {-1.0f, -mu_t, n/SPEED_OF_LIGHT_MM_PER_NS};
        m_mats[i].m_init = {1.0f/mu_t, 1.0f, 0.0f};
        m_mats[i].absfrac = mu_a / mu_t;
        m_mats[i].scatters = (mu_s != 0.0f);
        m_mats[i].isotropic = (g < 0.001f);
    }
    ////////////////////////////////
    
    ////////////////////////////////
    //// Geometry setup
    const Geometry* G = geometry();

    if(!G) {
        throw std::logic_error("TetraMCFPGACLKernel<RNG>::parentPrepare() no geometry specified");
    }

    //unsigned numFaces_ = 0;
	if(const TetraMesh* M = dynamic_cast<const TetraMesh*>(G)) {
        //// TETRA MESH ////
		cout << "Building FPGACL kernel tetras from TetraMesh" << endl;
        
        // grab number of faces
        //numFaces_ = M->faceTetraLinks()->size();

        FPGACLTetrasFromTetraMesh TF;
		TF.mesh(M);
		TF.update();
		m_tetras = TF.tetras();

		if(!m_src) {
            throw std::logic_error("TetraMCFPGACLKernel<RNGT>::parentPrepare() no sources specified");
        }

        // TODO: Move this out of here as it doesn't quite belong
        Emitter::TetraEmitterFactory<RNGT> factory(M,MS);
        ((Source::Abstract*)m_src)->acceptVisitor(&factory);
        m_emitter = factory.emitter();
    } else if (const Layered* L = dynamic_cast<const Layered*>(G)) {
        //// LAYERED ////
        cout << "Building FPGACL kernel tetras from Layered geometry (" << L->layerCount() << " layers)" << endl;
        
        // no faces in Layered, cannot score faces
        //numFaces_ = 0;

        FPGACLTetrasFromLayered TL;       
        TL.layers(L);
        TL.update();
        m_tetras = TL.tetras();

        if(m_src) {
            cout << "TetraMCFPGACLKernel::parentPrepare() ignoring provided source with Layered geometry (only PencilBeam allowed)" << endl;
        }

//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
        SSE::UnitVector3 d{{{ 0.0f, 0.0f, 1.0f }}};
        SSE::UnitVector3 a{{{ 1.0f, 0.0f, 0.0f }}};
        SSE::UnitVector3 b{{{ 0.0f, 1.0f, 0.0f }}};

        array<float,3> origin{{0.0f,0.0f,0.0f}};
#else
        SSE::UnitVector3 d({0.0f, 0.0f, 1.0f, 0.0f });
        SSE::UnitVector3 a({1.0f, 0.0f, 0.0f, 0.0f });
        SSE::UnitVector3 b({0.0f, 1.0f, 0.0f, 0.0f });

        array<float,3> origin{{0.0f,0.0f,0.0f}};
#endif
        Emitter::Point P{1U,SSE::Vector3(origin.data())};
        Emitter::Directed D(PacketDirection(d,a,b));
        
        m_emitter = new Emitter::PositionDirectionEmitter<RNGT,Emitter::Point,Emitter::Directed>(P,D);
    } else {
        throw std::logic_error("TetraMCFPGACLKernel<RNG>::parentPrepare() geometry is neither Layered nor TetraMesh");
    }

    ////////////////////////////////

    // seed the RNG
    m_rng->seed(rngSeed_);

    //// Initialize the FPGACL accelerator.
    // Must be done AFTER materials and tetras are setup (above)
    std::chrono::steady_clock::time_point a = std::chrono::steady_clock::now();

    FPGACL::init(
        m_aocxBinFile,
        rngSeed_,
        wmin_,
        prwin_,
        m_mats,
        m_tetras
    );

    std::chrono::steady_clock::time_point b = std::chrono::steady_clock::now();
    std::cout << "OpenCL FPGA initialization took " << std::chrono::duration_cast<std::chrono::milliseconds>(b - a).count() << "ms" << std::endl;
}

/**
 * Start the simulator
 * Launches the actual Intel FPGA OpenCL accelerator
 */
template<class RNGT>
void TetraMCFPGACLKernel<RNGT>::parentStart() {
    // check if we have emitters
    if(!m_emitter) {
        throw std::logic_error("Missing emitter");
    }

    //// LAUNCHING PACKETS IN THE HOST ////
    // TODO(FUTURE): Do this in a loop to reduce globally memory requirement of this array in the device??

    // reserve space for for Npkt_ packets
    std::vector<FPGACL::LaunchPacket> launched_packets;

    // launch each packet
    // TODO: thread?
    FPGACL::LaunchPacket tmp;
    for(unsigned long long i = 0; i < Npkt_; i++) {
        // generator a launch packet in the host format
        LaunchPacket lpkt = m_emitter->emit(*m_rng, Npkt_, i);

        // convert the host style launch packet to the FPGACL version
        // position
        tmp.p = {
            lpkt.pos[0],
            lpkt.pos[1],
            lpkt.pos[2]
        };

        // direction
        tmp.dir = {
            {lpkt.dir.d[0], lpkt.dir.d[1], lpkt.dir.d[2]},
            {lpkt.dir.a[0], lpkt.dir.a[1], lpkt.dir.a[2]},
            {lpkt.dir.b[0], lpkt.dir.b[1], lpkt.dir.b[2]}
        };

        // tetra ID
        tmp.tetraID = lpkt.element;

        launched_packets.push_back(tmp);
    }
    
    // actually launch the accelerator (async)
    FPGACL::launch(launched_packets);
    
    start = std::chrono::steady_clock::now();
}

/**
 * Wait for the simulation to finish.
 */
template<class RNGT>
void TetraMCFPGACLKernel<RNGT>::parentSync() {
    // wait for the Intel FPGA OpenCL kernel to finish
    FPGACL::sync();

    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    std::cout << "OpenCL FPGA kernel took " << std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count() << "ms" << std::endl;
}

/**
 * Gather the results from this simulation.
 */
template<class RNGT>
void TetraMCFPGACLKernel<RNGT>::parentGather() {
    // clear the results
    OutputDataCollection* C = results();
    C->clear();
    
    // only volume absorption supported for now - do it
    gatherVolume();
}

/**
 * Gather the volume absorption data from the device.
 */
template<class RNGT>
void TetraMCFPGACLKernel<RNGT>::gatherVolume() {
    // gather the results to the OutputDataCollection
    OutputDataCollection* C = results();

    // get data from device and convert to float
    std::vector<float> vf(m_tetras.size());
    FPGACL::copyTetraEnergy(vf);

    // NOTE: Since we ALWAYS insert a zero tetra into the mesh, we need to account for this
    // and remove the first entry (the zero tetra) from the output array
    vf.erase(vf.begin());

    // create map
    SpatialMap<float> *vmap = new SpatialMap<float>(std::move(vf), AbstractSpatialMap::Volume, AbstractSpatialMap::Scalar,AbstractSpatialMap::PhotonWeight);
    vmap->name("VolumeEnergy");

    // add the volume absorptions to the output data
    C->add(vmap);
}


#endif

