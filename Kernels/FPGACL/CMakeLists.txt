# check that INTELFPGAOCLSDKROOT env var is set
if(NOT DEFINED ENV{INTELFPGAOCLSDKROOT})
    message(FATAL_ERROR "Set INTELFPGAOCLSDKROOT to the root directory of the Intel(R) FPGA SDK for OpenCL(TM) software installation")
endif()

# ensure we can find 'aocl'
find_program(AOCL_PROG aocl)
if(AOCL_PROG STREQUAL "AOCL-NOTFOUND")
    message(FATAL_ERROR "Could not find the 'aocl' program")
endif()

# create FullMonteFPGACLKernel from the files
ADD_LIBRARY(FullMonteFPGACLKernel SHARED
    FPGACLTetrasFromLayered.cpp
    FPGACLTetrasFromTetraMesh.cpp
    FPGACLMCKernelBase.cpp
    TetraMCFPGACLKernel.cpp
)

# tell the compiler we are building the host
add_definitions(-DFPGACL_HOST_BUILD)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wno-unknown-pragmas")

############################
## Include directories
# execute 'aocl compile-config' to get the OpenCL include directories
execute_process(
    COMMAND aocl compile-config
    OUTPUT_VARIABLE AOCL_COMPILE_CONFIG
)
# strip leading and trailing white space from aocl command
string(STRIP ${AOCL_COMPILE_CONFIG} AOCL_COMPILE_CONFIG)

# replace '-I' with nothing since -I will be added back by cmake
string(REPLACE "-I" "" AOCL_COMPILE_CONFIG ${AOCL_COMPILE_CONFIG})

# separate the string arguments into a list of strings
separate_arguments(AOCL_COMPILE_FLAGS)

# the include directories for AOC and the utils in External/FPGACLAccel
include_directories(
    ../../External/FPGACLAccel/opencl_util/inc
    ${AOCL_COMPILE_CONFIG}
)
############################

############################
## Library linking
# execute 'aocl link-config' to get the OpenCL dynamic librariese
execute_process(
    COMMAND aocl link-config
    OUTPUT_VARIABLE AOCL_LINK_CONFIG
)
# strip leading and trailing spaces from aocl command
string(STRIP ${AOCL_LINK_CONFIG} AOCL_LINK_CONFIG)

# separate the string by spaces for a list of arguments
separate_arguments(AOCL_LINK_FLAGS)
############################

# linked libraries
TARGET_LINK_LIBRARIES(FullMonteFPGACLKernel
    SFMT
    FullMonteGeometry
    FullMonteKernelBase
    FullMonteSWKernel
    FullMonteData
    FullMonteMCMLFile
    FullMonteGeometryPredicates
    FullMonteFPGACLAccel
)

# build FullMonteFPGACLKernel
INSTALL(TARGETS FullMonteFPGACLKernel DESTINATION lib)

# TCL wrapping
IF(WRAP_TCL)
    SET_SOURCE_FILES_PROPERTIES(FullMonteFPGACLKernel.i PROPERTIES CPLUSPLUS ON)
    SWIG_ADD_LIBRARY(FullMonteFPGACLKernelTCL 
        TYPE SHARED 
        LANGUAGE tcl 
        SOURCES FullMonteFPGACLKernel.i
    )
    SWIG_LINK_LIBRARIES(FullMonteFPGACLKernelTCL 
        ${TCL_LIBRARY} 
        FullMonteGeometry 
        FullMonteSWKernel 
        FullMonteFPGACLKernel 
        FullMonteData 
        FullMonteFPGACLAccel 
        ${AOCL_LINK_CONFIG}
    )

    INSTALL(TARGETS FullMonteFPGACLKernelTCL DESTINATION lib)
ENDIF()

# Python wrapping
IF(WRAP_PYTHON)
    SET_SOURCE_FILES_PROPERTIES(FullMonteFPGACLKernel.i PROPERTIES CPLUSPLUS ON)
    
    SWIG_ADD_LIBRARY(FullMonteFPGACLKernelPython 
        TYPE SHARED 
        LANGUAGE python 
        SOURCES FullMonteFPGACLKernel.i
    )
    SWIG_LINK_LIBRARIES(FullMonteFPGACLKernelPython
        ${Python3_LIBRARIES}
        FullMonteGeometry
        FullMonteSWKernel
        FullMonteFPGACLKernel
        FullMonteData
        FullMonteFPGACLAccel
        ${AOCL_LINK_CONFIG}
    )
    SET_TARGET_PROPERTIES(_FPGACLKernel PROPERTIES LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib/fmpy)
ENDIF()

