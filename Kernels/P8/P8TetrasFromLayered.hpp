/*
 * P8TetrasFromLayers.hpp
 *
 *  Created on: Mar 10, 2019
 *      Author: fynns
 */

#ifndef KERNELS_P8_P8TETRASFROMLAYERED_HPP_
#define KERNELS_P8_P8TETRASFROMLAYERED_HPP_

#include <vector>

#include <unordered_map>
#include <boost/range/adaptor/indexed.hpp>
#include <boost/functional/hash.hpp>

#include <FullMonteHW/Host/CXXFullMonteTypes.hpp>

#include <FullMonteSW/Geometry/MaterialSet.hpp>
#include <FullMonteSW/Geometry/Material.hpp>
#include <FullMonteSW/Kernels/Software/Material.hpp>

/** Create kernel tetras from a layered geometry description
 *
 * Uses degenerate tetras (with 2 faces at infinity) that will never see a photon interaction
 *
 *
 * For i=1..N, tetra i spans faces i-1,i and has thickness t_i
 * Face i is at z_i
 *
 * z_0 = 0
 * z_i = z_(i-1)+t_i
 *
 * Initial launch direction should be +z
 */

class Layered;

class P8TetrasFromLayered
{
public:
	P8TetrasFromLayered();
	~P8TetrasFromLayered();

	void									layers(const Layered* L);
	void									update(const MaterialSet* Mat);

	const std::vector<FullMonteHW::TetraDef>&	tetras() const;

private:
	const Layered*							m_layers=nullptr;
	std::vector<FullMonteHW::TetraDef>		m_tetras;

	// This simply is a function to hash combinations of material properties into one unsigned number
	// The hashes are not used in HW, perhaps in SW tests
	std::unordered_map< std::pair<unsigned,unsigned>, unsigned, boost::hash<std::pair<unsigned,unsigned>>> m_interfaceMap;

};



#endif /* KERNELS_P8_P8TETRASFROMLAYERED_HPP_ */
