FIND_PACKAGE(FullMonteHW)

# Normal P8 library
ADD_LIBRARY(FullMonteP8Kernel SHARED 
		P8TetrasFromLayered.cpp
		P8TetrasFromTetraMesh.cpp
		P8MCKernelBase.cpp 
		TetraMCP8Kernel.cpp
		TetraP8InternalKernel.cpp
		TetraMCP8DebugKernel.cpp
		TetraP8DebugInternalKernel.cpp
	)
TARGET_LINK_LIBRARIES(FullMonteP8Kernel
		SFMT 
		BlueLinkHost 
		${CAPI_CXL_LIBRARY} 
		gmp 
		BitPacker
		CXXFullMonteTypes 
		FullMonteKernelBase
		FullMonteSWKernel
		FullMonteGeometry
		FullMonteGeometryPredicates 
		FullMonteData
		FullMonteMCMLFile
	)

INSTALL(TARGETS FullMonteP8Kernel
	DESTINATION lib)

# TCL wrapping
IF(WRAP_TCL)
	SET_SOURCE_FILES_PROPERTIES(FullMonteP8Kernel.i PROPERTIES CPLUSPLUS ON)
	SWIG_ADD_LIBRARY(FullMontePowerKernelTCL 
		TYPE SHARED 
		LANGUAGE tcl 
		SOURCES FullMonteP8Kernel.i
	)
	SWIG_LINK_LIBRARIES(FullMontePowerKernelTCL 
		${TCL_LIBRARY} 
		FullMonteGeometry 
		FullMonteSWKernel 
		FullMonteP8Kernel 
		FullMonteData
	)

	INSTALL(TARGETS FullMontePowerKernelTCL LIBRARY 
		DESTINATION lib
	)
ENDIF()

# Python wrapping
IF(WRAP_PYTHON)
	SET_SOURCE_FILES_PROPERTIES(FullMonteP8Kernel.i PROPERTIES CPLUSPLUS ON)
	SWIG_ADD_LIBRARY(P8Kernel 
		TYPE SHARED 
		LANGUAGE python 
		SOURCES FullMonteP8Kernel.i
	)
	SWIG_LINK_LIBRARIES(P8Kernel 
		${Python3_LIBRARIES} 
		FullMonteGeometry 
		FullMonteSWKernel 
		FullMonteP8Kernel 
		FullMonteData
	)
	SET_TARGET_PROPERTIES(_P8Kernel PROPERTIES LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib/fmpy)
ENDIF()