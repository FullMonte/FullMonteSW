/*
 * TetraP8DebugInternalKernel.cpp
 *
 *  Created on: Jan 22, 2019
 *      Author: fynns
 */

#include "TetraP8DebugInternalKernel.hpp"



void TetraP8DebugInternalKernel::parentPrepare()
{
	TetraMCP8DebugKernel<RNG_SFMT_AVX>::parentPrepare();
}

void TetraP8DebugInternalKernel::parentStart()
{
	TetraMCP8DebugKernel<RNG_SFMT_AVX>::parentStart();
}
