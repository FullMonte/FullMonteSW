/*
 * TetrasFromTetraMesh.hpp
 *
 *  Created on: May 17, 2017
 *      Author: jcassidy
 */

#ifndef KERNELS_P8_P8TETRASFROMTETRAMESH_HPP_
#define KERNELS_P8_P8TETRASFROMTETRAMESH_HPP_


/** Creates kernel tetras from a TetraMesh description
 *
 *
 */

#include <FullMonteSW/Logging/FullMonteLogger.hpp>

#include <FullMonteHW/Host/CXXFullMonteTypes.hpp>
#include <FullMonteSW/Geometry/TetraMesh.hpp>

#include <FullMonteSW/Geometry/MaterialSet.hpp>
#include <FullMonteSW/Geometry/Material.hpp>
#include <FullMonteSW/Kernels/Software/Material.hpp>

#include <unordered_map>
#include <boost/functional/hash.hpp>
#include <boost/range/adaptor/indexed.hpp>
#include <cstdlib>

#include <vector>

class Partition;
class TetraMesh;

class P8TetrasFromTetraMesh
{
public:
	P8TetrasFromTetraMesh();
	~P8TetrasFromTetraMesh();

	std::pair<std::array<double,3>,double> mesh(const TetraMesh* M);
	void update(const MaterialSet* Mat);

	/// Checks that the kernel faces make sense
	bool checkKernelFaces() const;

	unsigned int face_numbers() const;

	const std::vector<FullMonteHW::TetraDef>& tetras() const;

	unsigned int getInterfaceID(unsigned matID1, unsigned matID2);

private:

	void makeKernelTetras(const MaterialSet* Mat);

	static FullMonteHW::TetraDef convert(const TetraMesh& M,TetraMesh::TetraDescriptor T, const MaterialSet* Mat, const double scale, const std::array<double,3> translation_vector, const std::unordered_map<std::pair<unsigned,unsigned>, unsigned, boost::hash<std::pair<unsigned,unsigned>>> interfaceMap);

	/** Packed and aligned tetra representation for the kernel, holding face normals, constants, adjacent tetras,
	 * bounding faces, material ID, and face flags (for logging).
	 */
	std::array<double,3>						m_translation_vector;
	double 										m_scaling_parameter;
	const TetraMesh*							m_mesh=nullptr;
    std::vector<FullMonteHW::TetraDef>       	m_tetras;
	
	std::unordered_map<std::pair<unsigned,unsigned>, unsigned, boost::hash<std::pair<unsigned,unsigned>>> m_interfaceMap;

};

#endif /* KERNELS_P8_P8TETRASFROMTETRAMESH_HPP_ */
