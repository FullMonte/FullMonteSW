/*
 * SeedSweep.cpp
 *
 *  Created on: Jul 8, 2017
 *      Author: jcassidy
 */

#include "SeedSweep.hpp"
#include "MCKernelBase.hpp"

#include <FullMonteSW/OutputTypes/OutputDataCollection.hpp>
#include <FullMonteSW/OutputTypes/OutputData.hpp>
#include <FullMonteSW/OutputTypes/OutputDataType.hpp>

using namespace std;

#include <iostream>
#include <iomanip>

#include <boost/iterator/counting_iterator.hpp>



SeedSweep::SeedSweep()
{

}

SeedSweep::~SeedSweep()
{

}

void SeedSweep::kernel(MCKernelBase* k)
{
	m_kernel=k;
}

void SeedSweep::seedRange(unsigned from,unsigned to)
{
	boost::counting_iterator<unsigned> b(from);
	boost::counting_iterator<unsigned> e(to);
	boost::iterator_range<boost::counting_iterator<unsigned>> R(b,e);;

	m_seedRange = R;
}

void SeedSweep::run()
{
	for(auto r : m_resultsByType)
		delete r;
	m_resultsByType.clear();

	for(const auto s : m_seedRange)
	{
		if (m_printProgress)
			cout << setw(4) << s << ' ' << flush;
		m_kernel->randSeed(s);
		m_kernel->runSync();

		OutputDataCollection* res = m_kernel->results();

		for(unsigned i=0;i<res->size();++i)
		{
			if (i >= m_resultsByType.size())
			{
				m_resultsByType.resize(i+1);
				m_resultsByType[i] = new OutputDataCollection;
			}
			m_resultsByType[i]->add(res->getByIndex(i));
		}
	}
	cout << endl;
}

void SeedSweep::printProgress(bool p)
{
	m_printProgress=p;
}

unsigned SeedSweep::runs() const
{
	if (m_resultsByType.size() == 0 || !m_resultsByType[0])
		return 0;
	else
		return m_resultsByType[0]->size();
}

OutputDataCollection* SeedSweep::resultsByName(string tn) const
{
	for(const auto r : m_resultsByType)
	{
		OutputData *d = r->getByIndex(0);
		if (!d)
		{}//	cout << "No data" << endl;
		else if(d->name() == tn)
			return r;
//		else
//			cout << "Not a '" << d->name() << "'" << endl;
	}
	return nullptr;
}

