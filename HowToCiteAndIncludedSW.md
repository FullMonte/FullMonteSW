Citations
If your work with this software results in any academic publications, please give credit by citing:

High-performance, robustly verified Monte Carlo simulation with FullMonte. http://apps.webofknowledge.com.myaccess.library.utoronto.ca/full_record.do?product=UA&search_mode=GeneralSearch&qid=1&SID=7EYb5H7oXj2hSz2LnJp&page=1&doc=1 By: Cassidy, Jeffrey; Nouri, Ali; Betz, Vaughn; et al. Journal of biomedical optics  Volume: 23   Issue: 8   Pages: 1-11   Published: 2018-Aug

Bibtex:

@article{doi: 10.1117/1.JBO.23.8.085001,
author = { Jeffrey  Cassidy,Ali  Nouri,Vaughn  Betz,Lothar  Lilge},
title = {High-performance, robustly verified Monte Carlo simulation with FullMonte},
journal = {Journal of Biomedical Optics},
volume = {23},
number = {},
pages = {23 - 23 - 11},
year = {2018},
doi = {10.1117/1.JBO.23.8.085001},
URL = {https://doi.org/10.1117/1.JBO.23.8.085001},
eprint = {}
}



Automatic interstitial photodynamic therapy planning via convex optimization http://apps.webofknowledge.com.myaccess.library.utoronto.ca/full_record.do?product=UA&search_mode=GeneralSearch&qid=1&SID=7EYb5H7oXj2hSz2LnJp&page=1&doc=2 By: Yassine, Abdul-Amir; Kingsford, William; Xu, Yiwen; et al. BIOMEDICAL OPTICS EXPRESS  Volume: 9   Issue: 2   Pages: 898-920   Published: FEB 1 2018

Bibtex:

@article{doi: 10.1117/1.JBO.23.8.085001,
author = { Yassine, A.-A., Kingsford, W., Xu, Y., Cassidy, J., Lilge, L., & Betz, V.},
title = {Automatic interstitial photodynamic therapy planning via convex optimization},
journal = {Biomedical Optics Express},
volume = {9},
number = {2},
pages = {989 - 920},
year = {2018},
doi = {10.1117/1.JBO.23.8.085001},
URL = {http://doi.org/10.1364/BOE.9.000898},
eprint = {}
}



Treatment plan evaluation for interstitial photodynamic therapy in a mouse model by Monte Carlo simulation with FullMonte
http://apps.webofknowledge.com.myaccess.library.utoronto.ca/full_record.do?product=UA&search_mode=GeneralSearch&qid=1&SID=7EYb5H7oXj2hSz2LnJp&page=1&doc=4 By: Cassidy, Jeffrey; Betz, Vaughn; Lilge, Lothar FRONTIERS IN PHYSICS  Volume: 3     Article Number: 6   Published: FEB 24 2015

Bibtex:

@article{10.3389/fphy.2015.00006,
author={Cassidy, Jeffrey and Betz, Vaughn and Lilge, Lothar},
title={Treatment plan evaluation for interstitial photodynamic therapy in a mouse model by Monte Carlo simulation with FullMonte},
journal={Frontiers in Physics},
volume={3},
pages={6},
year={2015},
url={https://www.frontiersin.org/article/10.3389/fphy.2015.00006},
doi={10.3389/fphy.2015.00006},
issn={2296-424X}
}


Use of data from Digimouse or TIM-OS should also be cited appropriately as specified on the websites listed below.

External packages

cJSON
Contains Dave Gamble's cJSON parser for dealing with MMC-format input files.

SFMT
Contains SFMT (Matsumoto and Saito's SIMD-Oriented Fast Mersenne Twister) in SFMT/, which is subject to separate copyright and
license provisions (see SFMT/LICENSE). The CMakeLists.txt file contained there is my own work, but the files are otherwise the original SFMT-1.4.1 from the authors' website.

TIM-OS test data
The files in data/TIM-OS contain the TIM-OS test suite available here from Shen and Wang (2010), including a version of Dogdas et al's Digimouse (2007).

Julien Pommier's SSEMATH
Contains Julien Pommier's "ssemath.h" SSE math library which is available under the zlib license as Kernels/Software/sse_mathfun.h. The SSE implementations were extended to larger vector lengths using AVX instructions avx\_mathfun.h.

MCML
Contains a copy of MCML (Externals/MCML), obtained from OMLC.
