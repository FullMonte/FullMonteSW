/*
 * fullmonte_memtrace.cpp
 *
 *  Created on: Nov 9, 2017
 *      Author: jcassidy
 */

#include <FullMonteSW/Kernels/Software/RNG_SFMT_AVX.hpp>
#include <FullMonteSW/Kernels/Software/TetraMCKernel.hpp>

#include <FullMonteSW/Kernels/Software/Logger/LoggerTuple.hpp>
#include <FullMonteSW/Kernels/Software/TetraMCKernelThread.hpp>

#include <FullMonteSW/Storage/TIMOS/TIMOSMeshReader.hpp>
#include <FullMonteSW/Storage/TIMOS/TIMOSMaterialReader.hpp>
#include <FullMonteSW/Storage/TIMOS/TIMOSSourceReader.hpp>

#include <FullMonteSW/OutputTypes/MemTraceSet.hpp>
#include <FullMonteSW/OutputTypes/MemTrace.hpp>

#include <FullMonteSW/OutputTypes/OutputDataCollection.hpp>
#include <FullMonteSW/OutputTypes/OutputDataSummarize.hpp>

#include <FullMonteSW/Warnings/Push.hpp>
#include <FullMonteSW/Warnings/Boost.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/value_semantic.hpp>
#include <FullMonteSW/Warnings/Pop.hpp>

#include <FullMonteSW/Kernels/Software/Logger/MemTraceScorer.hpp>
#include <FullMonteSW/Kernels/Software/Logger/EventScorer.hpp>


#include <fstream>
#include <string>
#include <tuple>

// Define the kernel that just scores memory traces

typedef tuple<
		EventScorer,
		TetraMemTraceScorer> MemTraceKernelScorer;

class MemTraceKernel : public TetraMCKernel<RNG_SFMT_AVX,MemTraceKernelScorer>
{
public:
	/**
	 * @brief Construct a new Mem Trace Kernel object. This is just a test class for memory traces when using FullMonte
	 * and doesn't belong to the normal Kernel types of FullMonte <-- Can be ignored for TCL 
	 * 
	 */
	MemTraceKernel(){}
	virtual ~MemTraceKernel(){}

	typedef RNG_SFMT_AVX RNG;
};



using namespace std;

int main(int argc,char **argv)
{
	boost::program_options::options_description cmdline;

	string fnMesh, fnMat, fnSource, fnOut;

	unsigned long long Npkt;
	unsigned Nth;

	cmdline.add_options()
			("mesh,m",boost::program_options::value<string>(&fnMesh),"Mesh to process")
			("materials,o",boost::program_options::value<string>(&fnMat),"Materials file")
			("source,s",boost::program_options::value<string>(&fnSource),"Source definitions file")
			("packets,N",boost::program_options::value<unsigned long long>(&Npkt),"Packet count")
			("threads,t",boost::program_options::value<unsigned>(&Nth),"Thread count")
			("output-name",boost::program_options::value<string>(&fnOut),"Output file prefix")
			("help,h","Get help")
			;

	boost::program_options::variables_map vm;

	boost::program_options::store(boost::program_options::parse_command_line(argc,argv,cmdline),vm);
	vm.notify();

	if (vm.count("help"))
	{
		cout << cmdline << endl;
		return -1;
	}
	else if (fnMesh.empty() || fnMat.empty() || fnSource.empty())
	{
		cout << "Missing mesh, materials, or source file" << endl;
		cout << cmdline << endl;
		return -1;
	}

	cout << "Mesh: " << fnMesh << endl;

	TIMOSMeshReader MR;
	MR.filename(fnMesh);
	MR.read();
	TetraMesh* M = MR.mesh();
	
	cout << "Materials: " << fnMat << endl;

	TIMOSMaterialReader OR;
	OR.filename(fnMat);
	OR.read();
	MaterialSet* MS = OR.materials();

	cout << "Sources: " << fnSource << endl;

	TIMOSSourceReader SR;
	SR.filename(fnSource);
	SR.read();
	Source::Abstract* S = SR.source();

	cout << "Packets: " << Npkt << endl;
	cout << "Threads: " << Nth << endl;

	MemTraceKernel K;

	K.packetCount(Npkt);
	K.threadCount(Nth);
	K.geometry(M);
	K.source(S);
	K.materials(MS);

	K.runSync();

	OutputDataCollection* res = K.results();

	const auto memtrace = static_cast<const MemTraceSet*>			(res->getByName("TetraMemTrace"));
	//const auto memcount = static_cast<const SpatialMap<unsigned>*>	(res->getByName("MemAccessCount"));

	const auto events = static_cast<MCEventCountsOutput*>		(res->getByName("EventCounts"));

	if (events)
	{
		OutputDataSummarize OS;
		OS.visit(events);
	}
	else
		cout << "Event counts missing" << endl;


	if (memtrace)
	{
		// Write raw traces, and accumulate total access counts
		vector<unsigned> accessCounts;

		ofstream os;

		//cout << "Memory trace set found with " << memtrace->size() << " entries" << endl;
		unsigned Ntr=0,Nacc=0;
		for(unsigned i=0;i<memtrace->size();++i)
		{
			os.open(fnOut+".traces_photon"+to_string(i)+".txt");
			MemTrace* tr = memtrace->get(i);
			Ntr += tr->size();
			unsigned Nacc_tr=0;
			for(unsigned j=0;j<tr->size();++j)
			{
				unsigned addr=tr->get(j).address;
				unsigned count=tr->get(j).count;
				Nacc_tr += count;

				if (addr >= accessCounts.size())
					accessCounts.resize(addr+1,0);

				accessCounts[addr] += count;
				os << addr << ' ' << count << endl;
			}
			Nacc += Nacc_tr;
			//cout << "  [" << setw(6) << i << "] trace with " << tr->size() << " entries covering " << Nacc_tr << " accesses" << endl;
			os.close();
		}
		//cout << " Total " << Ntr << " addresses, " << Nacc << " accesses with max address " << accessCounts.size()-1 << endl;


		// write total access counts
		os.open(fnOut+".frequency.txt");

		unsigned check=0;
		for(unsigned i=0;i<accessCounts.size();++i)
		{
			os << i << ' ' << accessCounts[i] << endl;
			check += accessCounts[i];
		}
		//cout << "Access checksum: " << check << endl;

		os.close();


		// Create a permutation into descending access frequency
		// Write into file with 3 columns
		os.open(fnOut+".frequency_perm.txt");

		os << "# First column: address (TetraID)" << endl;
		os << "# Second column: access frequency (Number of access)" << endl;
		os << "# Third column: cumulative access probability (What kind of hit rate would we get when including all tetras up to this line?)" << endl;

		vector<unsigned> perm(accessCounts.size());
		for(unsigned i=0;i<accessCounts.size();++i)
			perm[i]=i;


		boost::sort(perm,[&accessCounts](unsigned l,unsigned r){ return accessCounts[l] > accessCounts[r]; });

		// inverse permutation
		vector<unsigned> invperm(accessCounts.size());
		for(unsigned i=0;i<perm.size();++i)
			invperm[perm[i]]=i;

		unsigned nnz=0,csum=0;
		for(unsigned i=0;i<perm.size();++i)
		{
			if (accessCounts[perm[i]] > 0)
			{
				csum += accessCounts[perm[i]];
				os << setw(6) << perm[i] << ' ' << setw(9) << accessCounts[perm[i]] << ' ' << fixed << setprecision(5) << double(csum)/double(Nacc) << endl;
				++nnz;
			}
		}
		os.close();


		// write traces with addresses permuted so lowest address is most frequently accessed

		os.open(fnOut+".trace_perm.txt");
		Nacc=0;
		Ntr=0;
		for(unsigned i=0;i<memtrace->size();++i)
		{
			MemTrace* tr = memtrace->get(i);
			Ntr += tr->size();
			unsigned Nacc_tr=0;
			for(unsigned j=0;j<tr->size();++j)
			{
				unsigned addr=tr->get(j).address;
				unsigned count=tr->get(j).count;
				Nacc_tr += count;

				os << invperm[addr] << ' ' << count << endl;
			}
			Nacc += Nacc_tr;
			//cout << "  [" << setw(6) << i << "] trace with " << tr->size() << " entries covering " << Nacc_tr << " accesses" << endl;
			os << endl;
		}
		cout << " Total " << Ntr << " addresses, " << Nacc << " accesses" << endl;

		os.close();
	}
	else
		cout << "Memory trace missing" << endl;

	cout << "Done" << endl;
}
