###### Setup basic BLI script (data path, VTK version)
#CONFIGURE_FILE(basic_bli.tcl.in basic_bli.tcl)
#CONFIGURE_FILE(mouse_energy.tcl.in mouse_energy.tcl)
#CONFIGURE_FILE(mouse_fluence.tcl.in mouse_fluence.tcl)
#CONFIGURE_FILE(colin27_energy.tcl.in colin27_energy.tcl)
#CONFIGURE_FILE(colin27_fluence.tcl.in colin27_fluence.tcl)
#CONFIGURE_FILE(colin27_comsol.tcl.in colin27_comsol.tcl)
#CONFIGURE_FILE(HeadAndNeckModel.tcl.in HeadAndNeckModel.tcl)

#ADD_SUBDIRECTORY(FullMonte_MMC_Comparison)
#ADD_SUBDIRECTORY(FullMonte_self)
#ADD_SUBDIRECTORY(FullMonte_approx)
#ADD_SUBDIRECTORY(FullMonte_composite)
#ADD_SUBDIRECTORY(FullMonte_visualize)
MESSAGE("${Blue}Entering subdirectory Examples/FullMonte_line_source${ColourReset}")
ADD_SUBDIRECTORY(FullMonte_line_source)
MESSAGE("${Blue}Entering subdirectory Examples/BladderDirectional${ColourReset}")
ADD_SUBDIRECTORY(BladderDirectional)
MESSAGE("${Blue}Entering subdirectory Examples/Paper2017${ColourReset}")
ADD_SUBDIRECTORY(Paper2017)
MESSAGE("${Blue}Entering subdirectory Examples/MemTraces${ColourReset}")
ADD_SUBDIRECTORY(MemTraces)

ADD_EXECUTABLE(run_fullmonte fullmonte_tcl_main.cpp)
INCLUDE_DIRECTORIES(${CMAKE_CUDA_TOOLKIT_INCLUDE_DIRECTORIES})
TARGET_LINK_LIBRARIES(run_fullmonte FullMonteData FullMonteGeometry FullMonteVTKFiles FullMonteSWKernel FullMonteTIMOS ${Boost_PROGRAM_OPTIONS_LIBRARY})

#### If hardware accelerators are included add to the target link libraries and add a compile definition (used in source file)
# CUDA
IF(BUILD_CUDAACCEL)
    TARGET_LINK_LIBRARIES(run_fullmonte FullMonteCUDAKernel)
    TARGET_COMPILE_DEFINITIONS(run_fullmonte PRIVATE CUDAACCEL_ENABLED)
ENDIF()

# FPGACL
IF(BUILD_FPGACLACCEL)
    ## Include directories
    # execute 'aocl compile-config' to get the OpenCL include directories
    execute_process(
        COMMAND aocl compile-config
        OUTPUT_VARIABLE AOCL_COMPILE_CONFIG
    )
    # strip leading and trailing white space from aocl command
    string(STRIP ${AOCL_COMPILE_CONFIG} AOCL_COMPILE_CONFIG)

    # replace '-I' with nothing since -I will be added back by cmake
    string(REPLACE "-I" "" AOCL_COMPILE_CONFIG ${AOCL_COMPILE_CONFIG})

    # separate the string arguments into a list of strings
    separate_arguments(AOCL_COMPILE_FLAGS)

    # the include directories
    include_directories(
        ../External/FPGACLAccel/opencl_util/inc
        ${AOCL_COMPILE_CONFIG}
    )

    ADD_EXECUTABLE(run_fpgacl fpgacl_main.cpp)
    TARGET_LINK_LIBRARIES(run_fpgacl FullMonteData FullMonteGeometry FullMonteVTKFiles FullMonteSWKernel FullMonteFPGACLKernel FullMonteTIMOS FullMonteTextFile  ${Boost_PROGRAM_OPTIONS_LIBRARY})
    TARGET_COMPILE_DEFINITIONS(run_fpgacl PRIVATE FPGACL_ENABLED)
    add_definitions(-DFPGACL_HOST_BUILD)
    target_compile_options(run_fpgacl PUBLIC "-Wno-unknown-pragmas")
    target_compile_options(run_fpgacl PUBLIC "-Wno-unknown-pragmas")
ENDIF()


MESSAGE("${Blue}Leaving subdirectory Examples${ColourReset} \ 
")
