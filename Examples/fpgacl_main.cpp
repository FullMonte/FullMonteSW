#include <boost/timer/timer.hpp>

#include <string>

#include <FullMonteSW/Storage/VTK/VTKMeshReader.hpp>
#include <FullMonteSW/Storage/VTK/VTKMeshWriter.hpp>
#include <FullMonteSW/Storage/VTK/VTKSurfaceWriter.hpp>

#include <FullMonteSW/Storage/TIMOS/TIMOSMeshReader.hpp>
#include <FullMonteSW/Storage/TIMOS/TIMOSMaterialReader.hpp>
#include <FullMonteSW/Storage/TIMOS/TIMOSSourceReader.hpp>

#include <FullMonteSW/Geometry/Sources/Point.hpp>
#include <FullMonteSW/Geometry/Sources/PencilBeam.hpp>
#include <FullMonteSW/Geometry/Sources/Ball.hpp>
#include <FullMonteSW/Geometry/Sources/Line.hpp>
#include <FullMonteSW/Geometry/Sources/Volume.hpp>
#include <FullMonteSW/Geometry/Sources/Fiber.hpp>
#include <FullMonteSW/Geometry/Predicates/VolumeCellInRegionPredicate.hpp>

#include <FullMonteSW/Kernels/Software/TetraInternalKernel.hpp>
#include <FullMonteSW/Kernels/Software/TetraSurfaceKernel.hpp>

#include <FullMonteSW/Kernels/SeedSweep.hpp>
#include <FullMonteSW/Queries/BasicStats.hpp>
#include <FullMonteSW/Queries/EnergyToFluence.hpp>

#include <FullMonteSW/OutputTypes/MeanVarianceSet.hpp>
#include <FullMonteSW/Storage/TextFile/TextFileMeanVarianceWriter.hpp>

#include <FullMonteSW/Storage/TextFile/TextFileMatrixWriter.hpp>

#include <FullMonteSW/OutputTypes/OutputDataCollection.hpp>
#include <FullMonteSW/OutputTypes/SpatialMap2D.hpp>

#include <FullMonteSW/Queries/EventCountComparison.hpp>

#include <boost/program_options/cmdline.hpp>
#include <boost/program_options/option.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>

#include <FullMonteSW/Kernels/FPGACL/TetraFPGACLVolumeKernel.hpp>

/**
 * Main program to run FPGACL acclerated FullMonte using various inputs.
 * Example run:
 *      bin/run_fpgacl --input=0 --packets=10 --output=output_mesh --volume=true
 */
int main(int argc,char **argv) {
	int input=0;				// choose between two input files

	string oPrefix;

    vector<float> point;
    vector<float> pencilbeam;
    vector<float> ball;
    vector<float> line;
    unsigned source_volume;
    vector<float>fiber;

	unsigned long long Npkt;
	float wmin;
    float prwin;
    unsigned maxHits;
    unsigned maxSteps;
    unsigned seed;

    bool volume;
    bool surface;
    unsigned region;

	boost::program_options::options_description cmdline("Allowed options");

    // Explanation of the available command line options to make this program as flexible as possible
	cmdline.add_options()
			("help","Show help")
			("input",boost::program_options::value<int>(&input)->default_value(0),"Which mesh should be used as input file? {0:TIMOS cube (default), 1:TIMOS 1-layer, 2:TIMOS 4-layer, 3:TIMOS mouse, 4:TIMOS half-sphere, 5:HeadNeck, 6:HeadNeckTumor, 7:Colin27}")
			("point",boost::program_options::value<vector<float>>(&point)->multitoken(),"Define position of a point source (syntax {1.0,2.0,3.0})")
            ("pencil",boost::program_options::value<vector<float>>(&pencilbeam)->multitoken(),"Define position (1st 3 values) and direction (next 3 values) of a pencilbeam source (syntax {1.0,2.0,3.0,1.0,2.0,3.0})")
            ("line",boost::program_options::value<vector<float>>(&line)->multitoken(),"Define start position (1st 3 values) and end position (next 3 values) of a line source (syntax {1.0,2.0,3.0,1.0,2.0,3.0})")
            ("ball",boost::program_options::value<vector<float>>(&ball)->multitoken(),"Define position (1st 3 values) and radius (4th value) of a ball source (syntax {1.0,2.0,3.0,4.0})")
            ("srcvolume",boost::program_options::value<unsigned>(&source_volume)->default_value(0),"Define the element ID of a volume source (syntax 123456")
            ("fiber",boost::program_options::value<vector<float>>(&fiber)->multitoken(),"Define position (1st 3 values), direction (next 3 values), radius (7th value), and numerical aperture (8th value) of a fibercone source (syntax 123456")
			("output",boost::program_options::value<string>(&oPrefix),"Output file")
			("packets",boost::program_options::value<unsigned long long>(&Npkt)->default_value(10000),"Number of packets to run (default: 10000)")
			("wmin",boost::program_options::value<float>(&wmin)->default_value(1e-5),"Roulette threshold (default: 1e-5)")
            ("prwin",boost::program_options::value<float>(&prwin)->default_value(0.1),"Roulette threshold (default: 0.1)")
            ("hits",boost::program_options::value<unsigned>(&maxHits)->default_value(10000),"Number of maximum hits of a photon (default: 10000)")
            ("steps",boost::program_options::value<unsigned>(&maxSteps)->default_value(10000),"Number of maximum steps of a photon (default: 10000)")
            ("volume",boost::program_options::value<bool>(&volume)->default_value(0),"Output the volume fluence to a mesh and textfile (default: false)")
            ("surface",boost::program_options::value<bool>(&surface)->default_value(0),"Output theexterior surface fluence to a mesh and textfile (default: false)")
            ("region",boost::program_options::value<unsigned>(&region)->default_value(0),"Set the region for outputting the surface fluence of that specific region to a mesh and textfile (default: 0)")
            ("rng",boost::program_options::value<unsigned>(&seed)->default_value(0),"Seed for RNG (default: no seed)");

	boost::program_options::variables_map vm;
	boost::program_options::store(boost::program_options::parse_command_line(argc,argv,cmdline, boost::program_options::command_line_style::unix_style ^ boost::program_options::command_line_style::allow_short),vm);
	boost::program_options::notify(vm);

	if (vm.count("help")) {
		cmdline.print(cout);
		return 0;
	}

    const std::string model_name[] = {
        "TIM-OS cube",
        "TIM-OS 1-layer",
        "TIM-OS 4-layer",
        "TIM-OS mouse",
        "TIM-OS half-sphere",
        "HeadNeck"
        "HeadNeckTumor",
        "Colin27"
    };

    const std::string timos_mesh_suffix[] = {
        "/cube_5med/cube_5med.mesh",
        "/onelayer/one_layer_18_18_1_2.mesh",
        "/fourlayer/FourLayer.mesh",
        "/mouse/mouse.mesh",
        "/half_sphere/spherelens.mesh"
    };

    const std::string timos_opt_suffix[] = {
        "/cube_5med/cube_5med.opt",
        "/onelayer/mua005_mus05.opt",
        "/fourlayer/FourLayer.opt",
        "/mouse/mouse.opt",
        "/half_sphere/tissue.opt"
    };

    const std::string timos_source_suffix[] = {
        "/cube_5med/cube_5med.source",
        "/onelayer/one_layer_18_18_1_2.source",
        "/fourlayer/FourLayer.source",
        "/mouse/mouse.source",
        "/half_sphere/sphere.source"
    };

    printf("Running %s model\n", model_name[input].c_str());

    if(input >= 0 && input <= 4) {
        TIMOSMeshReader meshReader;
        TIMOSMaterialReader materialReader;
        TIMOSSourceReader sourceReader;

        string filepath_prefix = string(FULLMONTE_DATA_DIR) + string("/TIM-OS");
        meshReader.filename(filepath_prefix + timos_mesh_suffix[input]);
        materialReader.filename(filepath_prefix + timos_opt_suffix[input]);
        sourceReader.filename(filepath_prefix + timos_source_suffix[input]);

        meshReader.read();
        materialReader.read();
        sourceReader.read();

        TetraFPGACLVolumeKernel K;
        K.packetCount(Npkt);
        K.source(sourceReader.source());
        K.geometry(meshReader.mesh());
        K.materials(materialReader.materials());
        K.roulettePrWin(prwin);
        K.rouletteWMin(wmin);
        K.maxHits(maxHits);
        K.maxSteps(maxSteps);
        K.binaryFile("/home/tanner/native_build/FullMonteSW/External/FPGACLAccel/kernels/Build/full_monte_device.aocx");  // TODO: MAKE THIS NOT HARDCODED...
        if (seed != 0) K.randSeed(seed);

        // Run the kernel
        K.runSync();
        
        // Convert output data
        OutputDataCollection* ODC = K.results();
        EnergyToFluence EF;
        EF.kernel(&K);
        EF.data(ODC->getByName("VolumeEnergy"));
        EF.inputEnergy();
        EF.outputFluence();
        EF.update();

        // Write it to an output file
        TextFileMatrixWriter TW;
        TW.filename(oPrefix+"_volume_fpgacl.fluence");
        TW.source(EF.result());
        TW.write();
    } else {
        VTKMeshReader R;
        MaterialSet MS;
        string filename = FULLMONTE_DATA_DIR;
        
        if (input == 5) {
            //// HEADNECK ////
            // set filename
            filename.append("/HeadNeck/HeadNeck.mesh.vtk");

            // set materials
            Material air;
            Material tongue;
            Material larynx;
            Material tumour;
            Material teeth;
            Material bone;
            Material surroundingtissues;
            Material subcutaneousfat;
            Material skin;

            air.scatteringCoeff(0);
            air.absorptionCoeff(0);
            air.anisotropy(0.0);
            air.refractiveIndex(1.0);

            tongue.scatteringCoeff(83.3);
            tongue.absorptionCoeff(0.95);
            tongue.anisotropy(0.926);
            tongue.refractiveIndex(1.37);

            larynx.scatteringCoeff(15);
            larynx.absorptionCoeff(0.55);
            larynx.anisotropy(0.9);
            larynx.refractiveIndex(1.36);

            tumour.scatteringCoeff(9.35);
            tumour.absorptionCoeff(0.13);
            tumour.anisotropy(0.92);
            tumour.refractiveIndex(1.39);

            teeth.scatteringCoeff(60);
            teeth.absorptionCoeff(0.99);
            teeth.anisotropy(0.95);
            teeth.refractiveIndex(1.48);

            bone.scatteringCoeff(100);
            bone.absorptionCoeff(0.3);
            bone.anisotropy(0.9);
            bone.refractiveIndex(1.56);

            surroundingtissues.scatteringCoeff(10);
            surroundingtissues.absorptionCoeff(1.49);
            surroundingtissues.anisotropy(0.9);
            surroundingtissues.refractiveIndex(1.35);
            
            subcutaneousfat.scatteringCoeff(30);
            subcutaneousfat.absorptionCoeff(0.2);
            subcutaneousfat.anisotropy(0.78);
            subcutaneousfat.refractiveIndex(1.32);
        
            skin.scatteringCoeff(187);
            skin.absorptionCoeff(2.0);
            skin.anisotropy(0.93);
            skin.refractiveIndex(1.38);

            MS.exterior(&air);
            MS.append(&tongue);
            MS.append(&tumour);
            MS.append(&larynx);
            MS.append(&teeth);
            MS.append(&bone);
            MS.append(&surroundingtissues);
            MS.append(&subcutaneousfat);
            MS.append(&skin);
        } else if(input == 6) {
            //// HEADNECKTUMOR ////
            // set filename
            filename.append("/HeadNeck/HeadNeck_Tumor.mesh.vtk");

            // set materials
            Material air;
            Material tumor;

            air.scatteringCoeff(0);
            air.absorptionCoeff(0);
            air.anisotropy(0.0);
            air.refractiveIndex(1.0);
            
            tumor.scatteringCoeff(9.35);
            tumor.absorptionCoeff(0.13);
            tumor.anisotropy(0.92);
            tumor.refractiveIndex(1.39);

            MS.exterior(&air);
            MS.append(&tumor);
            MS.append(&tumor);

        } else if(input == 7) {
            //// COLIN27 //// 
            // set filename
            filename.append("/Colin27/Colin27.vtk");

            // set materials
            Material air;
            Material skull;
            Material csf;
            Material gray;
            Material white;

            air.scatteringCoeff(0);
            air.absorptionCoeff(0);
            air.anisotropy(0.0);
            air.refractiveIndex(1.0);
            
            skull.scatteringCoeff(7.8);
            skull.absorptionCoeff(0.019);
            skull.anisotropy(0.89);
            skull.refractiveIndex(1.37);
            
            csf.scatteringCoeff(0.009);
            csf.absorptionCoeff(0.004);
            csf.anisotropy(0.89);
            csf.refractiveIndex(1.37);
            
            gray.scatteringCoeff(9.0);
            gray.absorptionCoeff(0.02);
            gray.anisotropy(0.89);
            gray.refractiveIndex(1.37);
            
            white.scatteringCoeff(40.9);
            white.absorptionCoeff(1.8);
            white.anisotropy(0.89);
            white.refractiveIndex(1.37);
            
            MS.exterior(&air);
            MS.append(&skull);
            MS.append(&csf);
            MS.append(&gray);
            MS.append(&white);
        } else {
            std::cerr << "Invalid input model " << input << std::endl;
            return 1;
        }

        // read the mesh
        R.filename(filename);
        R.read();
        TetraMesh* mesh = R.mesh();
    
        // Configure the sources; if they are defined in the arguments, we will add them to the composite source.
        Source::Composite C;

        Source::Point P;
        if(!point.empty() && point.size() == 3) {
            P.position({point[0], point[1], point[2]});
            C.add(&P);
        }
    
        Source::PencilBeam PB;
        if(!pencilbeam.empty() && pencilbeam.size() == 6) {
            PB.position({pencilbeam[0], pencilbeam[1], pencilbeam[2]});
            PB.direction({pencilbeam[3], pencilbeam[4], pencilbeam[5]});
            C.add(&PB);
        }

        Source::Volume V;
        if(source_volume != 0) {
            V.elementID(source_volume);
            C.add(&V);  
        }

        Source::Ball B;
        if(!ball.empty() && ball.size() == 4) {
            B.position({ball[0], ball[1], ball[2]});
            B.radius(ball[3]);
            C.add(&B);
        }

        Source::Line L;
        if(!line.empty() && line.size() == 6) {
            L.endpoint(0, {line[0], line[1], line[2]});
            L.endpoint(1, {line[3], line[4], line[5]});
            C.add(&L);
        }

        Source::Fiber F;
        if(!fiber.empty() && fiber.size() == 8) {
            F.fiberPos({fiber[0], fiber[1], fiber[2]});
            F.fiberDir({fiber[3], fiber[4], fiber[5]});
            F.radius(fiber[6]);
            F.numericalAperture(fiber[7]);
            C.add(&F);
        }

        // setup kernel
        TetraFPGACLVolumeKernel K;
        K.packetCount(Npkt);
        K.source(&C);
        K.geometry(mesh);
        K.materials(&MS);
        K.roulettePrWin(prwin);
        K.rouletteWMin(wmin);
        K.maxHits(maxHits);
        K.maxSteps(maxSteps);
        if (seed != 0) K.randSeed(seed);
        K.binaryFile("/home/tanner/native_build/FullMonteSW/External/FPGACLAccel/kernels/Build/full_monte_device.aocx");  // TODO: MAKE THIS NOT HARDCODED...
        
        // run kernel
        K.runSync();

        // get results
        OutputDataCollection* ODC = K.results();
    
        EnergyToFluence EF;
        EF.kernel(&K);
        EF.inputEnergy();
        EF.outputFluence();
        EF.materials(&MS);
        EF.data(ODC->getByName("VolumeEnergy"));
        EF.update();

        //VTKMeshWriter W;
        //W.filename(oPrefix+"_volume.vtk");
        //W.addData("Fluence", EF.result());
        //W.mesh(mesh);
        //W.write();

        TextFileMatrixWriter TW;
        TW.filename(oPrefix+"_volume_fpgacl.fluence");
        TW.source(ODC->getByName("VolumeEnergy"));
        TW.source(EF.result());
        TW.write();
    }
    
    return 0;
}
