package require FullMonte
package require vtk

load libFullMonteMMCFileTCL.so


## Read MMC fluence output (should use -C 0 simulation option to ensure it outputs fluence sums per element instead of smoothed
#       results at mesh nodes; will notice an attribute mismatch against CellData if incorrect)

MMCResultReader R
R filename "mouse.dat"
R read
set phi [R output]


## Read the original mesh from TIM-OS dataset

TIMOSMeshReader TR
TR filename "$FullMonte::datadir/TIM-OS/mouse/mouse.mesh"
TR read

set M [TR mesh]


## Wrap the mesh and array into VTK structures to write out

vtkFullMonteArrayAdaptor VTKA
    VTKA source $phi

set vtkPhi [VTKA array]

vtkFullMonteTetraMeshWrapper VTKM
    VTKM mesh $M
    VTKM update


set UG [VTKM regionMesh]

$vtkPhi SetName "Fluence (au)"
[$UG GetCellData] AddArray $vtkPhi

vtkUnstructuredGridWriter VTKW
VTKW SetFileName "mouse.mmc.vtk"
VTKW SetInputData $UG
VTKW Update
VTKW Delete

puts "phi=$phi"

OutputDataCollection C
C add $phi

SparseMergeWrite SW
SW filename "mouse.merge.out"
SW data C
SW write

exit
