package require FullMonte
package require vtk

## Read Digimouse mesh and optical properties

TIMOSMeshReader R
    R filename "$FullMonte::datadir/TIM-OS/mouse/mouse.mesh"
    R read
set M [R mesh]

TIMOSMaterialReader OR
    OR filename "$FullMonte::datadir/TIM-OS/mouse/mouse.opt"
    OR read
set MS [OR materials]


## Position point source

Point P
P position "16.3 42.2 7.9"


## Configure kernel

TetraVolumeKernel K
K geometry $M
K source P
K materials $MS
K rouletteWMin 1e-5
K packetCount 1000000
K rouletteWMin
K threadCount $FullMonte::defaultThreadCount


## Run

puts "Start"
set tStart [clock milliseconds]
K runSync
set tEnd [clock milliseconds]
puts "End - kernel time [expr $tEnd-$tStart] ms"

set RES [K results]

$RES start

for {$RES start } { ![$RES done] } { $RES next } { puts "  Result: [[$RES current] name]" }

set E [$RES getByName "VolumeEnergy"]

EnergyToFluence EF
    EF kernel K
    EF data $E
    EF update


VTKMeshWriter W
    W filename "mouse.out.vtk"
    W addData "Fluence" [EF result]
    W mesh $M
    W write

exit
