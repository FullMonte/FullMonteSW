/*
 * Chi2Similarity.cpp
 *
 *  Created on: Dec 8, 2017
 *      Author: jcassidy
 */

#include "Chi2Similarity.hpp"

#include <FullMonteSW/Warnings/Push.hpp>
#include <FullMonteSW/Warnings/Boost.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/math/distributions/chi_squared.hpp>
#include <FullMonteSW/Warnings/Pop.hpp>

#include <boost/math/distributions/normal.hpp>



#include <FullMonteSW/OutputTypes/SpatialMap.hpp>
#include <FullMonteSW/Geometry/Partition.hpp>

#include <FullMonteSW/Queries/Permute.hpp>
#include <FullMonteSW/Geometry/Permutation.hpp>
#include <FullMonteSW/Queries/Sort.hpp>


#include <FullMonteSW/OutputTypes/MeanVarianceSet.hpp>

Chi2Similarity::Chi2Similarity(std::ostream& os) : m_os(os)
{
	m_sort=new Sort();
	m_sort->setAscending();

	m_permMu=new Permute();
	m_permSigma2=new Permute();
	m_permTest=new Permute();
}

void Chi2Similarity::extraStdDevMultiplier(float k)
{
	m_extraStdDevMultiplier=k;
}

float Chi2Similarity::extraStdDevMultiplier() const
{
	return m_extraStdDevMultiplier;
}

void Chi2Similarity::extraCvToAdd(float delta_cv)
{
	m_extraStdDevPerMean=delta_cv;
}

float Chi2Similarity::extraCvToAdd() const
{
	return m_extraStdDevPerMean;
}

Chi2Similarity::~Chi2Similarity()
{
}

void Chi2Similarity::partition(Partition* p)
{
	m_partition=p;
}

void Chi2Similarity::update()
{
	if (!m_ref)
		cout << "NewChi2Compare::update() failed - no reference set" << endl;

	if (!(m_refMu = m_ref->mean()))
		cout << "NewChi2Compare::update() failed - no reference mean" << endl;

	if (!(m_refSigma2 = m_ref->variance()))
		cout << "NewChi2Compare::update() failed - no reference variance" << endl;

	if (!m_test)
		cout << "NewChi2Compare::update() failed - no test values" << endl;

	// sort inputs in ascending order of coefficient of variation
	SpatialMap<float>* refCV = new SpatialMap<float>(m_refMu->dim(),AbstractSpatialMap::UnknownSpaceType,AbstractSpatialMap::Scalar,AbstractSpatialMap::UnknownOutputType);

	for(unsigned i=0;i<refCV->dim();++i)
	{
		float mu = m_refMu->get(i);
		if (mu != 0)
		{
			float sigma = sqrt(m_refSigma2->get(i));
			refCV->set(i,sigma/mu);
		}
		else
			refCV->set(i,std::numeric_limits<float>::quiet_NaN());
	}

	m_sort->input(refCV);
	m_sort->update();

	delete refCV;

	m_permutation = m_sort->output();


	// permute the inputs
	m_permSigma2->permutation(m_permutation);
	m_permMu->permutation(m_permutation);
	m_permTest->permutation(m_permutation);

	m_permSigma2->input(m_refSigma2);
	m_permSigma2->update();
	m_pSigma2 = static_cast<SpatialMap<float>*>(m_permSigma2->output());

	m_permMu->input(m_refMu);
	m_permMu->update();
	m_pMu = static_cast<SpatialMap<float>*>(m_permMu->output());

	m_permTest->input(m_test);
	m_permTest->update();
	m_pTest = static_cast<SpatialMap<float>*>(m_permTest->output());

	if(m_chi2)
		delete m_chi2;
	m_chi2 = new SpatialMap<float>(m_permutation->dim(),AbstractSpatialMap::UnknownSpaceType,AbstractSpatialMap::Scalar,AbstractSpatialMap::UnknownOutputType);

	if(m_chi2p)
		delete m_chi2p;
	m_chi2p = new SpatialMap<float>(m_permutation->dim(),AbstractSpatialMap::UnknownSpaceType,AbstractSpatialMap::Scalar,AbstractSpatialMap::UnknownOutputType);

	if(m_Esum)
		delete m_Esum;
	m_Esum = new SpatialMap<float>(m_permutation->dim(),AbstractSpatialMap::UnknownSpaceType,AbstractSpatialMap::Scalar,AbstractSpatialMap::UnknownOutputType);


	// process elements sequentially
	vector<float> pCrit{ 0.90, 0.95, 0.99 };
	vector<float> chi2Crit(pCrit.size());
	float chi2p;

	double csum_refE=0.0, csum_refSigma2=0.0;
	double sum_refE=0.0, sum_testE=0.0;

	// Accumulate total energy
	for(unsigned i=0;i<m_pMu->dim();++i)
	{
		sum_refE  += m_pMu->get(i);
		sum_testE += m_pTest->get(i);
	}


	float ref_Nr=m_ref->runs();
	float ref_Np=m_ref->packetsPerRun();
	float test_Np=m_testNp;

	float k = float(test_Np)/float(ref_Np);

	// print header strip

	// NOTE: test values are scaled by the packet ratio
	m_os << "Total energy: reference set " << sum_refE << ", test set " << sum_testE << " (" <<
			(sum_testE-sum_refE*k)/sum_testE*100.0 << "% diff post-scaling)" << endl;

	m_os << setw(7) << "rank" << ' ' <<
			setw(7) << "IDt" << ' ' <<
			setw(3)  << "rgn" << ' ' <<
			setw(10) << "mean" << ' ' <<
			setw(10) << "stddev" << ' ' <<
			setw(8) << "CV" << ' ' <<
			setw(8) << "%E" << ' ' <<

			setw(10) << "test" << ' ' <<
			setw(9) << "%diff" << ' ' <<
			setw(10) << "chi2_stat" << ' ' <<
			setw(10) << "p";

	for(const auto p : pCrit)
		m_os << " p=" << setw(10) << p;
	m_os << endl;


	double chi2stat = 0.0f;
	m_os.precision(5);


	for(unsigned i=0;i<m_pMu->dim();++i)
	{
		float refMu 	= m_pMu->get(i);
		float refSigma2 = m_pSigma2->get(i);

		float test 		= m_pTest->get(i)/k;

		double var =
			  	refSigma2*(1.0f+m_extraStdDevMultiplier)*(1.0f+m_extraStdDevMultiplier)*
					(1.0f/ref_Nr +								// variance of N-sample mean is 1/N * variance of sample
					ref_Np/test_Np) +							// variance of test set, estimated based on reference set variance and packet ratio
				refMu*refMu*m_extraStdDevPerMean*m_extraStdDevPerMean;	// allow population means to vary by some percentage of reference

		double dchi2stat = (refMu-test)*(refMu-test)/var;

		chi2stat += dchi2stat;

			if (i < 2000)
			{
				boost::math::chi_squared_distribution<double> chi2(i+1);

				chi2p = cdf(chi2,chi2stat);

				boost::transform(pCrit, chi2Crit.begin(), [chi2](float p){ return boost::math::quantile(chi2,p); });
			}
			else {
				boost::math::normal_distribution<double> norm(i+1,sqrt(2*(i+1)));
				chi2p = cdf(norm,chi2stat);

				boost::transform(pCrit, chi2Crit.begin(), [norm](float p){ return boost::math::quantile(norm,p); });
			}


		csum_refE += refMu;
		csum_refSigma2 += refSigma2;

		m_Esum->set(i,csum_refE);
		m_chi2->set(i,chi2stat);
		m_chi2p->set(i,chi2p);

		float stddev = sqrt(refSigma2);

		m_os << setw(7) << i+1 << ' ' <<
				setw(7) << m_permutation->get(i) << ' ' <<
				setw(3)  << (m_partition ? m_partition->get(m_permutation->get(i)) : 0) << ' ' <<
				setw(10) << setprecision(5) << refMu << ' ' <<
				setw(10) << stddev << ' ' <<
				setw(7) << setprecision(3) << stddev/refMu*100.0f << "% " <<
				setw(7) << csum_refE/sum_refE*100.0f << "% " <<
				setw(10) << setprecision(5) << test << ' ' <<
				setw(8) << setprecision(3) << showpos << (test-refMu)/refMu*100.0 << noshowpos << "% " <<
				setw(10) << chi2stat << ' ' <<
				setw(10) << chi2p << ' ';

		for(const auto chi2 : chi2Crit)
			m_os << setw(10) << chi2 << ' ';

		m_os << endl;
	}
}

void Chi2Similarity::reference(MeanVarianceSet* mvs)
{
	m_ref=mvs;
}

void Chi2Similarity::test(SpatialMap<float>* x)
{
	m_test=x;
}

void Chi2Similarity::testPacketCount(unsigned long long Np)
{
	m_testNp = Np;
}

Chi2Similarity::Chi2Entry Chi2Similarity::getChi2Entry(unsigned i) const
{
	Chi2Entry ret;
	ret.index=i;
	ret.ID=m_partition ? m_partition->get(m_permutation->get(i)) : 0;
	ret.mu=m_pMu->get(i);
	ret.sigma=std::sqrt(m_pSigma2->get(i));
	ret.cv=ret.sigma/ret.mu;
	ret.test=m_pTest->get(i);
	ret.Esum=m_Esum->get(i)/m_Esum->get(m_Esum->dim()-1);
	ret.chi2=m_chi2->get(i);
	ret.df=i+1;
	ret.p=m_chi2p->get(i);

	return ret;
}

Chi2Similarity::Chi2Entry		Chi2Similarity::getByEnergyFraction(float E) const
{
	unsigned begin=0;
	unsigned end=m_Esum->dim()-1;
	unsigned mid = end/2;

	float E0=m_Esum->get(end-1);

	while(end-begin > 1)
	{
		float Esum = m_Esum->get(mid)/E0;

		if (Esum < E)
			begin=mid;
		else
			end=mid;

		mid = (begin+end)/2;
	}

	return getChi2Entry(end);
}

Chi2Similarity::Chi2Entry		Chi2Similarity::getByCVThreshold(float cv) const
{
	unsigned begin=0;
	unsigned end=m_pSigma2->dim()-1;
	unsigned mid = end/2;

	while(end-begin > 1)
	{
		float sigma2=m_pSigma2->get(mid);
		float mu=m_pMu->get(mid);

		if (sigma2/mu/mu < cv*cv)
			begin=mid;
		else
			end=mid;

		mid = (begin+end)/2;
	}
	return getChi2Entry(end);
}

float Chi2Similarity::Chi2Entry::criticalValue(float pCrit) const
{
	float chi2crit;

	// use normal approximation if df > 2000 (too large for Boost chi2 implementation)
	if (df > 2000)
	{
		boost::math::normal_distribution<double> norm(df,sqrt(df*2));
		chi2crit = quantile(norm,pCrit);
	}
	else
	{
		boost::math::chi_squared_distribution<double> chi2(df);
		chi2crit = quantile(chi2,pCrit);
	}
	return chi2crit;
}

float Chi2Similarity::Chi2Entry::pValue() const
{
	float p;

	if (df > 2000)
	{
		boost::math::normal_distribution<double> norm(df ,sqrt(df/2));
		p = 1.0-cdf(norm,chi2);
	}
	else
	{
		boost::math::chi_squared_distribution<double> chi2dist(df);
		p = 1.0-cdf(chi2dist,chi2);
	}
	return p;
}


