/*
 * Chi2Similarity.hpp
 *
 *  Created on: Dec 8, 2017
 *      Author: jcassidy
 */

#ifndef EXAMPLES_PAPER2017_CHI2SIMILARITY_HPP_
#define EXAMPLES_PAPER2017_CHI2SIMILARITY_HPP_


/** Implements a chi^2 test of similarity between two vectors.
 *
 * The reference set must supply a MeanVarianceSet with mean and variance for the expected value.
 * The test set supplies a single dataset, which may have a different number of packets simulated.
 *
 * Inputs must be non-normalized (ie. in raw weight terms)
 *
 */

#include <iosfwd>

class MeanVarianceSet;
class Partition;
class Permutation;
class Sort;
class Permute;
template<typename T>class SpatialMap;

class Chi2Similarity
{
public:
	Chi2Similarity(std::ostream& os);
	~Chi2Similarity();

	void reference(MeanVarianceSet* mvs);

	void test(SpatialMap<float>* mu);
	void testPacketCount(unsigned long long Npackets);

	void partition(Partition* p);							///< Optional: specify the region partition

	void 	extraStdDevMultiplier(float k);
	float 	extraStdDevMultiplier() const;

	void 	extraCvToAdd(float delta_cv);
	float 	extraCvToAdd() const;

	void update();


	struct Chi2Entry
	{
		unsigned index;
		unsigned ID;
		unsigned region;
		float mu;
		float sigma;
		float cv;
		float Esum;
		float test;
		float chi2;
		float p;
		unsigned df;

		/// Return the critical value at the chosen level
		float criticalValue(float p=0.95) const;

		/// Get the p-value associated with this entry
		float pValue() const;
	};

	Chi2Entry		getByEnergyFraction(float E) const;
	Chi2Entry		getByCVThreshold(float cv) const;

private:
	Chi2Entry			getChi2Entry(unsigned i) const;

	std::ostream&		m_os;

	MeanVarianceSet*	m_ref=nullptr;
	SpatialMap<float>*	m_test=nullptr;

	unsigned long long	m_testNp=0ULL;

	float				m_extraStdDevPerMean=0.000f;	///< Extra standard deviation as ratio of mean (coefficient of variation)
	float				m_extraStdDevMultiplier=0.00f;	///< Extra factor to apply on std.dev. (ie. sigma_applied = (1+m_extraStdDevMultiplier)*sigma

	SpatialMap<float>*	m_refMu=nullptr;
	SpatialMap<float>* 	m_refSigma2=nullptr;

	/// Query to sort by coefficient of variation
	Sort*				m_sort=nullptr;
	Permutation*		m_permutation=nullptr;

	/// Queries to permute inputs into desired order
	Permute*			m_permMu=nullptr;
	Permute*			m_permSigma2=nullptr;
	Permute*			m_permTest=nullptr;

	///
	SpatialMap<float>*	m_pMu=nullptr;
	SpatialMap<float>* 	m_pSigma2=nullptr;
	SpatialMap<float>* 	m_pTest=nullptr;

	SpatialMap<float>*	m_Esum=nullptr;
	SpatialMap<float>*	m_chi2=nullptr;
	SpatialMap<float>*	m_chi2p=nullptr;

	Partition*			m_partition=nullptr;
};




#endif /* EXAMPLES_PAPER2017_CHI2SIMILARITY_HPP_ */
